export interface StateDecoratorChange {
    ngStateDecoratorChange: (state) => void;
}

interface IConfig {
    models: string[]
}

/**
 * Set an instance property called 'STATE' on class 
 * 
 * @param {IConfig} config 
 * @returns 
 */
export function SetState(config: IConfig) {

    return function <T extends { new(...args: any[]): any }>(constructor: T) {
        return class extends constructor {
            subscriptions = [];

            state = config.models.reduce((state, model) => {

                // update method ngStateDecoratorChange with state changes
                this.subscription = this.stateManager.getModel(model)
                    .map(result => ({ [model]: result }))
                    .subscribe(this.ngStateDecoratorChange.bind(this))

                return Object.assign({}, state, {
                    [model + '$']: this.stateManager.getModel(model),
                })
            }, {});

            // Push subscription into subscriptions array
            set subscription(subscription) {
                this.subscriptions.push(subscription)
            }

            // handles unsubscribing subscriptions
            unsubscribe() {
                this.subscriptions.forEach(subscription => subscription.unsubscribe());
            }

        }
    }
}

/**
 * Auto unsubscribe any subscriptions pushed into subscriptions array
 * 
 * @returns {*} 
 */
export function AutoUnsubscribe(): any {
    return function (target: any, propertyKey: string | symbol, descriptor: TypedPropertyDescriptor<Function>) {
        const method = descriptor.value;

        descriptor.value = function () {
            this.unsubscribe()
            return method.apply(this, arguments)
        }

        return descriptor;
    }
}