export enum ConstructionType {
    Brick,
    Block,
    Stone,
    Stucco,
    Siding,
    WoodChaseWSiding,
    WoodChaseWStucco
}