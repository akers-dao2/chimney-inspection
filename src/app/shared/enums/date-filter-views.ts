export enum DateFilterView {
    today,
    tomorrow,
    all,
    incomplete,
    completed
} 