export enum RoofMaterial {
    Asphalt,
    Cedar,
    Slate,
    Asbestos,
    ClayTile,
    Rubber,
    Metal
}