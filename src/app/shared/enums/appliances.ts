export enum ApplianceTypes {
    'Flue/Vent',
    'Masonry Fireplace',
    'Prefab Fireplace',
    Stoves,
    'Heating Appliances',
    'Dryer Vent',
    Chimney
}