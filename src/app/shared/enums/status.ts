export enum Status {
    open,
    closed,
    pending,
    printed,
    downloaded
}
