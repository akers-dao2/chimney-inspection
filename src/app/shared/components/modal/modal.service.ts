import { Injectable, ComponentRef } from '@angular/core';
import { ReplaySubject } from 'rxjs/ReplaySubject';

@Injectable()
export class ModalService {

  public content$ = new ReplaySubject<any>(1);
  public data$ = new ReplaySubject<any>(1);


  /**
   * Sets the content of the modal
   *
   * @template T
   * @param {T} content
   *
   * @memberOf ModalService
   */
  public setContent<T>(content: T): void {
    this.content$.next(content);
  }


  /**
   * Allows arbituary data to be pass from modal to components and vice-versa
   *
   * @param {*} data
   *
   * @memberOf ModalService
   */
  public passData(data: any): void {
    this.data$.next(data);
  }
}
