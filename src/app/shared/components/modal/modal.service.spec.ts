/* tslint:disable:no-unused-variable */
import { Component } from '@angular/core';
import { TestBed, async, inject, } from '@angular/core/testing';
import { ModalService } from './modal.service';

@Component({
  selector: 'wells-mock',
  template: `<div id="content">Content</div>`
})
export class MockComponent {}

describe('ModalService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ModalService]
    });
  });

  it('should ...', inject([ModalService], (service: ModalService) => {
    expect(service).toBeTruthy();
  }));

  it('should pass a Content Component ', inject([ModalService], (service: ModalService) => {
    spyOn(service.content$, 'next');

    service.setContent(MockComponent);
    expect(service.content$.next).toHaveBeenCalledWith(MockComponent);
  }));

  it('should pass random data', inject([ModalService], (service: ModalService) => {
    spyOn(service.data$, 'next');

    service.passData('hello');
    expect(service.data$.next).toHaveBeenCalledWith('hello');
  }));
});
