/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { DebugElement, Component, ElementRef } from '@angular/core';

import { ModalComponent } from './modal.component';
import { ReplaySubject, Observable, Subject } from 'rxjs';
import { ModalService } from './../modal.service';


@Component({
  selector: 'wells-mock',
  template: `<div id="content">Content</div>`
})
export class MockComponent {}

describe('ModalComponent', () => {
  let component: ModalComponent;
  let fixture: ComponentFixture<ModalComponent>;
  let modal: ModalService;
  let compiled: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalComponent, MockComponent ],
      providers: [
        ModalService
      ]
    })
    .overrideComponent(ModalComponent, {
      set: {
        entryComponents: [MockComponent]
      }
    });
    TestBed.compileComponents();
  }));

  beforeEach(inject([ModalService], (_ModalService_) => {
    modal = _ModalService_;
    fixture = TestBed.createComponent(ModalComponent);
    component = fixture.componentInstance;
    compiled = fixture.debugElement.nativeElement;
    fixture.detectChanges();
  }));

  it('should run Life Cycle - OnInit()', () => {
    modal.setContent(MockComponent);

    expect(compiled.querySelector('#content').textContent).toEqual('Content');
  });

  it('should run life cycle - OnDestroy()', () => {
    component.ngOnDestroy();

    expect(modal.content$.observers.length).toEqual(0);
  });
});
