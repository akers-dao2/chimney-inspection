import {
  Component,
  ViewChild,
  ViewContainerRef,
  OnInit,
  OnDestroy,
  ComponentFactoryResolver
} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ISubscription } from 'rxjs/Subscription'
import { ModalService } from '../modal.service';


@Component({
  selector: 'wells-modal',
  templateUrl: './modal.component.html'
})
export class ModalComponent implements OnInit, OnDestroy {
  @ViewChild('contentAnchor', { read: ViewContainerRef }) private contentAnchor: ViewContainerRef;
  private subscription: ISubscription;


  /**
   * Creates an instance of ModalComponent.
   *
   * @param {ComponentFactoryResolver} componentFactoryResolver
   * @param {ModalService} modalService
   *
   * @memberOf ModalComponent
   */
  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private modalService: ModalService
  ) { }


  /**
   * Sets the subscription for content$ Observable
   *
   * @memberOf ModalComponent
   */
  ngOnInit() {
    this.subscription = this.modalService.content$
      .subscribe(<T>(content: T) => {
        this.contentAnchor.clear();
       const instance = this.createContentComponent(content).instance;
      });
  }


  /**
   * Clean up all subscriptions
   *
   * @memberOf ModalComponent
   */
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }


  /**
   * Creates the dynamic component for content
   *
   * @private
   * @template T
   * @param {any} contentClass
   * @returns
   *
   * @memberOf ModalComponent
   */
  private createContentComponent<T>(contentClass) {
    return this.contentAnchor.createComponent(
      this.componentFactoryResolver.resolveComponentFactory<T>(contentClass)
    );
  }
}
