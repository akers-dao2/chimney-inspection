import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DesktopCalendarComponent } from './desktop-calendar.component';

describe('DesktopCalendarComponent', () => {
  let component: DesktopCalendarComponent;
  let fixture: ComponentFixture<DesktopCalendarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DesktopCalendarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DesktopCalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
