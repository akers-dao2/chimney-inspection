import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ViewController } from 'ionic-angular';

declare const Flatpickr: any;

@Component({
  selector: 'wells-desktop-calendar',
  template: '<input type="text" #date style="visibility: hidden;">'
})
export class DesktopCalendarComponent implements OnInit {
  @ViewChild('date') date: ElementRef;

  constructor(
    private viewController: ViewController
  ) { }

  ngOnInit() {
    const actions = new Map()
    const flatpickr = new Flatpickr(this.date.nativeElement, { onChange: this.onChange.bind(this) });
    actions.set('onChange', this.onChange)
    flatpickr.open();
  }

  public onChange(selectedDates, dateStr, instance) {
    const date = new Date(selectedDates[0]).valueOf()
    this.viewController.dismiss(date)
  }
}
