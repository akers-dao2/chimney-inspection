import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { ModalController, Platform, NavParams, ViewController, ActionSheetController } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { StateManagerService } from 'app/shared/services/sassy-state-manager-ng2/src/state-manager.service';

import { IComment } from '../../../interfaces/comments';
import { IRecommendation } from '../../../interfaces/recommendations';
import { ICommentList } from '../../../interfaces/comment-list';
import { IRecommendationList } from '../../../interfaces/recommendation-list';
import { CommentOrRecommendationType } from 'app/shared/enums/comment-or-rec-types';

import { PresentToastService } from 'app/shared/services/present-toast.service';

@Component({
  templateUrl: 'pane-comment.component.html',
  styleUrls: ['./pane-comment.component.scss']
})
export class PaneCommentComponent implements OnInit, OnDestroy {

  public commentsList: Observable<ICommentList[]>;
  public recommendationsList: Observable<IRecommendationList[]>;
  public comments: number[];
  public recommendations: number[];
  // the type of comments or recommendations to display via the list
  public typeOfComRec: number;
  public failureReasonCode: number[];
  public additionalComments: string[];
  public subscriptions: any[] = [];

  constructor(
    private platform: Platform,
    private params: NavParams,
    private viewCtrl: ViewController,
    private stateManager: StateManagerService,
    private presentToast: PresentToastService,
    public actionSheetCtrl: ActionSheetController
  ) { }

  public ngOnInit() {

    this.comments = this.params.get('comments') || [];
    this.recommendations = this.params.get('recommendations') || [];
    this.additionalComments = this.params.get('additionalComments') || [];
    this.typeOfComRec = this.params.get('typeOfComRec');
    this.failureReasonCode = this.params.get('failureReasonCode');

    this.commentsList = this.generateList<ICommentList[]>('comment') as any;

    this.recommendationsList = this.generateList<IRecommendationList[]>('recommendation') as any;

  }

  /**
   * Handles dimissing the modal window
   * 
   * @param {any} type 
   * @memberof PaneCommentComponent
   */
  public dismiss(type) {
    switch (type) {
      case 'close':
        this.viewCtrl.dismiss();
        break;
      default:
        this.subscription = this.allowSubmit
          .subscribe(result => {
            if (result) {
              this.viewCtrl.dismiss({
                comments: this.comments,
                recommendations: this.recommendations,
                additionalComments: this.additionalComments,
                typeOfComRec: this.typeOfComRec
              });
            } else {
              this.presentToast.show('You must select at least one comment and recommendation.');
            }

          });
        break;
    }
  }

  /**
   * Add additional comment
   * 
   * @param {string} comment 
   * @memberof PaneCommentComponent
   */
  public addAdditionalComment(comment: string) {
    this.additionalComments.push(comment);
  }

  /**
   * Remove additional comment
   * 
   * @param {number} index 
   * @memberof PaneCommentComponent
   */
  public removeAdditionalComment(index: number) {
    this.additionalComments = this.additionalComments.filter((value, i) => i !== index);
  }

  /**
   * Add or remove comments
   * 
   * @param {number} index 
   * @memberof PaneCommentComponent
   */
  public commentSelected(index: number) {
    this.comments = this.generateNewSelection(this.comments, index);
  }

  /**
   * Add or remove  recommendations
   * 
   * @param {number} index 
   * @memberof PaneCommentComponent
   */
  public recommendationSelected(index: number) {
    this.recommendations = this.generateNewSelection(this.recommendations, index);
  }

  public editOrDeleteAddComment(index: number, input: HTMLInputElement) {
    const actionSheet = this.actionSheetCtrl.create({
      title: 'Modify comment',
      buttons: [
        {
          text: 'Delete',
          role: 'destructive',
          handler: () => {
            this.removeAdditionalComment(index);
          }
        },
        {
          text: 'Edit',
          handler: () => {
            input.value = this.additionalComments[index];
            this.removeAdditionalComment(index);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });

    actionSheet.present();
  }

  public ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe);
  }

  private set subscription(subscription) {
    this.subscriptions.push(subscription);
  }

  /**
   * generates a new comment or recommendation selection
   * 
   * @private
   * @param {number[]} selection 
   * @param {number} index 
   * @returns 
   * @memberof PaneCommentComponent
   */
  private generateNewSelection(selection: number[], index: number) {
    // remove existing value
    const newSelection = selection.filter(comment => comment !== index);

    // add only new values. You know this if there is no different newSelection and selection
    if (newSelection.length === selection.length) {
      newSelection.push(index);
    }

    // sort so selected comments or recommendations are in order
    newSelection.sort();

    return Array.from(newSelection);
  }

  /**
   * Is submitting allowing
   * 
   * @readonly
   * @private
   * @memberof PaneCommentComponent
   */
  private get allowSubmit() {
    return this.generateList<ICommentList>('comment')
      .map(commentsList => {
        return this.comments.filter(commentIndex =>
          !commentsList[commentIndex].comment.toLowerCase().includes('trigger')
        );
      })
      .map(comments => !!comments.length && !!this.recommendations.length);
  }

  /**
   * generates a list for the comments and recommendations
   * 
   * @private
   * @param {string} listName 
   * @returns 
   * @memberof PaneCommentComponent
   */
  private generateList<T>(listName: string) {
    return this.stateManager.getModel(listName + 's')
      .map<IComment, string[]>(result =>
        result[CommentOrRecommendationType[this.typeOfComRec]])
      .filter(comments => comments !== undefined)
      .map<string[], T[]>(comments => {
        return comments.map((comment, index, arr) => {
          let checked = this[listName + 's'].includes(index);

          // deselect conflicting options to start
          if ([1, 2].includes(index) && Array.isArray(this.failureReasonCode)) {
            checked = false;
            this[listName + 's'] = this[listName + 's'].filter(commentUuid => commentUuid !== index);
          }

          if (Array.isArray(this.failureReasonCode)) {
            if (listName === 'comment' && this.failureReasonCode.includes(index)) {
              checked = true;
              this[listName + 's'].push(index);
            }

            if (listName === 'recommendation' && arr.length === 1) {
              checked = true;
              this[listName + 's'].push(index);
            } else if (listName === 'recommendation' && this.failureReasonCode.includes(index)) {
              checked = true;
              this[listName + 's'].push(index);
            }

          }


          return Object.assign({}, { [listName]: comment, checked });
        }) as any;
      });
  }
}