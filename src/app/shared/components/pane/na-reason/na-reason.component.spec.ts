import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NaReasonComponent } from './na-reason.component';

describe('NaReasonComponent', () => {
  let component: NaReasonComponent;
  let fixture: ComponentFixture<NaReasonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NaReasonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NaReasonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
