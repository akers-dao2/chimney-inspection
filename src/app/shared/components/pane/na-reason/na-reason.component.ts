import { Component, OnInit } from '@angular/core';
import { ViewController } from 'ionic-angular';

@Component({
  selector: 'wells-na-reason',
  templateUrl: './na-reason.component.html',
  styleUrls: ['./na-reason.component.scss']
})
export class NaReasonComponent implements OnInit {
  public items: string[];

  constructor(
    public viewCtrl: ViewController
  ) { }

  ngOnInit() {
    this.items = [
      'N/A - Weather',
      'N/A - Client refusal',
      'N/A - Inaccessible',
      'Not Applicable'
    ];
  }

  public onClick(item: string) {
    this.viewCtrl.dismiss(item)
  }

}
