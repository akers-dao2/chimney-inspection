import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges
} from '@angular/core';
import { ModalController, AlertController, PopoverController } from 'ionic-angular';

import { InspectionStatus } from '../../enums/inspection-status';

import { PaneCommentComponent } from './pane-comment/pane-comment.component';

import { DataService } from '../../../shared/services/data.service';
import { PresentToastService } from '../../../shared/services/present-toast.service';
import { StateManagerService } from 'app/shared/services/sassy-state-manager-ng2/src/state-manager.service';

import { PaneEvent } from '../../interfaces/pane-event';
import { IPhoto } from '../../interfaces/photo';
import { IAlertOptionInput } from '../../interfaces/alert-option-input';
import { ICommentEvent } from '../../interfaces/commentEvent';
import { ITestItem } from '../../interfaces/test-item';
import { NaReasonComponent } from './na-reason/na-reason.component';
import { map, take } from 'rxjs/operators';
import { LocalDbStatus } from 'app/shared/enums/local-db-status';
import { Observable, Subject } from 'rxjs';

declare let EXIF;

@Component({
  selector: 'wells-inspection-sheet-pane',
  templateUrl: './pane.component.html',
  styleUrls: ['./pane.component.scss'],
})
export class PaneComponent implements OnInit, OnChanges {

  @Input() public name: string;
  @Input() public value: string;
  @Input() public hide: boolean;
  @Input() public statusValue: number;
  @Input() public uuid: number;
  @Input() public id: number;
  @Input() public photos: IPhoto[];
  @Input() public numOfPhotos = 1;
  @Input() public section: string;
  @Input() public type: string;
  @Input() public testItem: ITestItem = {};
  @Input() public failureReasonCode: number[];
  @Input() public disabled: boolean;
  @Input() public hideFrontHousePic: boolean;
  @Input() disable: boolean;

  // the type of comments or recommendations to display via the list
  @Input() public typeOfComRec: number;

  @Output() public status: EventEmitter<PaneEvent> = new EventEmitter<PaneEvent>();
  @Output() public deletePhoto: EventEmitter<IPhoto> = new EventEmitter<IPhoto>();
  @Output() public commentsAndRec: EventEmitter<ICommentEvent> = new EventEmitter<ICommentEvent>();


  public statusName = '';
  public photoName: string;
  public InspectionStatus = InspectionStatus;
  public requiredChimney = [];
  public requiredFrontOfHouse = [];
  public lockUpload = false;
  public stopUpload$ = new Subject();

  constructor(
    private modalCtrl: ModalController,
    private dataService: DataService,
    private presentToast: PresentToastService,
    private alertCtrl: AlertController,
    private stateManager: StateManagerService,
    private popoverCtrl: PopoverController
  ) { }

  /****************************************************************
   *
   * Public Methods
   *
   ****************************************************************/


  /**
   * LifeCycle Hook
   *
   *
   * @memberOf InspectionSheetPaneComponent
   */
  public ngOnInit() {
    this.photos = this.photos || [];
  }

  public ngOnChanges(simpleChanges: SimpleChanges) {
    if (simpleChanges['statusValue']) {
      const statusValue = simpleChanges['statusValue'];
      if (!statusValue.isFirstChange() &&
        statusValue.currentValue !== statusValue.previousValue &&
        statusValue.currentValue === 0 &&
        Array.isArray(this.failureReasonCode)) {
        this.setStatus(InspectionStatus[statusValue.currentValue]);
      }

    }
  }
  /**
   * Set Status
   *
   * @param {string} status
   *
   * @memberOf InspectionSheetPaneComponent
   */
  public setStatus(status: string, event?: Event) {
    switch (status) {
      case 'failed':
        this.handleFailedStatus(status);
        break;
      case 'Not Applicable':
        this.handleNAStatus(event);
        break;

      default:
        this.emitStatusChange(status);
        break;
    }
  }

  /**
   * Handle File Upload
   * 
   * @param {FileList} file 
   * 
   * @memberof PaneComponent
   */
  public handleFile(file: FileList, required = false) {
    const imageToUpload = file[0];

    // resize image code
    const image = new Image();
    const imageURL = URL.createObjectURL(imageToUpload);

    image.onload = async () => {
      const getEXIFData = Observable.bindCallback(EXIF.getData) as any;
      await getEXIFData(image).take(1).toPromise();
      const orientation = EXIF.getTag(image, 'Orientation');

      const width = 320;
      const scaleFactor = width / image.width;
      const height = 480;  // image.height * scaleFactor
      const elem = document.createElement('canvas');
      elem.width = width;
      elem.height = height;
      const ctx = elem.getContext('2d');
      if (orientation === 6 || image.width === 1280 && image.height === 720) {
        // move to the center of the canvas
        ctx.translate(width / 2, height / 2);

        // rotate the canvas to the specified degrees
        ctx.rotate(90 * Math.PI / 180);

        // draw the image
        // since the ctx is rotated, the image will be rotated also
        ctx.drawImage(image, 0, 0, image.width, image.height, -height / 2, -width / 2, height, width);
      } else {
        switch (orientation) {
          case 2: ctx.transform(-1, 0, 0, 1, width, 0); break;
          case 3: ctx.transform(-1, 0, 0, -1, width, height); break;
          case 4: ctx.transform(1, 0, 0, -1, 0, height); break;
          case 5: ctx.transform(0, 1, 1, 0, 0, 0); break;
          case 6: ctx.transform(0, 1, -1, 0, height, 0); break;
          case 7: ctx.transform(0, -1, -1, 0, height, width); break;
          case 8: ctx.transform(0, -1, 1, 0, 0, width); break;
          default: break;
        }
        ctx.drawImage(image, 0, 0, width, height);
      }


      ctx.canvas.toBlob((blob) => {
        const resizeFile = new File([blob], 'image', {
          type: 'image/jpeg',
          lastModified: Date.now()
        });
        this.stateManager.getModel('openedInspectionReport')
          .take(1)
          .subscribe(reportData => {
            if (this.photos.length < this.numOfPhotos || required) {
              const name = required ? this.photoName : this.value.toLowerCase();
              const index = required ? 0 : this.photos.length;
              const fileName = `${name}_${this.uuid || '0'}_${index}_${reportData.jobKey}`;
              this.dataService.addImageFileNameToDB(fileName);
              this.dataService.uploadToStorage(resizeFile, fileName, this.photoName)
                .skipWhile(progress => progress === undefined)
                .takeUntil(this.stopUpload$)
                .subscribe(result => {
                  this.status.emit({
                    name: required ? result.photoName : this.name,
                    value: this.value,
                    progress: result.type === 'progress' ? result.progress : 100,
                    downloadLink: result.type === 'downloadedURL' ? result.downloadURL : undefined,
                    uuid: this.uuid,
                    id: reportData.jobDate,
                    index,
                    fileName
                  });

                  if (result.type === 'downloadedURL') {
                    this.presentToast.show(`${fileName} has been uploaded successfully`);
                    this.stopUpload$.next('stop');
                  }
                });
            } else {
              this.presentToast.show(`You have exceeded the number of photos for ${this.name}`);
            }
          });
      }, 'image/jpeg', 1);

    };

    image.src = imageURL;
  }

  /**
   * creates the alert radio input for existing pictures
   * 
   * @param {any} fileUpload 
   * @param {any} photoName 
   * 
   * @memberof PaneComponent
   */
  public displayExistingPic(fileUpload, photoName) {
    this.photoName = photoName;
    const photosInput = this.photos
      .filter(photo => photoName ? photo.name === photoName : true)
      .map((photo, index) => Object.assign({}, {
        name: 'photos',
        value: photo,
        label: `${photo.name.toUpperCase()} #${index + 1}`,
        type: 'radio'
      }));

    // if no pictures exist display the native upload dialog
    if (!photosInput.length) {
      fileUpload.click();
    } else {
      this.photoUpload(fileUpload, photosInput);
    }
  }

  /****************************************************************
   *
   * Private Methods
   *
   ****************************************************************/


  /**
   * remove any special chars except whitespaces within string and
   * trims whitespace from beginning and end
   *
   * @param {any} str
   * @returns
   *
   * @memberOf UtilityService
   */
  private removeSpecialChars(str) {
    return str.replace(/(?!\w|\s)./g, '')
      .replace(/\s+/g, ' ')
      .replace(/^(\s*)([\W\w]*)(\b\s*$)/g, '$2');
  }

  private photoUpload(fileUpload: HTMLInputElement, photosInput: IAlertOptionInput[]) {

    const buttons = [
      {
        text: 'Cancel',
        role: 'cancel'
      },
      {
        text: 'Upload Pictures',
        handler: data => {
          fileUpload.click();
        }
      },
      {
        text: 'Delete Photos',
        handler: (data: IPhoto) => {
          if (data) {
            this.stateManager.getModel('downloadLinks')
              .switchMap<any, { key: string, value: string }>(link => link)
              .filter(link => link.value === data.fileName)
              .take(1)
              .subscribe(link => this.dataService.removeImageFileNameFromDB(link.key));
            this.deletePhoto.emit(data);
          }
        }
      }
    ]
      .filter(button => !photosInput.length ? button.text !== 'Delete Photos' : true)
      .filter(this.hasUploadPicturesOption.bind(this, photosInput));

    const alert = this.alertCtrl.create({
      title: 'Pictures',
      inputs: photosInput,
      buttons: buttons
    });
    alert.present();
  }

  private hasUploadPicturesOption(photosInput, button) {
    if (!photosInput.length) {
      return true;
    }

    const hasRequiredPhoto = photosInput.some(photo =>
      photo.value.name === 'front_of_house' ||
      photo.value.name === 'chimney' ||
      photo.value.name === 'appliance'
    );

    if (hasRequiredPhoto) {
      return button.text !== 'Upload Pictures';
    }

    return true;
  }
  /**
   * emitStatusChange
   * 
   * @private
   * @param {string} status 
   * @memberof PaneComponent
   */
  private emitStatusChange(status: string) {
    this.statusValue = status ? InspectionStatus[status] : -1;
    this.statusName = status ? InspectionStatus[InspectionStatus[status]] : '';

    this.status.emit({
      name: this.name,
      value: this.value,
      status: this.statusValue
    });
  }

  /**
   * Present NA reason selection popover
   *
   * @private
   * @param {*} $event
   * @returns
   * @memberof PaneComponent
   */
  private presentPopover($event) {
    return new Promise<string>((resolve) => {
      const popover = this.popoverCtrl.create(NaReasonComponent);

      popover.onDidDismiss((data) => {
        if (data !== null) {
          resolve(data);
        }

      });

      popover.present({
        ev: $event
      });

    });
  }

  /**
   * Handle the na status selection
   *
   * @private
   * @param {*} status
   * @param {*} event
   * @memberof PaneComponent
   */
  private async handleNAStatus(event) {
    const result = this.shouldPresentPopover ? await this.presentPopover(event) : 'Not Applicable';
    this.emitStatusChange(result);
  }

  private get shouldPresentPopover() {
    const panels = ['Crown', 'Flashing', 'Height', 'Cap'];
    return panels.includes(this.name);
  }

  /**
   * Handle the failed status selection
   *
   * @private
   * @param {*} status
   * @memberof PaneComponent
   */
  private handleFailedStatus(status) {
    const modal = this.modalCtrl.create(PaneCommentComponent,
      {
        comments: this.testItem.comments,
        recommendations: this.testItem.recommendations,
        additionalComments: this.testItem.additionalComments,
        typeOfComRec: this.typeOfComRec,
        failureReasonCode: this.failureReasonCode
      });

    modal.onDidDismiss(data => {
      if (data !== undefined && data !== null) {
        const { comments, recommendations, additionalComments, typeOfComRec } = data;
        this.commentsAndRec.emit({
          comments,
          recommendations,
          additionalComments,
          typeOfComRec
        });

        this.emitStatusChange(status);
      }
    });

    modal.present();
  }
}