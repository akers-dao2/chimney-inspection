import { Pipe, PipeTransform } from '@angular/core';

/** Interfaces */
import { IClient } from '../interfaces/client';

@Pipe({
  name: 'clientAddress'
})
export class ClientAddressPipe implements PipeTransform {
  constructor() { }

  transform(client: IClient): any {
    return `${client.address} ${client.city}, ${client.state} ${client.zip}`;
  }

}
