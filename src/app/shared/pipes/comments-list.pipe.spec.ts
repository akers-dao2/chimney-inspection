import { CommentsListPipe } from './comments-list.pipe';
import { StateManager } from '../services/sassy-state-manager-ng2/src/classes/stateManager';

describe('CommentsListPipe', () => {
  it('create an instance', () => {
    const stateManager = new StateManager();
    const pipe = new CommentsListPipe(stateManager);
    expect(pipe).toBeTruthy();
  });
});
