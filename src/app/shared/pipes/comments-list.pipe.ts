import { Pipe, PipeTransform } from '@angular/core';
import { IReport } from '../interfaces/report';
import { StateManagerService } from 'app/shared/services/sassy-state-manager-ng2/src/state-manager.service';
import { GetCommentPipe } from 'app/shared/pipes/get-comment.pipe';
import { Observable } from 'rxjs/Observable';
import isObject from 'isobject';

@Pipe({
  name: 'commentsList'
})
export class CommentsListPipe implements PipeTransform {

  constructor(
    private stateManager: StateManagerService
  ) { }

  transform(inspectItems: IReport, isDanger = false): any {

    return Observable.from(Object.keys(inspectItems))
      .mergeMapTo(this.stateManager.getModel('comments').take(1), (key, comments) => ({ key, comments }))
      .reduce<any>((keys, { key, comments }) => {
        let hasMatch = false;
        if (isObject(inspectItems[key]) && inspectItems[key].status === 0) {
          hasMatch = Object.keys(inspectItems[key])
            .some(key$ => {
              const hasKey = this.keysToDisplay.includes(key$);

              if (hasKey) {
                if (inspectItems[key].comments === null || !inspectItems[key].comments.length) {
                  return false;
                }

                return inspectItems[key].comments.some(i => {
                  if (key === 'condition') {
                    return true;
                  }

                  return isDanger ? comments[key][i].includes('Trigger') : true;
                });
              }

              return false;
            });
        }

        return hasMatch ? Array.of(...keys, key) : keys;
      }, []);

  }

  private get keysToDisplay() {
    return [
      'comments',
      'recommendations'
    ];
  }

  private comment(key, type, index) {
    return this.stateManager.getModel(type)
      .map(comments => comments[key][index]);
  }


}
