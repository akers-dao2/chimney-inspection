import { Pipe, PipeTransform } from '@angular/core';

/** Services */
import { StateManagerService } from 'app/shared/services/sassy-state-manager-ng2/src/state-manager.service';

/** Interfaces */
import { IClient } from '../interfaces/client';

@Pipe({
  name: 'clientName'
})
export class ClientNamePipe implements PipeTransform {

  constructor(
    private stateManager: StateManagerService
  ) { }

  transform(client: IClient): string {
    return client !== null ? `${client.firstName} ${client.lastName}` : '';
  }

}
