import { ClientAddressPipe } from './client-address.pipe';
import { StateManager } from '../services/sassy-state-manager-ng2/src/classes/stateManager';

describe('ClientAddressPipe', () => {
  it('create an instance', () => {
    const stateManager = new StateManager();
    const pipe = new ClientAddressPipe(stateManager);
    expect(pipe).toBeTruthy();
  });
});
