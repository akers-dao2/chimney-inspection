import { RemoveTriggerCommentPipe } from './remove-trigger-comment.pipe';
import { StateManager } from '../services/sassy-state-manager-ng2/src/classes/stateManager';

describe('RemoveTriggerCommentPipe', () => {
  it('create an instance', () => {
    const stateManager = new StateManager();
    const pipe = new RemoveTriggerCommentPipe(stateManager);
    expect(pipe).toBeTruthy();
  });
});
