import { CrewNamePipe } from './crew-name.pipe';
import { StateManager } from '../services/sassy-state-manager-ng2/src/classes/stateManager';

describe('CrewNamePipe', () => {
  it('create an instance', () => {
    const stateManager = new StateManager();
    const pipe = new CrewNamePipe(stateManager);
    expect(pipe).toBeTruthy();
  });
});
