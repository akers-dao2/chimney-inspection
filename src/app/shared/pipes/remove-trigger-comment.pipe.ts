import { Pipe, PipeTransform } from '@angular/core';
import { StateManagerService } from 'app/shared/services/sassy-state-manager-ng2/src/state-manager.service';
import { Observable } from 'rxjs/Observable';

@Pipe({
  name: 'removeTriggerComment'
})
export class RemoveTriggerCommentPipe implements PipeTransform {
  constructor(
    private stateManager: StateManagerService
  ) { }

  transform(comments: any[], key?: any): any {
    return  this.stateManager.getModel('comments')
      .map(comments$ => {
       return comments.filter(commentIndex => !comments$[key][commentIndex].toLowerCase().includes('trigger'))

      });
  }

}
