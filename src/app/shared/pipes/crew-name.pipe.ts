import { Pipe, PipeTransform, Injectable } from '@angular/core';

/** Services */
import { StateManagerService } from 'app/shared/services/sassy-state-manager-ng2/src/state-manager.service';

/** Interfaces */
import { ICrew } from '../interfaces/crew';
import { from } from 'rxjs/observable/from';

@Injectable()

@Pipe({
  name: 'crewName'
})
export class CrewNamePipe implements PipeTransform {

  constructor(
    private stateManager: StateManagerService
  ) { }

  transform(key: string): any {
    return this.stateManager.getModel('crews')
      .switch<ICrew>()
      .filter(crew => crew.key === key)
      .map(crew => crew.name);
  }

}
