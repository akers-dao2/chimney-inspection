import { Pipe, PipeTransform } from '@angular/core';
import { DataService } from '../../shared/services/data.service';
import { LoginService } from '../services/login.service';

@Pipe({
  name: 'getFileUrl'
})
export class GetFileUrlPipe implements PipeTransform {
  constructor(
    private dataService: DataService,
    private loginService: LoginService
  ) { }

  transform(downloadLink: any, args?: any): any {
    const fileName = new RegExp(/(?:o\/)(.+)(?=\?)/).exec(downloadLink as string)[1];

    if (this.loginService.retrieveCredentialsLocally.online) {
      return this.dataService.getImage(fileName);
    }

    return this.dataService.getStorageItem(fileName) as any;

  }

}
