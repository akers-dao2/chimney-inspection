import { Pipe, PipeTransform } from '@angular/core';

/** Services */
import { StateManagerService } from 'app/shared/services/sassy-state-manager-ng2/src/state-manager.service';

/** Interfaces */
import { IClient } from '../interfaces/client';

@Pipe({
  name: 'clientRecord'
})
export class ClientRecordPipe implements PipeTransform {

  constructor(
    private stateManager: StateManagerService
  ) { }

  transform(param: string | { key: string }): any {
    const key = typeof param === 'string' ? param : param.key;
    
    return this.stateManager.getModel('clients')
      .switch<IClient>()
      .filter(client => client.key === key);
  }

}
