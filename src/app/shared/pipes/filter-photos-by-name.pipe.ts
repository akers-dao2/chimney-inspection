import { Pipe, PipeTransform } from '@angular/core';
import { IPhoto } from '../interfaces/photo';

@Pipe({
  name: 'filterPhotosByNameCount'
})
export class FilterPhotosByNamePipe implements PipeTransform {

  transform(photos: IPhoto[], name: string): number {

    return photos.filter(photo => photo.name === name).length;
  }

}
