import { Pipe, PipeTransform } from '@angular/core';
import { InspectionStatus } from '../enums/inspection-status';

@Pipe({
  name: 'getStatusColor'
})
export class GetStatusColorPipe implements PipeTransform {

  transform(statusValue: number, type: 'passed' | 'failed' | 'na'): any {
    switch (statusValue + type) {
      case InspectionStatus.failed + 'failed' :
        return 'danger'
      case InspectionStatus.passed + 'passed':
        return 'secondary'
      case InspectionStatus['N/A - Inaccessible'] + 'na':
      case InspectionStatus['Not Applicable'] + 'na':
      case InspectionStatus['N/A - Client refusal'] + 'na':
      case InspectionStatus['N/A - Weather'] + 'na':
        return 'yellow'
      default:
        return 'light'
    }
  }

}
