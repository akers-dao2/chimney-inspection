import { Pipe, PipeTransform } from '@angular/core';
import { StateManagerService } from 'app/shared/services/sassy-state-manager-ng2/src/state-manager.service';
import { Observable } from 'rxjs/Observable';

@Pipe({
  name: 'getComment'
})
export class GetCommentPipe implements PipeTransform {

  constructor(
    private stateManager: StateManagerService
  ) { }

  transform(index: number, key: string, type: string): any {
    return this.comment(key, type, index);
  }

  private comment(key, type, index) {
    return this.stateManager.getModel(type)
      .map(comments => comments[key][index]);
  }

}
