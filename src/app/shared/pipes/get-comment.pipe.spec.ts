import { GetCommentPipe } from './get-comment.pipe';
import { StateManager } from '../services/sassy-state-manager-ng2/src/classes/stateManager';

describe('GetCommentPipe', () => {
  it('create an instance', () => {
    const stateManager = new StateManager();
    const pipe = new GetCommentPipe(stateManager);
    expect(pipe).toBeTruthy();
  });
});
