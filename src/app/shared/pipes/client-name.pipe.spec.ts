import { ClientNamePipe } from './client-name.pipe';
import { StateManager } from '../services/sassy-state-manager-ng2/src/classes/stateManager';

describe('ClientNamePipe', () => {
  it('create an instance', () => {
    const stateManager = new StateManager();
    const pipe = new ClientNamePipe(stateManager);
    expect(pipe).toBeTruthy();
  });
});
