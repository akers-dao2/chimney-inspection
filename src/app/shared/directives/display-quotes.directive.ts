import { Directive, ElementRef, Renderer2, AfterViewInit, Input } from '@angular/core';

@Directive({
  selector: '[wellsDisplayQuotes]'
})
export class DisplayQuotesDirective implements AfterViewInit {

  constructor(
    private el: ElementRef,
    private renderer: Renderer2
  ) { }




  ngAfterViewInit() {
    const node: HTMLDivElement = this.el.nativeElement;
    const previousElementSibling: HTMLDivElement = node.previousElementSibling as HTMLDivElement;
    if (isNaN(parseInt(node.innerText, 10))) {
      return;
    }

    if (this.doNotAddQuote(previousElementSibling.innerText)) {
      return;
    }

    node.innerText = node.innerText + '"';
  }

  private doNotAddQuote(value) {
    const doNotAddQuoteList = [
      'Pipe Size'
    ];

    return doNotAddQuoteList.includes(value.slice(0, -1));
  }
}
