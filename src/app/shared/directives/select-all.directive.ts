import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
    selector: '[wellsSelectAll]'
})
export class SelectAllDirective {

    constructor(private el: ElementRef) { }

    @HostListener('focus')
    selectAll() {
        // access to the native input element
        const nativeEl: HTMLInputElement = this.el.nativeElement.querySelector('input');

        if (nativeEl) {
            if (nativeEl.setSelectionRange) {
                // select the text from start to end
                setTimeout(() => {
                    nativeEl.setSelectionRange(0, nativeEl.value.length);
                }, 0);
            }
            nativeEl.select();
        }
    }

}