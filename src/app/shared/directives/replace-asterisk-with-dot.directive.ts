import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
    selector: '[wellsReplaceAsterisk]'
})
export class ReplaceAsteriskWithDotDirective {
    private element: HTMLInputElement;

    constructor(private el: ElementRef) {
        this.element = this.el.nativeElement as HTMLInputElement;
    }

    @HostListener('keydown', ['$event'])
    onKeydown($event: KeyboardEvent) {
        if ($event.key === '*') {
            const input = this.element.querySelector('input');
            input.value = input.value + '.';
            input.focus();
            return false;
        }
    }

}