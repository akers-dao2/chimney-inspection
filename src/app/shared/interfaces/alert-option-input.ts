import { IInspectionItem } from './inspection-item';

export interface IAlertOptionInput {
    name: string;
    value: any;
    label: string;
    type: string;
}
