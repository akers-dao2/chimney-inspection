import { IAppliance } from './appliance';

export interface IChimneyInfo {
    inspectionItems?: IAppliance[];
    name?: string;
    uuid?: string;
    notValid?: boolean;
    clientId?: string;
}

export interface IInspection {
    clientId?: string;
    date?: number;
    [chimney: string]: any;
    key?: string;
    jobKey?: string;
}
