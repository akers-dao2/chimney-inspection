import { ICrew } from './crew';
import { IClient } from './client';
import { Status } from '../enums/status';
import { IUser } from './user';

export interface IJob {
    assigned: string;
    key?: string;
    client: string;
    status: Status;
    date: number;
    inspection: boolean;
    internal: boolean;
    certification: boolean;
    signature: string;
    order: number;
    completionDate?: number;
    jobKey?: string;
    user: IUser;
}
