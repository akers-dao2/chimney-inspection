import { ChimneyComponent } from '../../pages/inspection/inspection-sheet/chimney/chimney.component';
import { FlueVentComponent } from '../../pages/inspection/inspection-sheet/flue-vent/flue-vent.component';
import { HeatingApplianceComponent } from '../../pages/inspection/inspection-sheet/heating-appliance/heating-appliance.component';
import { MasonryFireplaceComponent } from '../../pages/inspection/inspection-sheet/masonry-fireplace/masonry-fireplace.component';
import { WoodStoveInsertComponent } from '../../pages/inspection/inspection-sheet/wood-stove-insert/wood-stove-insert.component';

export interface Sheets {
    chimney: ChimneyComponent;
    flueVent: FlueVentComponent;
    heatingAppliance: HeatingApplianceComponent;
    masonryFireplace: MasonryFireplaceComponent;
    woodStoveInsert: WoodStoveInsertComponent;
}