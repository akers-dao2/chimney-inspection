import { ApplianceTypes } from '../enums/appliances';

export interface IAppliance {
    name: string;
    parentIndex: number;
    index: number;
    locationIndex: number;
    type: ApplianceTypes;
    uuid: string;
    notValid: boolean;
}