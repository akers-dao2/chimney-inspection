export interface IInspectionReport {
    reports?: {
        [date: string]: any;
    };
    clientId?: string;
    jobKey?: string;
    key?: string;
    jobDate?: string;
    _id?: string;
    _rev?: string;
}