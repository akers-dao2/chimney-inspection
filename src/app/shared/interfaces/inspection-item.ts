import { ApplianceTypes } from '../enums/appliances';

export interface IInspectionItem {
    name: string;
    parentIndex: number;
    index: number;
    locationIndex: number;
    type: ApplianceTypes;
}
