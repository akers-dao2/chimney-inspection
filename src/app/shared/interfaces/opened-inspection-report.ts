export interface IOpenedInspectionReport {
    clientId: string;
    jobKey: string;
    jobDate: number;
}