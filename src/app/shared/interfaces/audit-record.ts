export interface IAuditRecord {
    date: number;
    description: string;
    page: string;
    key?: string;
    name?: string
}