export interface IStorageResp {
    type?: string; 
    progress?: number; 
    downloadURL?: string; 
    photoName?: string;
}