import { CommentOrRecommendationType } from '../enums/comment-or-rec-types';

export interface ICommentEvent {
    comments?: number[],
    recommendations?: number[],
    additionalComments?: number[],
    typeOfComRec: CommentOrRecommendationType
}