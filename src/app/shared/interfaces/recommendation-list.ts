export interface IRecommendationList {
    recommendation: string;
    checked: boolean;
}