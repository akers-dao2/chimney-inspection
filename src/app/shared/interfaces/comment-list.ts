export interface ICommentList {
    comment: string;
    checked: boolean;
}