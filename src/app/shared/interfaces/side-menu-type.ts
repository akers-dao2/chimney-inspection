export type SideMenuType = 'inspectionItemMenu' | 'standardMenu' | 'inspectionMenu' | 'inspectionConfigItemsMenu';
