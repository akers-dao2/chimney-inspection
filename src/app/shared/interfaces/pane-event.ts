import { InspectionStatus } from '../enums/inspection-status';

export interface PaneEvent {
    name: string;
    value: string;
    progress?: number;
    downloadLink?: string;
    status?: InspectionStatus;
    id?: number;
    uuid?: number;
    index?: number;
    fileName?: string;
}