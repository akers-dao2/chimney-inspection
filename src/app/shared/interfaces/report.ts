import { ApplianceTypes } from '../enums/appliances';

export interface IReport {
    applianceType: ApplianceTypes;
    uuid: string;
    name: string;
    id: number;
    [data: string]: any;
}
