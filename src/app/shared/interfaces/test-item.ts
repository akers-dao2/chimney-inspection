export interface ITestItem {
    status?: number,
    pictures?: string[],
    comments?: number[],
    recommendations?: number[],
    additionalComments?: string[],
}