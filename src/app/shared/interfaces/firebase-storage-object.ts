export interface IFirebaseStorageObject {
    bucket: string;
    cacheControl: string;
    contentDisposition: string;
    contentEncoding: string;
    contentLanguage: string
    contentType: string;
    customMetadata: string;
    downloadURLs: string;
    fullPath: string;
    generation: number;
    md5Hash: string;
    metageneration: number;
    name: string;
    size: number;
    timeCreated: string;
    type: string
    updated: string;
    downLoadLinkKey: string;
}