import { IAppliance } from './appliance';

export interface IChimneyItem {
    name: string;
    uuid: string;
    index: number;
    appliances: IAppliance[];
    notValid: boolean;
}
