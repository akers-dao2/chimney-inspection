export interface IComment {
    [name: string]: string[]
}