import { SideMenuType } from './side-menu-type';

export interface INavigation {
    sideMenuType: SideMenuType;
}