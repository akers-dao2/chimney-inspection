export interface IRecommendation {
    [name: string]: string[]
}