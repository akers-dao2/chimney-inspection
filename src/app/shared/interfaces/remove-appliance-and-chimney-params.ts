import { IInspection } from './inspection';
import { IInspectionItem } from './inspection-item';
import { IInspectionReport } from './inspection-report';
import { IAppliance } from './appliance';
import { ApplianceTypes } from '../enums/appliances';

export interface IItemToRemove {
    inspectionItems?: IAppliance[];
    name?: string;
    parentIndex: number;
    index: number;
    locationIndex: number;
    uuid: string;
    type: ApplianceTypes;
    notValid: boolean;
}

export interface IRemoveApplianceAndChimneyParams {
    newInspection: IInspection;
    newInspectionReports: IInspectionReport[];
    clientId: string;
    date: number;
    itemToRemove: IItemToRemove;
    chimney: string;
    parentIndex: number;
    index: number;
}