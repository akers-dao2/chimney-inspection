import { ApplianceTypes } from '../enums/appliances';

export interface IApplianceParams {
    name: string;
    parentIndex: number;
    index: number;
    locationIndex: number;
    type: ApplianceTypes;
};