export interface IClient {
    id?: any;
    firstName: string;
    lastName: string;
    address: string;
    city: string;
    state: string;
    zip: string;
    township: string;
    isValid: boolean;
    key: string;
}
