export interface IUser {
    CSIA: string;
    adminstration: boolean;
    createJob: boolean;
    crew?: string;
    deleteJob: boolean;
    editJob: boolean;
    emailAddress?: string;
    firstName?: string;
    key?: string;
    lastName?: string;
    password?: string;
    printJob: boolean;
    jobListView?: string;
    online?: boolean;
}