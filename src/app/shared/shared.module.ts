import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ClientNamePipe } from './pipes/client-name.pipe';
import { CrewNamePipe } from './pipes/crew-name.pipe';
import { ClientAddressPipe } from './pipes/client-address.pipe';
import { FilterPhotosByNamePipe } from './pipes/filter-photos-by-name.pipe';
import { CreateListPipe } from './pipes/create-list.pipe';
import { StatusNamePipe } from './pipes/status-name.pipe';
import { RowHeaderNamePipe } from './pipes/row-header-name.pipe';
import { SliceObjectPipe } from './pipes/slice-object.pipe';
import { GetFractionPipe } from './pipes/get-fraction.pipe';
import { CommentsListPipe } from './pipes/comments-list.pipe';
import { GetCommentPipe } from './pipes/get-comment.pipe';
import { GetCommentIndexesPipe } from './pipes/get-comment-indexes.pipe';
import { DisplayQuotePipe } from './pipes/display-quote.pipe';
import { DisplayQuotesDirective } from './directives/display-quotes.directive';
import { ReplaceAsteriskWithDotDirective } from './directives/replace-asterisk-with-dot.directive';
import { SelectAllDirective } from './directives/select-all.directive';
import { RemoveTriggerCommentPipe } from './pipes/remove-trigger-comment.pipe';
import { GetFileUrlPipe } from './pipes/get-file-url.pipe';
import { GetStatusColorPipe } from './pipes/get-status-color.pipe';
import { ClientRecordPipe } from './pipes/client-record.pipe';


@NgModule({
  imports: [],
  exports: [
    CommonModule,
    FormsModule,
    HttpModule,
    ClientNamePipe,
    ClientRecordPipe,
    CrewNamePipe,
    ClientAddressPipe,
    FilterPhotosByNamePipe,
    CreateListPipe,
    StatusNamePipe,
    RowHeaderNamePipe,
    SliceObjectPipe,
    GetFractionPipe,
    CommentsListPipe,
    GetCommentPipe,
    GetCommentIndexesPipe,
    DisplayQuotePipe,
    DisplayQuotesDirective,
    RemoveTriggerCommentPipe,
    GetFileUrlPipe,
    GetStatusColorPipe,
    SelectAllDirective,
    ReplaceAsteriskWithDotDirective
  ],
  declarations: [
    ClientNamePipe,
    CrewNamePipe,
    ClientAddressPipe,
    ClientRecordPipe,
    FilterPhotosByNamePipe,
    CreateListPipe,
    StatusNamePipe,
    RowHeaderNamePipe,
    SliceObjectPipe,
    GetFractionPipe,
    CommentsListPipe,
    GetCommentPipe,
    GetCommentIndexesPipe,
    DisplayQuotePipe,
    DisplayQuotesDirective,
    RemoveTriggerCommentPipe,
    GetFileUrlPipe,
    GetStatusColorPipe,
    SelectAllDirective,
    ReplaceAsteriskWithDotDirective
  ]
})
export class SharedModule { }
