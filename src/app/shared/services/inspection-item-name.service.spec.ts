import { TestBed, inject } from '@angular/core/testing';

import { InspectionItemNameService } from './inspection-item-name.service';

describe('InspectionItemNameService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InspectionItemNameService]
    });
  });

  it('should be created', inject([InspectionItemNameService], (service: InspectionItemNameService) => {
    expect(service).toBeTruthy();
  }));
});
