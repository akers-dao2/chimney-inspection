import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { DataService } from 'app/shared/services/data.service';
import { PresentToastService } from 'app/shared/services/present-toast.service';
import { GetPicturePropService } from 'app/shared/services/get-picture-prop.service';

import { IPhoto } from 'app/shared/interfaces/photo';

@Injectable()
export class DeletePhotoService {

  constructor(
    private dataService: DataService,
    private presentToast: PresentToastService,
    private getPictureProp: GetPicturePropService,
  ) { }

  public get(photo: IPhoto) {
    const result$ = new BehaviorSubject(undefined);
    const downloadLink = this.getDownloadLink(photo.downloadLink);
    let fileName = photo.fileName ? photo.fileName : new RegExp(/(?:o\/)(.+)(?=\?)/).exec(downloadLink as string);

    if (fileName === null) {
      result$.next({ code: 'storage/object-not-found' });
    } else {
      fileName = typeof fileName !== 'string' ? fileName[1] : fileName;
      const name = photo.name.replace(/\s/g, '');
      const prop = this.invalidPropMapper(this.getPictureProp.get(name));
      const result = photo.value || prop[0].toLowerCase() + prop.substring(1);

      this.dataService.deleteFromStorage(fileName, photo.name)
        .then(() => {
          result$.next(result);
          // File deleted successfully
          this.presentToast.show(`${photo.name} Photo# ${photo.index + 1} has been deleted successfully`);
        }).catch((error) => {
          if (error.code === 'storage/object-not-found') {
            result$.next(result);
            this.presentToast.show(`${photo.name} Photo# ${photo.index + 1} has been deleted successfully`);
          } else {
            // Uh-oh, an error occurred!
            console.error(error);
            this.presentToast.show(error.code);
          }
        });
    }

    return result$;
  }

  private getDownloadLink(link) {
    if (typeof link !== 'string') {
      link = Object.keys(link).filter(key => typeof link[key] === 'string').map(key => link[key]);
      link = link !== null ? link[0] : undefined;
    }

    return link;
  }

  private invalidPropMapper(key) {
    const mapper = new Map<string, string>([
      ['Brick/Stone', 'brickStone']
    ]);

    return mapper.get(key) || key;
  }
}
