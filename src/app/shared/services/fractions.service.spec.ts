import { TestBed, inject } from '@angular/core/testing';

import { FractionsService } from './fractions.service';

describe('FractionsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FractionsService]
    });
  });

  it('should be created', inject([FractionsService], (service: FractionsService) => {
    expect(service).toBeTruthy();
  }));
});
