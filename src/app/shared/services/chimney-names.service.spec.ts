import { TestBed, inject } from '@angular/core/testing';

import { ChimneyNamesService } from './chimney-names.service';

describe('GetChimneyNamesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ChimneyNamesService]
    });
  });

  it('should ...', inject([ChimneyNamesService], (service: ChimneyNamesService) => {
    expect(service).toBeTruthy();
  }));
});
