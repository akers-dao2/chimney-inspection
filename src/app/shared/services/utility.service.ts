import { Injectable } from '@angular/core';

@Injectable()
export class UtilityService {

  constructor() { }

  /**
   * Create clone of object
   *
   * @template T
   * @param {T} object
   * @returns
   *
   * @memberOf UtilityService
   */
  public clone<T>(object: T) {
    return JSON.parse(JSON.stringify(object)) as T;
  }

  /**
   * Convert object to array
   *
   * @param {any} object
   * @returns
   *
   * @memberOf UtilityService
   */
  public toArray(object) {
    return Object.keys(object).map(key => ({ ...object[key], key }));
  }
}
