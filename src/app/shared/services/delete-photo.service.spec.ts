import { TestBed, inject } from '@angular/core/testing';

import { DeletePhotoService } from './delete-photo.service';

describe('DeletePhotoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DeletePhotoService]
    });
  });

  it('should be created', inject([DeletePhotoService], (service: DeletePhotoService) => {
    expect(service).toBeTruthy();
  }));
});
