import { Injectable } from '@angular/core';
import { LoadingController, Loading } from 'ionic-angular';
import { ReplaySubject } from 'rxjs/ReplaySubject';

@Injectable()
export class LoaderService {
  private loader: Loading;
  private loaderPresent = new ReplaySubject();

  constructor(
    private loadingCtrl: LoadingController
  ) { }

  show(msg = 'Please wait...') {
    this.loader = this.loadingCtrl.create({
      content: msg
    });

    return this.loader.present();
  }

  hide() {
    this.loader.dismiss();
  }
}
