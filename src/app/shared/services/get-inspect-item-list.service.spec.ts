import { TestBed, inject } from '@angular/core/testing';

import { GetInspectItemListService } from './get-inspect-item-list.service';

describe('GetInspectItemListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetInspectItemListService]
    });
  });

  it('should be created', inject([GetInspectItemListService], (service: GetInspectItemListService) => {
    expect(service).toBeTruthy();
  }));
});
