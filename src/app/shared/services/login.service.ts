import { Injectable } from '@angular/core';
import { AlertController } from 'ionic-angular';

/** Services */
import { DataService } from './data.service';
import { ActionService } from './action.service';
import { PouchDbService } from './pouch-db.service';


/** Classes */
import * as firebase from 'firebase';

@Injectable()
export class LoginService {

  constructor(
    private alertCtrl: AlertController,
    private dataService: DataService,
    private actionService: ActionService,
    private pouchDB: PouchDbService,
  ) { }

  /**
   * Display login alert window
   * 
   * 
   * @memberOf LoginService
   */
  public displayLogin() {
    const loginScreen = this.alertCtrl.create({
      title: 'Login',
      inputs: [
        {
          name: 'email',
          placeholder: 'Email Address',
          type: 'email'
        },
        {
          name: 'password',
          placeholder: 'Password',
          type: 'password'
        }
      ],
      buttons: [
        {
          text: 'Login',
          handler: data => {
            this.dataService.signIn(data.email, data.password)
              .skipWhile(user =>
                user === undefined
              )
              .switchMap(user => this.setCurrentUser(user))
              .take(1)
              .subscribe(async (user) => {
                if (!user.online) {
                  this.pouchDB.initialize();
                 await this.actionService.installLocalDB();
                } 
                this.actionService.initialDataAppLoad();
                return true;
              },
              error => this.presentErrorMessage(error)
              );
          }
        }
      ],
      enableBackdropDismiss: false
    });

    loginScreen.present();
  }

  /**
   * Display firebase error messages from logging in
   * 
   * @param {firebase.FirebaseError} error 
   * 
   * @memberOf LoginService
   */
  public presentErrorMessage(error: firebase.FirebaseError) {
    const alert = this.alertCtrl.create({
      title: 'Error',
      message: error.message,
      buttons: [{
        text: 'Dismiss',
        role: 'cancel',
        handler: () => {
          this.displayLogin();
        }
      }]
    });
    alert.present();
  }

  /**
   * Save credentials to localstorage
   * 
   * @param {any} user 
   * 
   * @memberOf LoginService
   */
  public saveCredentialsLocally(user) {
    localStorage.setItem('user', JSON.stringify(user));
  }

  /**
   * Retrieve credentials from localstorage
   * 
   * @readonly
   * 
   * @memberOf LoginService
   */
  public get retrieveCredentialsLocally() {
    const savedUser = JSON.parse(localStorage.getItem('user'));

    return savedUser === null ? {} : savedUser;
  }

  /**
   * Set the current user in the application state
   * This allows you to pull the current login user 
   * via state manager
   * 
   * @param {*} firebaseUser 
   * @returns 
   * 
   * @memberOf LoginService
   */
  public setCurrentUser(firebaseUser: any) {
    const email = firebaseUser.email ? firebaseUser.email : firebaseUser.emailAddress;

    return this.dataService.getUserInfo(email)
      .skipWhile(user => {
        return user === undefined;
      })
      .map(user => {
        this.saveCredentialsLocally(user);
        this.actionService.setCurrentUser(user);
        return user;
      });
  }


}
