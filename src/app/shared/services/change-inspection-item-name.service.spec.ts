import { TestBed, inject } from '@angular/core/testing';

import { ChangeInspectionItemNameService } from './change-inspection-item-name.service';

describe('ChangeInspectionItemNameService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ChangeInspectionItemNameService]
    });
  });

  it('should be created', inject([ChangeInspectionItemNameService], (service: ChangeInspectionItemNameService) => {
    expect(service).toBeTruthy();
  }));
});
