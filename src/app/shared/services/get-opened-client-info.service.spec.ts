import { TestBed, inject } from '@angular/core/testing';

import { GetOpenedClientInfoService } from './get-opened-client-info.service';

describe('GetOpenedClientInfoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetOpenedClientInfoService]
    });
  });

  it('should be created', inject([GetOpenedClientInfoService], (service: GetOpenedClientInfoService) => {
    expect(service).toBeTruthy();
  }));
});
