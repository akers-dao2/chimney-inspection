import { TestBed, inject } from '@angular/core/testing';

import { HandleStatusPanesService } from './handle-status-panes.service';

describe('HandleStatusPanesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HandleStatusPanesService]
    });
  });

  it('should be created', inject([HandleStatusPanesService], (service: HandleStatusPanesService) => {
    expect(service).toBeTruthy();
  }));
});
