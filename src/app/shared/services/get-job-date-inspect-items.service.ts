import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { StateManagerService } from 'app/shared/services/sassy-state-manager-ng2/src/state-manager.service';

import { IOpenedInspectionReport } from '../../shared/interfaces/opened-inspection-report';
import { IInspectionReport } from '../../shared/interfaces/inspection-report';
import { IInspection } from '../../shared/interfaces/inspection';

@Injectable()
export class GetJobDateInspectItemsService {

  constructor(
    private stateManager: StateManagerService
  ) { }

  /**
   * Returns the properties of the inspection reports object for an open report
   * 
   * @returns 
   * @memberof GetJobDateInspectItemsService
   */
 public get() {
    return Observable.combineLatest<IInspectionReport[], IOpenedInspectionReport>(
      this.stateManager.getModel('inspectionReports'),
      this.stateManager.getModel('openedInspectionReport'),
    )
      .map(([inspectionReports, openedReport]) => {
        const inspection = inspectionReports.find(item => Object.keys(item)[0] === openedReport.clientId)
        return { openedReport, inspection }
      })
      .map(({ openedReport, inspection }) => {
        const { clientId, jobDate, jobKey } = openedReport;
        return { inspection, clientId, jobDate, jobKey }
      })
  }

}
