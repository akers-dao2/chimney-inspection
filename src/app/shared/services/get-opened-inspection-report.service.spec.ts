import { TestBed, inject } from '@angular/core/testing';

import { GetOpenedInspectionReportService } from './get-opened-inspection-report.service';

describe('GetOpenedInspectionReportService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetOpenedInspectionReportService]
    });
  });

  it('should be created', inject([GetOpenedInspectionReportService], (service: GetOpenedInspectionReportService) => {
    expect(service).toBeTruthy();
  }));
});
