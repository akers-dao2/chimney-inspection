import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

/** Interfaces */
import { IInspectionReport } from 'app/shared/interfaces/inspection-report';
import { IOpenedInspectionReport } from 'app/shared/interfaces/opened-inspection-report';
import { IClient } from 'app/shared/interfaces/client';
import { IReport } from 'app/shared/interfaces/report';
import { IInspection } from 'app/shared/interfaces/inspection';

@Injectable()
export class GetOrderListOfInspectionsService {

  constructor() { }

  /**
   * Returns a list of inspected items in the report
   * 
   * @private
   * @param {{ [id: number]: {}, inspection: IInspection }} reports 
   * @returns 
   * @memberof GetOrderListOfInspectionsService
   */
  public get(reports: { [id: number]: {}, inspection: IInspection }): Observable<IReport[]> {
    const reportsKeys = Object.keys(reports)
      .filter(key => key !== 'inspection');

    return Observable.of(reportsKeys.reduce((items, key) => {
      const report = reports[key];
      return Array.of(...items, report)
    }, []));
  }
}
