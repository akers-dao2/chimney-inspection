import { Injectable } from '@angular/core';

@Injectable()
export class SortService {

  constructor() { }

  get(x, y) {
    if (isNaN(x)) {
      x = x.toLowerCase();
      y = y.toLowerCase();
    }
    return +(x > y) || +(x === y) - 1
  }
}
