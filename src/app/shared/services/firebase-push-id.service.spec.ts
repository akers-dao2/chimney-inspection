import { TestBed, inject } from '@angular/core/testing';

import { FirebasePushIdService } from './firebase-push-id.service';

describe('FirebasePushIdService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FirebasePushIdService]
    });
  });

  it('should be created', inject([FirebasePushIdService], (service: FirebasePushIdService) => {
    expect(service).toBeTruthy();
  }));
});
