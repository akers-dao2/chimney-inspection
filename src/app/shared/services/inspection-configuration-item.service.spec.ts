import { TestBed, inject } from '@angular/core/testing';

import { InspectionConfigurationItemService } from './inspection-configuration-item.service';

describe('InspectionConfigurationItemService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InspectionConfigurationItemService]
    });
  });

  it('should be created', inject([InspectionConfigurationItemService], (service: InspectionConfigurationItemService) => {
    expect(service).toBeTruthy();
  }));
});
