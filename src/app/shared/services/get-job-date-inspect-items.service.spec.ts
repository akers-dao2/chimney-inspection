import { TestBed, inject } from '@angular/core/testing';

import { GetJobDateInspectItemsService } from './get-job-date-inspect-items.service';

describe('GetJobDateInspectItemsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetJobDateInspectItemsService]
    });
  });

  it('should be created', inject([GetJobDateInspectItemsService], (service: GetJobDateInspectItemsService) => {
    expect(service).toBeTruthy();
  }));
});
