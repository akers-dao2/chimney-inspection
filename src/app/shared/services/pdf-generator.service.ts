import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { StateManagerService } from 'app/shared/services/sassy-state-manager-ng2/src/state-manager.service';

import { DataService } from '../../shared/services/data.service';

import { IClient } from '../../shared/interfaces/client';
import { IUser } from '../../shared/interfaces/user';
import { IReport } from '../../shared/interfaces/report';
import { IPhoto } from '../../shared/interfaces/photo';
import { IJob } from '../../shared/interfaces/job';
import { IOpenedInspectionReport } from '../../shared/interfaces/opened-inspection-report';

import { InspectionStatus } from '../../shared/enums/inspection-status';

@Injectable()
export class PdfGeneratorService {

  private user: IUser;
  private client: IClient;
  private isCertification: boolean;
  // private inspectItemsInReport: IReport[];
  private commentsAndRecommendations: any[];
  private requiredPics: IPhoto[];

  constructor(
    private stateManager: StateManagerService,
    private dataService: DataService
  ) { }

  public pdfData(client, isCertification, itemsInReport, completionDate) {

    this.commentsAndRecommendations = this.getCommentsAndRecommendations(itemsInReport);
    this.requiredPics = this.getRequiredPics(itemsInReport);
    const requirePicsDownloadLinks$ = this.picsDownloadLinks(this.requiredPics);
    return Observable.combineLatest(
      this.stateManager.getModel('comments'),
      this.stateManager.getModel('recommendations'),
      this.getJobDetails.pluck('signature'),
      this.getJobDetails.pluck('user'),
      this.addDownloadLink(itemsInReport),
      this.dataService.getIPAddress(),
      ...requirePicsDownloadLinks$
    )
      .take(1)
      .mergeMap(result => {
        const [comments, recommendations, signature, user, inspectItemsInReport, ipAddress] = result;
        const requirePicsDownloadLinks = result.slice(6);

        return this.dataService.generatePDFFromHTML(ipAddress, {
          client,
          user,
          isCertification,
          commentsAndRecommendations: this.commentsAndRecommendations,
          inspectItemsInReport,
          requiredPics: this.addActualDownloadLinkToPics(this.requiredPics, requirePicsDownloadLinks),
          comments,
          recommendations,
          signature,
          completionDate
        });
      });

  }

  private addDownloadLink(itemsInReport) {
    return Observable.from(itemsInReport)
      .mergeMap(v => Object.keys(v).filter(k =>
        k !== 'requiredPics' &&
        v[k] !== null &&
        v[k].hasOwnProperty('pictures')
      ), (item, key) => ({ item, key }))
      .mergeMap(({ item, key }) => {
        return item[key].pictures;
      }, ({ item, key }, picture: IPhoto) => ({ item, key, picture }))
      .mergeMap(({ item, picture }, index) => this.dataService.getImage(picture.fileName),
      ({ item, key, picture }, link) => ({ item, key, picture, link }))
      .map(({ item, key, picture, link }) => {
        const record: IPhoto = item[key].pictures.find(p => p.fileName === picture.fileName);
        record.downloadLink = link;
      })
      .toArray()
      .map(() => {
        return itemsInReport;
      });
  }

  private addActualDownloadLinkToPics(objectsWithPics: { [name: string]: any }[], downloadLinks: string[]) {
    return objectsWithPics.map((p, i) => {
      const pic = Object.assign({}, p);
      pic.downloadLink = downloadLinks[i];
      return pic;
    });
  }

  private picsDownloadLinks(objectWithPics: { [name: string]: any }[]) {
    return objectWithPics.map(pics => Observable.fromPromise(pics.downloadLink as Promise<string>)) as Observable<string>[];
  }

  /**
   * Returns the properties with comments and recommendations on them
   * 
   * @private
   * @param {any} inspectItemsInReport 
   * @returns 
   * @memberof ReportComponent
   */
  private getCommentsAndRecommendations(inspectItemsInReport) {
    return inspectItemsInReport
      .filter(item => {
        if (!!Object.keys(item).length) {
          return Object.keys(item).some(key => item[key] !== null && item[key].hasOwnProperty('comments') &&
            item[key].status === InspectionStatus['failed']);
        }

        return false;
      })
      .map(inspectItems => {
        return Object.assign({}, inspectItems, { name: this.newInspectName(inspectItems, inspectItemsInReport) });
      });
  }

  private newInspectName(item, inspectItemsInReport) {
    const chimneysList = this.chimneysList(inspectItemsInReport);
    const parentIndex = item.uuid[0];
    const chimneyName = this.getChimneyName(chimneysList, parentIndex);
    const applianceName = item.name;
    let name: string;

    if (chimneyName === applianceName) {
      name = chimneyName;
    } else {
      name = `${chimneyName}: ${applianceName}`;
    }

    return name;
  }

  private chimneysList(inspectItemsInReport) {
    return inspectItemsInReport.filter(item => item.uuid.length === 1);
  }

  private getChimneyName(chimneysList: IReport[], index: string) {
    return chimneysList.find(chimney => chimney.uuid === index).name;
  }

  private get getJobDetails() {
    return Observable.combineLatest<IJob[], IOpenedInspectionReport>(
      this.stateManager.getModel('jobs'),
      this.stateManager.getModel('openedInspectionReport')
    )
      .map(([jobs, openedInspection]) =>
        jobs.find(j => j.key === openedInspection.jobKey)
      );
  }

  /**
 * Returns the list of properties with required pictures on them
 * 
 * @private
 * @param {any} inspectItemsInReport 
 * @returns 
 * @memberof ReportComponent
 */
  private getRequiredPics(inspectItemsInReport) {
    return inspectItemsInReport
      .filter(item => Object.keys(item).some(key => key === 'requiredPics'))
      .reduce((pics, item) => {
        const newPics = item.requiredPics.pictures.map((pic: IPhoto) => {
          let name = item.name;

          if (pic.name === 'front_of_house') {
            name = 'Front Of House';
          }

          if (!navigator.onLine) {
            pic.downloadLink = this.dataService.getStorageItem(pic.fileName);
          } else {
            pic.downloadLink = this.dataService.getImage(pic.fileName) as any;
          }

          return Object.assign({}, pic, { name });
        });

        return Array.of(...pics, ...newPics);
      }, []);
  }

}
