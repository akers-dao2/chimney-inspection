import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subject } from 'rxjs/Subject';
import { StateManagerService } from 'app/shared/services/sassy-state-manager-ng2/src/state-manager.service';

import * as firebase from 'firebase';

import 'rxjs/add/operator/skipWhile';
import 'rxjs/add/operator/switch';
import 'rxjs/add/observable/fromPromise';

/** Interfaces */
import { ICrew } from '../interfaces/crew';
import { IClient } from '../interfaces/client';
import { IInspection } from '../interfaces/inspection';
import { IComment } from '../interfaces/comments';
import { IRecommendation } from '../interfaces/recommendations';
import { IJob } from '../interfaces/job';
import { IAuditRecord } from '../interfaces/audit-record';
import { IStorageResp } from '../interfaces/storage-response';

/** Services */
import { UtilityService } from './utility.service';
import { FirebaseService } from './firebase.service';
import { LoaderService } from './loader.service';
import { FirebasePushIdService } from './firebase-push-id.service';
import { PouchDbService } from './pouch-db.service';
import { ActionService } from './action.service';
import { Client } from 'app/shared/classes/client';

/** Classes */
import { Inspection } from '../classes/inspection';
import { Job } from '../classes/job';
import { User } from '../classes/user';
import { from } from 'rxjs/observable/from';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';
import { IFirebaseStorageObject } from '../interfaces/firebase-storage-object';
import { LocalDbStatus } from '../enums/local-db-status';
import { take } from 'rxjs/operators';
import { IInspectionReport } from '../interfaces/inspection-report';

@Injectable()
export class DataService {

  constructor(
    private utility: UtilityService,
    private firebase: FirebaseService,
    private firebasePushId: FirebasePushIdService,
    private pouchDB: PouchDbService,
    private http: HttpClient,
    private stateManager: StateManagerService,
  ) { }

  /****************************************************************
   *
   * Public Methods
   *
   ****************************************************************/

  public getFileMetaDataFromFirebaseStorage = ({ value, key }) => {
    if (navigator.onLine) {
      const metaData = this.firebase.get.storage().ref().child(value).getMetadata();
      return from(metaData)
        .pipe(
        map(meta => ({ ...meta, downLoadLinksKey: key })),
        catchError(this.handleFirebaseStorageError.bind(this, key))
        );
    }

    return of(null);
  }

  public deleteFileFromFirebaseStorage = ({ name, downLoadLinkKey }: IFirebaseStorageObject) => {
    if (navigator.onLine) {
      const metaData = this.firebase.get.storage().ref().child(name).delete();
      return from(metaData)
        .pipe(
        tap(() => this.removeImageFileNameFromDB(downLoadLinkKey))
        );
    }

    return of(null);
  }

  private handleFirebaseStorageError = (key, err) => {
    if (err.code === 'storage/object-not-found') {
      this.removeImageFileNameFromDB(key);
    }
    return of(null);
  }

  public updateApplicationMaintenanceJobs() {
    const updates = {};
    updates[`applicationMaintenanceJobs/value`] = Date.now();

    return from(this.firebase.get.database().ref().update(updates));

  }

  public getApplicationMaintenanceJobs() {
    const applicationMaintenanceJobs$: BehaviorSubject<{ value: number }> = new BehaviorSubject({ value: undefined });

    this.valueChangeFactory<{}>(`applicationMaintenanceJobs`, 'once', applicationMaintenanceJobs$, false);

    return applicationMaintenanceJobs$;

  }

  public addImageFileNameToDB(fileName: string) {
    return this.updateValueFactory('downLoadLinks', fileName);
  }

  public getImageFileNamesFromDB() {
    const links$: BehaviorSubject<{ key: string, value: string }[]> = new BehaviorSubject([]);

    const data = this.firebase.get.database().ref('downLoadLinks');

    data.once('value', (snapshot) => {
      let results = snapshot.val();
      results = Object.keys(results)
        .map(key => Object.assign({}, { value: results[key] }, { key }));
      links$.next(results);
    });

    return links$;
  }

  public removeImageFileNameFromDB(key: string) {
    return this.removeRef('downLoadLinks', key);
  }

  public generatePDFFromHTML(ipAddress, pdfData) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/pdf'
    });
    const body = JSON.stringify(pdfData);
    return this.http.post(`${ipAddress.value}/pdf`, body, { headers, responseType: 'arraybuffer' })
      .map(res => new Blob([res], { type: 'application/pdf' }));

  }

  public addIpAddress(ip) {
    const updates = {};
    updates[`ipAddress`] = ip;

    return Observable.fromPromise(
      this.firebase.get.database().ref().update(updates)
        .then(response => response)
    );
  }

  public getIPAddress() {
    const ip$: BehaviorSubject<{ value: string }> = new BehaviorSubject({ value: undefined });

    this.valueChangeFactory<{ value: string }>(`ipAddress`, 'on', ip$, false);

    return ip$;
  }

  /**
  * Add a new job
  *
  * @returns
  *
  * @memberOf DataService
  */
  public updateJobCount(count: number) {
    return this.updateValueFactory('jobCount', count);
  }

  /**
   * Get a list of jobs
   *
   * @returns {BehaviorSubject<{ value: number }>}
   *
   * @memberOf DataService
   */
  public getJobCount() {
    const jobCount$: BehaviorSubject<{ value: number }> = new BehaviorSubject(undefined);

    this.valueChangeFactory<{}>(`jobCount`, 'on', jobCount$, false);

    return jobCount$;
  }

  /**
   * Get a list of jobs
   *
   * @returns {BehaviorSubject<IJob[]>}
   *
   * @memberOf DataService
   */
  public getJobs(limit?: number) {
    const jobs$: BehaviorSubject<IJob[]> = new BehaviorSubject([]);

    this.valueChangeFactory<IJob[]>(`jobs`, 'on', jobs$, true, limit);

    return jobs$;
  }

  public getJobsOnce(limit?: number) {
    const jobs$: BehaviorSubject<IJob[]> = new BehaviorSubject([]);

    this.valueChangeFactory<IJob[]>(`jobs`, 'once', jobs$, true, limit);

    return jobs$;
  }


  /**
   * Get a specific job
   *
   * @param {string} id
   * @returns {BehaviorSubject<Job[]>}
   *
   * @memberOf DataService
   */
  public getjob(id: string) {
    const job$: BehaviorSubject<Job[]> = new BehaviorSubject([]);

    this.valueChangeFactory<Job[]>(`jobs/${id}`, 'on', job$);

    return job$;
  }


  /**
   * Add a new job
   *
   * @returns
   *
   * @memberOf DataService
   */
  public addJob(job: IJob, doUpdate?: boolean) {
    return this.updateValueFactory('jobs', job, doUpdate);
  }

  public get jobOrderKey() {
    const orderKey$: BehaviorSubject<{ value: number }> = new BehaviorSubject({ value: undefined });

    this.valueChangeFactory<{}>(`jobsOrder`, 'once', orderKey$, false);

    return orderKey$;
  }

  /**
   * Save jobs order
   *
   * @returns
   *
   * @memberOf DataService
   */
  public saveJobsOrder(jobsOrder: string[]) {
    this.jobOrderKey
      .subscribe((key) => {
        this.updateValueFactory('jobsOrder', { jobsOrder, key }, true);
      });
  }


  /**
   * Delete Job
   *
   * @param {string} id
   * @returns {Observable<firebase.Promise<T>>}
   *
   * @memberOf DataService
   */
  public deleteJob(id: string) {
    return this.removeRef('jobs', id);
  }

  /**
   * Get a user's info
   *
   * @param {string} id
   * @returns {BehaviorSubject<User>}
   *
   * @memberOf DataService
   */
  public getUserInfo(emailAddress: string): BehaviorSubject<User> {
    const user$: BehaviorSubject<User> = new BehaviorSubject(undefined);

    const data = this.firebase.get.database().ref(`users`);

    data.on('value', (snapshot) => {
      const users: User[] = this.utility.toArray(snapshot.val());

      const user = users.find(_user => _user.emailAddress.toLowerCase().trim() === emailAddress.toLowerCase().trim());

      user$.next(user);
    });

    return user$;
  }


  /**
   * Add a new user
   *
   * @returns {Observable<firebase.Promise<T>>}
   *
   * @memberOf DataService
   */
  public addUser(user: User) {
    return this.firebase.get.auth()
      .createUserWithEmailAndPassword(user.emailAddress, user.password)
      .then((d) => {
        return this.updateValueFactory('users', user);
      })
      .catch((error: any) => {
        const { code, message } = error;
        return error;
      });
  }

  /**
    * Update a client
    *
    * @returns {Observable<firebase.Promise<T>>}
    *
    * @memberOf DataService
    */
  public updateUser(user: User) {
    return this.updateValueFactory('users', user, true);
  }
  /**
   * Get a list of crews
   *
   * @returns {BehaviorSubject<string[]>}
   *
   * @memberOf DataService
   */
  public getUsers(): BehaviorSubject<User[]> {
    const users$: BehaviorSubject<User[]> = new BehaviorSubject([]);

    this.valueChangeFactory<User[]>(`users`, 'on', users$, true);

    return users$;
  }


  /**
   * Delete User
   *
   * @param {string} id
   * @returns {Observable<firebase.Promise<T>>}
   *
   * @memberOf DataService
   */
  public deleteUser(id: string) {
    return this.removeRef('users', id);
  }


  /**
   * Get a list of crews
   *
   * @returns {BehaviorSubject<string[]>}
   *
   * @memberOf DataService
   */
  public getCrews(): BehaviorSubject<ICrew[]> {
    const crews$: BehaviorSubject<ICrew[]> = new BehaviorSubject([]);

    this.valueChangeFactory<ICrew[]>(`crews`, 'on', crews$, true, 25);

    return crews$;
  }


  /**
   * Add a new crew
   *
   * @returns {Observable<firebase.Promise<T>>}
   *
   * @memberOf DataService
   */
  public addCrew<T>(crew: ICrew, doUpdate?: boolean): Observable<Promise<T>> {
    return this.updateValueFactory('crews', crew, doUpdate);
  }


  /**
   * Delete Crew
   *
   * @param {string} id
   * @returns {Observable<firebase.Promise<T>>}
   *
   * @memberOf DataService
   */
  public deleteCrew(id: string) {
    return this.removeRef('crews', id);
  }


  /**
   * Get a list of clients
   *
   * @returns {BehaviorSubject<IClient[]>}
   *
   * @memberOf DataService
   */
  public getClients(limit?: number) {
    const clients$: BehaviorSubject<Client[]> = new BehaviorSubject([]);

    this.valueChangeFactory<Client[]>(`clients`, 'on', clients$, true, limit);

    return clients$;
  }

  public getClientsOnce(limit?: number) {
    const clients$: BehaviorSubject<Client[]> = new BehaviorSubject([]);

    this.valueChangeFactory<Client[]>(`clients`, 'once', clients$, true, limit);

    return clients$;
  }


  /**
   * Get a specific client
   *
   * @returns BehaviorSubject<string[]>
   *
   * @memberOf DataService
   */
  public getClient(id: string) {
    const client$: BehaviorSubject<string[]> = new BehaviorSubject([]);

    this.valueChangeFactory<string[]>(`clients/${id}`, 'on', client$, false);

    return client$;
  }


  /**
   * Add a new client
   *
   * @returns {Observable<firebase.Promise<T>>}
   *
   * @memberOf DataService
   */
  public addClient(client: IClient) {
    return this.updateValueFactory('clients', client);
  }


  /**
   * Update a client
   *
   * @returns {Observable<firebase.Promise<T>>}
   *
   * @memberOf DataService
   */
  public updateClient(client: IClient) {
    return this.updateValueFactory('clients', client, true);
  }


  /**
   * Delete Client
   *
   * @param {string} id
   * @returns {Observable<firebase.Promise<T>>}
   *
   * @memberOf DataService
   */
  public deleteClient(id: string) {
    return this.removeRef('clients', id);
  }


  /**
   * Add a new inspection
   *
   * @returns {Observable<firebase.Promise<T>>}
   *
   * @memberOf DataService
   */
  public createNewOrUpdateExistingInspection(inspection: IInspection, update?: boolean) {
    return this.updateValueFactory('inspections', inspection, update);
  }

  /**
   * Load an inspection
   *
   * @param {string} id
   * @returns {Observable<firebase.Promise<T>>}
   *
   * @memberOf DataService
   */
  public getInspection(id: string) {
    const inspection$: BehaviorSubject<IInspection> = new BehaviorSubject(new Inspection());

    this.valueChangeFactory<IInspection>(`inspections/${id}`, 'once', inspection$);

    return inspection$;
  }


  /**
   * Load inspections
   *
   * @returns {Observable<firebase.Promise<T>>}
   *
   * @memberOf DataService
   */
  public getInspections(limit?: number) {
    const inspections$: BehaviorSubject<IInspection[]> = new BehaviorSubject([]);

    this.valueChangeFactory<IInspection[]>(`inspections`, 'on', inspections$, true, limit);

    return inspections$;
  }

  public getInspectionsOnce(limit?: number) {
    const inspections$: BehaviorSubject<IInspection[]> = new BehaviorSubject([]);

    this.valueChangeFactory<IInspection[]>(`inspections`, 'once', inspections$, true, limit);

    return inspections$;
  }


  /**
   * Delete an inspection
   *
   * @param {string} id
   * @returns {Observable<firebase.Promise<T>>}
   *
   * @memberOf DataService
   */
  public deleteInspection(id: string) {
    return this.removeRef('inspections', id);
  }

  /**
   * getTownships
   * 
   * @returns 
   * 
   * @memberof DataService
   */
  public getTownships(): Observable<string[]> {
    const townships$: BehaviorSubject<string[]> = new BehaviorSubject([]);

    this.valueChangeFactory<string[]>(`townships`, 'on', townships$, true, 1000);

    return (townships$ as Observable<string[]>)
      .skipWhile(townships => !townships.length)
      .map(townships =>
        Object.keys(townships[0])
          .filter(key => key !== 'key')
          .map(key => townships[0][key])
      );
  }

  /**
   * updateTownships
   * 
   * @param {string[]} townships 
   * 
   * @memberof DataService
   */
  public updateTownships(townships: string[]) {
    this.firebase.get.database().ref('townships')
      .remove()
      .then(() => {
        this.updateValueFactory('townships', townships);
      })
      .catch(error => console.log(error));
  }

  public createNewOrUpdateExistingInspectionReport(inspectionReport: any, update?: boolean) {
    return this.updateValueFactory('inspectionReports', inspectionReport, update);
  }

  /**
   * Load an inspection report
   *
   * @param {string} id
   * @returns {Observable<firebase.Promise<T>>}
   *
   * @memberOf DataService
   */
  public getInspectionReport(id: string) {
    const inspectionReport$: BehaviorSubject<any[]> = new BehaviorSubject([]);

    this.valueChangeFactory<any[]>(`inspectionReports/${id}`, 'on', inspectionReport$);

    return inspectionReport$;
  }

  /**
   * Load inspections
   *
   * @returns {Observable<firebase.Promise<T>>}
   *
   * @memberOf DataService
   */
  public getInspectionReports(limit) {
    const inspections$: BehaviorSubject<IInspectionReport[]> = new BehaviorSubject([]);

    this.valueChangeFactory<IInspectionReport[]>(`inspectionReports`, 'on', inspections$, true, limit);

    return inspections$;
  }

  public getInspectionReportsOnce(limit?: number) {
    const inspections$: BehaviorSubject<IInspection[]> = new BehaviorSubject([]);

    this.valueChangeFactory<IInspection[]>(`inspectionReports`, 'once', inspections$, true, limit);

    return inspections$;
  }

  /**
   * Delete an inspection report from inspectionReports table
   *
   * @param {string} reportId
   * @returns
   * @memberof DataService
   */
  public deleteInspectionReport(reportId: string) {
    const result = this.firebase.get.database().ref(`inspectionReports/${reportId}`).remove() as Promise<any>;
    return Observable.fromPromise(result);
  }


  /**
   * Removes an inspection report item from reports > date array
   *
   * @param {string} reportId
   * @param {string} clientId
   * @param {number} date
   * @param {number} reportItemId
   * @returns
   * @memberof DataService
   */
  public deleteInspectionReportItem(reportId: string, clientId: string, date: number, reportItemId: number) {
    const user = JSON.parse(localStorage.getItem('user'));
    if (user.online) {
      const path = `inspectionReports/${reportId}/${clientId}/reports/${date}/${reportItemId}`;
      const result = this.firebase.get.database().ref(path).remove();
      return Observable.fromPromise(result);
    }

    // user set for offline mode
    return this.pouchDB.db.get('offlineQueue')
      .then(results => {
        const record = results[`inspectionReports/${reportId}`];
        delete record[clientId].reports[date][reportItemId];
        return this.addToPouchDB({ [`inspectionReports/${reportId}`]: record }, 'offlineQueue');
      })
      .catch(console.error);
  }

  /**
   * deletePhotoFromOfflineQueue
   */
  public async deletePhotoFromOfflineQueue(fileName) {
    try {
      const doc = await this.pouchDB.db.get('offlineQueue');
      for (const key in doc) {
        if (doc.hasOwnProperty(key)) {
          const value = doc[key];
          if (value === fileName) {
            delete doc[key];
          }
        }
      }
     return await this.pouchDB.db.put({ ...doc, _id: 'offlineQueue', _rev: doc._rev });

    } catch (error) {
      console.log(error);
    }
  }

  /**
   * Removes an inspection item from chimney's inspectionItems array
   *
   * @param {string} clientId
   * @param {string} chimneyName
   * @param {number} index
   * @returns
   * @memberof DataService
   */
  public deleteInspectionItem(clientId: string, chimneyName: string, inspectionsItems) {
    const path = `inspections/${clientId}/${chimneyName}`;
    return this.updateValueFactory(path, { key: 'inspectionItems', ...inspectionsItems }, true);

  }

  /**
   * Removes a chimney from the inspections table
   *
   * @param {string} clientId
   * @param {string} chimneyName
   * @returns
   * @memberof DataService
   */
  public deleteInspectionChimney(clientId: string, chimneyName: string) {
    const path = `inspections/${clientId}/${chimneyName}`;
    const user = JSON.parse(localStorage.getItem('user'));
    let result: Promise<any> = Promise.resolve();
    if (user !== null && user.online) {
      result = this.firebase.get.database().ref(path).remove();

    } 

    return Observable.fromPromise(result);
  }

  /**
   * Load an comment
   *
   * @param {string} id
   * @returns {Observable<firebase.Promise<T>>}
   *
   * @memberOf DataService
   */
  public getComment(id: string) {
    const comment$: BehaviorSubject<IComment[]> = new BehaviorSubject([]);

    this.valueChangeFactory<IComment[]>(`comments/${id}`, 'on', comment$, false);

    return comment$;
  }

  /**
   * Load comments
   *
   * @returns {Observable<firebase.Promise<T>>}
   *
   * @memberOf DataService
   */
  public getComments() {
    const comments$: BehaviorSubject<IComment> = new BehaviorSubject(undefined);

    this.valueChangeFactory<IComment>(`comments`, 'on', comments$, false, 1000);

    return comments$;
  }

  /**
   * Delete an comment
   *
   * @param {string} id
   * @returns {Observable<firebase.Promise<T>>}
   *
   * @memberOf DataService
   */
  public deleteComment(id: string) {
    return this.removeRef('comments', id);
  }

  /**
   * Add a new comment
   *
   * @returns {Observable<firebase.Promise<T>>}
   *
   * @memberOf DataService
   */
  public createNewOrUpdateComment(comment: IComment, update?: boolean) {
    return this.updateValueFactory('comments', comment, update);
  }

  public createNewOrUpdateCommentOrRecommendation(commentOrRecommendation: string[], grouping: string, type: string) {
    const result = new BehaviorSubject(null);
    const updates = {};

    updates[`${type}/${grouping}`] = commentOrRecommendation;
    const user = JSON.parse(localStorage.getItem('user'));

    if (user !== null && user.online) {
      this.firebase.get.database().ref().update(updates)
        .then(response => {
          result.next('success');
        });
    } else {
      this.addToPouchDB(updates, 'offlineQueue')
        .then(data => result.next('success'))
        .catch(console.log);
    }

    return result;
  }

  /**
 * Load an comment
 *
 * @param {string} id
 * @returns {Observable<firebase.Promise<T>>}
 *
 * @memberOf DataService
 */
  public getRecommendation(id: string) {
    const recommendation$: BehaviorSubject<any[]> = new BehaviorSubject([]);

    this.valueChangeFactory<any[]>(`recommendations/${id}`, 'on', recommendation$, false);

    return recommendation$;
  }

  /**
   * Load comments
   *
   * @returns {Observable<firebase.Promise<T>>}
   *
   * @memberOf DataService
   */
  public getRecommendations() {
    const recommendations$: BehaviorSubject<IRecommendation> = new BehaviorSubject(undefined);

    this.valueChangeFactory<IRecommendation>(`recommendations`, 'on', recommendations$, false, 1000);

    return recommendations$;
  }

  /**
   * Delete an comment
   *
   * @param {string} id
   * @returns {Observable<firebase.Promise<T>>}
   *
   * @memberOf DataService
   */
  public deleteRecommendation(id: string) {
    return this.removeRef('recommendations', id);
  }

  /**
   * Add a new comment
   *
   * @returns {Observable<firebase.Promise<T>>}
   *
   * @memberOf DataService
   */
  public createNewOrUpdateRecommendation(recommendation: IRecommendation, update?: boolean) {
    return this.updateValueFactory('recommendations', recommendation, update);
  }

  /**
   * Handles the sign in process
   *
   * @param {string} email
   * @param {string} password
   * @returns {BehaviorSubject<string>}
   *
   * @memberOf DataService
   */
  public signIn(email: string, password: string): BehaviorSubject<firebase.User> {
    const auth$: BehaviorSubject<firebase.User> = new BehaviorSubject(undefined);

    this.firebase.get.auth()
      .signInWithEmailAndPassword(email, password)
      .then((resolve) => {
        auth$.next(resolve);
      })
      .catch((error: firebase.FirebaseError) => {
        auth$.error(error);
      });

    return auth$;
  }


  /**
   * Handles the sign out process
   *
   * @returns {BehaviorSubject<any>}
   * @memberOf DataService
   */
  public signOut(): BehaviorSubject<any> {
    const auth$: BehaviorSubject<any> = new BehaviorSubject(undefined);

    this.firebase.get.auth().signOut()
      .then((resolve) => {
        auth$.next(resolve);
      })
      .catch((error: any) => {
        const { code, message } = error;
        auth$.error({ code, message });
      });

    return auth$;
  }

  public getImage(name: string) {
    // Create a root reference
    const storageRef = this.firebase.get.storage().ref();
    return name ? storageRef.child(name).getDownloadURL() : Promise.resolve('');
  }

  public uploadToRemoteStorage(file: Blob | string, name: string) {
    const storageRef = this.firebase.get.storage().ref().child(name);
    return storageRef.putString(file as string, 'base64');

  }

  public uploadToStorage(file: Blob | string, name: string, photoName: string):
    Observable<IStorageResp> {
    const status$ = new Subject<IStorageResp>();
    const user = JSON.parse(localStorage.getItem('user'));

    if (user !== null && !user.online) {
      this.offlineStorage(name, file, name, photoName)
        .then((downloadURL) => {
          if (downloadURL.ok) {
            status$.next({ type: 'downloadedURL', downloadURL: `o/${downloadURL.id}?`, photoName });
          }
        });
      return status$;
    }

    // Create a root reference
    const storageRef = this.firebase.get.storage().ref().child(name);
    const uploadTask = !user.online ? storageRef.putString(file as string, 'base64') : storageRef.put(file);

    // Listen for state changes, errors, and completion of the upload.
    uploadTask.on(this.firebase.get.storage.TaskEvent.STATE_CHANGED,
      (snapshot: any) => {
        // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
        const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        status$.next({ type: 'progress', progress });
        // console.log('Upload is ' + progress + '% done');
        switch (snapshot.state) {
          case this.firebase.get.storage.TaskState.PAUSED:
            console.log('Upload is paused');
            break;
          case this.firebase.get.storage.TaskState.RUNNING:
            // console.log('Upload is running');
            break;
        }
      }, (error: firebase.FirebaseError) => {
        status$.error(error.code);
      }, () => {
        // Upload completed successfully, now we can get the download URL
        const downloadURL = uploadTask.snapshot.downloadURL;
        status$.next({ type: 'downloadedURL', downloadURL, photoName });
        return undefined;
      });

    return status$;
  }

  private offlineStorage(id, file, name, photoName) {
    return this.pouchDB.db.get(id)
      .then((doc) => {
        return this.pouchDB.db.put(this.putAttachmentObj(id, doc, file, name, photoName));
      })
      .catch((err) => {
        console.log(err);
        if (err.status === 404) {
          return this.pouchDB.db.put(this.putAttachmentObj(id, null, file, name, photoName, false));
        }
      })
      .then(resp => {
        return Object.assign({}, resp, { photoName });
      });
  }

  public getStorageItem(id) {
    return from(this.pouchDB.db.get(id, { attachments: true }))
      .pipe(
      map((blob: any) => {
        const { content_type, data } = blob._attachments[id];
        return `data:${content_type};base64,${data}`;
      }),
      catchError(e => {
        if (e.status === 404) {
          return from(this.getImage(id));
        }
        throw new Error(e);
      }),
      take(1)
      )
      .toPromise();
  }

  public getPhotoFile(id: string): Promise<any> {
    return this.pouchDB.db.get(id, { attachments: true });
  }

  private putAttachmentObj(id, doc, file, name, photoName, addRev = true) {
    const rev = addRev ? { _rev: doc._rev } : {};

    return Object.assign({}, {
      _id: id,
      _attachments: {
        [name]: {
          content_type: file.type,
          data: file
        }
      }
    },
      rev);
  }

  /**
   * deleteFromStorage
   * 
   * @param {string} filename 
   * 
   * @memberof DataService
   */
  public async deleteFromStorage(filename: string, name: string) {
    const user = JSON.parse(localStorage.getItem('user')) as User;

    if (user.online) {
      // Create a root reference
      const storageRef = this.firebase.get.storage().ref();

      // Create a reference to the file to delete
      const image = storageRef.child(filename);

      // Delete the file
      return image.delete();
    }

    try {
      const file = await this.getPhotoFile(filename);
      // remove attachment from pouchdb
      await this.pouchDB.db.removeAttachment(filename, filename, file._rev);

      // remove attachment from offlineQueue db in pouchdb
      return this.deletePhotoFromOfflineQueue(filename);
    } catch (err) {
      console.log(err);
    }

  }

  /**
   * Get a specific client
   *
   * @returns BehaviorSubject<string[]>
   *
   * @memberOf DataService
   */
  public getAuditRecords() {
    const auditRecords$: BehaviorSubject<IAuditRecord[]> = new BehaviorSubject([]);

    this.valueChangeFactory<IAuditRecord[]>(`auditRecords`, 'once', auditRecords$, true);

    return auditRecords$;
  }


  /**
   * Add a new client
   *
   * @returns {Observable<firebase.Promise<T>>}
   *
   * @memberOf DataService
   */
  public addAuditRecord(record: IAuditRecord) {
    return this.firebase.get.database().ref().update(record)
      .catch(err => { throw err; });
  }


  /****************************************************************
   *
   * Private Methods
   *
   ****************************************************************/

  /**
   * Value Change Factory
   *
   * @private
   * @template T
   * @param {string} path
   * @param {string} method
   * @param {BehaviorSubject<T[]>} obs$
   *
   * @memberOf DataService
   */
  private async valueChangeFactory<T>(path: string, method: 'once' | 'on', obs$: BehaviorSubject<T>, isArray = false, limit = 50) {
    const localDBStatus = await this.stateManager.getModel('localDB').pipe(take(1)).toPromise<LocalDbStatus>();
    const user = JSON.parse(localStorage.getItem('user'));

    if ((user !== null && user.online) || localDBStatus === LocalDbStatus.SYNCFROM) {
      this.firebaseUpdate(path, method, obs$, isArray, limit);
    } else {
      this.pouchDB.db.get(path)
        .then(results => {
          delete results._id;
          delete results._rev;
          if (isArray) {
            results = Object.keys(results)
              .map((key) => Object.assign({}, results[key]));
          }
          obs$.next(results);
        })
        .catch(console.error);
    }
  }

  private firebaseUpdate(path: string, method: string, obs$: BehaviorSubject<any>, isArray: boolean, limit: number) {
    let data: {};
    if (path === 'clients') {
      data = this.firebase.get.database().ref(path).orderByChild('lastName');
    } else {
      data = this.firebase.get.database().ref(path).orderByChild('date').limitToLast(limit);
    }

    data[method]('value', (snapshot) => {
      let results = snapshot.val();
      if (isArray) {
        results = snapshot.val() ? this.utility.toArray(snapshot.val()) : [];
      }
      obs$.next(results);
    });
  }


  /**
   * Retrieve the key for path
   *
   * @private
   * @param {any} path
   * @returns {string}
   *
   * @memberOf DataService
   */
  public getKey(path) {
    return navigator.onLine ? this.firebase.get.database().ref().child(path).push().key : this.firebasePushId.getId;
  }


  /**
   * Update Value Factory
   *
   * @private
   * @param {string} key
   * @param {*} update
   * @returns {Observable<firebase.Promise<T>>}
   *
   * @memberOf DataService
   */
  private updateValueFactory(key: string, update: any, doUpdate?: boolean) {
    const result = new BehaviorSubject(null);
    let newKey = (doUpdate) ? update.key : this.getKey(key);
    const updates = {};
    const user = JSON.parse(localStorage.getItem('user'));

    if (key === 'inspections') {
      update.key = newKey;
    }

    if (key === 'jobCount') {
      newKey = 'value';
    }

    updates[`${key}/${newKey}`] = update;

    if (user !== null && user.online) {
      this.firebase.get.database().ref().update(updates)
        .then(response => {
          result.next({ response, newKey });
        });

    } else {
      if (newKey !== 'inspectionItems') {
        this.addToPouchDB(updates, 'offlineQueue')
          .then(data => result.next({ data, newKey }))
          .catch(console.log);
      }
    }

    return result;
  }

  private async addToPouchDB(data, model) {
    try {
      const doc = await this.pouchDB.db.get(model);
      data = Object.assign({}, doc, data, { _id: model, _rev: doc._rev });
      return this.pouchDB.db.put(data);

    } catch (err) {
      if (err.status === 404) {
        data = Object.assign({}, data, { _id: model });
        this.pouchDB.db.put(data);
      }
      console.log(err);

    }


  }

  public syncRemote(updates) {
    return this.firebase.get.database().ref().update(updates)
      .then(response => {
        return response;
      })
      .catch(err => { throw err; });
  }


  /**
   * Removes the data at this Database location.
   *
   * @private
   * @param {string} base
   * @param {string} key
   * @returns {string}
   *
   * @memberOf DataService
   */
  private removeRef(base: string, key: string) {
    const result = this.firebase.get.database().ref(`${base}/${key}`).remove() as Promise<any>;
    return Observable.fromPromise(result);
  }
}
