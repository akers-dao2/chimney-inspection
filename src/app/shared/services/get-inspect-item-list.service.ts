import { Injectable } from '@angular/core';
import { StateManagerService } from 'app/shared/services/sassy-state-manager-ng2/src/state-manager.service';
import { IInspection } from '../../shared/interfaces/inspection';
import { IChimneyItem } from '../../shared/interfaces/chimney-item';
import { IJob } from '../../shared/interfaces/job';
import { ChimneyNamesService } from '../../shared/services/chimney-names.service';

@Injectable()
export class GetInspectItemListService {

  constructor(
    private stateManager: StateManagerService,
    private chimneyNames: ChimneyNamesService
  ) { }

  public get(clientId, date) {
    return this.stateManager.getModel('inspections')
      .switchMap<IInspection[], IInspection>(inspections => inspections)
      .filter(inspection =>
        inspection.clientId === clientId && inspection.date === date)
      .map<IInspection, IChimneyItem[]>(chimneys => {
        return this.chimneyNames.get(chimneys)
          .filter(key => !(key === 'clientId' || key === 'date' || key === 'jobKey'))
          .map((key, index) => Object.assign({},
            {
              name: chimneys[key].name || key,
              uuid: chimneys[key].uuid,
              index,
              appliances: chimneys[key].inspectionItems,
              notValid: chimneys[key].notValid || false,
            })
          );
      });
  }

  public getSortByOrder(clientId, date) {
    return this.get(clientId, date)
      .map(list => {
        return list.reduce((newList, item) => {
          return Array.of(...newList, item.uuid, ...this.getApplianceName(item.appliances));
        }, []);
      });
  }

  private getApplianceName(appliances: any[]) {
    return appliances ? appliances.map(appliance => appliance.uuid) : [];
  }
}
