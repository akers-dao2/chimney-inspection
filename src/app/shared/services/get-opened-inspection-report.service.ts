import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/combineLatest';
import 'rxjs/add/operator/withLatestFrom';

import { StateManagerService } from 'app/shared/services/sassy-state-manager-ng2/src/state-manager.service';
import { GetInspectItemListService } from '../../shared/services/get-inspect-item-list.service';

/** Interfaces */
import { IInspectionReport } from 'app/shared/interfaces/inspection-report';
import { IOpenedInspectionReport } from 'app/shared/interfaces/opened-inspection-report';
import { IClient } from 'app/shared/interfaces/client';
import { IInspection } from 'app/shared/interfaces/inspection';

@Injectable()
export class GetOpenedInspectionReportService {

  constructor(
    private stateManager: StateManagerService,
    private getInspectItemList: GetInspectItemListService
  ) { }

  public get() {
    return Observable.combineLatest<IInspectionReport[], IOpenedInspectionReport, IInspection[]>(
      this.stateManager.getModel('inspectionReports'),
      this.stateManager.getModel('openedInspectionReport'),
      this.stateManager.getModel('inspections'),
    )

      .map(([inspectionReports, openedReport, inspectionsList]) => {
        const inspection = inspectionReports.find(item => {
          const key = Object.keys(item)[0];
          return item[key].clientId === openedReport.clientId && item[key].jobKey === openedReport.jobKey;
        });
        return { inspectionReports, openedReport, inspection };
      })
      .map(({ inspectionReports, openedReport, inspection }) =>
        inspection ? inspection[openedReport.clientId].reports[openedReport.jobDate] : {}
      )
      .withLatestFrom(this.stateManager.getModel('openedInspectionReport'))
      .switchMap(([inspectionsList, openedReport]) => {
        return this.getInspectItemList.getSortByOrder(openedReport.clientId, openedReport.jobDate);
      }, ([inspectionsList, openedReport], sortedInspectList) => {

        const reportsKeys = Object.keys(inspectionsList)
          .filter(key => key !== 'inspection');

        return sortedInspectList.reduce((list, uuid) => {
          const key$ = reportsKeys.find(key => inspectionsList[key].uuid === uuid);
          return key$ ? Array.of(...list, inspectionsList[key$]) : list;
        }, []);

      });
  }
}
