import { Injectable } from '@angular/core';
import { IPhoto } from '../interfaces/photo';

@Injectable()
export class RemovePictureService {

  constructor() { }

  public execute(pictures: IPhoto[], photo: IPhoto) {
    return pictures.filter(picture => picture.fileName !== photo.fileName);
  }

}
