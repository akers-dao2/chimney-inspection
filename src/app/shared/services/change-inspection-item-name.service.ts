import { Injectable } from '@angular/core';
import { StateManagerService } from 'app/shared/services/sassy-state-manager-ng2/src/state-manager.service';
import { ActionService } from 'app/shared/services/action.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/empty';

@Injectable()
export class ChangeInspectionItemNameService {

  constructor(
    private actionService: ActionService,
    private stateManager: StateManagerService,
  ) { }

  execute(name: string, origName: string, prop: string, uuid: string, type?: string): Observable<string> {
    if (prop === 'name') {
      return this.actionService.changeInspectionName(name, origName, uuid, type) as Observable<string>;
    }
    return Observable.empty();
  }
}
