import { Injectable } from '@angular/core';
import { InspectionStatus } from 'app/shared/enums/inspection-status';

@Injectable()
export class NotApplicablePaneResetService {

  constructor() { }

  /**
   * 
   * 
   * @param {any} equipment 
   * @returns 
   * @memberof NotApplicablePaneResetService
   */
  public execute(equipment, className) {
    const newEquipment = Object.assign(new className(), equipment);
    // loop through the equipment properties
    for (const key in newEquipment) {
      if (newEquipment.hasOwnProperty(key) && newEquipment[key] !== undefined && newEquipment[key] !== null) {
        // check the equipment property for property call status
        if (newEquipment[key].hasOwnProperty('status')) {
          // If status exist and it equals not applicable
          //  loop through the pane properties and reset to empty array or null
          if (newEquipment[key].status === InspectionStatus['Not Applicable'] ||
          newEquipment[key].status === InspectionStatus['N/A - Client refusal'] ||
          newEquipment[key].status === InspectionStatus['N/A - Inaccessible'] ||
          newEquipment[key].status === InspectionStatus['N/A - Weather']) {
            for (const key$ in newEquipment[key]) {
              if (newEquipment[key].hasOwnProperty(key$) && key$ !== 'status') {
                newEquipment[key][key$] = Array.isArray(newEquipment[key][key$]) ? [] : null;
              }
            }
          }
        }
      }
    }

    return newEquipment;
  }
}
