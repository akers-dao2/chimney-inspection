import { Injectable } from '@angular/core';
import { ActionService } from './action.service';

@Injectable()
export class InspectionConfigurationItemService {

  constructor(
    private actions: ActionService
  ) { }

  public open(item: string) {
    this.actions.openInspectionConfigurationItem(item)
  }
}
