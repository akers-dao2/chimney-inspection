import { Injectable } from '@angular/core';

@Injectable()
export class ChimneyNamesService {

  constructor() { }
  get(obj) {
    return Object.keys(obj).filter(key => /chimney-[0-9]/ig.test(key));
  }
}
