import { TestBed, inject } from '@angular/core/testing';

import { RemovePictureService } from './remove-picture.service';

describe('RemovePictureService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RemovePictureService]
    });
  });

  it('should be created', inject([RemovePictureService], (service: RemovePictureService) => {
    expect(service).toBeTruthy();
  }));
});
