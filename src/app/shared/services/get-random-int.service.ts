import { Injectable } from '@angular/core';

@Injectable()
export class GetRandomIntService {

  constructor() { }

  /**
   * Returns a random integer between min (inclusive) and max (inclusive)
   * Using Math.round() will give you a non-uniform distribution!
   */
  get(min = 100, max = 100000) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

}
