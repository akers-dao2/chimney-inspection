import { TestBed, inject } from '@angular/core/testing';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import {
  NavParams,
  ItemSliding,
  App, Config,
  Form,
  IonicModule,
  Keyboard,
  DomController,
  MenuController,
  NavController,
  Platform,
  GestureController,
  ModalController,
  AlertController
} from 'ionic-angular';

import { ConfigMock, PlatformMock } from '../../shared/mocks/ionic-mocks';

/** Services */
import { ActionService } from '../../shared/services/action.service';
import { ChimneyNamesService } from '../../shared/services/chimney-names.service';
import { StateManagerService } from 'app/shared/services/sassy-state-manager-ng2/src/state-manager.service';
import { InspectionItemService } from './inspection-item.service';
import { UtilityService } from '../../shared/services/utility.service';
import { DataService } from '../../shared/services/data.service';
import { FirebaseService } from '../../shared/services/firebase.service';

describe('InspectionItemService', () => {
  let actionService: ActionService;
  let stateManager: StateManagerService;
  let inspectionItemService: InspectionItemService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ActionService,
        StateManagerService,
        UtilityService,
        DataService,
        FirebaseService,
        InspectionItemService,
        ChimneyNamesService,
        AlertController,
        App,
        DomController,
        Form,
        Keyboard,
        MenuController,
        NavController,
        GestureController,
        ModalController,
        { provide: NavParams, useClass: () => { } },
        { provide: Platform, useClass: PlatformMock },
        { provide: Config, useClass: ConfigMock }
      ]
    });
  });

  beforeEach(inject(
    [
      ActionService,
      StateManagerService,
      InspectionItemService,
    ],
    (
      _actionService_: ActionService,
      _stateManager_: StateManagerService,
      _inspectionItemService_: InspectionItemService,
    ) => {
      actionService = _actionService_;
      stateManager = _stateManager_;
      inspectionItemService = _inspectionItemService_;

      spyOn(actionService, 'addInspectionItemToList');
      spyOn(actionService, 'addChimneyAndFlue');

      spyOn(stateManager, 'getModel').and.callFake(() => new BehaviorSubject([]));

    }));

  it('should ...', inject([InspectionItemService], (service: InspectionItemService) => {
    expect(service).toBeTruthy();
  }));
});
