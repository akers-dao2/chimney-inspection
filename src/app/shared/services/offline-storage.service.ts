import { Injectable } from '@angular/core';
import { ActionService } from './action.service';

@Injectable()
export class OfflineStorageService {

  constructor(
    private action: ActionService
  ) { }

  add(item) {
    this.action.addOrRemoveItemOfflineStorage(item)
  }

}
