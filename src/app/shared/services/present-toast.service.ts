import { Injectable } from '@angular/core';
import { ToastController } from 'ionic-angular';

@Injectable()
export class PresentToastService {

  constructor(
    private toastCtrl: ToastController
  ) { }

  /**
   * Display message at the bottom of the screen
   * 
   * @private
   * 
   * @memberof TownshipLoaderComponent
   */
  public show(message) {
    const toast = this.toastCtrl.create({
      message,
      duration: 3000,
      position: 'bottom'
    });

    toast.present();
  }
}
