import { Injectable } from '@angular/core';
import { TitleCasePipe } from '@angular/common';
import { Http } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

import { IPDFOptions } from '../interfaces/pdf-options';
import { IClient } from '../interfaces/client';
import { IReport } from '../interfaces/report';

import { ApplianceTypes } from '../enums/appliances';
import { InspectionStatus } from '../enums/inspection-status';

import { Client } from '../classes/client';

import { RowHeaderNamePipe } from '../pipes/row-header-name.pipe'
import { CreateListPipe } from '../pipes/create-list.pipe'
import { SliceObjectPipe } from '../pipes/slice-object.pipe'
import { StatusNamePipe } from '../pipes/status-name.pipe'

import * as moment from 'moment';

declare const pdfMake;

@Injectable()
export class PdfMakerService {

  private dd: {};
  private styles: {};
  private pdf: any;
  private content: any[] = [];
  private pageHeader = 'Wells Sons';
  private opt: IPDFOptions;
  private client: IClient;
  private report: IReport[];

  constructor(
    private http: Http
  ) { }

  public createPdf(docDefinition, opt: IPDFOptions, client: IClient, report: IReport[]) {

    this.opt = opt;
    this.client = client || new Client();
    this.report = report;

    return Observable.of(this.pageSetup)
      .switchMap((that) => that.addCoverPage)
      .switchMap((that) => that.addImagesPage)
      .switchMap((that) => that.addChimneyImagePage)
      .switchMap((that) => that.addReportDetailsPage)
      .switchMap((that) => that.addCommentPage)
      .map(that => {
        return that
          .addStyles
          .addHeadersAndFooters
          .loadDefinitions(docDefinition)
          .docDefinition
      })
      .map(docDefinition$ => {
        delete docDefinition$.http
        return docDefinition$
      })
  }

  public resetState() {
    this.content = [];
  }

  private get docDefinition() {
    return Object.assign({}, this, { content: this.content });
  }

  private set docDefinition(docDefinition: any) {
    this.content = Array.of(...this.content, docDefinition);
  }

  private loadDefinitions(docDefinition) {
    this.docDefinition = docDefinition;
    return this;
  }

  private get pageSetup() {
    const pageSize = {
      pageSize: 'A4'
    };

    const pageMargins = {
      pageMargins: [20, 80, 20, 80]
    };

    return Object.assign(this, pageSize, pageMargins);
  }

  private get addHeadersAndFooters() {
    const header = {
      header: (currentPage, pageCount) => {
        return {
          table: {
            widths: ['33%', '33.3%', '33.3%'],

            body: [
              [
                {
                  text: `${this.client.firstName} ${this.client.lastName} \n ${this.client.address} 
                  ${this.client.city}, ${this.client.state} ${this.client.zip}`,
                  margin: [30, 10, 0, 0],
                  border: [false, false, false, true]
                },
                {
                  text: `Inspection Report \n ${moment().format('MM/DD/YYYY')}`,
                  margin: [0, 10, 0, 0],
                  alignment: 'center',
                  border: [false, false, false, true]
                },
                {
                  text: `Page ${currentPage.toString()}`,
                  margin: [0, 10, 30, 0],
                  alignment: 'right',
                  border: [false, false, false, true]
                }
              ],
            ]
          },
          layout: {
            defaultBorder: false
          },
          style: 'pageHeader'
        }
      }
    };

    const background = {
      background: function (currentPage) {
        if (currentPage < 4) {
          return;
        }
        return {
          layout: {
            defaultBorder: true,
            vLineColor: '#073F80',
            hLineColor: '#073F80',
          },
          margin: [19, 82, 19, 0],
          table: {
            widths: ['*'],
            body: [
              [
                {
                  border: [true, true, true, true],
                  margin: [20, 0, 20, 650],
                  text: ''
                }
              ]
            ]
          }
        }
      },
    }

    const footer = {
      footer: (currentPage, pageCount) => {
        return {
          table: {
            widths: ['33%', '33.3%', '33.3%'],

            body: [
              [
                {
                  text: `Prepared By: ${this.opt.name}`,
                  colSpan: 3,
                  bold: true,
                  border: [false, true, false, false],
                  margin: [30, 10, 0, 0]
                }, {}, {}
              ],
              [
                {
                  text: 'Wells & Sons Chimney Service \n 69 Congo Road \n Gilbertsville, PA 19525',
                  margin: [30, 0, 0, 0]
                },
                {
                  text: `610.473.6000 \n office@wellssons.com`,
                  margin: [0, 15, 0, 0],
                  alignment: 'center'
                },
                {
                  text: `CSIA:${this.opt.name}`,
                  margin: [0, 25, 30, 0],
                  alignment: 'right'
                }
              ],
            ]
          },
          layout: {
            defaultBorder: false
          },
          style: 'footer'
        };
      }
    };

    return Object.assign(this, footer, header, background);
  }

  private get addCoverPage(): Observable<this> {
    const subject = new Subject<this>();

    this.logo
      .subscribe(image => {
        this.docDefinition = [
          {
            table: {
              widths: ['35%', '50%'],
              body: [
                [
                  {
                    image,
                    width: 100,
                    height: 100,
                    margin: [100, 0, 0, 0]

                  },
                  {
                    text: 'Wells & Sons Chimney Service \n Inspection Report',
                    style: 'header',
                    alignment: 'center',

                  }],
              ],
            },
            layout: 'noBorders',
          },
          {
            text: 'Attention:',
            bold: true
          },
          {
            text: `This inspection report is a result of a visual inspection done at the time of the cleaning. 
        It is intended as a convenience to our clients, not as a certification (unless noted), of fire 
        worthiness or safety. Since conditions of use and hidden construction defects are beyond our 
        control, we make no warranty of the safety or function of any appliance and none is to be implied.`
          },
          {
            text: `Customer Signature \n`,
            margin: [0, 300, 0, 80]
          },
          {
            text: `By signing this electronic document, the customer acknowledges that this document will be 
        sent via electronic mail (email) or sent in the mail to the customer. 
        It is the customer’s responsibility to follow through with any recommendations.`,
            pageBreak: 'after'
          }
        ]
        subject.next(this)
      })

    return subject;
  }

  private get addImagesPage(): Observable<this> {
    const subject = new BehaviorSubject(null);

    this.placeHolderImage
      .subscribe(image => {
        this.docDefinition = [
          {
            table: {
              widths: ['*', '*', '*'],
              body: [
                [{
                  image

                },
                {
                  image

                },
                {
                  image
                }],
              ],
            },
            layout: 'noBorders',
            alignment: 'center',
            pageBreak: 'after'
          }
        ];

        subject.next(this);
      })

    return subject.skipWhile(that$ => that$ === null)
  }

  private get addChimneyImagePage(): Observable<this> {
    const subject = new BehaviorSubject(null)

    this.getImage('chimney_picture.png')
      .subscribe(image => {
        this.docDefinition = [
          {
            image,
            pageBreak: 'after',
            alignment: 'center'
          }
        ];

        subject.next(this)
      })

    return subject.skipWhile(that$ => that$ === null);
  }

  private get addReportDetailsPage() {
    this.docDefinition = [
      {
        layout: 'noBorders',
        table: {
          widths: ['*'],
          body: [
            [
              {
                layout: 'noBorders',
                table: {
                  widths: ['*'],
                  body: this.reportDetails
                  // [
                  //   [
                  //     this.chimneyPDFViewData()
                  //   ],
                  //   [
                  //     this.fluePDFViewData()
                  //   ],
                  //   [
                  //     this.appliancePDFViewData()
                  //   ]
                  // ]
                },
              }
            ]
          ]
        },
        style: 'table',
        pageBreak: 'after'
      }
    ]
    return Observable.of(this);
  }

  private get reportDetails() {
    return this.report.map(data => {
      let pdfViewData: any[];
      switch (data.applianceType) {
        case ApplianceTypes.Chimney:
          pdfViewData = [this.chimneyPDFViewData(data)];
          break;
        case ApplianceTypes.Stoves:
          pdfViewData = [this.appliancePDFViewData(data)];
          break;
        case ApplianceTypes['Flue/Vent']:
          pdfViewData = [this.fluePDFViewData(data)]
          break;
        case ApplianceTypes['Masonry Fireplace']:
          pdfViewData = [this.appliancePDFViewData(data)];
          break;
        case ApplianceTypes['Prefab Fireplace']:
          pdfViewData = [this.appliancePDFViewData(data)];
          break;
        case ApplianceTypes['Heating Appliances']:
          pdfViewData = [this.appliancePDFViewData(data)];
          break;
        case ApplianceTypes['Dryer Vent']:
          pdfViewData = [this.appliancePDFViewData(data)];
          break;

        default:
          break;
      }
      return pdfViewData;
    })
  }

  private chimneyPDFViewData(data) {
    return {
      border: [true, true, true, false],
      margin: [0, 0, 0, 0],
      table: this.chimneyTempTable(data),
      layout: {
        defaultBorder: true,
        vLineColor: '#cecece',
        hLineColor: '#cecece',
      }
    }
  }

  private fluePDFViewData(data) {
    return {
      border: [true, false, true, false],
      margin: [10, 0, 0, 0],
      table: this.flueTempTable(data),
      layout: {
        defaultBorder: true,
        vLineColor: '#cecece',
        hLineColor: '#cecece',
      }
    }
  }

  private appliancePDFViewData(data) {
    return {
      border: [true, false, true, true],
      margin: [20, 0, 0, 0],
      table: this.masonryTempTable(data),
      layout: {
        defaultBorder: true,
        vLineColor: '#cecece',
        hLineColor: '#cecece',
      }
    }
  }

  private get rowBorderData() {
    return [
      { border: [true, false, false, true] },
      { border: [false, false, false, true] },
      { border: [true, false, false, true] },
      { border: [false, false, true, true] },
      { border: [false, false, false, true] },
      { border: [false, false, true, true] }
    ]
  }

  private get chimneyMapper() {
    return [
      {
        name: 'constructionType',
        text: 'Construction Type:',
      },
      {
        name: 'roofMaterial',
        text: 'Roof Material:',
      },
      {
        name: 'flues',
        text: 'Number of Flues:',
      },
      {
        name: 'crown',
        text: 'Crown:',
      },
      {
        name: 'brickStone',
        text: 'Brick Stone:',
      },
      {
        name: 'mortar',
        text: 'Mortar:',
      },
      {
        name: 'stucco',
        text: 'Stucco:',
      },
      {
        name: 'flashing',
        text: 'Flashing:',
      },
      {
        name: 'height',
        text: 'Height:',
      },
      {
        name: 'structureIntegrity',
        text: 'Structure Integrity:',
      },
      {
        name: 'locationOnHouse',
        text: 'Location On House:',
      },
      {
        name: 'location',
        text: 'Location:',
      },
    ]
  }

  private generateTableData(data: IReport, mapper) {
    const newMapper = this.mergeMapper(data, mapper);

    return newMapper.reduce((table, items) => {

      return table.concat(Array.of(items.reduce((t, i, idx) => {
        return Array.of<any>(
          ...t,
          Object.assign({}, { text: i.text, bold: true }),
          Object.assign({}, { text: this.getText(data[i.name]) || '' }));
      }, [])
        .map((item, index) => Object.assign({}, item, this.rowBorderData[index]))))

    }, [])

  }

  private getText(data) {
    let text;
    if (data === undefined) {
      return '';
    }

    if (this.varTypes.includes(typeof data)) {
      text = data;
    } else {
      if (data.hasOwnProperty('status') && Object.keys(data).length === 1) {
        text = new TitleCasePipe().transform(InspectionStatus[data.status]);
      } else {
        text = JSON.stringify(Object.keys(data)
          .filter(key => !this.keysToByPass.includes(key))
          .reduce((a, key) => Object.assign({}, a, { [key]: data[key] }), {}))
      }
    }
    return 'text';
  }

  private get keysToByPass() {
    return [
      'pictures',
      'comments',
      'recommendations',
      'additionalComments',
      'enableXSide',
      'enableYSide',
      'enableZSide'
    ];
  }

  private get varTypes() {
    return ['string', 'number'];
  }

  private mergeMapper(data, mapper) {
    const newList: any[][] = new CreateListPipe().transform(data);

    return newList.map(list => list
      .map(item => this[mapper] ? this[mapper].find(value => value.name === item) : {
        name: 'name',
        text: 'text',
      }))
    // .filter(item => item !== undefined);
  }

  private chimneyTempTable(data) {
    return {
      widths: ['*', 'auto', '*', 'auto', '*', 'auto'],
      body: [
        [
          { text: data.name, colSpan: 6, fillColor: '#073F80', color: '#FFFFFF', }, {}, {}, {}, {}, {}
        ],
        ...this.generateTableData(data, 'chimneyMapper')
        // [
        //   { text: 'Construction Type:', bold: true, border: [true, false, false, true] },
        //   { text: 'Brick', border: [false, false, false, true] },
        //   { text: 'Roof Material:', bold: true, border: [true, false, false, true] },
        //   { text: 'Asphalt', border: [false, false, true, true] },
        //   { text: 'Number of Flues:', bold: true, border: [false, false, false, true] },
        //   { text: '3', border: [false, false, true, true] }
        // ],

      ]
    }
  }

  private flueTempTable(data) {
    return {
      margin: [0, 0, 0, 0],
      widths: ['*', 'auto', 'auto', '*', 'auto', 'auto'],

      body: [
        [
          { text: 'Flue-1', colSpan: 6, fillColor: '#073F80', color: '#FFFFFF', }, {}, {}, {}, {}, {}
        ],
        ...this.generateTableData(data, 'flueMapper')
        // [
        //   { text: 'Flue Size:', bold: true, border: [true, false, false, true] },
        //   { text: 'Brick', border: [false, false, false, true], colSpan: 2, style: 'rightPadding' },
        //   {},
        //   { text: 'Flue Material:', bold: true, border: [true, false, false, true] },
        //   { text: 'Asphalt', border: [false, false, true, true], colSpan: 2, style: 'rightPadding' },
        //   {}
        // ],
        // [
        //   { text: 'Flue Length:', bold: true, border: [true, false, false, true] },
        //   { text: 'Interior', border: [false, false, false, true], colSpan: 2, style: 'rightPadding' },
        //   {},
        //   { text: 'Condition:', bold: true, border: [true, false, false, true] },
        //   { text: 'Right Center', border: [false, false, true, true], colSpan: 2, style: 'rightPadding' },
        //   {}
        // ],
      ]
    }
  }

  private masonryTempTable(data) {
    return {
      margin: [0, 0, 0, 0],
      widths: ['*', 'auto', '*', 'auto', '*', 'auto'],
      body: [
        [
          {
            text: 'Masonry Fireplace - 1', colSpan: 6, fillColor: '#073F80', color: '#FFFFFF',
          }, {}, {}, {}, {}, {}
        ],
        [
          { text: 'Smoke Chamber Material:', bold: true, border: [true, false, false, true] },
          { text: 'Brick', border: [false, false, false, true], style: 'rightPadding' },
          { text: 'Smoke Chamber:', bold: true, border: [true, false, false, true] },
          { text: 'Unsatisfactory', border: [false, false, false, true], style: 'rightPadding' },
          { text: 'FireBox Material:', bold: true, border: [true, false, false, true] },
          { text: 'Masonry', border: [false, false, true, true], style: 'rightPadding' },
        ],
        [
          { text: 'Location:', bold: true, border: [true, false, false, true] },
          { text: 'Interior', border: [false, false, false, true] },
          { text: 'Fuel Type:', bold: true, border: [true, false, false, true] },
          { text: 'Asphalt', border: [false, false, true, true] },
          { text: 'Damper:', bold: true, border: [false, false, false, true] },
          { text: 'Right Center', border: [false, false, true, true] }
        ],
        [
          {
            border: [true, false, false, true],
            stack: [
              { text: 'FireBox:', bold: true },
              { text: 'Width:', bold: true, margin: [0, 10, 0, 0] },
              { text: 'Height:', bold: true, margin: [0, 10, 0, 0] },
              { text: 'Depth:', bold: true, margin: [0, 10, 0, 0] },
            ]
          },
          {
            border: [false, false, false, true],
            stack: [
              { text: 'Satisfactory' },
              { text: '36”', bold: true, margin: [0, 10, 0, 0] },
              { text: '29”', bold: true, margin: [0, 10, 0, 0] },
              { text: '24”', bold: true, margin: [0, 10, 0, 0] },
            ]
          },

          {
            border: [true, false, false, true],
            stack: [
              { text: 'Hearth:', bold: true },
              { text: 'Depth:', bold: true, margin: [0, 10, 0, 0] },
              { text: 'Right Side:', bold: true, margin: [0, 10, 0, 0] },
              { text: 'Left Side:', bold: true, margin: [0, 10, 0, 0] },
            ]
          },
          {
            border: [false, false, false, true],
            stack: [
              { text: 'Satisfactory' },
              { text: '20”', bold: true, margin: [0, 10, 0, 0] },
              { text: '12”', bold: true, margin: [0, 10, 0, 0] },
              { text: '12”', bold: true, margin: [0, 10, 0, 0] },
            ]
          },

          {
            border: [true, false, false, true],
            stack: [
              { text: 'Surround Clearances:', bold: true },
              { text: 'Left Side:', bold: true, margin: [0, 10, 0, 0] },
              { text: 'Right Side:', bold: true, margin: [0, 10, 0, 0] },
              { text: 'Above:', bold: true, margin: [0, 10, 0, 0] },
            ]
          },
          {
            border: [false, false, true, true],
            stack: [
              { text: 'Unsatisfactory' },
              { text: '8”', bold: true, margin: [0, 10, 0, 0] },
              { text: '8”', bold: true, margin: [0, 10, 0, 0] },
              { text: '15”', bold: true, margin: [0, 10, 0, 0] },
            ]
          }
        ],
      ]
    }
  }

  private get addCommentPage(): Observable<this> {
    const subject = new BehaviorSubject(null);

    this.getImage('pic_1.png')
      .subscribe(image => {
        this.docDefinition = [
          {
            table: {
              widths: ['*', '*'],

              body: [
                [
                  { text: 'Chimney-1', fillColor: '#073F80', color: '#FFFFFF', },
                  { text: 'Comments and Recommendations', alignment: 'right', fillColor: '#073F80', color: '#FFFFFF', },
                ],
                [
                  { text: 'Stucco Comments:', bold: true, },
                  { text: 'Stucco Recommendations', bold: true, }
                ],
                [
                  { text: 'Stucco is deteriorating/falling off', },
                  { text: 'Wire, brown coat and stucco finish', }
                ],
                [
                  { text: 'No wire', },
                  { text: '', }
                ],
                [
                  { text: 'Height Comments:', bold: true, },
                  { text: 'Height Recommendations', bold: true, }
                ],
                [
                  // tslint:disable-next-line:max-line-length
                  {
                    text: 'The chimney is not tall enough.  The top of the chimney must be 2 feet higher than anything within 10 feet and must be a minimum of 3 feet tall',

                  },
                  {
                    text: 'Extend chimney with brick, block, and/or terra cotta to meet code requirements',
                  }
                ]
              ]
            },
            style: 'table',
            layout: {
              defaultBorder: true,
              vLineColor: '#cecece',
              hLineColor: '#cecece',
            },
          },
          {
            margin: [0, 20, 0, 0],
            alignment: 'center',
            columns: [
              {
                image
              },
              {
                image
              },
              {
                image
              },
              {
                image
              }
            ]
          }
        ];

        subject.next(this);
      })

    return subject.skipWhile(that$ => that$ === null);
  }

  private get addStyles() {
    const styles = {
      styles: {
        footer: {
          fontSize: 11,
        },
        header: {
          fontSize: 18,
          bold: true,
          alignment: 'center',
          margin: [0, 30, 0, 80]
        },
        pageHeader: {
          fontSize: 11,
        },
        table: {
          fontSize: 9
        },
        rightPadding: {
          margin: [0, 0, 10, 0]
        }
      }
    };

    return Object.assign(this, styles);
  }

  private get placeHolderImage() {
    return this.getImage('pic_1.png');
  }

  private get logo() {
    return this.getImage('wellsson-logo.png')
  }

  private getImage(url) {
    const subject = new BehaviorSubject(null);

    this.http.get(`/assets/${url}`)
      .subscribe(image =>
        this.toDataURL(image.url)
          .then(data => {
            subject.next(data)
          })
          .catch(error => subject.error(error))

      );

    return subject.skipWhile(data => data === null);

  }

  private toDataURL(url) {
    return fetch(url)
      .then(response => response.blob())
      .then(blob => new Promise((resolve, reject) => {
        const reader = new FileReader()
        reader.onloadend = () => resolve(reader.result)
        reader.onerror = reject
        reader.readAsDataURL(blob)
      }))
  }
}
