import { TestBed, inject } from '@angular/core/testing';

import { OfflineStorageService } from './offline-storage.service';

describe('AddToOfflineStorageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OfflineStorageService]
    });
  });

  it('should be created', inject([OfflineStorageService], (service: OfflineStorageService) => {
    expect(service).toBeTruthy();
  }));
});
