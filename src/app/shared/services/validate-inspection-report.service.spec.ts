import { TestBed, inject } from '@angular/core/testing';

import { ValidateInspectionReportService } from './validate-inspection-report.service';

describe('ValidateInspectionReportService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ValidateInspectionReportService]
    });
  });

  it('should be created', inject([ValidateInspectionReportService], (service: ValidateInspectionReportService) => {
    expect(service).toBeTruthy();
  }));
});
