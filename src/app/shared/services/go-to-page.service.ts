import { Injectable } from '@angular/core';
import { App } from 'ionic-angular';

@Injectable()
export class GoToPageService {

  constructor(
    protected app: App
  ) { }

  /**
   * Goto page/component
   * 
   * @private
   * @param {any} component
   *
   * @memberOf SideMenuComponent
   */
  public execute(component, opts?) {
   const  navCtrl = this.app.getRootNav()
    navCtrl.setRoot(component, opts, { animate: true, direction: 'forward' });
  }
}
