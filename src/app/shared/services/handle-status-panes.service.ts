import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { PaneEvent } from '../interfaces/pane-event'
@Injectable()
export class HandleStatusPanesService {

  constructor() { }

  updateStatus(obj: PaneEvent) {
    const result$ = new BehaviorSubject(undefined)
    if (obj.progress === undefined) {
      result$.next(obj.status)
    } else if (obj.downloadLink) {
      result$.next({
        downloadLink: obj.downloadLink,
        name: obj.name,
        id: obj.id,
        uuid: obj.uuid,
        index: obj.index,
        fileName: obj.fileName,
        value: obj.value,
      })
    }

    return result$
  }
}
