import { TestBed, inject } from '@angular/core/testing';

import { PresentToastService } from './present-toast.service';

describe('PresentToastService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PresentToastService]
    });
  });

  it('should ...', inject([PresentToastService], (service: PresentToastService) => {
    expect(service).toBeTruthy();
  }));
});
