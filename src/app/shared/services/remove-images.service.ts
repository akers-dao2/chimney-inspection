import { Injectable } from '@angular/core';
import { StateManagerService } from 'app/shared/services/sassy-state-manager-ng2/src/state-manager.service';
import { from } from 'rxjs/observable/from';
import { filter, skipWhile, mergeMap, switchAll } from 'rxjs/operators';
import { DataService } from './data.service';
import { IFirebaseStorageObject } from '../interfaces/firebase-storage-object';
import * as moment from 'moment';

@Injectable()
export class RemoveImagesService {

  constructor(
    private stateManager: StateManagerService,
    private dataService: DataService
  ) { }

  public execute() {
    const downLoadLinks$ = this.stateManager.getModel('downloadLinks');
    return from(downLoadLinks$)
      .pipe(
        this.skipUndefined(),
        switchAll(),
        mergeMap(this.dataService.getFileMetaDataFromFirebaseStorage),
        filter(this.filterDownloadLinks),
        mergeMap(this.dataService.deleteFileFromFirebaseStorage)
      )
  }


  private filterDownloadLinks = (linkMetaData: IFirebaseStorageObject) => {
    const deletionDate = moment().subtract(14, 'days');

    if (linkMetaData === null) {
      return false
    }

    return moment(linkMetaData.timeCreated).isSameOrBefore(deletionDate)
  }

  private skipUndefined = () => {
    return skipWhile<any>(param => param === undefined || param.length === 0)
  }

}
