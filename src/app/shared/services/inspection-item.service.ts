import { Injectable } from '@angular/core';
import { AlertController } from 'ionic-angular';

/** Services */
import { ActionService } from '../../shared/services/action.service';
import { ChimneyNamesService } from '../../shared/services/chimney-names.service';
import { StateManagerService } from 'app/shared/services/sassy-state-manager-ng2/src/state-manager.service';

/** Interfaces */
import { IInspectionItem } from '../../shared/interfaces/inspection-item';
import { IInspection } from '../../shared/interfaces/inspection';
import { IAlertOptionInput } from '../../shared/interfaces/alert-option-input';

/** Enums */
import { ApplianceTypes } from '../../shared/enums/appliances';

@Injectable()
export class InspectionItemService {

  constructor(
    private actionService: ActionService,
    private stateManager: StateManagerService,
    private alertCtrl: AlertController,
    private chimneyNames: ChimneyNamesService
  ) { }

  /**
   * Add Inspection Item To List
   * 
   * @private
   * @param {string} inspectionItem 
   * 
   * @memberOf InspectionItemService
   */
  public addToList(inspectionItem: string) {

    this.stateManager.getModel('inspection')
      .take(1)
      .subscribe((chimneys: IInspection) => {

        // number of chimneys on the inspection report
        const countOfChimneys = this.chimneyNames.get(chimneys).length;

        // retrieve the flues from each chimney
        const listOfflues: IInspectionItem[] = this.chimneyNames.get(chimneys)
          .filter(key => {
            if (chimneys[key].inspectionItems === undefined) {
              return false;
            }

            return chimneys[key].inspectionItems
              .some(appliance =>
                appliance.type === ApplianceTypes['Flue/Vent']
              );
          })
          .reduce((_flues_: IInspectionItem[], key) => {
            return Array.of(..._flues_, ...chimneys[key].inspectionItems
              .filter(appliance =>
                appliance.type === ApplianceTypes['Flue/Vent']));
          }, []);

        // the set the name to use the alert box 
        let titleParam;

        // if there is more than one chimney or flue display
        // alert asking which chimney or flue to add the appliance to
        if ((countOfChimneys > 1 || listOfflues.length > 1) && inspectionItem !== 'Chimney') {
          let inputs: IAlertOptionInput[];

          if (listOfflues.length > 1 && inspectionItem !== 'Flue/Vent') {
            inputs = this.getFlueAlertInputs(listOfflues);
            titleParam = 'flue';
          } else {
            inputs = this.getChimneyAlertInputs(chimneys, inspectionItem);
            titleParam = 'chimney'
          }

          // if there is only one chimney or flue just 
          // add inspection item to the list without 
          // displaying alert modal
          if (inputs.length === 1) {
            this.actionService.addInspectionItemToList(inspectionItem, <number>inputs[0].value);
          } else {
            this.selectParentItem(inputs, titleParam, inspectionItem);
          }

          // add chimney and flue automatically if they do not exist
        } else if (inspectionItem !== 'Chimney' && (countOfChimneys < 1 || listOfflues.length < 1)) {
          this.actionService.addChimneyAndFlue(inspectionItem);
        } else {
          this.actionService.addInspectionItemToList(inspectionItem);
        }

      });
  }

  /**
   * selectParentItem
   * 
   * @param {{}[]} inputs 
   * @param {string} titleParam 
   * @param {string} inspectionItem 
   * 
   * @memberOf InspectionItemService
   */
  private selectParentItem(inputs: {}[], titleParam: string, inspectionItem: string) {
    const alert = this.alertCtrl.create({

      title: `Choose a ${titleParam}`,
      message: `There are several ${titleParam}s, choose one:`,
      inputs,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Add',
          handler: (value) => {
            if (value === undefined) {
              return false;
            }
            if (Object(value) === value) {
              const { name, parentIndex, index } = value as IInspectionItem;
              this.actionService.addInspectionItemToList(inspectionItem, parentIndex, index);
            } else {
              this.actionService.addInspectionItemToList(inspectionItem, parseInt(value, 10));
            }
          }
        }
      ]
    });
    alert.present();
  }

  /**
   * The alert Inputs to use when a multiple flues exist
   * 
   * @private
   * @param {IInspectionItem[]} listOfflues 
   * @returns 
   * 
   * @memberof InspectionItemService
   */
  private getFlueAlertInputs(listOfflues: IInspectionItem[]) {
    return listOfflues
      .map((flue, index) => {
        return {
          name: 'flue',
          value: flue,
          label: `Flue/Vent-${flue.parentIndex + 1}.${flue.locationIndex}`,
          type: 'radio'
        };
      });
  }

  /**
   * The alert Inputs to use when a multiple chimneys exist 
   * 
   * @private
   * @param {IInspection} chimneys 
   * @param {string} inspectionItem 
   * @returns 
   * 
   * @memberof InspectionItemService
   */
  private getChimneyAlertInputs(chimneys: IInspection, inspectionItem: string) {
    return this.chimneyNames.get(chimneys)
      .filter(key => {
        if (inspectionItem !== 'Flue/Vent') {
          if (chimneys[key].inspectionItems === undefined) {
            return false;
          }

          return chimneys[key].inspectionItems
            .some(appliance =>
              appliance.type === ApplianceTypes['Flue/Vent']);
        }
        return true;
      })
      .map((key, index) => {
        return {
          name: 'chimney',
          value: index,
          label: chimneys[key].name,
          type: 'radio'
        };
      });
  }
}
