import { TestBed, inject } from '@angular/core/testing';

import { PdfMakerService } from './pdf-maker.service';

describe('PdfMakerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PdfMakerService]
    });
  });

  it('should ...', inject([PdfMakerService], (service: PdfMakerService) => {
    expect(service).toBeTruthy();
  }));
});
