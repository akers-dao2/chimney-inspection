import { Injectable } from '@angular/core';

import { IInspection } from '../interfaces/inspection';
import { IAppliance } from '../interfaces/appliance';
import { IReport } from '../interfaces/report';

import { ApplianceTypes } from '../enums/appliances';
import { InspectionStatus } from '../enums/inspection-status';

import {
  Chimney,
  FlueVent,
  Fireplace,
  Stoves,
  HeatingAppliances,
  DryerVent
} from '../classes';

import { UtilityService } from '../services/utility.service';

@Injectable()
export class ValidateInspectionReportService {

  constructor(
    private utility: UtilityService
  ) { }

  /**
   * Verify inspections reports are complete
   * 
   * @param {{ [id: number]: {}, inspection: IInspection }} reports 
   * @memberof ValidateInspectionReportService
   */
  public verify(reports: { [id: number]: {}, inspection: IInspection }) {
    const inspectionItems = this.getListOfInspectionItems(reports.inspection);
    const inspectionReports = this.getListOfInspectionReports(reports);
    const missingItems = this.getListOfInspectionItemsBaseOnPresent(inspectionItems, inspectionReports, false);
    const getListOfIncompletedReports = this.getListOfIncompletedReports(inspectionReports);

    // console.log(inspectionReports)
    // console.log(inspectionItems)
    // console.log(missingItems)
    // console.log(getListOfIncompletedReports)

    return Array.of<any>(...missingItems, ...getListOfIncompletedReports);
  }

  /**
   * getListOfInspectionItems
   * 
   * @private
   * @param {IInspection} inspection 
   * @returns {IAppliance[]}
   * @memberof ValidateInspectionReportService
   */
  private getListOfInspectionItems(inspection: IInspection): IAppliance[] {
    const chimneys = Object.keys(inspection)
      .filter(key => key.toLowerCase().includes('chimney-'));

    return chimneys.reduce((items, key) => {
      const chimney = inspection[key];
      const { name, uuid, inspectionItems } = chimney;
      return Array.of(...items, { name, uuid }, ...inspectionItems);
    }, []);
  }

  /**
   * getListOfInspectionReports
   * 
   * @private
   * @param {{ [id: number]: {}, inspection: IInspection }} reports 
   * @returns 
   * @memberof ValidateInspectionReportService
   */
  private getListOfInspectionReports(reports: { [id: number]: {}, inspection: IInspection }): IReport[] {
    const reportsKeys = Object.keys(reports)
      .filter(key => key !== 'inspection');

    return reportsKeys.reduce((items, key) => {
      const report = reports[key];
      return Array.of(...items, report);
    }, []);
  }

  private getListOfInspectionItemsBaseOnPresent(inspectionItems: IAppliance[], inspectionReports: IReport[], isPresent: boolean) {
    return inspectionItems
      .filter(item => item !== undefined)
      .filter(item =>
        inspectionReports.some(report => report.uuid === item.uuid) === isPresent);
  }

  private getListOfIncompletedReports(inspectionReports: IReport[]) {
    return inspectionReports.filter(report => {
      let isNotValid = false;
      switch (ApplianceTypes[report.applianceType]) {
        case 'Flue/Vent':
          isNotValid = (<FlueVent>report).material === 'No Flue Vent' ? false : !this.isReportValid(report, FlueVent, (r, k) => {
            return true;
          });
          break;
        case 'Dryer Vent':
          isNotValid = !this.isReportValid(report, DryerVent);
          break;
        case 'Masonry Fireplace':
        case 'Prefab Fireplace':
          isNotValid = !this.isReportValid(report, Fireplace);
          break;
        case 'Stoves':
          isNotValid = !this.isReportValid(report, Stoves);
          break;
        case 'Heating Appliances':
          isNotValid = !this.isReportValid(report, HeatingAppliances);
          break;
        default:
          isNotValid = (<Chimney>report).constructionType === 'No Chimney' ? false : !this.isReportValid(report, Chimney);
          break;
      }

      return isNotValid;
    });
  }

  private isReportValid(record, className, callback: (v: any, k: string) => boolean = null) {
    let instance = Object.assign(new className(), record);
    instance = this.addMissingProperties(instance, instance.missingProperties, className);
    instance = this.addPicturePropIfNonExist(instance, className);

    return this.isValid(instance, !(record.uuid === '0'), callback);
  }

  /**
   * Verifies all data is popluated
   * 
   * @readonly
   * 
   * @memberof Client
   */
  private isValid(report, notFirstChimney: boolean, callback: (v: any, k: string) => boolean = null) {
    return Object.keys(report)
      .filter(key => !this.fieldsSkipValidate(report).includes(key))
      .filter(key => callback === null ? true : !callback(report, key))
      .every(key => {
        if (key === 'requiredPics') {
          const pictures = report[key].pictures;
          const hasChimneyRequirePictures = pictures.reduce(
            (requirePictures, picture) => {
              return requirePictures.includes(picture.name) ? requirePictures : Array.of(...requirePictures, picture.name);
            }, [])
            .length > 1;

          return notFirstChimney ? pictures.length > 0 : hasChimneyRequirePictures;
        }

        if (this.isObject(report[key])) {
          if (report[key].hasOwnProperty('status')) {
            if (report[key].status >= 2) {
              return true;
            }
          }
          return this.isValid(report[key], notFirstChimney);
        }

        if (Array.isArray(report[key])) {
          return report[key].length > 0;
        }

        return this.isPopulated(report[key]);
      });
  }

  /**
   * A list of fields on the Client class to skip validate
   * 
   * @readonly
   * @private
   * 
   * @memberof Client
   */
  private fieldsSkipValidate(report) {
    const status = report.status;
    const fieldsToSkip = InspectionStatus[status] !== 'failed' ? [
      'additionalComments',
      'comments',
      'pictures',
      'recommendations'
    ] : [
        'additionalComments'
      ];
    return fieldsToSkip;
  }

  /**
   * isObject
   * 
   * @private
   * @param {any} value 
   * @returns 
   * @memberof ValidateInspectionReportService
   */
  private isObject(value) {
    if (typeof value === 'object' && value !== null) {
      return !Array.isArray(value);
    }

    return false;
  }

  /**
   * isPopulated
   * 
   * @private
   * @param {any} value 
   * @returns 
   * @memberof ValidateInspectionReportService
   */
  private isPopulated(value) {
    return value !== '' && value !== undefined && value !== null && value !== -1;
  }

  /**
   * addPicturePropIfNonExist
   * 
   * @private
   * @param {any} instance 
   * @returns 
   * @memberof ValidateInspectionReportService
   */
  private addPicturePropIfNonExist(instance, className) {
    const newInstance = this.utility.clone(instance);

    for (const key in newInstance) {
      // property tied to main object not prototype
      if (newInstance.hasOwnProperty(key)) {
        // verify the property is an object
        if (this.isObject(newInstance[key])) {
          // if no picture property exist add it
          if (!newInstance[key].hasOwnProperty('pictures')) {
            newInstance[key] = Object.assign({}, newInstance[key], { pictures: [] });
          }
        }
      }
    }

    return Object.assign(new className(), newInstance);
  }

  private addMissingProperties(instance, props, className) {
    // const newInstance = this.utility.clone(instance);

    for (const key in props) {
      // property tied to main object not prototype
      if (props.hasOwnProperty(key)) {
        instance[key] = Object.assign({}, props[key], instance[key]);
      }
    }

    return Object.assign(new className(), instance);
  }

}
