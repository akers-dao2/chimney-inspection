import { Injectable } from '@angular/core';

@Injectable()
export class FractionsService {

  constructor() { }

  get get() {
    return [
      {
        name: '1/16',
        value: 1 / 16
      },
      {
        name: '1/8',
        value: 1 / 8
      },
      {
        name: '3/16',
        value: 3 / 16
      },
      {
        name: '1/4',
        value: 1 / 4
      },
      {
        name: '5/16',
        value: 5 / 16
      },
      {
        name: '3/8',
        value: 3 / 8
      },
      {
        name: '7/16',
        value: 7 / 16
      },
      {
        name: '1/2',
        value: 1 / 2
      },
      {
        name: '9/16',
        value: 9 / 16
      },
      {
        name: '5/8',
        value: 5 / 8
      },
      {
        name: '11/16',
        value: 11 / 16
      },
      {
        name: '3/4',
        value: 3 / 4
      },
      {
        name: '13/16',
        value: 13 / 16
      },
      {
        name: '7/8',
        value: 7 / 8
      },
      {
        name: '15/16',
        value: 15 / 16
      },
      {
        name: '0',
        value: 0
      },
    ]

  }

}
