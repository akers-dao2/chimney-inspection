import { Injectable } from '@angular/core';

/** Enums */
import { ApplianceTypes } from '../enums/appliances';

import { IInspection } from 'app/shared/interfaces/inspection'

@Injectable()
export class InspectionItemNameService {

  constructor() { }

  public get(inspectionList: IInspection, inspectionItemName: string, index: number, flueIndex?: number) {
    const numberOfExistingInspectionItem = this.getCountOfItems(inspectionList, inspectionItemName, index, flueIndex);

    return `${inspectionItemName}-${numberOfExistingInspectionItem + 1}`
  }

  public getCountOfItems(inspectionList: IInspection, inspectionItemName: string, index: number, flueIndex?: number) {
    let endIndex;
    const inspectionItems = inspectionList[`Chimney-${index}`].inspectionItems;
    const totalNumberInspectionItems = inspectionItems.length;

    if (flueIndex !== undefined) {
      endIndex = inspectionItems
        .slice(flueIndex + 1)
        .findIndex(appliance => appliance.type === ApplianceTypes['Flue/Vent']);

      endIndex = endIndex === -1 ? totalNumberInspectionItems : endIndex + 1;
    }

    return inspectionItems
      .slice(flueIndex, endIndex)
      .filter(appliance => appliance.type === ApplianceTypes[inspectionItemName])
      .length;
  }

}
