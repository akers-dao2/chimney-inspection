import { TestBed, inject, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';

import { DataService } from './data.service';
import { UtilityService } from './utility.service';
import { FirebaseService } from './firebase.service';

import { Job } from '../classes/job';
import { Client } from '../classes/client';
import { User } from '../classes/user';
import { Status } from '../enums/status';

xdescribe('DataService', () => {
  let utility: UtilityService;
  let dataService: DataService;
  let firebase: FirebaseService;
  let onceResp: any;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        DataService,
        UtilityService,
        FirebaseService
      ]
    });
  });

  beforeEach(inject(
    [
      UtilityService,
      DataService,
      FirebaseService
    ],
    (
      _utility_: UtilityService,
      _dataService_: DataService,
      _firebase_: FirebaseService
    ) => {
      utility = _utility_;
      dataService = _dataService_;
      firebase = _firebase_;

      spyOn(utility, 'toArray').and.callFake((results) => results);

      spyOn(firebase.get, 'database').and.callFake(() => {
        return {
          ref: (path) => {
            return {
              on: (value, func) => func.call(null, { val: () => [path, 'on'] }),
              once: (value, func) => func.call(null, { val: () => [path, 'once'].concat(onceResp) }),
              update: (updates) => Promise.resolve(updates),
              child: (childPath) => {
                return {
                  push: () => {
                    return { key: 'key-1' };
                  }
                };
              },
              remove: () => Promise.resolve(path)
            };
          }
        };
      });

      spyOn(firebase.get, 'auth').and.callFake(() => {
        return {
          signInWithEmailAndPassword: (email, password) => {
            return Promise.resolve({ email, password });
          },
          signOut: () => {
            return Promise.resolve('signOut');
          },
          createUserWithEmailAndPassword: () => {
            return Promise.resolve('signOut');
          },
          currentUser: {
            delete: () => Promise.resolve()
          }
        };

      });

    }));

  it('#getJobs', () => {
    dataService.getJobs()
      .subscribe((results: any) => {
        expect(results).toEqual(jasmine.arrayContaining(['jobs', 'on']));
      });
  });

  it('#getjob', () => {
    dataService.getjob('1')
      .subscribe((results: any) => {
        expect(results).toEqual(jasmine.arrayContaining(['jobs/1', 'once']));
      });
  });

  it('#getCrews', () => {
    dataService.getCrews()
      .subscribe((results: any) => {
        expect(results).toEqual(jasmine.arrayContaining(['crews', 'once']));
      });
  });

  it('#getUsers', () => {
    onceResp = { emailAddress: 'emailAddress' };
    dataService.getUserInfo('emailAddress')
      .subscribe((results: any) => {
        expect(results).toEqual(Object({ emailAddress: 'emailAddress' }));
      });
  });

  it('#getClients', () => {
    dataService.getClients()
      .subscribe((results: any) => {
        expect(results).toEqual(jasmine.arrayContaining(['clients', 'once']));
      });
  });

  it('#getClient', () => {
    dataService.getClient('1')
      .subscribe((results: any) => {
        expect(results).toEqual(jasmine.arrayContaining(['clients/1', 'once']));
      });
  });

  it('#addJob', () => {
    const job = new Job('', '', Status.open, 1, false, false, 1)
    dataService.addJob(job)
      .subscribe((results: any) => {
        expect(results).toEqual(Object({ 'jobs/key-1': job }));
      });
  });

  it('#addCrew', () => {
    dataService.addCrew({ name: 'crew' })
      .subscribe((results: any) => {
        expect(results).toEqual(Object({ 'crews/key-1': Object({ name: 'crew' }) }));
      });
  });

  it('#addClient', () => {
    const client = {} as any;
    dataService.addClient(client)
      .subscribe((results: any) => {
        expect(results).toEqual(Object({ 'clients/key-1': client }));
      });
  });

  it('#deleteUser', (done) => {
    const client = {};
    dataService.deleteUser('keyUser')
      .subscribe((results: any) => {
        expect(results).toEqual('users/keyUser');
        done();
      });
  });

  it('#deleteJob', () => {
    const client = {};
    dataService.deleteJob('keyJob')
      .subscribe((results: any) => {
        expect(results).toEqual('jobs/keyJob');
      });
  });

  it('#deleteCrew', () => {
    const client = {};
    dataService.deleteCrew('keyCrew')
      .subscribe((results: any) => {
        expect(results).toEqual('crews/keyCrew');
      });
  });

  it('#deleteClient', () => {
    const client = {};
    dataService.deleteClient('keyClient')
      .subscribe((results: any) => {
        expect(results).toEqual('clients/keyClient');
      });
  });

  it('#signIn', () => {
    dataService.signIn('email', 'password')
      .skipWhile(results => results === undefined)
      .subscribe((results: any) => {
        expect(results).toEqual(Object({ email: 'email', password: 'password' }));
      });
  });

  it('#signOut', () => {
    dataService.signOut()
      .skipWhile(results => results === undefined)
      .subscribe((results: any) => {
        expect(results).toEqual('signOut');
      });
  });

  it('#addUser', async(() => {
    const user = new User();
    dataService.addUser(user)
      .then((results: Observable<any>) => {
        results.subscribe(result => {
          expect(result).toEqual(Object({ 'users/key-1': user }));
        });
      });
  }));

});
