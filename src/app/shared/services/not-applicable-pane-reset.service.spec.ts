import { TestBed, inject } from '@angular/core/testing';

import { NotApplicablePaneResetService } from './not-applicable-pane-reset.service';

describe('NotApplicablePaneResetService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NotApplicablePaneResetService]
    });
  });

  it('should be created', inject([NotApplicablePaneResetService], (service: NotApplicablePaneResetService) => {
    expect(service).toBeTruthy();
  }));
});
