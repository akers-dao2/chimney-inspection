import { Injectable } from '@angular/core';

import * as firebase from 'firebase';

@Injectable()
export class FirebaseService {

  constructor() { }

  /**
   * 
   * 
   * @returns 
   * 
   * @memberOf FirebaseService
   */
  public get get() {
    return firebase;
  }

  /**
   * initialize firebase app
   *
   *
   * @memberOf FirebaseService
   */
  public initialize() {
    // Initialize Firebase
    const config = {
      apiKey: '',
      authDomain: '',
      databaseURL: '',
      projectId: '',
      storageBucket: '',
      messagingSenderId: '',
      appId: ''
    };
    firebase.initializeApp(config);
  }

  /**
   * Provides the firebase version
   *
   * @readonly
   *
   * @memberOf FirebaseService
   */
  public get version() {
    return firebase.SDK_VERSION;
  }

}
