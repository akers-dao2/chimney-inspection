import { TestBed, inject } from '@angular/core/testing';

import { GetPicturePropService } from './get-picture-prop.service';

describe('GetPicturePropService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetPicturePropService]
    });
  });

  it('should be created', inject([GetPicturePropService], (service: GetPicturePropService) => {
    expect(service).toBeTruthy();
  }));
});
