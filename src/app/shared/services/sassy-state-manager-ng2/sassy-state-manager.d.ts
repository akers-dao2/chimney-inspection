import './src/state-manager.service';
import { Observable } from 'rxjs/Observable';

export declare class StateManagerService {

  /**
   * Create a model
   * 
   * @param {string} name - unique name of the model
   * 
   * @memberOf StateManagerService
   */
  public createModel(name: string): void; 


  /**
   * Return a model as an Observable
   * 
   * @param {string} name - the name of the model
   * @returns {Observable<any>}
   * 
   * @memberOf StateManagerService
   */
  public getModel(name: string): Observable<any>;


  /**
   * Returns true or false if the model exists in the state manager
   * 
   * @param {string} modelName - the name of the model
   * @returns {boolean}
   * 
   * @memberOf StateManagerService
   */
  public modelExists(modelName: string): boolean;


  /**
   * Set the value of the model regardless of previous model values
   * 
   * @param {string} name
   * @param {(Array<any>|Object)} collection
   * 
   * @memberOf StateManagerService
   */
  public setModel(name: string, collection: Array<any>|Object): void;


  /**
   * Return the update method for the specified model
   * 
   * @param {string} name - the model name
   * @returns {Function}
   * 
   * @memberOf StateManagerService
   */
  public update(name: string): Function;

}