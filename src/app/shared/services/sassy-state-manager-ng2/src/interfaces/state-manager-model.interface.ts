import { ReplaySubject, Subject, Observable } from 'rxjs';


/**
 * The model interface for state managers
 * 
 * @export
 * @interface IStateManagerModel
 */
export interface IStateManagerModel {
  model$: ReplaySubject<any>;
  updates$: ReplaySubject<any> | Subject<any>;
  name: string;
  update: Function;
  setModel: Function;
}
