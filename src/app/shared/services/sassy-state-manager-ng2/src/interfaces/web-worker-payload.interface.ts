/**
 * WebWorker payload interface
 * 
 * @export
 * @interface IWebWorkerPayload
 */
export interface IWebWorkerPayload {
  stateManagerCall: boolean;
  actionServiceCall: boolean;
  method: string;
  args: Array<any>;
}
