import { Injectable, Optional } from '@angular/core';
import { ReplaySubject, Subject, Observable } from 'rxjs/Rx';
import { StateManager } from './classes/stateManager';
import { StateManagerWithWebWorker } from './classes/stateManagerWithWebWorker';


/**
 * State Manager Service 
 * 
 * @export
 * @class StateManagerService
 * @extends {StateManager}
 */
@Injectable()
export class StateManagerService extends StateManager {
  constructor() {
    super();
  }
}


/**
 * NOT READY FOR USE, FUTURE ENHANCEMENT
 * State Manager With a Web Worker (aka Provider)
 * 
 * @export
 * @class StateManagerWithWebWorkerService
 * @extends {StateManagerWithWebWorker}
 */
@Injectable()
export class StateManagerWithWebWorkerService extends StateManagerWithWebWorker {
  constructor(protected WorkerInstance: Worker) {
    super(WorkerInstance);
  }
}
