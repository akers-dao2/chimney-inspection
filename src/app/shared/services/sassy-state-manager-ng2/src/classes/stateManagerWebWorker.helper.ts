import { ReplaySubject } from 'rxjs/ReplaySubject';
import { StateManager } from './stateManager';

interface IErrorType {
  typeId: string;
  typeName: string;
  typeMsg: string;
}

let test: IErrorType = {
  typeId: '1', typeName: '', typeMsg: ''
};

/**
 * WebWorker StateManager Helper
 * 
 * @export
 * @class WebWorkerStateManager
 */
export class WebWorkerStateManager {

  /** Private Members */
  private stateManager$: ReplaySubject<any> = new ReplaySubject<any>(1);
  private actions$: ReplaySubject<any> = new ReplaySubject<any>(1);


  /**
   * Creates an instance of WebWorkerStateManager
   * 
   * @param {StateManagerService} StateManager
   * @param {Object} ActionsService
   * @param {Worker} any
   * 
   * @memberOf WebWorkerStateManager
   */
  constructor(private StateManager: StateManager, private ActionsService: Object, private WebWorker: any) {
    this.stateManager$.subscribe(this.stateManagerSubscriber);
    this.actions$.subscribe(this.actionsSubscriber);
    this.WebWorker.onmessage = this.onMessageHandler();
  }


  /**
   * StateManager subscriber/handler 
   * 
   * @private
   * @returns
   * 
   * @memberOf WebWorkerStateManager
   */
  private stateManagerSubscriber() {
    return (data) => {
      this.StateManager[data.method].apply(this.StateManager, data.args);
      if (data.method === 'createModel') {
        this.StateManager
          .getModel(data.args[0])
          .subscribe(updatedModel => {
            this.WebWorker.postMessage(JSON.stringify({
              modelName: data.args[0],
              data: updatedModel
            }));
          });

        // prime the new model
        this.StateManager.update(data.args[0])(state => state);
      }
    };
  }


  /**
   * ActionService subscriber/handler
   * 
   * @private
   * @returns
   * 
   * @memberOf WebWorkerStateManager
   */
  private actionsSubscriber() {
    return (data) => {
      this.ActionsService[data.method].apply(StateManager, data.args);
    };
  }


  /**
   * WebWorker message handler
   * 
   * @private
   * @returns {*}
   * 
   * @memberOf WebWorkerStateManager
   */
  private onMessageHandler(): any {
    return event => {
      let payload = JSON.parse(event.data);

      if (payload.stateManagerCall) {
        this.stateManager$
          .next({
            method: payload.method,
            args: payload.args
          });
        }

        if (payload.actionServiceCall) {
          this.actions$
            .next({
              method: payload.method,
              args: payload.args
            });
        }

      };

  }

}