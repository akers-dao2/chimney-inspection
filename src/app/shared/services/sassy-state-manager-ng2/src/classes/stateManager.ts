import { ReplaySubject, Subject, Observable } from 'rxjs/Rx';

import { IStateManagerModel } from '../interfaces/state-manager-model.interface';
import { IWebWorkerPayload } from '../interfaces/web-worker-payload.interface';


/**
 * State Manager Class
 * 
 * @export
 * @class StateManager
 */
export class StateManager {

  /** Protected */
  protected webWorker$: ReplaySubject<any> = new ReplaySubject<any>(1);
  protected useWebWorker = false;
  protected WorkerInstance: Worker;

  /** Private */
  private models: Object = {};
  private wrapMethodCalled = false;
  private errorPreFix = '\n\nStateManagerService Error:\n';


  /**
   * Init the place holder for wrap actions handler
   * 
   * @type {Function}
   * @memberOf StateManagerService
   */
  public wrapActionsForWebWorker: Function;


  /**
   * Creates an instance of StateManagerService.
   * 
   * @param {Worker} WorkerInstance - the webworker instance
   * 
   * @memberOf StateManagerService
   */
  constructor() {
    // use the 'factory' to create the wrapActionsForWebWorker method
    this.wrapActionsForWebWorker = this.createWrapActionsHandler(this);

    // by default, don't use the web worker
    this.useWebWorker = false;
  }




  /************************************************************************************************************
   * 
   * PUBLIC METHODS
   * 
   ************************************************************************************************************/


  /**
   * Create/Register a model
   * 
   * @param {string} name
   * 
   * @memberOf StateManagerService
   */
  public createModel(name: string): void {

    if (this.models.hasOwnProperty(name)) {
     throw new Error(`${this.errorPreFix} Named model '${name}' already exists.\n\n`);
    }

    if (this.useWebWorker) {
      // create the model on the webworker
      this.WorkerInstance.postMessage(JSON.stringify(this.webWorkerPayload(true, false, 'createModel', arguments)));
    }

    // always create a local model so the app can get updates either way
    this.models[name] = this.modelFactory(name);
  }


  /**
   * Set the model with data
   * 
   * @param {string} name
   * @param {(Array<any>|Object)} collection
   * 
   * @memberOf StateManagerService
   */
  public setModel(name: string, collection: Array<any>|Object): void {
    if (!this.models.hasOwnProperty(name)) {
      throw new Error(`${this.errorPreFix} Named model '${name}' does not exist.\n\n`);
    }
    if (this.useWebWorker) {
      this.WorkerInstance.postMessage(JSON.stringify(this.webWorkerPayload(true, false, 'setModel', arguments)));
    }
    this.models[name].setModel(collection);
  }


  /**
   * Return the model based on name
   * 
   * @param {string} name
   * @returns {Observable<any>}
   * 
   * @memberOf StateManagerService
   */
  public getModel(name: string): Observable<any> {
    if (!this.models.hasOwnProperty(name)) {
      throw new Error(`${this.errorPreFix} Requested model '${name}' does not exist.\n\n`);
    }
    return this.models[name].model$.debounceTime(1).share();
  }


  /**
   * Return the update method for the named model
   * 
   * @param {string} name
   * @returns {Observable<any>}
   * 
   * @memberOf StateManagerService
   */
  public update(name: string): Function {
    this.checkSetWrapMethodCalledFlag();
    return this.models[name].update;
  }


  /**
   * Check is the specific model is registered
   * 
   * @param {string} modelName
   * @returns {boolean}
   * 
   * @memberOf StateManagerService
   */
  public modelExists(modelName: string): boolean {
    return this.models.hasOwnProperty(modelName);
  }


  /**
   * Returns the use WebWorker flag
   * 
   * @returns {boolean}
   * 
   * @memberOf StateManagerService
   */
  public isUsingWebWorker(): boolean {
    return this.useWebWorker;
  }


  /**
   * Send the action to the web worker
   * 
   * @param {string} actionName
   * @param {Array<any>} args
   * 
   * @memberOf StateManagerService
   */
  public postToWebWorker(actionName: string, args: Array<any>): void {
    this.checkSetWrapMethodCalledFlag();
    this.WorkerInstance.postMessage(JSON.stringify(this.webWorkerPayload(false, true, actionName, args)));
  }




  /************************************************************************************************************
   * 
   * PRIVATE METHODS
   * 
   ************************************************************************************************************/


  /**
   * Interface for the web worker call
   * 
   * @private
   * @param {boolean} isStateManagerCall
   * @param {boolean} isActionServiceCall
   * @param {string} method
   * @param {...Array<any>} args
   * @returns {IWebWorkerPayload}
   * 
   * @memberOf StateManagerService
   */
  private webWorkerPayload(stateManagerCall: boolean, actionServiceCall: boolean, method: string, ...args: Array<any>): IWebWorkerPayload {
    return { stateManagerCall, actionServiceCall, method, args };
  }


  /**
   * Return a model <IStateManagerModel>
   * 
   * @private
   * @param {string} name
   * @returns {IStateManagerModel}
   * 
   * @memberOf StateManagerService
   */
  private modelFactory(name: string): IStateManagerModel {

    const _updateSubject$ = (() => {
          if (this.useWebWorker) {
            return <ReplaySubject<any>>this.webWorker$
              .scan((acc, payload) => {
                return JSON.parse(payload);
              }, {})
              .filter((payload) => payload.modelName === name);
          } else {
            return new Subject<any>();
          }
        })(),
        _modelSubject$ = new ReplaySubject<any>(1);

    return {
      model$: _modelSubject$,
      updates$: _updateSubject$,
      name,
      update: this.buildUpdateAction(_updateSubject$),
      setModel: this.buildModelAssignment(_updateSubject$, _modelSubject$)
    };

  }


  /**
   * Build the update action for the model
   * 
   * @private
   * @param {(ReplaySubject<any> | Subject<any>)} updateSubject$
   * @returns Function
   * 
   * @memberOf StateManagerService
   */
  private buildUpdateAction(updateSubject$: ReplaySubject<any> | Subject<any>): Function {
    return (actionMethod: Function): void => updateSubject$.next(actionMethod);
  }


  /**
   * Returns a function to setup the Update and Model Subjects for subscription
   * 
   * @private
   * @param {(ReplaySubject<any> | Subject<any>)} updateSubject$
   * @param {ReplaySubject<any>} modelSubject$
   * @returns {Function}
   * 
   * @memberOf StateManagerService
   */
  private buildModelAssignment(updateSubject$: ReplaySubject<any> | Subject<any>, modelSubject$: ReplaySubject<any>): Function {
    return (seedCollection: Array<any> | Object): void => {
      if (this.useWebWorker) {
        // if we have a web worker, then the updates can go straight to the model
        updateSubject$.subscribe(response => modelSubject$.next(response.data));
      } else {
        // if we're running this locally, no web worker, then follow a standard pattern using an update subject
        updateSubject$
          .startWith(seedCollection)
          .scan((currentState, actionMethod) => Object.freeze(actionMethod(currentState)))
          .subscribe(modelSubject$);
        // prime it, only when its not using a webworker
        updateSubject$.next((state) => state);
      }
    };
  }


  /**
   * Check if the WrapMethod flag was set when using a WebWorker
   * 
   * @private
   * 
   * @memberOf StateManagerService
   */
  private checkSetWrapMethodCalledFlag(): void {
    if (this.wrapMethodCalled === false && this.useWebWorker) {
      throw new Error(`${this.errorPreFix} You did not call the 'wrapActionsForWebWorker()' method in your action service.\n\n`);
    }
  }


  /**
   * Helper to create the actions wrapper
   * 
   * @private
   * @param {any} StateManagerServiceClass
   * @returns
   * 
   * @memberOf StateManagerService
   */
  private createWrapActionsHandler(StateManagerServiceClass: StateManager): Function {
    const _stateManager = StateManagerServiceClass;
    return function _wrapper() {
      _stateManager.setWrapMethodCalledFlag(true);
      Object.keys(this)
        .forEach(function _forEach(key) {
          const _fn = this[key];
          this[key] = function () {
            if (_stateManager.isUsingWebWorker()) {
              _stateManager.postToWebWorker(key, Array.prototype.slice.call(arguments));
            } else {
              _fn.apply(this, Array.prototype.slice.call(arguments));
            }
          }.bind(this);
        }.bind(this));
    };
  }


  /**
   * Set the wrapper called flag
   * 
   * @private
   * @param {boolean} value
   * 
   * @memberOf StateManagerService
   */
  private setWrapMethodCalledFlag(value: boolean) {
    this.wrapMethodCalled = value;
  }

}
