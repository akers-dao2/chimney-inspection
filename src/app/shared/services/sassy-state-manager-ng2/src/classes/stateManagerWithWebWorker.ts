import { StateManager } from './stateManager';

/**
 * State Manager with a web worker
 * 
 * @export
 * @class StateManagerWithWebWorkerService
 * @extends {StateManagerService}
 */
export class StateManagerWithWebWorker extends StateManager {

  /**
   * Creates an instance of StateManagerWithWebWorkerService.
   * 
   * @param {Worker} WorkerInstance
   * 
   * @memberOf StateManagerWithWebWorkerService
   */
  constructor(protected WorkerInstance: Worker) {

    // all the parent
    super();

    // assign the worker instance to the protected member
    this.WorkerInstance = WorkerInstance;

    // if using a webworker, then we need to create it
    if (typeof this.WorkerInstance !== 'undefined' && WorkerInstance !== null) {
      this.WorkerInstance.onmessage = event => {
        this.webWorker$.next(event.data);
      };
      this.useWebWorker = true;
    }

  }

}
