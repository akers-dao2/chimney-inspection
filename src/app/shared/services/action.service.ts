import { Injectable } from '@angular/core';
import { StateManagerService } from 'app/shared/services/sassy-state-manager-ng2/src/state-manager.service';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/pluck';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { Observable } from 'rxjs/Observable';
import { ToastController, Toast, Platform, AlertController } from 'ionic-angular';

import * as firebase from 'firebase';

/** Classes */
import { Client } from '../classes/client';
import { Job } from '../classes/job';
import { User } from '../classes/user';
import { Appliance } from '../classes/appliance';

/** Services */
import { UtilityService } from './utility.service';
import { DataService } from './data.service';
import { ChimneyNamesService } from './chimney-names.service';
import { GetRandomIntService } from './get-random-int.service';
import { InspectionItemNameService } from './inspection-item-name.service';
import { SortService } from './sort.service';
import { PouchDbService } from './pouch-db.service';
import { DeletePhotoService } from 'app/shared/services/delete-photo.service';


/** Interfaces */
import { INavigation } from '../interfaces/navigation';
import { SideMenuType } from '../interfaces/side-menu-type';
import { IInspectionItem } from '../interfaces/inspection-item';
import { IInspection } from '../interfaces/inspection';
import { ICrew } from '../interfaces/crew';
import { IClient } from 'app/shared/interfaces';
import { IInspectionReport } from '../interfaces/inspection-report';
import { IComment } from '../interfaces/comments';
import { IRecommendation } from '../interfaces/recommendations';
import { IJob } from '../interfaces/job';
import { IAppliance } from '../interfaces/appliance';
import { IOpenedInspectionReport } from '../interfaces/opened-inspection-report';
import { IAuditRecord } from '../interfaces/audit-record';
import { IUser } from '../interfaces/user';
import { IPhoto } from '../interfaces/photo';

/** Enums */
import { ApplianceTypes } from '../enums/appliances';
import { DateFilterView } from '../enums/date-filter-views';
import { IRemoveApplianceAndChimneyParams, IItemToRemove } from '../interfaces/remove-appliance-and-chimney-params';
import { from } from 'rxjs/observable/from';
import { switchMap, take, tap, mergeMap, filter, delay, catchError, takeWhile, concatMap } from 'rxjs/operators';
import { LocalDbStatus } from '../enums/local-db-status';
import { combineLatest } from 'rxjs/observable/combineLatest';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { of } from 'rxjs/observable/of';
import { LoginService } from './login.service';
import { FirebaseService } from './firebase.service';

// tslint:disable-next-line:interface-over-type-literal
type firebaseDocInfo = { doc: IInspectionReport, inspectionReportKey: string, clientId: string };

@Injectable()
export class ActionService {

  private limit = 50;

  constructor(
    private stateManager: StateManagerService,
    private utility: UtilityService,
    private dataService: DataService,
    private chimneyNames: ChimneyNamesService,
    private inspectionItemName: InspectionItemNameService,
    private sortService: SortService,
    private pouchDB: PouchDbService,
    private toastCtrl: ToastController,
    private firebase: FirebaseService,
    private plt: Platform,
    private alertCtrl: AlertController,
    private deletePhoto: DeletePhotoService,
  ) { }

  /****************************************************************
   *
   * Public Methods
   *
   ****************************************************************/

  /**
   * Set State Models
   *
   *
   * @memberOf ActionService
   */
  public setModels() {
    this.stateManager.createModel('users');
    this.stateManager.setModel('users', []);

    this.stateManager.createModel('currentUser');
    this.stateManager.setModel('currentUser', {});

    this.stateManager.createModel('jobs');
    this.stateManager.setModel('jobs', []);

    this.stateManager.createModel('crews');
    this.stateManager.setModel('crews', []);

    this.stateManager.createModel('clients');
    this.stateManager.setModel('clients', []);

    this.stateManager.createModel('navigation');
    this.stateManager.setModel('navigation', { sideMenuType: '' });

    this.stateManager.createModel('inspection');
    this.stateManager.setModel('inspection', {});

    this.stateManager.createModel('inspections');
    this.stateManager.setModel('inspections', []);

    this.stateManager.createModel('townships');
    this.stateManager.setModel('townships', undefined);

    this.stateManager.createModel('inspectionReports');
    this.stateManager.setModel('inspectionReports', []);

    this.stateManager.createModel('openedInspectionReport');
    this.stateManager.setModel('openedInspectionReport', {});

    this.stateManager.createModel('openedInspection');
    this.stateManager.setModel('openedInspection', {});

    this.stateManager.createModel('activeMenuItem');
    this.stateManager.setModel('activeMenuItem', {});

    this.stateManager.createModel('comments');
    this.stateManager.setModel('comments', {});

    this.stateManager.createModel('recommendations');
    this.stateManager.setModel('recommendations', {});

    this.stateManager.createModel('jobsOrder');
    this.stateManager.setModel('jobsOrder', []);

    this.stateManager.createModel('auditRecords');
    this.stateManager.setModel('auditRecords', []);

    this.stateManager.createModel('jobCount');
    this.stateManager.setModel('jobCount', {});

    this.stateManager.createModel('appStatus');
    this.stateManager.setModel('appStatus', undefined);

    this.stateManager.createModel('offlineStorage');
    this.stateManager.setModel('offlineStorage', []);

    this.stateManager.createModel('downloadLinks');
    this.stateManager.setModel('downloadLinks', []);

    this.stateManager.createModel('applicationMaintenanceJobs');
    this.stateManager.setModel('applicationMaintenanceJobs', undefined);

    this.stateManager.createModel('inspectionConfigurationItem');
    this.stateManager.setModel('inspectionConfigurationItem', undefined);

    this.stateManager.createModel('localDB');
    this.stateManager.setModel('localDB', LocalDbStatus.NONE);

    this.stateManager.createModel('lastSyncUpdate');
    this.stateManager.setModel('lastSyncUpdate', { timeStamp: undefined });
  }

  public addOrRemoveItemOfflineStorage(item, isDelete = false) {
    return new Promise((resolve) => {
      const { fileName, name } = item;

      if (!JSON.parse(localStorage.getItem('user')).online) {
        this.stateManager.update('offlineStorage')((currentState) => {
          if (currentState) {
            let newState = this.utility.clone<any[]>(currentState);

            if (!newState.length && !isDelete) {
              newState = [{ fileName, name }];
            } else {
              const hasItem = isDelete || newState.some(o => o.fileName === fileName);
              newState = hasItem ? newState.filter(o => o.fileName !== fileName) : [...newState, { fileName, name }];
            }

            this.updateOfflineDBIfNeeded('offlineStorage', { photoNames: newState });
            resolve();
            return newState;
          }
        });
      } else {
        resolve();
      }

    });
  }

  private getDownloadLink(link) {
    if (typeof link !== 'string') {
      link = Object.keys(link).filter(key => typeof link[key] === 'string').map(key => link[key]);
      link = link !== null ? link[0] : undefined;
    }

    return link;
  }

  private removePouchDB(model) {
    return this.pouchDB.db.get(model)
      .then(async (doc) => {
        await this.pouchDB.db.remove(doc._id, doc._rev);
      })
      .catch(console.error);
  }

  public installLocalDB() {
    const getModel = mergeMap((model: string) => {
      return this.stateManager.getModel(model).pipe(take(1));
    }, (model, data) => ({ model, data }));

    return from(this.models)
      .pipe(
      getModel,
      tap(r => {
        this.pouchDB.db.get(r.model)
          .catch((err) => {
            if (err.status === 404) {
              this.pouchDB.db.put({ ...r.data, _id: r.model }, { force: true })
                .catch(console.error);

              if (r.model === 'lastSyncUpdate') {
                const alert = this.alertCtrl.create({
                  title: 'First Time Access',
                  message: 'Click the sync button located in the upper right corner, to load data.',
                  buttons: [
                    {
                      text: 'Will do!',
                      role: 'cancel'
                    }
                  ]
                });

                alert.present();
              }
            }

            console.log(err);
          });
      })
      )
      .toPromise();
  }

  public loadLocalDB() {
    const getModel = mergeMap((model: string) => {
      return this.stateManager.getModel(model).pipe(take(1));
    }, (model, data) => ({ model, data }));

    from(this.models)
      .pipe(
      getModel,
      tap(r => {
        this.pouchDB.db.get(r.model)
          .then((doc) => {
            const data = Object.assign({}, r.data, { _id: r.model, _rev: doc._rev });
            return this.pouchDB.db.put(data);
          })
          .catch((err) => {
            if (err.status === 404) {
              const data = Object.assign({}, r.data, { _id: r.model });
              this.pouchDB.db.put(data)
                .catch(console.error);
            }
            console.log(err);
          });
      })
      )
      .subscribe();
  }

  private async updateOfflineDBIfNeeded(model: string, state: any) {
    if (!this.currentUser.online) {
      try {
        const doc = await this.pouchDB.db.get(model);
        this.pouchDB.db.put({ ...doc, ...state });
      } catch (error) {
        console.log(error);
      }
    }
  }


  public syncOfflineQueue() {
    if (!navigator.onLine) {
      this.presentToast('Still offline. Please go online to sync.');
      return;
    }
    this.pouchDB.db.get('offlineQueue')
      .then((document: IInspectionReport) => {
        return this.presentToast('Syncing...')
          .then((toast: Toast) => {
            const doc = { ...document };
            delete doc._id;
            delete doc._rev;
            return this.syncAttachments()
              .then((attachments) => ({ toast, attachments, doc }))
              .catch(err => { throw err; });
          })
          .then(({ toast, attachments, doc }) => {
            if (Array.isArray(attachments)) {
              attachments.forEach((attachment: firebase.storage.UploadTaskSnapshot) => {
                const docInfo = this.getDocForJob(doc, attachment);
                if (docInfo) {
                  doc = this.updateDocDownloadURL(docInfo, attachment);
                }
              });
            }
            return { toast, doc };
          })
          .then(({ toast, doc }) =>
            this.dataService.syncRemote(doc).then(() => toast)
          )
          .catch(err => { throw err; });
      })
      .then(async (toast) => {
        await this.removePouchDB('offlineQueue');
        await this.removePouchDB('offlineStorage');
        toast.dismiss();
        this.syncLocalWithRemoteDB();
      })
      .catch(async err => {
        if (err.status === 404) {
          this.syncLocalWithRemoteDB();
        } else {
          console.error(err);
          try {
            await this.dataService.addAuditRecord(err);
            const document = await this.pouchDB.db.get('offlineQueue');
            const doc = { ...document };
            delete doc._id;
            delete doc._rev;
            await this.dataService.syncRemote(doc);
            this.syncLocalWithRemoteDB();
            this.presentToast('Data sync with error. Check data via an online user.');
          } catch (error) {
            this.presentToast('Still offline. Please go online to sync.');
          }
        }
      });
  }

  private updateDocDownloadURL({ doc, inspectionReportKey, clientId }: firebaseDocInfo, attachment: firebase.storage.UploadTaskSnapshot) {
    const downloadURL = attachment.downloadURL;
    const nameRegex = /^\D*[^_\d]/g;
    const uuidRegex = /\d+_\d+/g;
    const name = nameRegex.exec(attachment.metadata.fullPath)[0];
    // tslint:disable-next-line:prefer-const
    let index: number;
    const [uuid] = uuidRegex.exec(attachment.metadata.fullPath)[0].split('_');
    const reports = doc[inspectionReportKey][clientId].reports;
    for (const prop in reports) {
      if (reports.hasOwnProperty(prop)) {
        for (const key in reports[prop]) {
          if (reports[prop].hasOwnProperty(key)) {
            if (reports[prop][key].uuid === uuid) {
              if (this.getFireBaseObjectName(name) === 'requiredPics') {
                index = reports[prop][key].requiredPics.pictures.findIndex(p => p.name === name);
              } else {
                const fileName = attachment.metadata.fullPath;
                index = reports[prop][key][this.getFireBaseObjectName(name)].pictures.findIndex(p => p.fileName === fileName);
              }

              if (index !== -1) {
                reports[prop][key][this.getFireBaseObjectName(name)].pictures[index].downloadLink = downloadURL;
              }
            }
          }
        }
      }
    }
    return doc;
  }

  private getFireBaseObjectName(name: string) {
    const names = new Map();
    names.set('brickstone', 'brickStone');
    names.set('requiredpics', 'requiredPics');
    names.set('front_of_house', 'requiredPics');
    names.set('chimney', 'requiredPics');
    names.set('appliance', 'requiredPics');
    names.set('structureintegrity', 'structureIntegrity');
    names.set('surroundclearance', 'surroundClearance');
    return names.has(name) ? names.get(name) : name;
  }

  private getDocForJob(doc: IInspectionReport, attachment: firebase.storage.UploadTaskSnapshot) {
    const jobIdRegex = /-[\S]+/g;
    const jobId = jobIdRegex.exec(attachment.metadata.fullPath)[0];
    const filterDownloadLinks = Object.keys(doc).filter(o => o.includes('inspectionReports'));
    const filterDownloadLink = filterDownloadLinks.find(f => {
      const inspectionReportKey: string = f;
      const clientId: string = Object.keys(doc[f])[0];
      const jobKey = doc[inspectionReportKey][clientId].jobKey;
      return jobId === jobKey;
    });

    if (filterDownloadLink === undefined) {
      return undefined;
    }

    return { doc, inspectionReportKey: filterDownloadLink, clientId: Object.keys(doc[filterDownloadLink])[0] };

  }

  private syncLocalWithRemoteDB() {
    this.setLocalDBStatus(LocalDbStatus.SYNCFROM);
    this.initialDataAppLoad(() => {
      this.setLocalDBStatus(LocalDbStatus.NONE);
      this.syncUpdateComplete();
      this.loadLocalDB();
      this.presentToast('Syncing complete')
        .then(toast => {
          setTimeout(() => {
            toast.dismiss();
          }, 1000);
        });

    });
  }

  private syncUpdateComplete() {
    this.stateManager.update('lastSyncUpdate')(() => {
      const timeStamp = Date.now();
      return { timeStamp };
    });
  }

  public syncAttachments(): Promise<any> {
    return this.pouchDB.db.get('offlineStorage')
      .then((data) => {
        if (data.photoNames) {
          const photos = data.photoNames;
          return Promise
            .all(data.photoNames.map(o => this.dataService.getPhotoFile(o.fileName)))
            .then((blobs: any) => {
              return Promise.all(blobs.map((blob, i) => {
                const fileName = photos[i].fileName;
                const file = blob._attachments[fileName].data;
                return this.dataService.uploadToRemoteStorage(file, fileName);
              }))
                .catch(err => { throw err; });
            })
            .catch(err => { throw err; });
        }
        return data;
      })
      .catch(err => { throw err; });
  }

  private presentToast(message): Promise<Toast> {
    return new Promise<any>((resolve) => {

      const toast = this.toastCtrl.create({
        message,
        showCloseButton: true,
      });

      toast.present();

      resolve(toast);

    });
  }

  private get models() {
    return [
      'users',
      'currentUser',
      'jobs',
      'crews',
      'clients',
      'navigation',
      'inspection',
      'inspections',
      'townships',
      'inspectionReports',
      'openedInspectionReport',
      'openedInspection',
      'activeMenuItem',
      'comments',
      'recommendations',
      'jobCount',
      'offlineStorage',
      'lastSyncUpdate'
    ];

  }

  /**
   * Set Current User
   *
   * @param {User} currentUser
   *
   * @memberOf ActionService
   */
  public setCurrentUser(currentUser: IUser) {
    this.stateManager.update('currentUser')((currentState) => {
      let newState = this.utility.clone<IUser>(currentState);

      newState = Object.assign({}, newState, currentUser);

      return newState;
    });
  }


  /**
   * Load jobs
   *
   * @param {Job[]} jobs
   *
   * @memberOf ActionService
   */
  public loadJobs(jobs: IJob[]) {
    if (!!jobs.length) {
      this.stateManager.update('jobs')((currentState) => {
        return this.sortJobs(jobs.slice(0));
      });
    }
  }

  private sortJobs(jobs: IJob[]) {
    return jobs
      .filter(job => job.hasOwnProperty('order'))
      .sort((x, y) => this.sortService.get(x.order, y.order))
      .reverse();
  }


  /**
   * Load Crews
   *
   * @param {string[]} crews
   *
   * @memberOf ActionService
   */
  public loadCrews(crews: ICrew[]) {
    this.stateManager.update('crews')((currentState) => {
      return crews.slice(0);
    });
  }

  /**
   * Load Crews
   *
   * @param {string[]} crews
   *
   * @memberOf ActionService
   */
  public loadAuditRecords(auditRecords: IAuditRecord[]) {
    this.stateManager.update('auditRecords')((currentState) => {
      return auditRecords.slice(0);
    });
  }


  /**
   * Load Crews
   *
   * @param {string[]} crews
   *
   * @memberOf ActionService
   */
  public loadClients(clients: IClient[]) {
    this.stateManager.update('clients')((currentState) => {
      return clients.slice(0);
    });
  }


  /**
   * Load Users
   *
   * @param {string[]} users
   *
   * @memberOf ActionService
   */
  public loadUsers(users: User[]) {
    this.stateManager.update('users')((currentState) => {
      let newState = this.utility.clone<User[]>(currentState);


      newState = Array.of(...users);

      return newState;
    });
  }



  public loadMoreData() {
    this.limit = this.limit + 25;

    return Observable.combineLatest(
      this.dataService.getJobsOnce(this.limit).skipWhile(r => !r.length),
      this.dataService.getClientsOnce(this.limit).skipWhile(r => !r.length),
      this.dataService.getInspectionsOnce(this.limit).skipWhile(r => !r.length),
      this.dataService.getInspectionReportsOnce(500 + this.limit).skipWhile(r => !r.length)
    )
      .switchMapTo(this.stateManager.getModel('jobs'), ([jobs, clients, inspections, inspectionReports], localJobs) => {
        const disableScoll = jobs.length === localJobs.length;
        return { jobs, clients, inspections, inspectionReports, disableScoll };
      })
      .map(({ jobs, clients, inspections, inspectionReports, disableScoll }) => {

        this.loadJobs(jobs);
        this.loadClients(clients);
        this.loadInspections(inspections);
        this.loadInspectionReports(inspectionReports);
        return disableScoll;
      })
      .delay(0);

  }

  public loadMoreClients() {
    this.limit = this.limit + 25;

    return this.dataService.getClients(this.limit).skipWhile(r => !r.length)
      .switchMap(jobs => this.stateManager.getModel('clients'), (remoteClients, localClients) => {
        this.loadClients(remoteClients);
        const disableScoll = remoteClients.length === localClients.length;
        return disableScoll;
      });

  }

  async loadOfflineStorage() {
    try {
      const doc = await this.pouchDB.db.get('offlineStorage');
      this.stateManager.update('offlineStorage')(() => doc && doc.photoNames ? doc.photoNames : []);
    } catch (error) {
      if (error.status === 404) {
        this.stateManager.update('offlineStorage')(() => []);
      }
      console.log(error);
    }
  }

  private async loadLastSyncUpdate() {
    try {
      const doc = await this.pouchDB.db.get('lastSyncUpdate');
      this.stateManager.update('lastSyncUpdate')(() => ({ timeStamp: doc.timeStamp }));
    } catch (error) {
      this.stateManager.update('lastSyncUpdate')(() => ({ timeStamp: undefined }));
      console.log(error);
    }
  }

  async initialDataAppLoad(callback?: () => void) {
    const filterNil = filter<any[]>(r =>
      !r.some(v => {
        if (v === undefined) {
          return true;
        }

        if (Array.isArray(v) && !v.length) {
          return true;
        }

        return !Object.keys(v).length;
      })
    );

    const user = JSON.parse(localStorage.getItem('user')) as User;
    if (callback !== undefined) {
      const loginUser = this.firebase.get.auth().currentUser;
      if (loginUser === null) {
        await this.dataService.signIn(user.emailAddress, user.password)
          .pipe(take(1))
          .toPromise();
      }
    }

    if (!user.online) {
      this.loadOfflineStorage();
      this.loadLastSyncUpdate();
    }

    const group1 = combineLatest(
      this.dataService.getJobs(),
      this.dataService.getCrews(),
      this.dataService.getClients(),
      this.dataService.getUsers(),
      this.dataService.getInspections(),
      this.dataService.getInspectionReports(user.online && !this.plt.is('ios') ? 500 : 50),
    )
      .pipe(
      filterNil,
      tap(([jobs, crews, clients, users, inspections, inspectionReports]) => {
        this.loadJobs(jobs);
        this.loadCrews(crews);
        this.loadClients(clients);
        this.loadUsers(users);
        this.loadInspections(inspections);
        this.loadInspectionReports(inspectionReports);
      })
      );

    const group2 = combineLatest(
      this.dataService.getTownships(),
      this.dataService.getImageFileNamesFromDB(),
      this.dataService.getComments(),
      this.dataService.getRecommendations(),
      this.dataService.getJobCount()
    )
      .pipe(
      filterNil,
      tap(([townships, links, comments, recommendations, jobCount]) => {
        this.loadTownships(townships);
        this.loadDownloadLinks(links);
        this.loadComments(comments);
        this.loadRecommendations(recommendations);
        this.loadJobCount(jobCount);
      })
      );

    combineLatest(group1, group2)
      .pipe(
      takeWhile((v, i) => {
        return user.online ? true : false;
      })
      )
      .subscribe(null, null, callback);

  }

  loadJobCount(jobCount) {
    this.stateManager.update('jobCount')((currentState) => {
      const newState = this.utility.clone<any>(currentState);

      return jobCount;
    });
  }

  loadApplicationMaintenanceJobs(lastRunDate) {
    this.stateManager.update('applicationMaintenanceJobs')(() => lastRunDate);
  }

  loadDownloadLinks(links) {
    this.stateManager.update('downloadLinks')(() => links);
  }

  addJobToCount(count) {
    this.dataService.updateJobCount(count)
      .take(1)
      .subscribe(resp => {
        this.stateManager.update('jobCount')((currentState) => {
          const newState = this.utility.clone<any>(currentState);

          return { value: count };
        });
      });
  }

  /**
   * Log Out User
   *
   *
   * @memberOf ActionService
   */
  public logOutUser() {
    this.dataService.signOut()
      .take(1)
      .subscribe(() => {
        localStorage.removeItem('user');
        this.stateManager.update('currentUser')((currentState) => {
          return Object.assign({});
        });

      });
  }


  /**
   * changeSideMenu
   *
   * @param {SideMenuType} sideMenuType
   *
   * @memberOf ActionService
   */
  public changeSideMenu(sideMenuType: SideMenuType) {
    this.stateManager.update('navigation')((currentState) => {
      const newState = this.utility.clone<INavigation>(currentState);

      newState.sideMenuType = sideMenuType;

      return newState;
    });
  }


  /**
   * Add Inspection Item To List
   *
   * @param {string} inspectionItem
   * @param {number} [index=0]
   * @param {number} [flueIndex]
   *
   * @memberOf ActionService
   */
  public addInspectionItemToList(inspectionItemName: string, index = 0, flueIndex?: number) {
    this.stateManager.update('inspection')((currentState) => {
      let newState = this.utility.clone<IInspection>(currentState);
      let count: number;
      let item: IAppliance;

      if (inspectionItemName === 'Chimney') {
        // add new Chimney inspections object
        count = this.chimneyNames.get(newState).length;

        while (newState[`${inspectionItemName}-${count}`] !== undefined) {
          count++;
        }

        newState = Object.assign({}, newState, {
          [`${inspectionItemName}-${count}`]: {
            inspectionItems: [],
            name: `${inspectionItemName}-${count + 1}`,
            uuid: `${count}`
          },
          clientId: null,
          date: null
        });
      } else if (flueIndex !== undefined) {

        // create new appliance item
        item = new Appliance({
          name: this.inspectionItemName.get(newState, inspectionItemName, index, flueIndex),
          parentIndex: index,
          index: flueIndex,
          locationIndex: this.inspectionItemName.getCountOfItems(newState, inspectionItemName, index, flueIndex) + 1,
          type: ApplianceTypes[inspectionItemName]
        });

        // identifiy location for adding appliance
        count = newState[`Chimney-${index}`].inspectionItems
          .slice(flueIndex + 1)
          .findIndex(appliance => appliance.type === ApplianceTypes['Flue/Vent']);

        count = count === -1 ? newState[`Chimney-${index}`].inspectionItems.length : count + flueIndex + 1;

        // add new appliance to correct flue/vent under Chimney
        newState[`Chimney-${index}`].inspectionItems.splice(count, 0, item);

        // add index property to each appliance
        // update inspectionItems array for the Chimney
        newState[`Chimney-${index}`].inspectionItems = newState[`Chimney-${index}`].inspectionItems
          .map((appliance, _index_) =>
            Object.assign({}, appliance, { index: _index_ }));

      } else {
        // handles adding new appliance if there is only one flue/vent

        newState[`Chimney-${index}`] = this.addInspectionItemsProp(newState[`Chimney-${index}`]);

        const itemIndex = newState[`Chimney-${index}`].inspectionItems.length;

        item = new Appliance({
          name: this.inspectionItemName.get(newState, inspectionItemName, index),
          parentIndex: index,
          index: itemIndex,
          locationIndex: this.inspectionItemName.getCountOfItems(newState, inspectionItemName, index, flueIndex) + 1,
          type: ApplianceTypes[inspectionItemName]
        });

        // add new appliance to Chimney-${index} array
        newState[`Chimney-${index}`].inspectionItems.push(item);
      }

      this.updateOfflineDBIfNeeded('inspection', newState);

      return newState;
    });
  }

  private addInspectionItemsProp(chimneyObject) {
    if (chimneyObject.inspectionItems === undefined) {
      return Object.assign({}, chimneyObject, { inspectionItems: [] });
    }

    return chimneyObject;
  }

  /**
   * addChimneyAndFlue
   *
   * @param {SideMenuType} sideMenuType
   *
   * @memberOf ActionService 
   */
  public addChimneyAndFlue(inspectionItem: string) {
    this.stateManager.update('inspection')((currentState) => {
      let newState = this.utility.clone<IInspection>(currentState);

      const item = new Appliance({
        name: `${inspectionItem}-1`,
        parentIndex: 0,
        index: 1,
        locationIndex: 1,
        type: ApplianceTypes[inspectionItem]
      });

      newState = Object.assign({}, newState, {
        [`Chimney-0`]: {
          inspectionItems: [
            new Appliance({
              name: 'Flue/Vent-1',
              parentIndex: 0,
              index: 0,
              locationIndex: 1,
              type: ApplianceTypes['Flue/Vent']
            })
          ],
          name: 'Chimney-1',
          uuid: '0'
        },
        clientId: null,
        date: null
      });

      if (inspectionItem !== 'Flue/Vent') {
        newState[`Chimney-0`].inspectionItems.push(item);
      }

      this.updateOfflineDBIfNeeded('inspection', newState);

      return newState;
    });
  }


  /**
   * Remove appliance Item
   *
   * @param {number} parentIndex
   * @param {number} index
   *
   * @memberOf ActionService
   */
  public removeAppliance(parentIndex: number, index: number) {

    const inspection$: Observable<IInspection> = this.stateManager.getModel('inspection');
    const inspectionReports$: Observable<IInspectionReport[]> = this.stateManager.getModel('inspectionReports');

    Observable.combineLatest(inspection$, inspectionReports$)
      .map(([inspection, inspectionReports]) => {
        const newInspection = this.utility.clone<IInspection>(inspection);
        const newInspectionReports = this.utility.clone<IInspectionReport[]>(inspectionReports);
        const chimneyNames = this.chimneyNames.get(inspection);
        const chimney = chimneyNames[parentIndex] || chimneyNames[index];
        let itemToRemove: any = parentIndex !== undefined ? newInspection[chimney].inspectionItems[index] : newInspection[chimney];
        const { clientId, date } = newInspection;

        if (this.hasOnlyOneFlueVent(itemToRemove, newInspection, chimney)) {
          itemToRemove = newInspection[chimney];
        }

        return { newInspection, newInspectionReports, clientId, date, itemToRemove, chimney, parentIndex, index };
      })
      .map(this.removePhotosFromOfflineQueue)
      .map(this.removeInspectionItemFromLocalChimney)
      .map(this.reorderAppliances)
      .switchMap(this.removeInspectionItemFromRemoteChimney)
      .take(1)
      .switchMap(this.removeInspectionItemFromReport)
      .do(this.updateInspection)
      .do(this.updateInspectionReports)
      .take(1)
      .subscribe();
  }

  private removePhotosFromOfflineQueue = (obj) => {
    const { newInspection, newInspectionReports, itemToRemove } = obj;
    const { clientId, date, jobKey } = newInspection;
    const inspectionReport = newInspectionReports.find(item =>
      item[clientId] &&
      item[clientId].jobKey === jobKey &&
      item[clientId].reports[date]
    );
    const appliances = inspectionReport[clientId].reports[date];
    const photos = [];
    for (const key in appliances) {
      if (appliances.hasOwnProperty(key)) {
        const appliance = appliances[key];
        if (appliance.name === itemToRemove.name && appliance.uuid === itemToRemove.uuid) {
          for (const key2 in appliance) {
            if (appliance.hasOwnProperty(key2)) {
              const item = appliance[key2];
              if (item.pictures && !!item.pictures.length) {
                item.pictures.forEach(photo => {
                  photos.push(photo);
                });
              }
            }
          }
        }
      }
    }
    console.log(photos);
    from(photos)
      .pipe(
      tap(console.log),
      delay(1000),
      concatMap(photo => this.deletePhotoFromStore(photo)),
      tap(() => console.log('finis')),
    )
      .subscribe(null, null, () => console.log('complete'));
    return obj;
  }

  private deletePhotoFromStore(photo) {
    return this.deletePhoto.get(photo)
      .skipWhile(prop => prop === undefined)
      .take(1)
      .switchMapTo(this.addOrRemoveItemOfflineStorage(photo, true))
      .toPromise();
  }

  private hasOnlyOneFlueVent(itemToRemove: IItemToRemove, newInspection: IInspection, chimney: string) {
    return itemToRemove.type === ApplianceTypes['Flue/Vent'] &&
      newInspection[chimney].inspectionItems
        .filter(i => i.type === ApplianceTypes['Flue/Vent'])
        .length === 1;
  }

  /**
   * Update appliance name
   *
   * @param {number} parentIndex
   * @param {number} index
   * @param {string} name
   *
   * @memberOf ActionService
   */
  public updateApplianceName(parentIndex: number, index: number, name: string) {
    this.stateManager.update('inspection')((currentState) => {
      const newState = this.utility.clone<IInspection>(currentState);
      const chimney = this.chimneyNames.get(newState)[parentIndex];
      const itemToUpdate = parentIndex !== undefined ?
        newState[chimney].inspectionItems[index] :
        newState[this.chimneyNames.get(newState)[index]];

      if (itemToUpdate) {
        itemToUpdate.name = name;
      } else {
        this.removeAppliance(parentIndex, index);
      }
    });
  }


  /**
   * add new client to the clients model
   *
   * @param {Client} client
   *
   * @memberOf ActionService
   */
  public addClient(client: Client) {
    this.stateManager.update('clients')((currentState) => {
      const newState = this.utility.clone<Client[]>(currentState);

      newState.push(client);

      this.updateOfflineDBIfNeeded('clients', newState);

      return newState;
    });
  }


  /**
   * update client info in the model
   *
   * @param {Client} client
   *
   * @memberOf ActionService
   */
  public updateClient(client: Client) {
    this.stateManager.update('clients')((currentState) => {
      let newState = this.utility.clone<Client[]>(currentState);

      newState = newState.map(_client_ => {
        if (_client_.key === client.key) {
          return Object.assign({}, _client_, client);
        }
        return _client_;
      });

      this.updateOfflineDBIfNeeded('clients', newState);

      return newState;
    });
  }


  /**
   * Delete Client
   *
   * @param {string} key
   * @returns {Observable<firebase.Promise<T>>}
   *
   * @memberOf DataService
   */
  public deleteClient(key: string) {
    this.stateManager.update('clients')((currentState) => {
      let newState = this.utility.clone<Client[]>(currentState);

      newState = newState.filter(client => (client.key !== key));

      this.updateOfflineDBIfNeeded('clients', newState);

      return newState;
    });
  }


  /**
   * update job info in the model
   *
   * @param {Job} job
   *
   * @memberOf ActionService
   */
  public updateJob(job: IJob) {
    this.dataService.addJob(job, true)
      .skipWhile(job$$ => job$$ === null)
      .pluck<IJob, string>('newKey')
      .take(1)
      .subscribe(key => {
        this.stateManager.update('jobs')((currentState) => {
          let newState = this.utility.clone<IJob[]>(currentState);

          newState = newState.map(_job_ => {
            if (_job_.key === job.key) {
              return Object.assign({}, _job_, job);
            }
            return _job_;
          });

          this.updateOfflineDBIfNeeded('jobs', newState);

          return newState;
        });

      });
  }

  private get currentUser() {
    return JSON.parse(localStorage.getItem('user')) as User;
  }

  /**
   * Delete Job
   *
   * @param {string} key
   * @returns {Observable<firebase.Promise<T>>}
   *
   * @memberOf DataService
   */
  public deleteItemFromModel(key: string, model) {
    this.stateManager.update(model)((currentState) => {
      let newState = this.utility.clone<Client[]>(currentState);

      newState = newState.filter(item => item.key !== key);

      this.updateOfflineDBIfNeeded(model, newState);

      return newState;
    });
  }


  /**
   * add new user
   *
   * @param {IUser} user
   *
   * @memberOf ActionService
   */
  public addUser(user: User) {
    this.stateManager.update('users')((currentState) => {
      const newState = this.utility.clone<User[]>(currentState);

      newState.push(user);

      return newState;
    });
  }

  /**
     * update user info in the model
     *
     * @param {User} user
     *
     * @memberOf ActionService
     */
  public updateUser(user: User) {
    this.stateManager.update('users')((currentState) => {
      const newState = this.utility.clone<User[]>(currentState);

      return newState.map(_user_ => {
        if (_user_.key === user.key) {
          return Object.assign({}, _user_, user);
        }
        return _user_;
      });
    });
  }


  /**
   * Delete Client
   *
   * @param {string} id
   * @returns {Observable<firebase.Promise<T>>}
   *
   * @memberOf DataService
   */
  public deleteUser(key: string) {
    this.stateManager.update('users')((currentState) => {
      const newState = this.utility.clone<Client[]>(currentState);

      return newState.filter(user => user.key !== key);
    });
  }


  /**
   * resetInspection
   *
   *
   * @memberOf ActionService
   */
  public resetInspection() {
    this.stateManager.update('inspection')((currentState) => {
      const newState = this.utility.clone<IInspection>(currentState);

      return Object.assign({});
    });
  }


  /**
   * loadInspection
   *
   * @param {IInspection} inspection
   *
   * @memberOf ActionService
   */
  public loadInspection(inspection: IInspection) {
    this.stateManager.update('inspection')(() => {
      return { ...inspection };
    });
  }

  /**
   * loadInspection
   *
   * @param {IInspection} inspection
   *
   * @memberOf ActionService
   */
  public loadInspections(inspections: IInspection[]) {
    this.stateManager.update('inspections')((currentState) => {
      return Array.of(...inspections);
    });
  }

  /**
   * loads all the Inspection reports from firebase
   *
   * @param {IInspection} inspection
   *
   * @memberOf ActionService
   */
  public loadInspectionReports(inspections: IInspectionReport[]) {
    this.stateManager.update('inspectionReports')((currentState) => {
      return Array.of(...inspections);
    });
  }

  /**
   * Create Job
   *
   * @param {any} { date, assigned, client }
   *
   * @memberOf ActionService
   */
  public createJob(job: Job) {
    this.stateManager.update('jobs')((currentState) => {
      let newState = this.utility.clone<IJob[]>(currentState);

      newState = newState.filter(existingJob => existingJob.key !== job.key);

      newState.push(job);

      newState = this.sortJobs(newState);

      this.updateOfflineDBIfNeeded('jobs', newState);

      return newState;
    });
  }


  /**
   * Add or Update inspection
   *
   * @param {any} { date, assigned, client }
   *
   * @memberOf ActionService
   */
  public createNewOrUpdateExistingInspectionReport(inspectionReport, clientId: string, jobDate: number, jobKey: string) {
    this.saveInspectionReport(inspectionReport, clientId, jobDate, jobKey)
      .take(1)
      .subscribe(record => {
        this.stateManager.update('inspectionReports')((currentState) => {
          let newState = this.utility.clone<IInspectionReport[]>(currentState);

          // remove existing record from list
          newState = newState.filter(item => item.key !== record.key);

          // add record to the list
          newState.push(record);

          this.updateOfflineDBIfNeeded('inspectionReports', newState);

          return newState;
        });
      });
  }

  public setJobListViewState(view: string) {
    this.stateManager.update('currentUser')((currentState) => {

      return Object.assign({}, currentState, { jobListView: view });
    });
  }

  /**
   * Stores the currently opened inspection report
   * 
   * @param {string} clientId 
   * @param {string} jobKey 
   * @param {number} jobDate 
   * 
   * @memberof ActionService
   */
  public openedInspectionReport(clientId: string, jobKey: string, jobDate: number) {
    this.stateManager.update('openedInspectionReport')((currentState) => {

      return Object.assign({}, currentState, { clientId, jobKey, jobDate });
    });
  }

  public openedInspection(inspection: Appliance) {
    this.stateManager.update('openedInspection')((currentState) => {

      return Object.assign({}, inspection);
    });
  }

  public resetOpenedInspection() {
    this.stateManager.update('openedInspection')((currentState) => {

      return Object.assign({});
    });
  }

  /**
   * Add To Inspection List
   *
   * @param {IInspection} inspection
   *
   * @memberOf ActionService
   */
  public addInspectionToList(inspection: IInspection) {
    this.stateManager.update('inspections')((currentState) => {
      // let newState = this.utility.clone<IInspection[]>(currentState);

      // remove existing record from list
      const newState = currentState.filter(item =>
        !(item.clientId === inspection.clientId &&
          item.date === inspection.date &&
          item.jobKey === inspection.jobKey)
      );

      // add record to the list
      newState.push(inspection);

      this.updateOfflineDBIfNeeded('inspections', newState);

      return newState;
    });

  }

  /**
   * Add Crew
   *
   * @param {string} id
   * @returns {Observable<firebase.Promise<T>>}
   *
   * @memberOf DataService
   */
  public addCrew(crew: ICrew, doUpdate = false) {
    this.stateManager.update('crews')((currentState) => {
      let newState = this.utility.clone<ICrew[]>(currentState);

      if (doUpdate) {
        newState = newState.filter(item => item.key !== crew.key);
      }

      newState = Array.of(...newState, crew);

      this.updateOfflineDBIfNeeded('crews', newState);

      return newState;
    });
  }

  /**
   * Delete Client
   *
   * @param {string} id
   * @returns {Observable<firebase.Promise<T>>}
   *
   * @memberOf DataService
   */
  public deleteCrew(key: string) {
    this.stateManager.update('crews')((currentState) => {
      const newState = this.utility.clone<ICrew[]>(currentState);

      return newState.filter(crew => crew.key !== key);
    });
  }

  /**
   * loadInspection
   *
   * @param {IInspection} inspection
   *
   * @memberOf ActionService
   */
  public loadTownships(townships: string[]) {
    this.stateManager.update('townships')((currentState) => {
      return Array.from(townships);
    });
  }

  /**
   * Handle changing the inspection name of an inspection sheet
   * 
   * @param {string} name 
   * @param {string} origName 
   * @param {string} [type] 
   * @returns 
   * 
   * @memberof ActionService
   */
  public changeInspectionName(name: string, origName: string, uuid: string, type?: string) {
    const result$ = new ReplaySubject<string>(1);
    let updated = false;
    this.stateManager.update('inspection')((currentState) => {
      const inspection = this.utility.clone<IInspection>(currentState);

      for (const key in inspection) {
        if (inspection.hasOwnProperty(key) && key !== 'clientId' && key !== 'date' && key !== 'key') {
          const isChimney = type === 'chimney';

          if (isChimney) {
            if (inspection[key].name === origName) {
              inspection[key].name = name;
              updated = true;
            }
          } else {
            inspection[key].inspectionItems
              .filter(item => item.name === origName && item.uuid === uuid)
              .forEach(item => {
                item.name = name;
                updated = true;
              });
          }
        }
      }

      if (updated) {
        result$.next(name);
        // save the inspection name change to inspection
        this.dataService.createNewOrUpdateExistingInspection(inspection, true)
          .take(1)
          .subscribe(() => {

            this.updateOfflineDBIfNeeded('inspection', inspection);
          });
      }

      return inspection;
    });

    return result$;
  }

  /**
   * setActiveMenuItem
   * 
   * @param {any} menuItem 
   * 
   * @memberof ActionService
   */
  public setActiveMenuItem(menuItem) {
    this.stateManager.update('activeMenuItem')((currentState) => {
      return menuItem;
    });
  }

  public addGrouping(grouping, type) {
    const record = { [grouping]: [] };
    this.stateManager.update(type)(state => {
      return { ...state, [grouping]: [] };
    });
  }

  public updateGrouping(grouping, originalGrouping, type) {
    this.stateManager.update(type)(state => {
      const newState = { ...state };
      const values = newState[originalGrouping];
      delete newState[originalGrouping];
      return { ...newState, [grouping]: values };
    });
  }

  public removeGrouping(grouping, type) {
    this.stateManager.update(type)(state => {
      const newState = { ...state };
      delete newState[grouping];
      return newState;
    });
  }

  public addItem(item, grouping, type) {
    this.updateCommentsOrRecommendation(type, grouping, (newState) => newState[grouping].concat(item));
  }

  public updateItem(item, grouping, type, index) {
    this.updateCommentsOrRecommendation(type, grouping, (newState) => {
      newState[grouping][index] = item;
      return newState[grouping];
    });
  }

  public updateItemsOrder(items: { name: string, index: number }[], grouping: string, type: string) {
    this.updateCommentsOrRecommendation(type, grouping, (newState) => {
      newState[grouping] = items.map(i => i.name);
      return newState[grouping];
    });
  }

  public removeItem(grouping, type, index) {
    this.updateCommentsOrRecommendation(type, grouping, (newState) => {
      newState[grouping].splice(index, 1);
      return newState[grouping];
    });
  }

  /**
   * load recommendations
   *
   * @param {IRecommendation} recommendations
   *
   * @memberOf ActionService
   */
  public loadRecommendations(recommendations: IRecommendation) {
    this.stateManager.update('recommendations')((currentState) => {
      return Object.assign({}, recommendations);
    });
  }

  /**
   * setInspectionItemsCompletionStatus
   * 
   * @param {any[]} _inspectionItems_ 
   * @param {boolean} [isValid=false] 
   * @memberof ActionService
   */
  public setInspectionItemsCompletionStatus(_inspectionItems_: any[], notValid = true) {

    this.stateManager.getModel('inspection')
      .map(inspection => {
        let newState = this.utility.clone<IInspection>(inspection);

        newState = this.resetInspectionItemsCompletionStatus(newState);

        const chimneys = Object.keys(newState)
          .filter(key => key.toLowerCase().includes('chimney-'));

        // first check if the uuid matches the chimney
        // if false then move to the inspectionItems array under the chimney
        // if match is found the isValid property is set isValid value

        _inspectionItems_.forEach(item => {
          for (const chimney of chimneys) {
            const { uuid, inspectionItems } = newState[chimney];

            if (item.uuid === uuid) {
              newState[chimney].notValid = notValid;
              continue;
            }

            for (const applianceItem of inspectionItems) {
              if (item.uuid === applianceItem.uuid) {
                applianceItem.notValid = notValid;
                break;
              }
            }
          }
        });

        return newState;
      })
      .switchMap(inspection =>
        this.dataService.createNewOrUpdateExistingInspection(inspection, true)
          .skipWhile(inspection$ => inspection$ === null),
      (inspection, result) => inspection)
      .take(1)
      .subscribe(inspection => this.updateLocalModelInspectionCompletionStatus(inspection));

  }

  /**
   * Save Signature
   * 
   * @param {any} signature 
   * @memberof ActionService
   */
  public updateReport(data) {
    Observable.combineLatest<IInspectionReport[], IOpenedInspectionReport>(
      this.stateManager.getModel('inspectionReports'),
      this.stateManager.getModel('openedInspectionReport'),
    )
      .map(([inspectionReports, openedReport]) => {
        const inspection = inspectionReports.find(item => Object.keys(item)[0] === openedReport.clientId);
        return { openedReport, inspection };
      })
      .map(({ openedReport, inspection }) => {
        const { clientId, jobDate, jobKey } = openedReport;
        Object.assign(inspection[openedReport.clientId].reports[openedReport.jobDate], data);
        return { inspection, clientId, jobDate, jobKey };
      })
      .take(1)
      .subscribe(({ inspection, clientId, jobDate, jobKey }) =>
        this.createNewOrUpdateExistingInspectionReport(inspection, clientId, jobDate, jobKey));
  }

  public addAuditRecord(record: IAuditRecord) {
    // this.stateManager.getModel('currentUser')
    //   .switchMap(user => {
    //     record.name = user.firstName + ' ' + user.lastName;
    //     return this.dataService.addAuditRecord(record);
    //   })
    //   .skipWhile(newClient => newClient === null)
    //   .pluck<Client, string>('newKey')
    //   .take(1)
    //   .subscribe(key => {
    //     record.key = key;
    //     this.stateManager.update('auditRecords')((state) => {
    //       return state.concat(record)
    //     })

    //   }, error => console.log(error))
  }

  public appStatus(status) {
    this.stateManager.update('appStatus')((currentState) => {
      return status;
    });
  }

  public openInspectionConfigurationItem(item) {
    this.stateManager.update('inspectionConfigurationItem')(() => {
      return item;
    });
  }

  public setLocalDBStatus(status: LocalDbStatus) {
    this.stateManager.update('localDB')(() => {
      return status;
    });
  }

  /****************************************************************
   *
   * Private Methods
   *
   ****************************************************************/

  private updateCommentsOrRecommendation(type, grouping, func) {
    this.stateManager.getModel(type)
      .switchMap(state => {
        const newState = { ...state };
        const result = func(newState);
        return this.dataService.createNewOrUpdateCommentOrRecommendation(result, grouping, type);
      })
      .take(1)
      .subscribe();
  }

  /**
   * updateChimneySortOrder
   *
   * @private
   * @param {IInspection} chimneys
   * @returns {IInspection}
   *
   * @memberOf ActionService
   */
  private updateChimneySortOrder(chimneys: IInspection): IInspection {
    this.chimneyNames.get(chimneys)
      .forEach((key, idx) => {
        if (chimneys[key].inspectionItems) {
          chimneys[key].inspectionItems.forEach(item => item.parentIndex = idx);
        }
      });

    return this.chimneyNames.get(chimneys)
      .reduce((newChimneys, name, index) => {
        return {
          ...newChimneys,
          [`Chimney-${index}`]: chimneys[name],
          clientId: chimneys.clientId,
          date: chimneys.date,
          key: chimneys.key,
          jobKey: chimneys.jobKey,
        };
      }, {}) as IInspection;
  }

  /**
   * saveInspectionReport
   * 
   * @private
   * @param {any} inspectionReport 
   * @param {string} clientId 
   * @param {number} jobDate 
   * @param {string} jobKey 
   * @returns 
   * 
   * @memberof ActionService
   */
  private saveInspectionReport(inspectionReport, clientId: string, jobDate: number, jobKey: string) {
    return this.stateManager.getModel('inspectionReports')
      .map(currentState => {
        const newState = this.utility.clone<IInspectionReport[]>(currentState);
        const key = Object.keys(inspectionReport)[0];
        const id = inspectionReport[key].id;
        let doUpdate = false;
        const recordIndex = this.getRecordIndex(newState, clientId, jobKey);
        let record;

        if (recordIndex !== -1) {
          const hasReports = newState[recordIndex][clientId].reports;
          if (!hasReports) {
            newState[recordIndex][clientId] = { reports: { [jobDate]: {} }, clientId, jobKey, key: jobKey };
          }
          let reports = newState[recordIndex][clientId].reports[jobDate];
          if (id !== undefined) {
            reports = Object.assign({}, reports, { [id]: inspectionReport[key] });
          }
          newState[recordIndex][clientId].reports[jobDate] = reports;
          record = newState[recordIndex];
          doUpdate = true;
        } else {
          record = {
            [clientId]: {
              reports: {
                [jobDate]: { [id]: inspectionReport[key] }
              },
              key: jobKey,
              clientId,
              jobKey
            }
          };

          newState.push(record);

          record = Object.assign({}, record);

          delete record.key;
        }

        return { record, doUpdate };
      })
      .skipWhile(({ record }) => record === undefined)
      .switchMap(({ record, doUpdate }) => {
        return this.dataService.createNewOrUpdateExistingInspectionReport(record, doUpdate)
          .skipWhile(newClient => newClient === null)
          .pluck<Client, string>('newKey')
          .take(1);
      }, ({ record, doUpdate }, fireBaseKey) => {
        record.key = fireBaseKey;
        return record;
      });
  }

  private getRecordIndex(newState, clientId: string, jobKey: string) {
    return newState.findIndex(item => {
      const itemKey = Object.keys(item)[0];
      return item[itemKey].clientId === clientId && item[itemKey].jobKey === jobKey;
    });
  }

  /**
   * load comments
   *
   * @param {IComment} comments
   *
   * @memberOf ActionService
   */
  public loadComments(comments: IComment) {
    this.stateManager.update('comments')((currentState) => {
      return { ...comments };
    });
  }

  private updateLocalModelInspectionCompletionStatus(inspection) {
    this.stateManager.update('inspection')((currentState) => {
      inspection = Object.assign({}, currentState, inspection);

      this.updateOfflineDBIfNeeded('inspection', inspection);

      return inspection;
    });
  }

  private resetInspectionItemsCompletionStatus(_inspectionItems_) {
    const inspectionItems$ = this.utility.clone<IInspection>(_inspectionItems_);

    Object.keys(inspectionItems$)
      .filter(key => key.toLowerCase().includes('chimney-'))
      .forEach(key => {
        const { notValid, inspectionItems } = inspectionItems$[key];
        inspectionItems$[key].notValid = false;
        if (inspectionItems) {
          inspectionItems.forEach(item => item.notValid = false);
        }
      });

    return inspectionItems$;
  }


  private removeInspectionItemFromLocalChimney = (obj: IRemoveApplianceAndChimneyParams) => {
    const { newInspection, itemToRemove, chimney, index } = obj;
    let inspection = this.utility.clone<IInspection>(newInspection);

    switch (itemToRemove.type) {
      case undefined:
        delete inspection[chimney];
        inspection = this.updateChimneySortOrder(inspection);
        break;
      case ApplianceTypes['Flue/Vent']:
        inspection[chimney].inspectionItems = this.removeItemFromInspectionsArray(newInspection, itemToRemove, chimney);
        if (newInspection[chimney].inspectionItems.length === 1) {
          inspection[chimney].inspectionItems.slice(index).every((appliance) => {
            if (appliance.type === ApplianceTypes['Flue/Vent']) {
              return false;
            }

            inspection[chimney].inspectionItems = this.removeItemFromInspectionsArray(newInspection, appliance, chimney);
            return true;
          });
        }
        break;
      default:
        inspection[chimney].inspectionItems = this.removeItemFromInspectionsArray(newInspection, itemToRemove, chimney);
        break;
    }

    return { ...obj, newInspection: inspection };
  }

  private removeItemFromInspectionsArray(newInspection, itemToRemove, chimney) {
    return newInspection[chimney].inspectionItems.filter(item => item.uuid !== itemToRemove.uuid);
  }

  private removeInspectionItemFromRemoteChimney = (obj: IRemoveApplianceAndChimneyParams) => {
    const { newInspection, chimney, index, itemToRemove } = obj;
    let obs$;

    switch (itemToRemove.type) {
      case undefined:
        obs$ = this.dataService.deleteInspectionChimney(newInspection.key, chimney);
        break;
      case ApplianceTypes['Flue/Vent']:
        obs$ = this.setObservableForFlueVent(obj);
        break;

      default:
        obs$ = this.dataService.deleteInspectionItem(newInspection.key, chimney, newInspection[chimney].inspectionItems);
        break;
    }

    return obs$
      .mapTo({ ...obj });

  }

  private setObservableForFlueVent(obj: IRemoveApplianceAndChimneyParams) {
    const { newInspection, chimney, index } = obj;
    if (!newInspection[chimney].inspectionItems.length) {
      return this.dataService.deleteInspectionChimney(newInspection.key, chimney);
    }

    return this.dataService.deleteInspectionItem(newInspection.key, chimney, newInspection[chimney].inspectionItems);
  }

  private reorderAppliances = (obj: IRemoveApplianceAndChimneyParams) => {
    const { newInspection, itemToRemove, chimney } = obj;
    const inspection = this.utility.clone<IInspection>(newInspection);
    if (itemToRemove.type !== undefined) {
      // reset the index count for each appliance
      inspection[chimney].inspectionItems = inspection[chimney].inspectionItems
        .map((appliance, i) => ({ ...appliance, index: i }));

      // remove chimney if no appliances exist under it
      if (!inspection[chimney].inspectionItems.length) {
        delete inspection[chimney];
      }
    }

    return { ...obj, newInspection: inspection };
  }

  private removeInspectionItemFromReport = (obj: IRemoveApplianceAndChimneyParams) => {
    const { newInspection, newInspectionReports, clientId, date, itemToRemove } = obj;
    const inspectionReports = this.utility.clone<IInspectionReport[]>(newInspectionReports);

    if (itemToRemove.type === undefined && itemToRemove.inspectionItems) {
      return this.removeInspectionItemsFromReport(obj, inspectionReports);
    }

    return Observable.of<IInspectionReport[]>(newInspectionReports)
      .switchMap(reports => {
        const foundReport = reports
          .filter(report => this.hasMatchingClientIdAndJobKey(report, newInspection))
          .find(report => report[clientId].reports[date] !== undefined);

        if (foundReport) {
          const { key } = foundReport;
          const items = foundReport[clientId].reports[date];
          const itemToRemoveKey = Object.keys(items).find(itemKey => items[itemKey].uuid === itemToRemove.uuid);
          const foundIndex = inspectionReports.findIndex(r => this.hasMatchingClientIdAndJobKey(r, newInspection));
          delete inspectionReports[foundIndex][clientId].reports[date][itemToRemoveKey];

          if (itemToRemoveKey) {
            return this.dataService.deleteInspectionReportItem(key, clientId, date, parseInt(itemToRemoveKey, 10));
          }

          return Observable.of({});
        }

        return Observable.of({});
      })
      .mapTo({ ...obj, newInspectionReports: inspectionReports });
  }

  private removeInspectionItemsFromReport = (obj: IRemoveApplianceAndChimneyParams, inspectionReports) => {
    const { itemToRemove } = obj;
    return Observable.from(itemToRemove.inspectionItems)
      .map(inspection => this.getInspectionRecordReportData(inspection, obj))
      .mergeMap(this.deleteInspectionReportItem)
      .map(({ itemToRemoveKey }) => this.deleteInspectionReportLocalItem(obj, itemToRemoveKey))
      .do(modInspectionReports => inspectionReports = modInspectionReports)
      .map(() => this.getRequestedDeleteRecordData(obj))
      .switchMap(this.deleteInspectionReportItem)
      .map(({ itemToRemoveKey }) =>
        this.deleteInspectionReportLocalItem({ ...obj, newInspectionReports: inspectionReports }, itemToRemoveKey))
      .do(modInspectionReports => inspectionReports = modInspectionReports)
      .map(() => ({ ...obj, newInspectionReports: inspectionReports }));
  }

  private getInspectionRecordReportData(inspection, obj) {
    const { newInspection, newInspectionReports, clientId, date } = obj;

    const foundReport = newInspectionReports
      .filter(report => this.hasMatchingClientIdAndJobKey(report, newInspection))
      .find(report => report[clientId].reports[date] !== undefined);

    if (foundReport) {
      const { key } = foundReport;
      const items = foundReport[clientId].reports[date];
      const itemToRemoveKey = Object.keys(items).find(itemKey => items[itemKey].uuid === inspection.uuid);

      return { key, clientId, date, itemToRemoveKey };
    }

    return {};

  }

  private getRequestedDeleteRecordData(obj) {
    const { newInspection, newInspectionReports, clientId, date, itemToRemove } = obj;

    const foundReport = newInspectionReports
      .filter(report => this.hasMatchingClientIdAndJobKey(report, newInspection))
      .find(report => report[clientId].reports[date] !== undefined);

    if (foundReport) {
      const { key } = foundReport;
      const items = foundReport[clientId].reports[date];
      const itemToRemoveKey = Object.keys(items).find(itemKey => items[itemKey].uuid === itemToRemove.uuid);

      return { key, clientId, date, itemToRemoveKey };
    }

    return {};

  }

  private deleteInspectionReportLocalItem(obj, itemToRemoveKey) {
    const { newInspection, newInspectionReports, clientId, date } = obj;
    const inspectionReports = this.utility.clone<IInspectionReport[]>(newInspectionReports);
    const foundIndex = inspectionReports.findIndex(r => this.hasMatchingClientIdAndJobKey(r, newInspection));
    if (foundIndex !== -1) {
      delete inspectionReports[foundIndex][clientId].reports[date][itemToRemoveKey];
    }
    return inspectionReports;
  }

  private deleteInspectionReportItem = ({ key, clientId, date, itemToRemoveKey }) => {
    if (itemToRemoveKey) {
      return this.dataService.deleteInspectionReportItem(key, clientId, date, itemToRemoveKey)
        .mapTo({ key, clientId, date, itemToRemoveKey });
    }

    return Observable.of({ key, clientId, date, itemToRemoveKey });
  }

  private hasMatchingClientIdAndJobKey(report, newInspection) {
    const key = Object.keys(report)[0];
    const reportKey = Object.keys(report[key].reports)[0];
    return report[key].clientId === newInspection.clientId &&
      report[key].jobKey === newInspection.jobKey &&
      parseInt(reportKey, 10) === newInspection.date;

  }

  private updateInspection = (obj) => {
    const { newInspection } = obj;
    this.stateManager.update('inspection')(() => newInspection);
  }

  private updateInspectionReports = (obj) => {
    const { newInspectionReports } = obj;
    this.stateManager.update('inspectionReports')(() => newInspectionReports);
  }
}
