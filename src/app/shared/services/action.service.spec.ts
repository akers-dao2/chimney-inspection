import { TestBed, inject } from '@angular/core/testing';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { ActionService } from './action.service';
import { StateManagerService } from 'app/shared/services/sassy-state-manager-ng2/src/state-manager.service';
import { UtilityService } from './utility.service';
import { DataService } from './data.service';
import { FirebaseService } from './firebase.service';
import { ChimneyNamesService } from './chimney-names.service';

import { User } from '../classes/user';
import { Job } from '../classes/job';
import { Client } from '../classes/client';
import { Status } from '../enums/status';

xdescribe('ActionService', () => {
  let service: ActionService;
  let stateManager: StateManagerService;
  let utility: UtilityService;
  let dataService: DataService;
  let firebase: FirebaseService;

  let updateFunc: (params) => void;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ActionService,
        StateManagerService,
        UtilityService,
        DataService,
        FirebaseService,
        ChimneyNamesService
      ]
    });
  });

  beforeEach(inject(
    [
      ActionService,
      StateManagerService,
      UtilityService,
      DataService,
      FirebaseService
    ],
    (
      _service_: ActionService,
      _stateManager_: StateManagerService,
      _utility_: UtilityService,
      _dataService_: DataService,
      _firebase_: FirebaseService,
    ) => {
      service = _service_;
      stateManager = _stateManager_;
      utility = _utility_;
      dataService = _dataService_;
      firebase = _firebase_;

      spyOn(stateManager, 'update').and.callFake(function () {
        const params = Array.from(arguments);
        return updateFunc;
      });

      spyOn(stateManager, 'setModel');

      spyOn(dataService, 'signOut').and.callFake(() => new BehaviorSubject(undefined));

    }));

  it('#setCurrentUser', () => {
    updateFunc = (update) => {

      expect(update({})).toEqual({
        firstName: 'Joe',
        lastName: '',
        email: '',
        password: '',
        createJob: true,
        editJob: false,
        deleteJob: true,
        adminstration: true,
        printJob: false
      });


    };

    const user = new User();
    user.firstName = 'Joe';
    user.lastName = '';
    user.emailAddress = '';
    user.password = '';
    user.createJob = true;
    user.editJob = false;
    user.deleteJob = true;
    user.adminstration = true;
    user.printJob = false;

    service.setCurrentUser(user);
  });

  it('#loadJobs', () => {
    const job = new Job('', '', Status.open, 1, false, false, 1)
    updateFunc = (update) => {
      expect(update([])).toEqual([job]);

    };

    const jobs = [job];

    service.loadJobs(jobs);
  });

  it('#loadCrews', () => {
    updateFunc = (update) => {
      expect(update([])).toEqual([{ name: 'DD&1' }]);

    };

    const crews = [{ name: 'DD&1' }];

    service.loadCrews(crews);
  });

  it('#loadClients', () => {
    const client = new Client('joe', ' ', ' ', ' ', ' ', ' ', ' ');
    client.firstName = 'Joe';
    client.lastName = 'Doe';
    const clients = [client];

    updateFunc = (update) => {
      expect(update([])).toEqual([client]);

    };

    service.loadClients(clients);
  });

  it('#logOutUser', () => {
    service.logOutUser();
    expect(dataService.signOut).toHaveBeenCalled();
    expect(stateManager.setModel).toHaveBeenCalled();
  });
});
