import { Injectable } from '@angular/core';
import PouchDB from 'pouchdb-browser';

@Injectable()
export class PouchDbService {

  public db;

  constructor() { }

  public initialize() {
    this.db = new PouchDB('offlineDB', { size: 100, auto_compaction: true });
    // PouchDB.debug.enable('*');
  }

  public destroy() {
    return new PouchDB('offlineDB').destroy()
      .then(function () {
        console.log('offlineDB destroyed');
      })
      .catch(console.error);
  }
}
