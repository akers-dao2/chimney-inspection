import { Injectable } from '@angular/core';

@Injectable()
export class GetPicturePropService {

  constructor() { }

  public get(nameOfProp) {
    const isRequiredPic = ['front_of_house', 'chimney', 'appliance'].find(name => name === nameOfProp);
    return isRequiredPic ? 'requiredPics' : nameOfProp;
  }
}
