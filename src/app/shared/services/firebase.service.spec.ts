import { TestBed, inject } from '@angular/core/testing';

import { FirebaseService } from './firebase.service';

describe('FirebaseService', () => {
  let service: FirebaseService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FirebaseService]
    });
  });

  beforeEach(inject(
    [FirebaseService],
    (_service_: FirebaseService) => {
      service = _service_;
    }));

  it('should create service', () => {
    expect(service).toBeTruthy();
  });

  it('should provide the firebase version', () => {
    expect(/\d.\d.\d/.test(service.version)).toBeTruthy();
  });

});
