import { TestBed, inject } from '@angular/core/testing';

import { GetOrderListOfInspectionsService } from './get-order-list-of-inspections.service';

describe('GetOrderListOfInspectionsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetOrderListOfInspectionsService]
    });
  });

  it('should be created', inject([GetOrderListOfInspectionsService], (service: GetOrderListOfInspectionsService) => {
    expect(service).toBeTruthy();
  }));
});
