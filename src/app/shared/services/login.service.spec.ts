import { TestBed, inject } from '@angular/core/testing';
import { AlertController } from 'ionic-angular';

import {
  NavParams,
  ItemSliding,
  App, Config,
  Form,
  IonicModule,
  Keyboard,
  DomController,
  MenuController,
  NavController,
  Platform,
  GestureController,
  ModalController
} from 'ionic-angular';

import { ConfigMock, PlatformMock } from '../../shared/mocks/ionic-mocks';
import { StateManagerService } from 'app/shared/services/sassy-state-manager-ng2/src/state-manager.service';
import { UtilityService } from '../../shared/services/utility.service';

/** Services */
import { DataService } from './data.service';
import { ActionService } from './action.service';
import { FirebaseService } from './firebase.service';
import { LoginService } from './login.service';
import { ChimneyNamesService } from './chimney-names.service';

describe('LoginService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        LoginService,
        ActionService,
        FirebaseService,
        DataService,
        AlertController,
        UtilityService,
        App,
        DomController,
        Form,
        Keyboard,
        MenuController,
        NavController,
        GestureController,
        ModalController,
        { provide: NavParams, useClass: () => { } },
        { provide: Platform, useClass: PlatformMock },
        { provide: Config, useClass: ConfigMock },
        StateManagerService,
        ChimneyNamesService
      ]
    });
  });

  it('should ...', inject([LoginService], (service: LoginService) => {
    expect(service).toBeTruthy();
  }));
});
