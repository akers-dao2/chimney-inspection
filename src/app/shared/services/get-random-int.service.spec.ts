import { TestBed, inject } from '@angular/core/testing';

import { GetRandomIntService } from './get-random-int.service';

describe('GetRandomIntService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetRandomIntService]
    });
  });

  it('should ...', inject([GetRandomIntService], (service: GetRandomIntService) => {
    expect(service).toBeTruthy();
  }));
});
