import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { StateManagerService } from 'app/shared/services/sassy-state-manager-ng2/src/state-manager.service';

/** Interfaces */
import { IInspectionReport } from 'app/shared/interfaces/inspection-report';
import { IOpenedInspectionReport } from 'app/shared/interfaces/opened-inspection-report';
import { IClient } from 'app/shared/interfaces/client';

@Injectable()
export class GetOpenedClientInfoService {

  constructor(
    private stateManager: StateManagerService
  ) { }

  /**
   * Returns the client's info for an opened inspection report
   * 
   * @returns 
   * @memberof GetOpenedClientInfoService
   */
  public get() {
    return this.stateManager.getModel('openedInspectionReport')
      .combineLatest<IOpenedInspectionReport, IClient[]>(this.stateManager.getModel('clients'))
      .map(([openedInspectionReport, clients]) => {
        const client = clients.find(client$ => client$.key === openedInspectionReport.clientId);
        return client;
      });
  }

}
