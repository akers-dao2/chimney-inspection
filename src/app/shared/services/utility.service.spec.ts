import { TestBed, inject } from '@angular/core/testing';

import { UtilityService } from './utility.service';

xdescribe('CloneObjectService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UtilityService]
    });
  });

  it('should clone user',
    inject([UtilityService],
      (service: UtilityService) => {
        const user = { name: 'Joe' };
        const cloneUser = service.clone(user);
        expect(user !== cloneUser).toBeTruthy();
      }));

  xit('should create array of an object',
    inject([UtilityService],
      (service: UtilityService) => {
        const usersObj = { name1: 'Joe', name2: 'Joe' };
        const users = service.toArray(usersObj);
        expect(users instanceof Array).toBeTruthy();
        expect(users).toEqual([ 'Joe', 'Joe' ]);
      }));
});
