import { TestBed } from '@angular/core/testing';

import { RemoveImagesService } from './remove-images.service';
import { take } from 'rxjs/operators';
import { StateManagerService } from './sassy-state-manager-ng2/src/state-manager.service';
import { of } from 'rxjs/observable/of';
import { DataService } from './data.service';
import { HttpClientModule } from '@angular/common/http';
import { UtilityService } from './utility.service';
import { FirebaseService } from './firebase.service';
import { FirebasePushIdService } from './firebase-push-id.service';
import { PouchDbService } from './pouch-db.service';

fdescribe('RemoveImagesService', () => {
  let service: RemoveImagesService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule
      ],
      providers: [
        RemoveImagesService,
        StateManagerService,
        DataService,
        UtilityService,
        FirebaseService,
        FirebasePushIdService,
        PouchDbService
      ]
    });

    service = TestBed.get(RemoveImagesService);
    const dataService = TestBed.get(DataService);
    const stateManager = TestBed.get(StateManagerService);
    spyOn(stateManager, 'getModel').and.returnValue(of([]))
    spyOn(dataService, 'getFileMetaDataFromFirebaseStorage').and.returnValue(of({ timeCreated: '8/12/2018' }))
  });

  it('should be created', () => {
    service.execute()
      .pipe(
        take(1)
      )
      .subscribe()

    expect(service).toBeTruthy();
  });
});
