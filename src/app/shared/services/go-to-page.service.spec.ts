import { TestBed, inject } from '@angular/core/testing';

import { GoToPageService } from './go-to-page.service';

describe('GoToPageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GoToPageService]
    });
  });

  it('should ...', inject([GoToPageService], (service: GoToPageService) => {
    expect(service).toBeTruthy();
  }));
});
