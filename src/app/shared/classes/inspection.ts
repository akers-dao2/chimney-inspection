import { IAppliance } from '../../shared/interfaces/appliance';

export interface IChimneyInfo {
    inspectionItems?: IAppliance[];
    name?: string;
    uuid?: string;
    notValid?: boolean;
    clientId?: string;
}

export class Inspection {
    public clientId: string;
    public date: number;
    [chimney: string]: any;
}