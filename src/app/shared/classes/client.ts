import { IClient } from '../interfaces/client';

export class Client implements IClient {

    public key: string;

    constructor(
        public firstName = '',
        public lastName = '',
        public address = '',
        public city = '',
        public state = '',
        public zip = '',
        public township = ''
    ) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.city = city;
        this.state = state;
        this.zip = zip;
        this.township = township || '';
    }

    /**
     * Verifies all data is popluated
     * 
     * @readonly
     * 
     * @memberof Client
     */
    public get isValid() {
        return Object.keys(this)
            .filter(key => !this.fieldsSkipValidate.includes(key))
            .every(key =>
                this[key] !== '' && this[key] !== undefined
            );
    }

    /**
     * A list of fields on the Client class to skip validate
     * 
     * @readonly
     * @private
     * 
     * @memberof Client
     */
    private get fieldsSkipValidate() {
        return ['isValid', 'township'];
    }
}