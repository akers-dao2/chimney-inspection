import { ApplianceTypes } from '../enums/appliances';
import { IApplianceParams } from '../interfaces/appliance-params';
import { IAppliance } from '../interfaces/appliance';
export class Appliance implements IAppliance {
    public name: string;
    public parentIndex: number;
    public index: number;
    public locationIndex: number;
    public type: ApplianceTypes;
    public uuid: string;
    public notValid: boolean;

    constructor({ name, parentIndex, index, type, locationIndex }: IApplianceParams) {
        this.name = name;
        this.parentIndex = parentIndex;
        this.index = index;
        this.locationIndex = locationIndex;
        this.type = type;
        this.uuid = `${parentIndex}${index}${type}${locationIndex}`;
        this.notValid = false;
    }
}
