import { InspectionStatus } from '../enums/inspection-status';
import { ApplianceTypes } from '../enums/appliances';

export class FlueVent {
    public uuid: string;
    public applianceType: ApplianceTypes;
    public flueSizeNAReason: string;

    constructor(
        public id?: number,
        public name?: string,
        public material?: string,
        public outside1?: string,
        public outside2?: string,
        public outside3?: string,
        public outside4?: string,
        public inside1?: string,
        public inside2?: string,
        public inside3?: string,
        public inside4?: string,
        public length?: number,
        public condition?: {
            status: InspectionStatus;
            pictures: string[];
            comments: number[];
            recommendations: number[];
            additionalComments: string[];
        },
        public insulation?: {
            status: InspectionStatus;
            pictures: string[];
            comments: number[];
            recommendations: number[];
            additionalComments: string[];
        },
        public cap?: {
            status: InspectionStatus;
            pictures: string[];
            comments: number[];
            recommendations: number[];
            additionalComments: string[];
        },

    ) {
        this.id = id;
        this.name = name;
        this.material = material;
        this.outside1 = outside1 || null;
        this.outside2 = outside2 || '0';
        this.outside3 = outside3 || null;
        this.outside4 = outside4 || '0';
        this.inside1 = inside1 || null;
        this.inside2 = inside2 || '0';
        this.inside3 = inside3 || null;
        this.inside4 = inside4 || '0';
        this.length = length;
        this.condition = {
            status: null,
            pictures: [],
            comments: null,
            recommendations: null,
            additionalComments: null,
        };
        this.insulation = {
            status: null,
            pictures: [],
            comments: null,
            recommendations: null,
            additionalComments: null,
        };
        this.cap = {
            status: null,
            pictures: [],
            comments: null,
            recommendations: null,
            additionalComments: null,
        };

        this.applianceType = ApplianceTypes['Flue/Vent'];

    }
}