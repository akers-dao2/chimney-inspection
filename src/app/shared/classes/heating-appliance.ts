import { InspectionStatus } from '../enums/inspection-status';
import { ApplianceTypes } from '../enums/appliances';

export class HeatingAppliances {
    public uuid: string;
    public applianceType: ApplianceTypes;

    constructor(
        public id?: string,
        public name?: string,
        public location?: string,
        public type?: string,
        public details?: string,
        public btu_input?: number,
        public gph_input?: number,
        public unit?: string,
        public stackPipe?: {
            status: InspectionStatus;
            pictures: string[];
            pipe_type: number;
            pipe_size: number;
            to_wall: number;
            to_ceiling: number;
            comments: number[];
            recommendations: number[];
            additionalComments: string[];
        },
        public requiredPics?: {
            pictures: string[];
        }
    ) {
        this.id = id;
        this.location = location;
        this.details = details;
        this.type = type;
        this.unit = unit;
        this.name = name;
        this.btu_input = btu_input;
        this.gph_input = gph_input || 0;
        this.stackPipe = {
            status: null,
            pictures: [],
            pipe_type: null,
            pipe_size: null,
            to_wall: null,
            to_ceiling: null,
            comments: null,
            recommendations: null,
            additionalComments: null,
        };
        this.requiredPics = {
            pictures: [],
        };

        this.applianceType = ApplianceTypes['Heating Appliances'];
    }

    public calculateBTU() {
        const gph = isNaN(this.gph_input) ? 0 : this.gph_input;
        const result = gph * 140000;
        return !result ? undefined : result;
    }

    public get missingProperties() {
        return Object.assign({}, {
            stackPipe: {
            status: null,
            pictures: [],
            pipe_type: null,
            pipe_size: null,
            to_wall: null,
            to_ceiling: null,
            comments: null,
            recommendations: null,
            additionalComments: null,
        }
        });
    }
}