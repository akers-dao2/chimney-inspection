import { InspectionStatus } from '../enums/inspection-status';
import { ApplianceTypes } from '../enums/appliances';

export class Stoves {
    public uuid: string;
    public applianceType: ApplianceTypes;

    constructor(
        public id?: number,
        public name?: string,
        public location?: string,
        public fuel?: string,
        public ifInsert?: string,
        public type?: string,
        public stoveCondition?: {
            status: InspectionStatus;
            pictures: string[];
            comments: number[];
            recommendations: number[];
            additionalComments: string[];
        },
        public stovePipe?: {
            status: InspectionStatus;
            pictures: string[];
            pipe_type: number;
            pipe_size: number;
            to_wall: number;
            to_ceiling: number;
            comments: number[];
            recommendations: number[];
            additionalComments: string[];
        },
        public stoveClearances?: {
            status: InspectionStatus;
            pictures: string[];
            back: number;
            right: number;
            left: number;
            comments: number[];
            recommendations: number[];
            additionalComments: string[];
        },
        public hearthPadProtection?: {
            status: InspectionStatus;
            pictures: string[];
            front: number;
            right: number;
            left: number;
            comments: number[];
            recommendations: number[];
            additionalComments: string[];
        },
        public firebrick?: {
            status: InspectionStatus;
            pictures: string[];
            comments: number[];
            recommendations: number[];
            additionalComments: string[];
        },
        public thimble?: {
            status: InspectionStatus;
            pictures: string[];
            comments: number[];
            recommendations: number[];
            additionalComments: string[];
        },
        public requiredPics?: {
            pictures: string[];
        }
    ) {
        this.id = id;
        this.fuel = fuel;
        this.type = type;
        this.name = name;
        this.stoveCondition = {
            status: null,
            pictures: [],
            comments: null,
            recommendations: null,
            additionalComments: null,
        };
        this.stovePipe = {
            status: null,
            pictures: [],
            pipe_type: null,
            pipe_size: null,
            to_wall: null,
            to_ceiling: null,
            comments: null,
            recommendations: null,
            additionalComments: null,
        };
        this.stoveClearances = {
            status: null,
            pictures: [],
            back: null,
            right: null,
            left: null,
            comments: null,
            recommendations: null,
            additionalComments: null,
        };
        this.hearthPadProtection = {
            status: null,
            pictures: [],
            front: null,
            right: null,
            left: null,
            comments: null,
            recommendations: null,
            additionalComments: null,
        };
        this.firebrick = {
            status: null,
            pictures: [],
            comments: null,
            recommendations: null,
            additionalComments: null,
        };
        this.thimble = {
            status: null,
            pictures: [],
            comments: null,
            recommendations: null,
            additionalComments: null,
        };
        this.requiredPics = {
            pictures: []
        };
        this.ifInsert = ifInsert || 'n/a';

        this.applianceType = ApplianceTypes.Stoves;
    }

    public get missingProperties() {
        return Object.assign({}, {
            stovePipe: {
                status: null,
                pictures: [],
                pipe_type: null,
                pipe_size: null,
                to_wall: null,
                to_ceiling: null,
                comments: null,
                recommendations: null,
                additionalComments: null,
            },
            hearthPadProtection: {
                status: null,
                pictures: [],
                front: null,
                right: null,
                left: null,
                comments: null,
                recommendations: null,
                additionalComments: null,
            },
            stoveClearances: {
                status: null,
                pictures: [],
                back: null,
                right: null,
                left: null,
                comments: null,
                recommendations: null,
                additionalComments: null,
            }
        });
    }
}