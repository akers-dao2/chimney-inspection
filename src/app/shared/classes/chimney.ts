import { InspectionStatus } from '../enums/inspection-status';
import { LocationOnHouse } from '../enums/location-on-house';
import { RoofMaterial } from '../enums/roof-material';
import { ApplianceTypes } from '../enums/appliances';

export class Chimney {
    public uuid: string;
    public applianceType: ApplianceTypes;

    constructor(
        public id?: number,
        public location?: string,
        public name?: string,
        public constructionType?: string,
        public crown?: {
            status: InspectionStatus;
            pictures: string[];
            material: string;
            length: number;
            width: number;
            comments: number[];
            recommendations: number[];
            additionalComments: string[];
        },
        public brickStone?: {
            status: InspectionStatus;
            pictures: string[];
            comments: number[];
            recommendations: number[];
            additionalComments: string[];
        },
        public mortar?: {
            status: InspectionStatus;
            pictures: string[];
            comments: number[];
            recommendations: number[];
            additionalComments: string[];
        },
        public stucco?: {
            status: InspectionStatus;
            pictures: string[];
            comments: number[];
            recommendations: number[];
            additionalComments: string[];
        },
        public flashing?: {
            status: InspectionStatus;
            pictures: string[];
            comments: number[];
            recommendations: number[];
            additionalComments: string[];
        },
        public height?: {
            status: InspectionStatus;
            pictures: string[];
            comments: number[];
            recommendations: number[];
            additionalComments: string[];
        },
        public structureIntegrity?: {
            status: InspectionStatus;
            pictures: string[];
            comments: number[];
            recommendations: number[];
            additionalComments: string[];
        },
        public requiredPics?: {
            pictures: string[];
        },
        public locationOnHouse?: LocationOnHouse,
        public flues?: number[],
        public roofMaterial?: RoofMaterial
    ) {
        this.id = id;
        this.location = location;
        this.name = name;
        this.constructionType = constructionType;
        this.crown = {
            status: null,
            pictures: [],
            material: null,
            length: null,
            width: null,
            comments: null,
            recommendations: null,
            additionalComments: null,
        };
        this.brickStone = {
            status: null,
            pictures: [],
            comments: null,
            recommendations: null,
            additionalComments: null,
        };
        this.mortar = {
            status: null,
            pictures: [],
            comments: null,
            recommendations: null,
            additionalComments: null,
        };
        this.stucco = {
            status: null,
            pictures: [],
            comments: null,
            recommendations: null,
            additionalComments: null,
        };
        this.flashing = {
            status: null,
            pictures: [],
            comments: null,
            recommendations: null,
            additionalComments: null,
        };
        this.height = {
            status: null,
            pictures: [],
            comments: null,
            recommendations: null,
            additionalComments: null,
        };
        this.structureIntegrity = {
            status: null,
            pictures: [],
            comments: null,
            recommendations: null,
            additionalComments: null,
        };
        this.requiredPics = {
            pictures: []
        };
        this.locationOnHouse = locationOnHouse;
        this.flues = flues;
        this.roofMaterial = roofMaterial;

        this.applianceType = ApplianceTypes.Chimney;
    }

    public get missingProperties() {
        return Object.assign({}, { crown: {
            status: null,
            pictures: [],
            length: null,
            material: null,
            width: null,
            comments: null,
            recommendations: null,
            additionalComments: null,
        } });
    }

}