import { InspectionStatus } from '../enums/inspection-status';
import { ApplianceTypes } from '../enums/appliances';

export class Fireplace {
    public uuid: string;
    public applianceType: ApplianceTypes;

    constructor(
        public id?: number,
        public name?: string,
        public location?: string,
        public fuelType?: string,
        public fireboxMaterial?: string,
        public firebox?: {
            status: InspectionStatus;
            pictures: string[];
            height_whole: string;
            width_whole: string;
            depth_whole: string;
            square_foot_total: string;
            comments: number[];
            recommendations: number[];
            additionalComments: string[];
        },
        public hearth?: {
            status: InspectionStatus;
            pictures: string[];
            depth: string;
            right: string;
            left: string;
            comments: number[];
            recommendations: number[];
            additionalComments: string[];
        },
        public surroundClearance?: {
            status: InspectionStatus;
            pictures: string[];
            x: string;
            y: string;
            z: string;
            sideZ: string;
            enableXSide: boolean;
            enableYSide: boolean;
            enableZSide: boolean;
            comments: number[];
            recommendations: number[];
            additionalComments: string[];
        },
        public damper?: {
            status: InspectionStatus;
            pictures: string[];
            comments: number[];
            recommendations: number[];
            additionalComments: string[];
        },
        public requiredPics?: {
            pictures: string[];
        },
        public smokeChamber?: {
            status: InspectionStatus;
            pictures: string[];
            comments: number[];
            recommendations: number[];
            additionalComments: string[];
        }
    ) {
        this.id = id;
        this.fuelType = fuelType;
        this.fireboxMaterial = fireboxMaterial;
        this.name = name;
        this.firebox = {
            status: null,
            pictures: [],
            height_whole: null,
            width_whole: null,
            depth_whole: null,
            square_foot_total: null,
            comments: [],
            recommendations: null,
            additionalComments: null,
        };
        this.hearth = {
            status: null,
            pictures: [],
            depth: null,
            right: null,
            left: null,
            comments: [],
            recommendations: null,
            additionalComments: null,
        };
        this.surroundClearance = {
            status: null,
            pictures: [],
            x: null,
            y: null,
            z: null,
            sideZ: null,
            enableXSide: true,
            enableYSide: true,
            enableZSide: true,
            comments: [],
            recommendations: null,
            additionalComments: null,
        };
        this.damper = {
            status: null,
            pictures: [],
            comments: [],
            recommendations: null,
            additionalComments: null,
        };
        this.smokeChamber = {
            status: null,
            pictures: [],
            comments: [],
            recommendations: null,
            additionalComments: null,
        };
        this.requiredPics = {
            pictures: [],
        };

        this.applianceType = ApplianceTypes['Prefab Fireplace'];
    }

    public calculateSQTotal() {
        const height = parseFloat(this.firebox.height_whole);
        const width = parseFloat(this.firebox.width_whole);
        return isNaN(height * width) ? '0' : (height * width).toFixed(2);
    }

    public get missingProperties() {
        return Object.assign({}, {
            surroundClearance: {
                status: null,
                pictures: [],
                x: null,
                y: null,
                z: null,
                sideZ: null,
                enableXSide: true,
                enableYSide: true,
                enableZSide: true,
                comments: null,
                recommendations: null,
                additionalComments: null,
            },
            firebox: {
                status: null,
                pictures: [],
                width_whole: null,
                height_whole: null,
                depth_whole: null,
                square_foot_total: null,
                comments: null,
                recommendations: null,
                additionalComments: null,
            },
            hearth: {
                status: null,
                pictures: [],
                depth: null,
                right: null,
                left: null,
                comments: null,
                recommendations: null,
                additionalComments: null,
            },
        });
    }
}