import { Status } from '../enums/status';
import { IClient } from '../interfaces/client';
import { IJob } from '../interfaces/job';
import { IUser } from '../interfaces/user';

export class Job implements IJob {
    public certification: boolean;
    public signature: string;
    public user: IUser;
    constructor(
        public client: string,
        public assigned: string,
        public status: Status,
        public date: number,
        public inspection: boolean,
        public internal: boolean,
        public order: number,
        public key?: string
    ) {
        this.client = client;
        this.assigned = assigned;
        this.status = status;
        this.date = date;
        this.inspection = inspection;
        this.internal = internal;
        this.order = order;
    }

    public get isValid() {
        return Object.keys(this)
            .filter(key => key !== 'isValid')
            .every(key =>
                this[key] !== '' && this[key] !== undefined
            );
    }
}
