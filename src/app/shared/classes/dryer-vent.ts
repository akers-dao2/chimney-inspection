import { InspectionStatus } from '../enums/inspection-status';
import { ApplianceTypes } from '../enums/appliances'

export class DryerVent {
    public uuid: string;
    public applianceType: ApplianceTypes;

    constructor(
        public id?: number,
        public name?: string,
        public dryerMake?: string,
        public distanceToTermination?: number,
        public flowBeforeCleaning?: string,
        public flowAfterCleaning?: string,
        public numberOfTurns?: number,
        public fuel?: string,
        public dryerType?: string,
        public ductMaterial?: string,
        public terminationLocation?: string,
        public ventCondition?: {
            status: InspectionStatus;
            pictures: string[];
            comments: number[];
            recommendations: number[];
            additionalComments: string[];
        },
        public terminationCondition?: {
            status: InspectionStatus;
            pictures: string[];
            comments: number[];
            recommendations: number[];
            additionalComments: string[];
        },
        public accessibility?: {
            status: InspectionStatus;
            pictures: string[];
            comments: number[];
            recommendations: number[];
            additionalComments: string[];
        },
        public requiredPics?: {
            pictures: string[];
        },
        public other?: {
            status: InspectionStatus;
            pictures: string[];
            comments: number[];
            recommendations: number[];
            additionalComments: string[];
        }
    ) {
        this.id = id;

        this.name = name;
        this.dryerMake = dryerMake;
        this.distanceToTermination = distanceToTermination;
        this.flowBeforeCleaning = flowBeforeCleaning;
        this.flowAfterCleaning = flowAfterCleaning;
        this.numberOfTurns = numberOfTurns;

        this.fuel = fuel;

        this.dryerType = dryerType;

        this.ductMaterial = ductMaterial;

        this.terminationLocation = terminationLocation;

        this.ventCondition = {
            status: null,
            pictures: [],
            comments: null,
            recommendations: null,
            additionalComments: null,
        };

        this.terminationCondition = {
            status: null,
            pictures: [],
            comments: null,
            recommendations: null,
            additionalComments: null,
        };

        this.accessibility = {
            status: null,
            pictures: [],
            comments: null,
            recommendations: null,
            additionalComments: null,
        };

        this.other = {
            status: null,
            pictures: [],
            comments: null,
            recommendations: null,
            additionalComments: null,
        };

        this.requiredPics = {
            pictures: [],
        };

        this.applianceType = ApplianceTypes['Dryer Vent']

    }

    public get missingProperties() {
        return Object.assign({});
    }
}