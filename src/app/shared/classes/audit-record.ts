import { IAuditRecord } from '../interfaces/audit-record';

export class AuditRecord implements IAuditRecord {
    public key: string;

    constructor(
        public description: string,
        public page: string,
        public date: number = new Date().valueOf(),
        public name?: string,
    ) { }
}