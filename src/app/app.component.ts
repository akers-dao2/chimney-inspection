import { Component, ViewChild, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';

/** Services */
import { ActionService } from 'app/shared/services/action.service';
import { DataService } from 'app/shared/services/data.service';
import { StateManagerService } from 'app/shared/services/sassy-state-manager-ng2/src/state-manager.service';
import { LoaderService } from 'app/shared/services/loader.service';
import { LoginService } from 'app/shared/services/login.service';
import { PouchDbService } from './shared/services/pouch-db.service';


import { SideMenuComponent } from './pages/side-menu/side-menu.component';
import { JobListComponent } from './pages/job/job-list/job-list.component';

import { IUser } from 'app/shared/interfaces/user';

import { NavController } from 'ionic-angular';

@Component({
  selector: 'ion-app',
  template: `
  <ion-split-pane when="lg" [ngStyle]="{filter: isBlur ? 'blur(4px)' : 'blur(0px)'}">
    <!--  our side menu  -->
    <ion-menu [content]="content" [persistent]="true">
      <ion-header>
        <ion-toolbar color="header">
          <ion-title></ion-title>
        </ion-toolbar>
      </ion-header>
      <ion-content>
        <wells-side-menu [nav]="nav"></wells-side-menu>
      </ion-content>
    </ion-menu>
    <!-- the main content -->
    <ion-nav [root]="rootPage" main #content></ion-nav>
  </ion-split-pane>
  `,
})
export class AppComponent implements OnInit {
  @ViewChild('content') nav: NavController;
  rootPage = JobListComponent;
  public isBlur = true;

  constructor(
    private stateManager: StateManagerService,
    private loader: LoaderService,
    private actionService: ActionService,
    private dataService: DataService,
    private loginService: LoginService,
    private pouchDB: PouchDbService,
  ) {

  }

  async ngOnInit() {
    this.loader.show().then(() => {
      this.stateManager.getModel('currentUser')
        .map(currentUser => !Object.keys(currentUser).length)
        .distinctUntilChanged((x, y) => x === y)
        .delay(1)
        .subscribe(result => {
          this.isBlur = result;
          if (!result) {
            this.loader.hide();
          }
        });
    });

    // pull localstorage credentials
    const savedUser: IUser = this.loginService.retrieveCredentialsLocally;

    // if there is no login user or saved user then display login
    this.stateManager.getModel('currentUser')
      .filter(currentUser => {
        return !Object.keys(currentUser).length && !Object.keys(savedUser).length;
      })
      .subscribe(() => {
        this.loginService.displayLogin();
      });

    // if localstorage credentials exist log user in automatically
    if (!!Object.keys(savedUser).length) {
      if (savedUser.online) {
        this.dataService.signIn(savedUser.emailAddress, savedUser.password)
          .skipWhile(resp => resp === undefined)
          .switchMapTo(this.loginService.setCurrentUser(savedUser))
          .take(1)
          .subscribe(() => this.actionService.initialDataAppLoad());
      } else {
        this.pouchDB.initialize();
        await this.actionService.installLocalDB();
        this.actionService.initialDataAppLoad();
        this.actionService.setCurrentUser(savedUser);
      }

    }
  }
}
