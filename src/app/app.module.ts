import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { Observable } from 'rxjs/Observable';

/** Modules */
import { SharedModule } from './shared/shared.module';
import { IonicApp, IonicModule } from 'ionic-angular';

/** Services */
import { FirebaseService } from './shared/services/firebase.service';
import { DataService } from './shared/services/data.service';
import { ActionService } from './shared/services/action.service';
import { UtilityService } from './shared/services/utility.service';
import { InspectionItemService } from './shared/services/inspection-item.service';
import { ChimneyNamesService } from './shared/services/chimney-names.service';
import { PdfMakerService } from './shared/services/pdf-maker.service';
import { LoginService } from './shared/services/login.service';
import { GoToPageService } from './shared/services/go-to-page.service';
import { LoaderService } from './shared/services/loader.service';
import { GetRandomIntService } from './shared/services/get-random-int.service';
import { StateManagerService } from 'app/shared/services/sassy-state-manager-ng2/src/state-manager.service';
import { HandleStatusPanesService } from './shared/services/handle-status-panes.service';
import { ChangeInspectionItemNameService } from './shared/services/change-inspection-item-name.service';
import { InspectionItemNameService } from './shared/services/inspection-item-name.service';
import { DeletePhotoService } from './shared/services/delete-photo.service';
import { GetPicturePropService } from './shared/services/get-picture-prop.service';
import { FractionsService } from './shared/services/fractions.service';
import { ValidateInspectionReportService } from './shared/services/validate-inspection-report.service';
import { NotApplicablePaneResetService } from './shared/services/not-applicable-pane-reset.service';
import { PresentToastService } from './shared/services/present-toast.service';
import { SortService } from './shared/services/sort.service';
import { GetOpenedClientInfoService } from './shared/services/get-opened-client-info.service';
import { GetOpenedInspectionReportService } from './shared/services/get-opened-inspection-report.service';
import { GetOrderListOfInspectionsService } from './shared/services/get-order-list-of-inspections.service';
import { GetInspectItemListService } from './shared/services/get-inspect-item-list.service';
import { GetJobDateInspectItemsService } from './shared/services/get-job-date-inspect-items.service';
import { FirebasePushIdService } from './shared/services/firebase-push-id.service';
import { PouchDbService } from './shared/services/pouch-db.service';
import { OfflineStorageService } from './shared/services/offline-storage.service';
import { PdfGeneratorService } from './shared/services/pdf-generator.service';

import { CrewNamePipe } from './shared/pipes/crew-name.pipe';
import { ClientNamePipe } from './shared/pipes/client-name.pipe';

/** Components */
import { AppComponent } from './app.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { SideMenuComponent } from './pages/side-menu/side-menu.component';
import { JobListComponent } from './pages/job/job-list/job-list.component';
import { ClientListComponent } from './pages/client/client-list/client-list.component';
import { SearchModalComponent } from './pages/search-modal/search-modal.component';
import { InspectionSheetComponent } from './pages/inspection/inspection-sheet/inspection-sheet.component';
import { PaneComponent } from './shared/components/pane/pane.component';
import { PaneCommentComponent } from './shared/components/pane/pane-comment/pane-comment.component';
import { CreateInspectionComponent } from './pages/inspection/create-inspection/create-inspection.component';
import { SignaturePadComponent } from './pages/inspection/signature-pad/signature-pad.component';
import { AddClientComponent } from './pages/client/add-client/add-client.component';
import { AddUserComponent } from './pages/admin/add-or-edit-user/add-or-edit-user.component';
import { UsersComponent } from './pages/admin/users/users.component';
import { EditClientComponent } from './pages/client/edit-client/edit-client.component';
import { JobHeaderMenuComponent } from './pages/job/job-list/job-header-menu/job-header-menu.component';
import { ViewInspectionComponent } from './pages/inspection/view-inspection/view-inspection.component';
import { CreateJobComponent } from './pages/job/create-job/create-job.component';
import { CrewComponent } from './pages/admin/crew/crew.component';
import { TownshipLoaderComponent } from './pages/admin/township-loader/township-loader.component';
import { PDFServerIPComponent } from './pages/admin/pdf-server-ip/pdf-server-ip.component';
import { ChimneyComponent } from './pages/inspection/inspection-sheet/chimney/chimney.component';
import { FlueVentComponent } from './pages/inspection/inspection-sheet/flue-vent/flue-vent.component';
import { DryerVentComponent } from './pages/inspection/inspection-sheet/dryer-vent/dryer-vent.component';
import { MasonryFireplaceComponent } from './pages/inspection/inspection-sheet/masonry-fireplace/masonry-fireplace.component';
import { WoodStoveInsertComponent } from './pages/inspection/inspection-sheet/wood-stove-insert/wood-stove-insert.component';
import { HeatingApplianceComponent } from './pages/inspection/inspection-sheet/heating-appliance/heating-appliance.component';
import { BoroTownshipSelectComponent } from './pages/client/boro-township-select/boro-township-select.component';
import { DesktopCalendarComponent } from './shared/components/desktop-calendar/desktop-calendar.component';
import { ReportComponent } from './pages/report/report.component';
import { AuditReportComponent } from './pages/audit-report/audit-report.component';
import { DateHeaderComponent } from './pages/job/job-list/date-header/date-header.component';
import { CommentsAndRecommendationComponent } from './pages/admin/comments-and-recommendation/comments-and-recommendation.component';
import { RemovePictureService } from './shared/services/remove-picture.service';
import { RemoveImagesService } from './shared/services/remove-images.service';
import { InspectionConfigurationComponent } from './pages/admin/inspection-configuration/inspection-configuration.component';
import { InspectionConfigurationItemService } from './shared/services/inspection-configuration-item.service';
import { ReportConflictManagerComponent } from './pages/admin/report-conflict-manager/report-conflict-manager.component';
import { NaReasonComponent } from './shared/components/pane/na-reason/na-reason.component';
import { environment } from 'environments/environment';
import { PurgeComponent } from './pages/purge/purge.component';


@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    SideMenuComponent,
    JobListComponent,
    SearchModalComponent,
    InspectionSheetComponent,
    PaneComponent,
    PaneCommentComponent,
    CreateInspectionComponent,
    SignaturePadComponent,
    AddClientComponent,
    ClientListComponent,
    SearchModalComponent,
    AddUserComponent,
    UsersComponent,
    EditClientComponent,
    JobHeaderMenuComponent,
    ViewInspectionComponent,
    CreateJobComponent,
    CrewComponent,
    TownshipLoaderComponent,
    ChimneyComponent,
    FlueVentComponent,
    MasonryFireplaceComponent,
    WoodStoveInsertComponent,
    HeatingApplianceComponent,
    DryerVentComponent,
    BoroTownshipSelectComponent,
    DesktopCalendarComponent,
    ReportComponent,
    AuditReportComponent,
    DateHeaderComponent,
    PDFServerIPComponent,
    CommentsAndRecommendationComponent,
    InspectionConfigurationComponent,
    ReportConflictManagerComponent,
    NaReasonComponent,
    PurgeComponent
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(AppComponent),
    SharedModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [
    FirebaseService,
    DataService,
    ActionService,
    UtilityService,
    StateManagerService,
    InspectionItemService,
    ChimneyNamesService,
    PdfMakerService,
    LoginService,
    ClientNamePipe,
    CrewNamePipe,
    PresentToastService,
    GetRandomIntService,
    GoToPageService,
    SortService,
    LoaderService,
    HandleStatusPanesService,
    ChangeInspectionItemNameService,
    InspectionItemNameService,
    DeletePhotoService,
    GetPicturePropService,
    FractionsService,
    ValidateInspectionReportService,
    NotApplicablePaneResetService,
    GetOpenedClientInfoService,
    GetOpenedInspectionReportService,
    GetOrderListOfInspectionsService,
    GetInspectItemListService,
    GetJobDateInspectItemsService,
    FirebasePushIdService,
    PouchDbService,
    OfflineStorageService,
    PdfGeneratorService,
    RemovePictureService,
    RemoveImagesService,
    InspectionConfigurationItemService
  ],
  entryComponents: [
    HomePageComponent,
    SideMenuComponent,
    JobListComponent,
    SearchModalComponent,
    InspectionSheetComponent,
    PaneComponent,
    PaneCommentComponent,
    CreateInspectionComponent,
    SignaturePadComponent,
    AddClientComponent,
    ClientListComponent,
    AddUserComponent,
    UsersComponent,
    EditClientComponent,
    JobHeaderMenuComponent,
    ViewInspectionComponent,
    CreateJobComponent,
    CrewComponent,
    TownshipLoaderComponent,
    ChimneyComponent,
    FlueVentComponent,
    MasonryFireplaceComponent,
    WoodStoveInsertComponent,
    HeatingApplianceComponent,
    DryerVentComponent,
    BoroTownshipSelectComponent,
    DesktopCalendarComponent,
    ReportComponent,
    AuditReportComponent,
    DateHeaderComponent,
    PDFServerIPComponent,
    CommentsAndRecommendationComponent,
    InspectionConfigurationComponent,
    ReportConflictManagerComponent,
    NaReasonComponent,
    PurgeComponent
    ],
  bootstrap: [IonicApp]
})

export class AppModule {
  constructor(
    private firebase: FirebaseService,
    private actions: ActionService,
    private pouchDB: PouchDbService,
    private stateManager: StateManagerService,
    private dataService: DataService
  ) {
    this.firebase.initialize();
    this.actions.setModels();
  }

  onStatusChange(result: Event) {
    this.actions.appStatus({ online: navigator.onLine });
  }

}
