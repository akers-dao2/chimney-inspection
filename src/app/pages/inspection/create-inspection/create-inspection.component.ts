import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/filter';

import { ActionService } from '../../../shared/services/action.service';
import { DataService } from '../../../shared/services/data.service';
import { UtilityService } from '../../../shared/services/utility.service';
import { ChimneyNamesService } from '../../../shared/services/chimney-names.service';
import { GoToPageService } from '../../../shared/services/go-to-page.service';
import { StateManagerService } from 'app/shared/services/sassy-state-manager-ng2/src/state-manager.service';

import { NavController } from 'ionic-angular';
import { ItemSliding } from 'ionic-angular';
import { NavParams } from 'ionic-angular';

import { IChimneyItem } from '../../../shared/interfaces/chimney-item';
import { SideMenuType } from '../../../shared/interfaces/side-menu-type';
import { IInspectionItem } from '../../../shared/interfaces/inspection-item';
import { IInspection } from '../../../shared/interfaces/inspection';

import { ViewInspectionComponent } from '../view-inspection//view-inspection.component';

import { JobListComponent } from '../../job/job-list/job-list.component';
import { InspectionSheetComponent } from '../inspection-sheet/inspection-sheet.component';

@Component({
  selector: 'wells-create-inspection',
  templateUrl: './create-inspection.component.html',
  styleUrls: ['./create-inspection.component.scss']
})
export class CreateInspectionComponent implements OnInit {

  public inspections$: any;
  public updateMode = false;

  private clientId: string;
  private date: number;
  private jobKey: string;
  private editInspection: boolean;

  constructor(
    private actionService: ActionService,
    private navCtrl: NavController,
    private stateManager: StateManagerService,
    private dataService: DataService,
    private navParams: NavParams,
    private utility: UtilityService,
    private chimneyNames: ChimneyNamesService,
    private goToPage: GoToPageService
  ) { }

  /****************************************************************
   * 
   * Public Methods
   *
   ****************************************************************/

  /**
   * OnInit life cycle hook
   * 
   * 
   * @memberOf CreateInspectionComponent
   */
  public ngOnInit() {
    this.clientId = this.navParams.get('clientId');
    this.date = this.navParams.get('date');
    this.jobKey = this.navParams.get('jobKey');
    this.editInspection = this.navParams.get('edit');

    // load inspection if one exist
    this.dataService.getInspections()
      .switchMap(inspection => inspection)
      .filter(inspection =>
        inspection.clientId === this.clientId &&
        inspection.date === this.date &&
        (inspection.jobKey ? inspection.jobKey === this.jobKey : true)
      )
      .take(1)
      .subscribe(inspection => {
        this.actionService.loadInspection(inspection);
        this.updateMode = true;
      });

    this.inspections$ = this.stateManager.getModel('inspection')
      .map<any, IChimneyItem[]>(chimneys => {
        return this.chimneyNames.get(chimneys)
          .filter(key => !(key === 'clientId' || key === 'date' || key === 'key' || key === 'jobKey'))
          .map((key, index) => Object.assign({},
            {
              name: chimneys[key].name || key,
              uuid: index.toString(),
              index,
              appliances: chimneys[key].inspectionItems,
              notValid: false
            })
          );
      });

  }

  /**
   * Handle button click
   *
   * @param {buttonName} string
   * @param {SideMenuType} SideMenuType
   *
   * @memberOf CreateInspectionComponent
   */
  public onButtonClick(buttonName: string, sideMenuType: SideMenuType) {
    switch (buttonName.toLowerCase()) {
      case 'create/update':
        this.generateInspection(sideMenuType);
        break;
      case 'cancel':
        this.actionService.changeSideMenu('standardMenu');
        this.actionService.resetInspection();
        this.goToPage.execute(JobListComponent);
        break;

      default:
        if (this.editInspection) {
          this.actionService.changeSideMenu('inspectionMenu');
          this.stateManager.getModel('openedInspection')
            .take(1)
            .subscribe(inspectionItem => {
              this.goToPage.execute(InspectionSheetComponent, inspectionItem);
              this.actionService.setActiveMenuItem(inspectionItem);
            });

        } else {
          this.actionService.changeSideMenu(sideMenuType);
          this.actionService.resetInspection();
          this.goToPage.execute(JobListComponent);
        }

        break;
    }

  }

  /**
   * Remove inspection item
   * 
   * @param {ItemSliding} slidingItem 
   * @param {IInspectionItem} applicance 
   * 
   * @memberOf CreateInspectionComponent
   */
  public removeAppliance(slidingItem: ItemSliding, appliance: IInspectionItem) {
    const { index, parentIndex } = appliance;
    this.actionService.removeAppliance(parentIndex, index);
  }

  /**
   * Update appliance name
   * 
   * @param {string} appliance 
   * @param {IInspectionItem} applicance 
   * 
   * @memberOf CreateInspectionComponent
   */
  public updateApplianceName(applianceName: string, appliance: IInspectionItem) {
    const { index, parentIndex } = appliance;
    this.actionService.updateApplianceName(parentIndex, index, applianceName);
  }

  /**
   * Generate Inspection
   * 
   * @private
   * @param {any} sideMenuType 
   * 
   * @memberof CreateInspectionComponent
   */
  private generateInspection(sideMenuType) {
    this.stateManager.getModel('inspection')
      .filter(inspection => !!Object.keys(inspection).length)
      .map((inspection: IInspection) => {

        const newInsp = this.utility.clone(inspection);

        newInsp.clientId = this.clientId;
        newInsp.date = this.date;
        newInsp.jobKey = this.jobKey;
        return newInsp;
      })
      .switchMap(inspection => {
        return this.dataService.createNewOrUpdateExistingInspection(inspection, this.updateMode)
          .skipWhile(newInspection => newInspection === null);
      }, (inspection, resp) => {
        inspection.key = resp.newKey;
        return { inspection, resp };
      })
      .take(1)
      .subscribe(({ inspection, resp }) => {

        // either adds 1 or more inspection item(s) to the inspections model
        // this is a list of all inspections items from DB
        this.actionService.addInspectionToList(inspection);

        // load inspection items for a specific client
        this.actionService.loadInspection(inspection);

        // set side menu type
        this.actionService.changeSideMenu(sideMenuType);
        this.goToPage.execute(InspectionSheetComponent, { name: inspection['Chimney-0'].name, uuid: inspection['Chimney-0'].uuid });

        // store the client's info in state manager
        // this.actionService.openedInspectionReport(clientId, key, date);
      }, (error) => console.log(error));
  }
}
