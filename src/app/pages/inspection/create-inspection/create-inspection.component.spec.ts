import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpModule } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import {
  NavParams,
  ItemSliding,
  App, Config,
  Form,
  IonicModule,
  Keyboard,
  DomController,
  MenuController,
  NavController,
  Platform,
  GestureController
} from 'ionic-angular';

import { CreateInspectionComponent } from './create-inspection.component';

import { ActionService } from 'app/shared/services/action.service';
import { DataService } from 'app/shared/services/data.service';
import { UtilityService } from 'app/shared/services/utility.service';
import { ChimneyNamesService } from 'app/shared/services/chimney-names.service';
import { FirebaseService } from 'app/shared/services/firebase.service';
import { StateManagerService } from 'app/shared/services/sassy-state-manager-ng2/src/state-manager.service';

import { ConfigMock, PlatformMock } from 'app/shared/mocks/ionic-mocks';

describe('CreateInspectionComponent', () => {
  let component: CreateInspectionComponent;
  let fixture: ComponentFixture<CreateInspectionComponent>;
  let actionService: ActionService,
    dataService: DataService,
    chimneyNames: ChimneyNamesService,
    stateManager: StateManagerService,
    navParams: NavParams;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CreateInspectionComponent
      ],
      providers: [
        ActionService,
        DataService,
        UtilityService,
        ChimneyNamesService,
        StateManagerService,
        FirebaseService,
        App,
        DomController,
        Form,
        Keyboard,
        MenuController,
        NavController,
        GestureController,
        { provide: NavParams, useClass: () => { } },
        { provide: Platform, useClass: PlatformMock },
        { provide: Config, useClass: ConfigMock }
      ],
      imports: [
        HttpModule,
        IonicModule
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ]
    })
      .compileComponents();
  }));

  beforeEach(
    inject([
      ActionService,
      DataService,
      ChimneyNamesService,
      StateManagerService,
      NavParams
    ], (
      _actionService_: ActionService,
      _dataService_: DataService,
      _chimneyNames_: ChimneyNamesService,
      _stateManager_: StateManagerService,
      _navParams_: NavParams
    ) => {
        actionService = _actionService_;
        dataService = _dataService_;
        chimneyNames = _chimneyNames_;
        stateManager = _stateManager_;
        navParams = _navParams_;

        spyOn(stateManager, 'getModel').and.returnValue(new BehaviorSubject({}))

        fixture = TestBed.createComponent(CreateInspectionComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
      }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
