import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeatingApplianceComponent } from './heating-appliance.component';

describe('HeatingApplianceComponent', () => {
  let component: HeatingApplianceComponent;
  let fixture: ComponentFixture<HeatingApplianceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeatingApplianceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeatingApplianceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
