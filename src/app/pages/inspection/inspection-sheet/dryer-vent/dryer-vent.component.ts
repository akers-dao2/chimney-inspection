import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { DryerVent } from '../../../../shared/classes';

import { HandleStatusPanesService } from 'app/shared/services/handle-status-panes.service';
import { ChangeInspectionItemNameService } from 'app/shared/services/change-inspection-item-name.service';
import { DeletePhotoService } from 'app/shared/services/delete-photo.service';
import { NotApplicablePaneResetService } from 'app/shared/services/not-applicable-pane-reset.service';
import { ActionService } from 'app/shared/services/action.service';
import { GetPicturePropService } from 'app/shared/services/get-picture-prop.service';

import { CommentOrRecommendationType } from 'app/shared/enums/comment-or-rec-types';

import { IPhoto } from 'app/shared/interfaces/photo';
import { ICommentEvent } from 'app/shared/interfaces/commentEvent';
import { RemovePictureService } from '../../../../shared/services/remove-picture.service';

@Component({
  selector: 'wells-dryer-vent',
  templateUrl: './dryer-vent.component.html',
  styleUrls: ['./dryer-vent.component.scss']
})
export class DryerVentComponent implements OnInit {
  public CommentOrRecType = CommentOrRecommendationType;

  private origName: string;
  @Input() public dryerVent: DryerVent;
  @Output() public output = new EventEmitter<any>();

  constructor(
    private handleStatusPanes: HandleStatusPanesService,
    private changeInspectionItemName: ChangeInspectionItemNameService,
    private deletePhoto: DeletePhotoService,
    private notApplicablePaneReset: NotApplicablePaneResetService,
    private actions: ActionService,
    private getPictureProp: GetPicturePropService,
    private removePicture: RemovePictureService
  ) { }

  ngOnInit() {
    // store the original name of the inspection item
    // this original name is used for pulling the item from the inspection model
    // the original name is updated on each successful name change
    this.origName = this.dryerVent.name;
  }

  /**
   * Handle input/select change events
   * 
   * @param {string} [prop] 
   * 
   * @memberof DryerVentComponent
   */
  public onChange(prop?: string) {
    // emit changes to InspectionSheet Component
    this.output.emit({ dryerVent: this.dryerVent });

    this.changeInspectionItemName.execute(this.dryerVent.name, this.origName, prop, this.dryerVent.uuid, 'dryerVent')
      .skipWhile(name => name === undefined)
      .take(1)
      .subscribe((name) =>
        // the original name is updated on each successful name change
        this.origName = name
      );

  }

  /**
   * Handle status change events for pane component
   * 
   * @param {any} $event 
   * 
   * @memberof DryerVentComponent
   */
  public handleStatusOfPanes($event) {
    this.handleStatusPanes.updateStatus($event)
      .take(1)
      .subscribe((result) => {
        // update status on dryerVent
        if (typeof result === 'number') {
          this.dryerVent[$event.value].status = result;
          this.dryerVent = this.notApplicablePaneReset.execute(this.dryerVent, DryerVent);
          this.onChange();
        } else if (result) {
          // add pictures prop if it does not exist
          if (this.dryerVent[$event.value].pictures === undefined) {
            this.dryerVent[$event.value] = Object.assign({}, this.dryerVent[$event.value], { pictures: [] });
          }

          this.dryerVent[$event.value].pictures = this.dryerVent[$event.value].pictures.concat(result);

          this.actions.addOrRemoveItemOfflineStorage(result);

          this.onChange();
        }
      });
  }

  public handleDeletePhoto(photo: IPhoto) {
    this.deletePhoto.get(photo)
      .skipWhile(prop => prop === undefined)
      .take(1)
      .subscribe(async prop => {
        const { pictures } = this.dryerVent[prop];
        this.dryerVent[prop].pictures = this.removePicture.execute(pictures, photo);

        await this.actions.addOrRemoveItemOfflineStorage(photo, true);

        this.onChange();
      });
  }

  /**
   * handle adding Comments And Recommendations to class
   * 
   * @param {ICommentEvent} data 
   * @memberof dryerVentComponent
   */
  public handleCommentsAndRec(data: ICommentEvent) {
    const typeOfComRec = CommentOrRecommendationType[data.typeOfComRec];
    this.dryerVent[typeOfComRec]['comments'] = data.comments;
    this.dryerVent[typeOfComRec]['recommendations'] = data.recommendations;
    this.dryerVent[typeOfComRec]['additionalComments'] = data.additionalComments;
  }
}
