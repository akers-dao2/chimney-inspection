import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DryerVentComponent } from './dryer-vent.component';

describe('DryerVentComponent', () => {
  let component: DryerVentComponent;
  let fixture: ComponentFixture<DryerVentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DryerVentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DryerVentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
