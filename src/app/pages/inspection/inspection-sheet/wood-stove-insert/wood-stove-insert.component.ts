import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Stoves } from '../../../../shared/classes';

import { HandleStatusPanesService } from 'app/shared/services/handle-status-panes.service';
import { ChangeInspectionItemNameService } from 'app/shared/services/change-inspection-item-name.service';
import { DeletePhotoService } from 'app/shared/services/delete-photo.service';
import { NotApplicablePaneResetService } from 'app/shared/services/not-applicable-pane-reset.service';
import { ActionService } from 'app/shared/services/action.service';
import { GetPicturePropService } from 'app/shared/services/get-picture-prop.service';

import { CommentOrRecommendationType } from 'app/shared/enums/comment-or-rec-types';

import { IPhoto } from 'app/shared/interfaces/photo';
import { ICommentEvent } from 'app/shared/interfaces/commentEvent';
import { RemovePictureService } from '../../../../shared/services/remove-picture.service';

@Component({
  selector: 'wells-wood-stove-insert',
  templateUrl: './wood-stove-insert.component.html',
  styleUrls: ['./wood-stove-insert.component.scss']
})
export class WoodStoveInsertComponent implements OnInit {
  public CommentOrRecType = CommentOrRecommendationType;

  private origName: string;
  @Input() public stoves: Stoves;
  @Output() public output = new EventEmitter<any>();

  constructor(
    private handleStatusPanes: HandleStatusPanesService,
    private changeInspectionItemName: ChangeInspectionItemNameService,
    private deletePhoto: DeletePhotoService,
    private notApplicablePaneReset: NotApplicablePaneResetService,
    private actions: ActionService,
    private getPictureProp: GetPicturePropService,
    private removePicture: RemovePictureService
  ) { }

  ngOnInit() {
    // store the original name of the inspection item
    // this original name is used for pulling the item from the inspection model
    // the original name is updated on each successful name change
    this.origName = this.stoves.name;
  }

  /**
   * Handle input/select change events
   * 
   * @param {string} [prop] 
   * 
   * @memberof StovesComponent
   */
  public onChange(prop?: string) {
    // emit changes to InspectionSheet Component
    this.output.emit({ stoves: this.stoves });

    this.changeInspectionItemName.execute(this.stoves.name, this.origName, prop, this.stoves.uuid, 'stoves')
      .skipWhile(name => name === undefined)
      .take(1)
      .subscribe((name) =>
        // the original name is updated on each successful name change
        this.origName = name
      );

  }

  /**
   * Handle status change events for pane component
   * 
   * @param {any} $event 
   * 
   * @memberof StovesComponent
   */
  public handleStatusOfPanes($event) {
    this.handleStatusPanes.updateStatus($event)
      .take(1)
      .subscribe((result) => {
        // update status on stoves
        if (typeof result === 'number') {
          this.stoves[$event.value].status = result;
          this.stoves = this.notApplicablePaneReset.execute(this.stoves, Stoves);
          this.onChange();
        } else if (result) {
          // add pictures prop if it does not exist
          if (this.stoves[$event.value].pictures === undefined) {
            this.stoves[$event.value] = Object.assign({}, this.stoves[$event.value], { pictures: [] });
          }

          this.stoves[$event.value].pictures = this.stoves[$event.value].pictures.concat(result);

          this.actions.addOrRemoveItemOfflineStorage(result);

          this.onChange();
        }
      });
  }

  public get locations() {
    return [
      'Basement',
      'Living Room',
      'Family Room',
      'Den',
      'Dining Room',
      'Kitchen',
      '1st Floor',
      'Bedroom',
      'Garage',
      '2nd Floor',
      '3rd Floor',
      'Attic',
      'Porch',
      'Patio',
      'Foyer',
    ];
  }

  public get pipeTypes() {
    return [
      'Black - Single Wall',
      'Black - DVL',
      'PVP',
      'Stainless Steel',
      'Class A - Double Wall',
    ];
  }

  public get pipeSizes() {
    return [
      '3 in. Round',
      '4 in. Round',
      '5 in. Round',
      '6 in. Round',
      '7 in. Round',
      '8 in. Round',
      '9 in. Round',
      '10 in. Round',
      '11 in. Round',
      '12 in. Round',
      '13 in. Round',
      '14 in. Round',
      '15 in. Round'
    ];
  }

  public handleDeletePhoto(photo: IPhoto) {
    this.deletePhoto.get(photo)
      .skipWhile(prop => prop === undefined)
      .take(1)
      .subscribe(async prop => {
        const { pictures } = this.stoves[prop];
        this.stoves[prop].pictures = this.removePicture.execute(pictures, photo);

        await this.actions.addOrRemoveItemOfflineStorage(photo, true);

        this.onChange();
      });
  }


  /**
   * handle adding Comments And Recommendations to class
   * 
   * @param {ICommentEvent} data 
   * @memberof ChimneyComponent
   */
  public handleCommentsAndRec(data: ICommentEvent) {
    const typeOfComRec = CommentOrRecommendationType[data.typeOfComRec];
    this.stoves[typeOfComRec]['comments'] = data.comments;
    this.stoves[typeOfComRec]['recommendations'] = data.recommendations;
    this.stoves[typeOfComRec]['additionalComments'] = data.additionalComments;
  }
}
