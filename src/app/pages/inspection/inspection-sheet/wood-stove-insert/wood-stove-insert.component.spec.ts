import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WoodStoveInsertComponent } from './wood-stove-insert.component';

describe('WoodStoveInsertComponent', () => {
  let component: WoodStoveInsertComponent;
  let fixture: ComponentFixture<WoodStoveInsertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WoodStoveInsertComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WoodStoveInsertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
