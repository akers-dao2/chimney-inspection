import { Component, OnInit, OnDestroy } from '@angular/core';
import { Form } from '@angular/forms';
import { NavController } from 'ionic-angular';
import { NavParams } from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { ISubscription } from 'rxjs/Subscription';
import 'rxjs/add/observable//combineLatest';

import { InspectionStatus } from '../../../shared/enums/inspection-status';
import { ApplianceTypes } from '../../../shared/enums/appliances';

import { JobListComponent } from '../../job/job-list/job-list.component';
import { CreateInspectionComponent } from '../../inspection/create-inspection/create-inspection.component';
import { EditClientComponent } from '../../client/edit-client/edit-client.component';

import { ActionService } from '../../../shared/services/action.service';
import { DataService } from '../../../shared/services/data.service';
import { GoToPageService } from '../../../shared/services/go-to-page.service';
import { GetRandomIntService } from '../../../shared/services/get-random-int.service';
import { StateManagerService } from 'app/shared/services/sassy-state-manager-ng2/src/state-manager.service';
import { ChimneyNamesService } from 'app/shared/services/chimney-names.service';
import { AuditRecord } from '../../../shared/classes/audit-record';

import { Chimney, FlueVent, Fireplace, Stoves, HeatingAppliances, DryerVent } from '../../../shared/classes';

import { Sheets } from '../../../shared/interfaces/sheets';
import { IInspectionReport } from 'app/shared/interfaces/inspection-report';
import { IChimneyItem } from 'app/shared/interfaces/chimney-item';
import { IAppliance } from 'app/shared/interfaces/appliance';
import { IClient } from 'app/shared/interfaces/client';

@Component({
  selector: 'wells-inspection-sheet',
  templateUrl: './inspection-sheet.component.html',
  styleUrls: ['./inspection-sheet.component.scss'],
})
export class InspectionSheetComponent implements OnInit, OnDestroy {

  public type: number;
  public ApplianceTypes = ApplianceTypes;
  public inspectionSheet: Chimney | FlueVent | Fireplace | Stoves | HeatingAppliances;
  public InspectionStatus = InspectionStatus;
  public clientName$: Observable<string>;

  private clientId: string;
  private jobKey: string;
  private jobDate: number;
  private nextTracker: 0;
  private subscriptions: ISubscription[] = [];
  private openedInspectionReport$: Observable<IInspectionReport>;
  private clients$: Observable<IClient[]>;
  private inspectionReports$: Observable<IInspectionReport[]>;

  constructor(
    private navCtrl: NavController,
    private action: ActionService,
    private goToPage: GoToPageService,
    private navParams: NavParams,
    private stateManager: StateManagerService,
    private getRandomInt: GetRandomIntService,
    private chimneyNames: ChimneyNamesService,
    private modalCtrl: ModalController,
  ) { }

  /****************************************************************
   *
   * Public Methods
   *
   ****************************************************************/

  /**
   * OnInit Lifecycle hook
   *
   * @memberof InspectionSheetComponent
   */
  ngOnInit() {
    this.type = this.navParams.get('type');
    const uuid = this.navParams.get('uuid');
    this.openedInspectionReport$ = this.stateManager.getModel('openedInspectionReport');
    this.clients$ = this.stateManager.getModel('clients');
    this.inspectionReports$ = this.stateManager.getModel('inspectionReports');

    this.openedInspectionReport$
      .take(1)
      .subscribe((report) => {
        const { clientId, jobKey, jobDate } = report;
        Object.assign(this, { clientId, jobKey, jobDate });
      });

    this.clientName$ = Observable.combineLatest(this.openedInspectionReport$, this.clients$)
      .map(([report, clients]) => {
        const client = clients.find(_client_ => _client_.key === report.clientId);
        return `${client.firstName} ${client.lastName}`;
      });


    this.subscription = this.getExistingInspectionRecord(uuid)
      .take(1)
      .subscribe(record => {
        this.setInspectionSheet(record);
      });

    this.action.addAuditRecord(new AuditRecord('Access inspection sheet page', 'inspection-sheet'));

  }

  /**
   * Go to job list and reset back to the standard side menu
   *
   *
   * @memberof InspectionSheetComponent
   */
  public goToJobList() {
    this.goToPage.execute(JobListComponent);
    this.action.changeSideMenu('standardMenu');
    this.action.resetInspection();
    this.action.resetOpenedInspection();
  }

  /**
   * Go to the next inspection item
   *
   *
   * @memberof InspectionSheetComponent
   */
  public goToNextItem() {
    // From inspection item list extract array of Chimneys.
    // From the list of Chimneys pull the  Chimney/appliance for the opened inspection
    this.stateManager.getModel('inspection')
      .map<any, IChimneyItem[]>(chimneys => {
        return this.chimneyNames.get(chimneys)
          .filter(key => !(key === 'clientId' || key === 'date'))
          .map((key, index) => Object.assign({},
            {
              name: chimneys[key].name || key,
              uuid: index.toString(),
              index,
              appliances: chimneys[key].inspectionItems,
              notValid: false
            })
          );
      })
      .withLatestFrom<IChimneyItem[], IAppliance>(this.stateManager.getModel('openedInspection'))
      .take(1)
      .subscribe(([inspections, appliance]) => {
        // For initial next inspection item pull the first appliance from the list
        let { parentIndex, index } = appliance;
        let inspectionItem = inspections[0].appliances[0];

        if (!!Object.keys(appliance).length) {
          if (parentIndex === undefined) {
            parentIndex = index;
            index = 0;
          } else {
            index = index + 1;
          }

          inspectionItem = inspections[parentIndex].appliances[index];
        }

        this.openedInspection(inspectionItem, inspections, parentIndex);
      });
  }

  /**
   * The output information from a sheet
   * 
   * @param {number} type 
   * @param {any} sheetClass 
   * 
   * @memberof InspectionSheetComponent
   */
  public sheetOutput(type: number, sheetClass) {
    this.saveData(sheetClass);
  }

  /**
   * Take you to the edit inspection page
   *
   *
   * @memberof InspectionSheetComponent
   */
  public editInspection() {
    this.action.changeSideMenu('inspectionItemMenu');
    this.goToPage.execute(CreateInspectionComponent, {
      clientId: this.clientId,
      date: this.jobDate,
      jobKey: this.jobKey,
      edit: true
    });
  }

  /**
   * Go to edit client screen
   * 
   * 
   * @memberof InspectionSheetComponent
   */
  public editClient() {
    this.stateManager.getModel('openedInspectionReport')
      .take(1)
      .subscribe((report) => {
        const modal = this.modalCtrl.create(EditClientComponent,
          {
            key: report.clientId,
            location: 'inspectionSheet',
            modal: true
          });

        modal.present();

        // this.goToPage.execute(EditClientComponent, { key: report.clientId, location: 'inspectionSheet' });
      });
  }

  public ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe);
  }

  /****************************************************************
   *
   * Private Methods
   *
   ****************************************************************/

  /**
   * Remove undefined values from a property
   *
   * @private
   * @param {any} object
   * @returns
   *
   * @memberof InspectionSheetComponent
   */
  private removeUndefinedValueProp(object) {
    let filterObject = Object.assign({}, object);
    const name = Object.keys(filterObject)[0];
    filterObject = filterObject[name];
    for (const key in filterObject) {
      if (filterObject.hasOwnProperty(key)) {
        if (filterObject[key] === undefined) {
          delete filterObject[key];
        }
      }
    }

    return { [name]: filterObject };
  }

  /**
   * saveData
   * 
   * @private
   * @param {any} sheetClass 
   * 
   * @memberof InspectionSheetComponent
   */
  private saveData(sheetClass) {
    const sheet = this.removeUndefinedValueProp(sheetClass);

    this.action.createNewOrUpdateExistingInspectionReport(sheet, this.clientId, this.jobDate, this.jobKey);
  }

  /**
   * getExistingInspectionRecord
   * 
   * @private
   * @param {string} name 
   * @returns {Observable<any>} 
   * 
   * @memberof InspectionSheetComponent
   */
  private getExistingInspectionRecord(uuid: string): Observable<any> {
    return this.inspectionReports$
      .withLatestFrom(this.openedInspectionReport$)
      .map(([inspectionReports, report]) => {
        const inspection = inspectionReports.find(item => {
          const key = Object.keys(item)[0];
          return item[key].clientId === report.clientId && item[key].jobKey === report.jobKey;
        });
        return { inspectionReports, report, inspection };
      })
      .map(({ inspectionReports, report, inspection }) => {
        if (inspection === undefined || inspection[report.clientId] === undefined) {
          return {};
        } else {

          if ('reports' in inspection[report.clientId]) {
            return inspection[report.clientId].reports[report.jobDate];
          }

          return {};
        }



      })
      .map(sheets => {
        if (sheets) {
          const matchKey = Object.keys(sheets)
            .find(key => sheets[key].uuid === uuid);

          return sheets[matchKey];
        }
        return undefined;
      }) as any;
  }

  getKey(report) {
    return Object.keys(report)[0];
  }

  /**
   * setInspectionSheet
   * 
   * @private
   * @param {any} record 
   * 
   * @memberof InspectionSheetComponent
   */
  private setInspectionSheet(record) {
    switch (ApplianceTypes[this.type]) {
      case 'Flue/Vent':
        this.inspectionSheet = this.inspectionSheetClassFactory(record, FlueVent);
        break;
      case 'Dryer Vent':
        this.inspectionSheet = this.inspectionSheetClassFactory(record, DryerVent);
        break;
      case 'Masonry Fireplace':
      case 'Prefab Fireplace':
        this.inspectionSheet = this.inspectionSheetClassFactory(record, Fireplace);
        break;
      case 'Stoves':
        this.inspectionSheet = this.inspectionSheetClassFactory(record, Stoves);
        break;
      case 'Heating Appliances':
        this.inspectionSheet = this.inspectionSheetClassFactory(record, HeatingAppliances);
        break;
      default:
        this.inspectionSheet = this.inspectionSheetClassFactory(record, Chimney);
        break;
    }
  }

  /**
   * inspectionSheetClassFactory
   * 
   * @private
   * @param {any} record 
   * @param {any} className 
   * @returns 
   * 
   * @memberof InspectionSheetComponent
   */
  private inspectionSheetClassFactory(record, className) {
    const name = this.navParams.get('name');
    const id = this.getRandomInt.get();
    const uuid = this.navParams.get('uuid');
    const instance = Object.assign(new className(), { name, id, uuid }, record);

    return instance;
  }

  /**
   * openedInspection
   * 
   * @private
   * @param {any} inspectionItem 
   * @param {any} inspections 
   * @param {any} parentIndex 
   * 
   * @memberof InspectionSheetComponent
   */
  private openedInspection(inspectionItem, inspections, parentIndex) {
    if (inspectionItem) {
      // Open appliance
      this.goToPage.execute(InspectionSheetComponent, inspectionItem);
      this.action.openedInspection(inspectionItem);
      this.action.setActiveMenuItem(inspectionItem);
    } else {
      // Open chimney
      parentIndex = parentIndex + 1;
      if (inspections[parentIndex]) {
        inspectionItem = inspections[parentIndex];
        this.openedInspection(inspectionItem, inspections, parentIndex);
      }
    }
  }

  /**
   * subscription
   * 
   * @private
   * 
   * @memberof JobListComponent
   */
  private set subscription(subscription) {
    this.subscriptions.push(subscription);

  }
}
