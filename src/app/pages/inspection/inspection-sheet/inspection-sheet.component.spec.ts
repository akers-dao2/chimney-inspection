import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InspectionSheetComponent } from './inspection-sheet.component';

xdescribe('InspectionSheetComponent', () => {
  let component: InspectionSheetComponent;
  let fixture: ComponentFixture<InspectionSheetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InspectionSheetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InspectionSheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
