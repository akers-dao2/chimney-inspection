import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChimneyComponent } from './chimney.component';

describe('ChimneyComponent', () => {
  let component: ChimneyComponent;
  let fixture: ComponentFixture<ChimneyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChimneyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChimneyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
