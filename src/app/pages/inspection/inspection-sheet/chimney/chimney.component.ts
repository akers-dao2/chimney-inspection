import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

import { Chimney } from '../../../../shared/classes/chimney';

import { HandleStatusPanesService } from 'app/shared/services/handle-status-panes.service';
import { ChangeInspectionItemNameService } from 'app/shared/services/change-inspection-item-name.service';
import { DataService } from 'app/shared/services/data.service';
import { PresentToastService } from 'app/shared/services/present-toast.service';
import { GetPicturePropService } from 'app/shared/services/get-picture-prop.service';
import { DeletePhotoService } from 'app/shared/services/delete-photo.service';
import { NotApplicablePaneResetService } from 'app/shared/services/not-applicable-pane-reset.service';
import { ActionService } from 'app/shared/services/action.service';

import { CommentOrRecommendationType } from 'app/shared/enums/comment-or-rec-types';
import { InspectionStatus } from 'app/shared/enums/inspection-status';

import { IPhoto } from 'app/shared/interfaces/photo';
import { ICommentEvent } from 'app/shared/interfaces/commentEvent';
import { RemovePictureService } from '../../../../shared/services/remove-picture.service';

@Component({
  selector: 'wells-chimney',
  templateUrl: './chimney.component.html',
  styleUrls: ['./chimney.component.scss']
})
export class ChimneyComponent implements OnInit {
  private origName: string;
  public photoName: string;
  public hideFrontHousePic: boolean;
  public CommentOrRecType = CommentOrRecommendationType;
  public disable = false;

  @Input() public chimney: Chimney;
  @Output() public output = new EventEmitter<any>();

  constructor(
    private handleStatusPanes: HandleStatusPanesService,
    private changeInspectionItemName: ChangeInspectionItemNameService,
    private dataService: DataService,
    private presentToast: PresentToastService,
    private getPictureProp: GetPicturePropService,
    private deletePhoto: DeletePhotoService,
    private notApplicablePaneReset: NotApplicablePaneResetService,
    private actions: ActionService,
    private removePicture: RemovePictureService
  ) { }

  ngOnInit() {
    // store the original name of the inspection item
    // this original name is used for pulling the item from the inspection model
    // the original name is updated on each successful name change
    this.origName = this.chimney.name;
    this.hideFrontHousePic = parseFloat(this.chimney.uuid) > 0;
  }

  /**
   * Handle input/select change events
   * 
   * @param {string} [prop] 
   * 
   * @memberof ChimneyComponent
   */
  public onChange(prop?: string) {

    this.disable = this.chimney.constructionType === 'No Chimney';

    if (this.disable) {
      // this.chimney = Object.assign(this.chimney, new Chimney());
    }

    // emit changes to InspectionSheet Component
    this.output.emit({ chimney: this.chimney });

    this.changeInspectionItemName.execute(this.chimney.name, this.origName, prop, undefined, 'chimney')
      .skipWhile(name => name === undefined)
      .take(1)
      .subscribe((name) =>
        // the original name is updated on each successful name change
        this.origName = name
      );

  }

  /**
   * Handle status change events for pane component
   * 
   * @param {any} $event 
   * 
   * @memberof ChimneyComponent
   */
  public handleStatusOfPanes($event) {
    this.handleStatusPanes.updateStatus($event)
      .take(1)
      .subscribe((result) => {
        // update status on chimney
        if (typeof result === 'number') {
          this.chimney[$event.value].status = result;
          this.chimney = this.notApplicablePaneReset.execute(this.chimney, Chimney);
          this.onChange();
        } else if (result) {
          // add pictures prop if it does not exist
          if (this.chimney[$event.value].pictures === undefined) {
            this.chimney[$event.value] = Object.assign({}, this.chimney[$event.value], { pictures: [] });
          }

          this.chimney[$event.value].pictures = this.chimney[$event.value].pictures.concat(result);

          this.actions.addOrRemoveItemOfflineStorage(result);

          this.onChange();
        }
      });
  }

  /**
   * handle deleting photos from classs
   * 
   * @param {IPhoto} photo 
   * @memberof ChimneyComponent
   */
  public handleDeletePhoto(photo: IPhoto) {
    this.deletePhoto.get(photo)
      .skipWhile(prop => prop === undefined)
      .take(1)
      .subscribe(async prop => {
        const { pictures } = this.chimney[prop];
        this.chimney[prop].pictures = this.removePicture.execute(pictures, photo);

        await this.actions.addOrRemoveItemOfflineStorage(photo, true);

        this.onChange();
      });
  }

  /**
   * handle adding Comments And Recommendations to class
   * 
   * @param {ICommentEvent} data 
   * @memberof ChimneyComponent
   */
  public handleCommentsAndRec(data: ICommentEvent) {
    const typeOfComRec = CommentOrRecommendationType[data.typeOfComRec];
    this.chimney[typeOfComRec]['comments'] = data.comments;
    this.chimney[typeOfComRec]['recommendations'] = data.recommendations;
    this.chimney[typeOfComRec]['additionalComments'] = data.additionalComments;
  }

  /**
   * provides the drop down called number of flues data
   * 
   * @readonly
   * @memberof ChimneyComponent
   */
  public get numberOfFlues() {
    let count = 1;
    const maxNumOfFlues = 10;
    const flues = [];

    while (flues.length < maxNumOfFlues) {
      flues.push(count);
      count++;
    }
    return flues;
  }

}
