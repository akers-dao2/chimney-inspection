import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasonryFireplaceComponent } from './masonry-fireplace.component';

describe('MasonryFireplaceComponent', () => {
  let component: MasonryFireplaceComponent;
  let fixture: ComponentFixture<MasonryFireplaceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasonryFireplaceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasonryFireplaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
