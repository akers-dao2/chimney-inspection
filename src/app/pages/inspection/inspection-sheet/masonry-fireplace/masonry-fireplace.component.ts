import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Fireplace } from '../../../../shared/classes';

import { HandleStatusPanesService } from 'app/shared/services/handle-status-panes.service';
import { ChangeInspectionItemNameService } from 'app/shared/services/change-inspection-item-name.service';
import { DeletePhotoService } from 'app/shared/services/delete-photo.service';
import { FractionsService } from 'app/shared/services/fractions.service';
import { NotApplicablePaneResetService } from 'app/shared/services/not-applicable-pane-reset.service';
import { ActionService } from 'app/shared/services/action.service';
import { GetPicturePropService } from 'app/shared/services/get-picture-prop.service';

import { InspectionStatus } from 'app/shared/enums/inspection-status';

import { CommentOrRecommendationType } from 'app/shared/enums/comment-or-rec-types';

import { IPhoto } from 'app/shared/interfaces/photo';
import { ICommentEvent } from 'app/shared/interfaces/commentEvent';
import { RemovePictureService } from '../../../../shared/services/remove-picture.service';

@Component({
  selector: 'wells-fireplace',
  templateUrl: './masonry-fireplace.component.html',
  styleUrls: ['./masonry-fireplace.component.scss']
})
export class MasonryFireplaceComponent implements OnInit {
  public CommentOrRecType = CommentOrRecommendationType;
  public scFailReasonCode: number[];
  public unlockhearthSection = false;

  private origName: string;
  @Input() public fireplace: Fireplace;
  @Input() public headerName: string;
  @Output() public output = new EventEmitter<any>();

  constructor(
    private handleStatusPanes: HandleStatusPanesService,
    private changeInspectionItemName: ChangeInspectionItemNameService,
    private deletePhoto: DeletePhotoService,
    public fractions: FractionsService,
    private notApplicablePaneReset: NotApplicablePaneResetService,
    private actions: ActionService,
    private getPictureProp: GetPicturePropService,
    private removePicture: RemovePictureService,

  ) { }

  ngOnInit() {
    // store the original name of the inspection item
    // this original name is used for pulling the item from the inspection model
    // the original name is updated on each successful name change
    this.origName = this.fireplace.name;
    this.fireplace.firebox.square_foot_total = this.fireplace.calculateSQTotal();
    this.setStatusOfHearth(false);
    this.enableHearthWhenRequired();
    if (this.fireplace.surroundClearance.status !== InspectionStatus['Not Applicable']) {
      this.setStatusOfSurroundClearance(false);
    }
  }

  /**
   * Handle input/select change events
   * 
   * @param {string} [prop] 
   * 
   * @memberof FireplaceComponent
   */
  public onChange(prop?: string) {
    this.fireplace.firebox.square_foot_total = this.fireplace.calculateSQTotal();

    // emit changes to InspectionSheet Component
    this.output.emit({ fireplace: this.fireplace });

    this.enableHearthWhenRequired();

    this.changeInspectionItemName.execute(this.fireplace.name, this.origName, prop, this.fireplace.uuid, 'fireplace')
      .skipWhile(name => name === undefined)
      .take(1)
      .subscribe((name) =>
        // the original name is updated on each successful name change
        this.origName = name
      );
  }

  /**
   * Handle status change events for pane component
   * 
   * @param {any} $event 
   * 
   * @memberof FireplaceComponent
   */
  public handleStatusOfPanes($event) {
    this.handleStatusPanes.updateStatus($event)
      .take(1)
      .subscribe((result) => {
        // update status on fireplace
        if (typeof result === 'number') {
          this.fireplace[$event.value].status = result;
          this.fireplace = this.notApplicablePaneReset.execute(this.fireplace, Fireplace);
          this.onChange();
        } else if (result) {
          // add pictures prop if it does not exist
          if (this.fireplace[$event.value].pictures === undefined) {
            this.fireplace[$event.value] = Object.assign({}, this.fireplace[$event.value], { pictures: [] });
          }

          this.fireplace[$event.value].pictures = this.fireplace[$event.value].pictures.concat(result);

          this.actions.addOrRemoveItemOfflineStorage(result);

          this.onChange();
        }
      });
  }

  /**
   * Provides of list of fire place locations
   * 
   * @readonly
   * 
   * @memberof MasonryFireplaceComponent
   */
  public get locations() {
    return [
      'Basement',
      'Living Room',
      'Family Room',
      'Den',
      'Dining Room',
      'Kitchen',
      '1st Floor',
      'Bedroom',
      'Garage',
      '2nd Floor',
      '3rd Floor',
      'Attic',
      'Porch',
      'Patio',
      'Foyer',
    ];
  }

  /**
   * handles deleting photos from local and remove storage
   * 
   * @param {IPhoto} photo 
   * 
   * @memberof MasonryFireplaceComponent
   */
  public handleDeletePhoto(photo: IPhoto) {
    this.deletePhoto.get(photo)
      .skipWhile(prop => prop === undefined)
      .take(1)
      .subscribe(async prop => {
        const { pictures } = this.fireplace[prop];
        this.fireplace[prop].pictures = this.removePicture.execute(pictures, photo);

        await this.actions.addOrRemoveItemOfflineStorage(photo, true);

        this.onChange();
      });
  }

  /**
   * Sets the status of hearth based on required criteria
   * 
   * 
   * @memberof MasonryFireplaceComponent
   */
  public setStatusOfHearth(update = true) {
    const sq = parseFloat(this.fireplace.calculateSQTotal());
    const depth = parseFloat(this.fireplace.hearth.depth);
    const left = parseFloat(this.fireplace.hearth.left);
    const right = parseFloat(this.fireplace.hearth.right);

    if (!isNaN(sq) && !isNaN(depth) && !isNaN(left) && !isNaN(right)) {
      if (sq < 864 && depth >= 16 && left >= 8 && right >= 8) {
        this.fireplace.hearth.status = InspectionStatus.passed;
      } else if (sq >= 864 && depth >= 20 && left >= 12 && right >= 12) {
        this.fireplace.hearth.status = InspectionStatus.passed;
      } else {
        this.scFailReasonCode = sq < 864 ? [1] : [2];
        this.fireplace.hearth.status = InspectionStatus.failed;
      }
    } else {
      if (this.fireplace.hearth.status !== 2) {
        this.fireplace.hearth.status = -1;
      }
    }

    if (update) {
      this.onChange();
    }
  }

  /**
   * Sets the status of surround clearance based on required criteria
   * 
   * 
   * @memberof MasonryFireplaceComponent
   */
  public setStatusOfSurroundClearance(update) {
    const x = parseFloat(this.fireplace.surroundClearance.x);
    const enableXSide = this.fireplace.surroundClearance.enableXSide;
    const y = parseFloat(this.fireplace.surroundClearance.y);
    const enableYSide = this.fireplace.surroundClearance.enableYSide;
    const z = parseFloat(this.fireplace.surroundClearance.z);
    const sideZ = parseFloat(this.fireplace.surroundClearance.sideZ);
    const enableZSide = this.fireplace.surroundClearance.enableZSide;

    this.resetValue('x');
    this.resetValue('y');
    this.resetValue('z');

    if (this.isValid(x, enableXSide) &&
      this.isValid(y, enableYSide) &&
      this.isValid(z, enableZSide) &&
      this.isValid(sideZ, enableZSide)) {

      const testResults = Array.of(
        this.testXYClearance(enableXSide, x),
        this.testXYClearance(enableYSide, y),
        this.testZClearance(enableZSide, z, sideZ)
      );
      const result = testResults.every(_result_ => _result_);
      const allUnCheck = [enableXSide, enableYSide, enableZSide].every(_result_ => !_result_);
      let status = InspectionStatus.passed;

      if (!result) {
        status = InspectionStatus.failed;
        this.scFailReasonCode = this.testXYClearance(enableXSide, x) && this.testXYClearance(enableXSide, y) ? [] : [2];
        if (!this.testZClearance(enableZSide, z, sideZ)) {
          this.scFailReasonCode.push(1);
        }
      }
      // set status to not selected when all checkboxes are unchecked and status is not equal not applicable
      if (allUnCheck && this.fireplace.surroundClearance.status !== InspectionStatus['Not Applicable']) {
        status = -1;
      }

      if (this.fireplace.surroundClearance.status !== InspectionStatus['Not Applicable']) {
        this.fireplace.surroundClearance.status = status;
      }


    } else {
      this.fireplace.surroundClearance.status = -1;
    }

    if (update) {
      this.onChange();
    }
  }

  /**
   * handle adding Comments And Recommendations to class
   * 
   * @param {ICommentEvent} data 
   * @memberof ChimneyComponent
   */
  public handleCommentsAndRec(data: ICommentEvent) {
    const typeOfComRec = CommentOrRecommendationType[data.typeOfComRec];
    this.fireplace[typeOfComRec]['comments'] = data.comments;
    this.fireplace[typeOfComRec]['recommendations'] = data.recommendations;
    this.fireplace[typeOfComRec]['additionalComments'] = data.additionalComments;
  }

  /**
   * Test X or Y surround clearance
   * 
   * @private
   * @param {any} enableSide 
   * @param {any} value 
   * @returns 
   * 
   * @memberof MasonryFireplaceComponent
   */
  private testXYClearance(enableSide, value) {
    let pass = false;

    if (enableSide && value >= 6 || !enableSide) {
      pass = true;
    }

    return pass;
  }

  /**
   * Test Z surround clearance
   * 
   * @private
   * @param {any} enableSide 
   * @param {any} z 
   * @param {any} sideZ 
   * @returns 
   * 
   * @memberof MasonryFireplaceComponent
   */
  private testZClearance(enableSide, z, sideZ) {
    let pass = false;

    if (enableSide && ((z >= 6 && sideZ <= 1.5) || (z >= 12 && sideZ > 1.5)) || !enableSide) {
      pass = true;
    }

    return pass;
  }

  /**
   * Verifies if the value is valid or needed for calculation
   * 
   * @private
   * @param {any} value 
   * @param {any} isEnabled 
   * @returns 
   * 
   * @memberof MasonryFireplaceComponent
   */
  private isValid(value, isEnabled) {
    if (!isEnabled) {
      return true;
    }
    return !isNaN(value);
  }

  /**
   * resetValue
   * 
   * @private
   * @param {string} prop 
   * @memberof MasonryFireplaceComponent
   */
  private resetValue(prop: string) {
    const enablePropName = `enable${prop.toUpperCase()}Side`;
    const sidePropName = `side${prop.toUpperCase()}`;
    const isEnabled = this.fireplace.surroundClearance[enablePropName];
    const hasSide = this.fireplace.surroundClearance[sidePropName];

    if (!isEnabled) {
      this.fireplace.surroundClearance[prop] = null;
      if (hasSide) {
        this.fireplace.surroundClearance[sidePropName] = null;
      }
    }
  }

  private enableHearthWhenRequired() {
    const fireplaceProps = ['height_whole', 'width_whole', 'depth_whole'];
    this.unlockhearthSection = Object.keys(this.fireplace.firebox)
      .filter(k => fireplaceProps.includes(k))
      .every(k => this.fireplace.firebox[k]);
  }
}
