import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { FlueVent } from 'app/shared/classes';

import { HandleStatusPanesService } from 'app/shared/services/handle-status-panes.service';
import { ChangeInspectionItemNameService } from 'app/shared/services/change-inspection-item-name.service';
import { DeletePhotoService } from 'app/shared/services/delete-photo.service';
import { FractionsService } from 'app/shared/services/fractions.service';
import { NotApplicablePaneResetService } from 'app/shared/services/not-applicable-pane-reset.service';
import { ActionService } from 'app/shared/services/action.service';
import { GetPicturePropService } from 'app/shared/services/get-picture-prop.service';

import { CommentOrRecommendationType } from 'app/shared/enums/comment-or-rec-types';

import { IPhoto } from 'app/shared/interfaces/photo';
import { ICommentEvent } from 'app/shared/interfaces/commentEvent';
import { RemovePictureService } from '../../../../shared/services/remove-picture.service';
import { NaReasonComponent } from 'app/shared/components/pane/na-reason/na-reason.component';
import { PopoverController } from 'ionic-angular';

@Component({
  selector: 'wells-flue-vent',
  templateUrl: './flue-vent.component.html',
  styleUrls: ['./flue-vent.component.scss']
})
export class FlueVentComponent implements OnInit {
  private origName: string;

  public CommentOrRecType = CommentOrRecommendationType;
  public flueSizeType: string;
  public flueSizeLocation: string;

  @Input() public flueVent: FlueVent;
  @Output() public output = new EventEmitter<any>();

  constructor(
    private handleStatusPanes: HandleStatusPanesService,
    private changeInspectionItemName: ChangeInspectionItemNameService,
    private deletePhoto: DeletePhotoService,
    public fractions: FractionsService,

    private notApplicablePaneReset: NotApplicablePaneResetService,
    private actions: ActionService,
    private removePicture: RemovePictureService,
    private popoverCtrl: PopoverController
  ) { }

  public ngOnInit() {
    // store the original name of the inspection item
    // this original name is used for pulling the item from the inspection model
    // the original name is updated on each successful name change
    this.origName = this.flueVent.name;

    this.setFlueSizeTypeAndLocation();
  }

  /**
   * Handle input/select change events
   *
   * @param {string} [prop]
   *
   * @memberof ChimneyComponent
   */
  public async onChange(prop?: string) {

    if (prop === 'materialChange') {
      this.resetFlueMaterial();
    }

    if (this.isFlueSizeZero) {
      if (this.flueVent.flueSizeNAReason === undefined) {
        const result = await this.presentPopover();
        this.flueVent.flueSizeNAReason = result;
        this.syncFlueSize();
      }
    } else {
      this.flueVent.flueSizeNAReason = undefined;
    }
    this.output.emit({ flueVent: this.flueVent });

    this.setFlueSizeTypeAndLocation();

    this.changeInspectionItemName.execute(this.flueVent.name, this.origName, prop, this.flueVent.uuid)
      .skipWhile(name => name === undefined)
      .take(1)
      .subscribe((name) =>
        // the original name is updated on each successful name change
        this.origName = name
      );
  }

  // Reset flue material on change material change
  private resetFlueMaterial() {
    const locations = [
      { name: 'outside1', value: null },
      { name: 'outside2', value: '0' },
      { name: 'outside3', value: null },
      { name: 'outside4', value: '0' },
      { name: 'inside1', value: null },
      { name: 'inside2', value: '0' },
      { name: 'inside3', value: null },
      { name: 'inside4', value: '0' }
    ];
    locations.forEach(location => {
      this.flueVent[location.name] = location.value;
    });
  }

  /**
   * Check if flue size is zero
   *
   * @readonly
   * @private
   * @memberof FlueVentComponent
   */
  private get isFlueSizeZero() {
    return [this.flueVent.outside1, this.flueVent.outside3, this.flueVent.inside1, this.flueVent.inside3]
      .some(f => f === '0');
  }

  /**
   * Sync flue size values
   *
   * @private
   * @param {string} [value='0']
   * @memberof FlueVentComponent
   */
  private syncFlueSize(value = '0') {
    ['outside1', 'outside3', 'inside1', 'inside3']
      .forEach(prop => this.flueVent[prop] = value);
  }

  /**
 * Present NA reason selection popover
 *
 * @private
 * @param {*} $event
 * @returns
 * @memberof PaneComponent
 */
  private presentPopover() {
    return new Promise<string>((resolve) => {
      const popover = this.popoverCtrl.create(NaReasonComponent);

      popover.onDidDismiss((data) => {
        if (data !== null) {
          resolve(data);
        }

      });

      popover.present();

    });
  }

  private setFlueSizeTypeAndLocation() {
    const flueSizeTypeObj = this.flueMaterials
      .find(material => this.flueVent.material === material.value);

    this.flueSizeType = flueSizeTypeObj ? flueSizeTypeObj.flueSizeType : undefined;

    const flueSizeLocationObj = this.flueMaterials
      .find(material => this.flueVent.material === material.value);

    this.flueSizeLocation = flueSizeLocationObj ? flueSizeLocationObj.flueSizeLocation : undefined;
  }

  /**
   * Handle Status changes for Panes
   *
   * @param {any} obj
   *
   * @memberof InspectionSheetComponent
   */
  public handleStatusOfPanes($event) {
    this.handleStatusPanes.updateStatus($event)
      .take(1)
      .subscribe((result) => {
        // update status on flueVent
        if (typeof result === 'number') {
          this.flueVent[$event.value].status = result;
          this.flueVent = this.notApplicablePaneReset.execute(this.flueVent, FlueVent);
          this.onChange();
        } else if (result) {
          // add pictures prop if it does not exist
          if (this.flueVent[$event.value].pictures === undefined) {
            this.flueVent[$event.value] = Object.assign({}, this.flueVent[$event.value], { pictures: [] });
          }

          this.flueVent[$event.value].pictures.push(result);

          this.actions.addOrRemoveItemOfflineStorage(result);

          this.onChange();
        }
      });
  }

  public handleDeletePhoto(photo: IPhoto) {
    this.deletePhoto.get(photo)
      .skipWhile(prop => prop === undefined)
      .take(1)
      .subscribe(async prop => {
        const { pictures } = this.flueVent[prop];
        this.flueVent[prop].pictures = this.removePicture.execute(pictures, photo);

        await this.actions.addOrRemoveItemOfflineStorage(photo, true);

        this.onChange();
      });
  }

  public get flueMaterials() {
    return [
      {
        name: 'Terra Cotta',
        value: 'Terra Cotta',
        flueSizeType: 'rectangle',
        flueSizeLocation: 'both'
      },
      {
        name: 'Terra Cotta Round',
        value: 'Terra Cotta Round',
        flueSizeType: 'rectangle',
        flueSizeLocation: 'both-one'
      },
      {
        name: 'Brick Unlined',
        value: 'Brick Unlined',
        flueSizeType: 'rectangle',
        flueSizeLocation: 'inside'

      },
      {
        name: 'Stone Unlined',
        value: 'Stone Unlined',
        flueSizeType: 'rectangle',
        flueSizeLocation: 'inside'
      },
      {
        name: 'Stainless Liner Round',
        value: 'Stainless Liner Round',
        flueSizeType: 'round',
        flueSizeLocation: 'inside'
      },
      {
        name: 'Air Cooled',
        value: 'Air Cooled',
        flueSizeType: 'round',
        flueSizeLocation: 'both'
      },
      {
        name: 'B-Vent',
        value: 'B-Vent',
        flueSizeType: 'round',
        flueSizeLocation: 'both'
      },
      {
        name: 'Class A - Double Wall',
        value: 'Class A - Double Wall',
        flueSizeType: 'round',
        flueSizeLocation: 'both'
      },
      {
        name: 'Aluminum Liner',
        value: 'Aluminum Liner',
        flueSizeType: 'round',
        flueSizeLocation: 'inside'
      },
      {
        name: 'Stainless Liner square/rectangle',
        value: 'Stainless Liner square/rectangle',
        flueSizeType: 'rectangle',
        flueSizeLocation: 'inside'
      },
      {
        name: 'No Flue Vent',
        value: 'No Flue Vent',
        flueSizeType: undefined,
        flueSizeLocation: undefined
      },
    ];
  }

  /**
   * handle adding Comments And Recommendations to class
   * 
   * @param {ICommentEvent} data 
   * @memberof ChimneyComponent
   */
  public handleCommentsAndRec(data: ICommentEvent) {
    const typeOfComRec = CommentOrRecommendationType[data.typeOfComRec];
    this.flueVent[typeOfComRec]['comments'] = data.comments;
    this.flueVent[typeOfComRec]['recommendations'] = data.recommendations;
    this.flueVent[typeOfComRec]['additionalComments'] = data.additionalComments;
  }
}
