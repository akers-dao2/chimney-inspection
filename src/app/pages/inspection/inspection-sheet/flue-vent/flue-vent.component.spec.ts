import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlueVentComponent } from './flue-vent.component';

describe('FlueVentComponent', () => {
  let component: FlueVentComponent;
  let fixture: ComponentFixture<FlueVentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlueVentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlueVentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
