import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  AfterViewInit,
  AfterViewChecked,
  Output,
  EventEmitter,
  Input,
  OnChanges,
  SimpleChanges,
  ChangeDetectorRef,
  ChangeDetectionStrategy
} from '@angular/core';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable//interval'
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/operator/debounce';
import 'rxjs/add/operator/delay';

declare const SignaturePad;

@Component({
  selector: 'wells-signature-pad',
  templateUrl: './signature-pad.component.html',
  styleUrls: ['./signature-pad.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SignaturePadComponent implements OnInit, OnChanges {

  /** Public */
  public signaturePad: any;
  public clientSignature: any;
  public hasNoSignature: boolean;
  public noSignatureReason
  public lockWidth: string;
  public lockHeight: string;


  @ViewChild('signaturePad') canvas: ElementRef;
  @ViewChild('signaturePadDiv') signaturePadDiv: ElementRef;

  /** Inputs */
  @Input() fromDataURL: string;
  @Input() lockSignature: boolean;

  /** Outputs */
  @Output() toDataURL: EventEmitter<any> = new EventEmitter();

  constructor(
    private ref: ChangeDetectorRef
  ) { }

  /****************************************************************
   * 
   * Public Methods
   *
   ****************************************************************/

  ngOnInit() {
    // Initialize Signature Pad
    this.signaturePad = new SignaturePad(this.canvas.nativeElement, {
      backgroundColor: 'rgba(255, 255, 255, 0)',
      penColor: 'rgb(0, 0, 0)',
      onEnd: this.strokeEnd.bind(this)
    });

    // resize canvas based on screen resize
    Observable.fromEvent(window, 'resize')
      .map(this.resizeCanvas)
      .map(this.loadSignature)
      .subscribe();

    this.resizeCanvas();

    const el = this.signaturePadDiv.nativeElement as HTMLDivElement;
    this.lockWidth = el.clientWidth + 'px';
    this.lockHeight = el.clientHeight + 'px';

  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['fromDataURL']) {
      if (!changes['fromDataURL'].isFirstChange()) {
        this.loadSignature();
      }
    }
  }

  /**
   * Save Signature as data URL
   * 
   * 
   * @memberOf SignaturePadComponent
   */
  public save() {
    const data = this.signaturePad.toDataURL();
    this.toDataURL.emit({ data });
  }

  /**
   * Clear Signature Pad
   * 
   * 
   * @memberOf SignaturePadComponent
   */
  public clear() {
    this.signaturePad.clear();
    this.hasNoSignature = false
    this.toDataURL.emit({ data: 'clear' })
  }

  /**
   * Load Signature data URL
   * 
   * 
   * @memberOf SignaturePadComponent
   */
  public loadSignature = () => {
    if (this.fromDataURL && this.fromDataURL.includes('data:')) {
      this.signaturePad.fromDataURL(this.fromDataURL);
    } else if (this.fromDataURL === 'notAvailable' || this.fromDataURL === 'customerRefused') {
      this.hasNoSignature = true;
      this.noSignatureReason = this.noSignatureMapper(this.fromDataURL);
    }
  }

  public noSignatureSave(reason: string) {
    this.signaturePad.clear();
    this.toDataURL.emit({ data: reason });
  }


  /****************************************************************
   * 
   * Private Methods
   *
   ****************************************************************/

  /**
   * Resize Canvas based on screen size
   * 
   * @private
   * 
   * @memberOf SignaturePadComponent
   */
  private resizeCanvas = () => {
    const ratio = Math.max(window.devicePixelRatio || 1, 1);
    this.canvas.nativeElement.width = this.canvas.nativeElement.offsetWidth * ratio;
    this.canvas.nativeElement.height = this.canvas.nativeElement.offsetHeight * ratio;
    this.canvas.nativeElement.getContext('2d').scale(ratio, ratio);
  }

  private strokeEnd() {
    Observable.empty()
      .debounce(() => Observable.interval(500))
      .subscribe(() => null, () => null, this.save.bind(this));
  }

  private noSignatureMapper(reason) {
    const mapper = {
      notAvailable: 'Not Available',
      customerRefused: 'Customer Refused',
    }

    return mapper[reason];
  }
}
