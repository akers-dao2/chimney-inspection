import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { StateManagerService } from 'app/shared/services/sassy-state-manager-ng2/src/state-manager.service';
import { IonicModule } from 'ionic-angular';

import { ClientNamePipe } from '../../../shared/pipes/client-name.pipe';
import { CrewNamePipe } from '../../../shared/pipes/crew-name.pipe';

import { JobListComponent } from './job-list.component';
import { Observable } from 'rxjs/Observable';

xdescribe('JobListComponent', () => {
  let component: JobListComponent;
  let fixture: ComponentFixture<JobListComponent>;
  let stateManager: StateManagerService;
  let functionTest: Function;
  const fakeData = { name: 'fake_client' };
  const fakeGetModel = jasmine.createSpy('getModel');

  class MockStateManagerService {
    getModel(str) {
      fakeGetModel(str);

      functionTest = () => {
        return fakeData;
      };
      return Observable.of([]);
    }
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        IonicModule.forRoot(JobListComponent)
      ],
      declarations: [
        JobListComponent,
        ClientNamePipe,
        CrewNamePipe
      ],
      providers: [
        CrewNamePipe,
        { provide: StateManagerService, useClass: MockStateManagerService }
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ]
    })
      .compileComponents();
  }));

  beforeEach(inject([StateManagerService], (_StateManagerService_) => {
    stateManager = _StateManagerService_;

    fixture = TestBed.createComponent(JobListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('Life Cycle methods', () => {
    it('should do setup - ngOnInit()', () => {
      expect(fakeGetModel).toHaveBeenCalledWith('jobs');
      expect(functionTest()).toEqual(fakeData);
    });
  });
});
