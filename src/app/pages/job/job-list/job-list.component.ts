import {
  Component, OnInit,
  ViewChild, TemplateRef, ChangeDetectorRef, OnDestroy
} from '@angular/core';
import { NavController, InfiniteScroll } from 'ionic-angular';
import { Platform } from 'ionic-angular';
import { StateManagerService } from 'app/shared/services/sassy-state-manager-ng2/src/state-manager.service';
import { PopoverController, AlertController } from 'ionic-angular';
import { reorderArray } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/switchMapTo';
import 'rxjs/add/operator/combineAll';
import 'rxjs/add/operator/groupBy';
import 'rxjs/add/operator/mergeAll';
import 'rxjs/add/operator/mergeMapTo';
import 'rxjs/add/operator/concatMapTo';
import 'rxjs/add/operator/toArray';
import 'rxjs/add/operator/reduce';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/merge';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/withLatestFrom';
import 'rxjs/add/observable/from';
import * as moment from 'moment';

/** Interfaces */
import { IInspection } from '../../../shared/interfaces/inspection';
import { ICrew } from '../../../shared/interfaces/crew';
import { IUser } from '../../../shared/interfaces/user';
import { IJob } from '../../../shared/interfaces/job';
import { IInspectionReport } from '../../../shared/interfaces/inspection-report';
import { IClient } from 'app/shared/interfaces/client';
import { IReport } from 'app/shared/interfaces/report';

/** Enums */
import { Status } from '../../../shared/enums/status';
import { DateFilterView } from '../../../shared/enums/date-filter-views';

/** Services */
import { ActionService } from '../../../shared/services/action.service';
import { DataService } from '../../../shared/services/data.service';
import { SortService } from '../../../shared/services/sort.service';
import { LoaderService } from '../../../shared/services/loader.service';
import { GoToPageService } from '../../../shared/services/go-to-page.service';
import { GetOpenedInspectionReportService } from '../../../shared/services/get-opened-inspection-report.service';
import { AuditRecord } from '../../../shared/classes/audit-record';
import { GetOpenedClientInfoService } from '../../../shared/services/get-opened-client-info.service';
import { PdfGeneratorService } from '../../../shared/services/pdf-generator.service';

/** Components */
import { JobHeaderMenuComponent } from './job-header-menu/job-header-menu.component';
import { CreateInspectionComponent } from '../../inspection/create-inspection/create-inspection.component';
import { InspectionSheetComponent } from '../../inspection/inspection-sheet/inspection-sheet.component';
import { CreateJobComponent } from '../create-job/create-job.component';
import { EditClientComponent } from '../../client/edit-client/edit-client.component';
import { ReportComponent } from '../../report/report.component';
import { DateHeaderComponent } from '../job-list/date-header/date-header.component';

/** Pipes */
import { CrewNamePipe } from '../../../shared/pipes/crew-name.pipe';
import { ClientNamePipe } from '../../../shared/pipes/client-name.pipe';

import { saveAs } from 'file-saver';
import { of } from 'rxjs/observable/of';
import { map } from 'rxjs/operators';
import { RemoveImagesService } from '../../../shared/services/remove-images.service';
import { ChimneyNamesService } from '../../../shared/services/chimney-names.service';
import { ClientRecordPipe } from '../../../shared/pipes/client-record.pipe';
import { from } from 'rxjs/observable/from';
import { LocalDbStatus } from 'app/shared/enums/local-db-status';

@Component({
  selector: 'wells-job-list',
  templateUrl: './job-list.component.html',
  styleUrls: ['./job-list.component.scss'],
})
export class JobListComponent implements OnInit, OnDestroy {
  public jobs: IJob[] = [];
  public nonFilterJobs: IJob[];
  public Status = Status;
  public DateFilterView = DateFilterView;
  public dateView: DateFilterView;
  public elseBlock: TemplateRef<any> = null;
  public hasAllTab = false;
  public hasTomorrowTab = false;
  public hasIncomplete = false;
  public hasCheckboxes = false;
  public showReorder = false;
  public showSyncBtn = false;
  public isJobReorder = false;
  public isFiltered = false;
  public filterMessage = '';
  public checkBoxesState = {};
  public downloadReportQueue = [];
  public showGenerateReport = false;
  public loadMoreBtnDisabled = false;

  private subscriptions: Subscription[] = [];
  private jobsInView: IJob[];
  private filterCriteria$ = new BehaviorSubject<{ type?: string, data?: any }>({});
  private currentUser$: Observable<IUser>;
  private infiniteScroll: InfiniteScroll;

  @ViewChild('noJobs') noJobsView: TemplateRef<any> = null;
  @ViewChild('loading') loadingView: TemplateRef<any> = null;

  constructor(
    private stateManager: StateManagerService,
    public popoverCtrl: PopoverController,
    private alertCtrl: AlertController,
    private actions: ActionService,
    private navCtrl: NavController,
    private dataService: DataService,
    private sort: SortService,
    public ref: ChangeDetectorRef,
    private loader: LoaderService,
    private goToPage: GoToPageService,
    private getOpenedInspectionReport: GetOpenedInspectionReportService,
    public plt: Platform,
    private getOpenedClientInfo: GetOpenedClientInfoService,
    private pdfGenerator: PdfGeneratorService,
    private removeImages: RemoveImagesService,
    private getChimneyName: ChimneyNamesService
  ) { }


  /****************************************************************
   *
   * Public Methods
   *
   ****************************************************************/

  /**
   * OnInit Lifecycle hook
   *
   *
   * @memberOf JobListComponent
   */
  ngOnInit() {

    // start with the loadingView
    this.elseBlock = this.loadingView;

    this.currentUser$ = this.stateManager.getModel('currentUser');
    const crews$ = this.stateManager.getModel('crews') as any;
    const client$ = this.stateManager.getModel('clients').take(1);

    this.setCurrentUserView(this.currentUser$);
    const crewName$ = this.getCrewNameForUser(crews$, this.currentUser$);

    // Only show jobs the currentUser is allow to see base on crew
    // Admin Crew will see all jobs
    this.subscription = this.stateManager.getModel('jobs')
      .skipWhile(jobs => !jobs.length)
      .map(this.fixDate)
      .map(jobs => this.sortJobsByProp(jobs, 'date', true))
      .switchMap(jobs => this.sortByCrews(jobs))
      .switchMapTo(crewName$, (jobs, { currentUserCrew }) => ({ currentUserCrew, jobs }))
      .map(this.filterByAssignCrew)
      // .switchMapTo(client$, this.filterByClient)
      .do(jobs => this.nonFilterJobs = jobs)
      .map(this.filterDateView)
      .subscribe(jobs => {
        this.jobsInView = this.jobsInView ? this.jobsInView : jobs;
        this.hasIncomplete = this.nonFilterJobs
          .filter(job => job.status === Status.open)
          .some(job => moment().isAfter(job.date, 'day'));
        this.filterJobList({});

      });

    // this.subscription = this.stateManager.getModel('jobs')
    //   .skipWhile(jobs => !!Object.keys(jobs).length)
    //   .delay(100)
    //   .take(1)
    //   .subscribe(jobs => {
    //     this.elseBlock = this.noJobsView;
    //     this.ref.markForCheck();
    //   });


    Observable.combineLatest(
      this.stateManager.getModel('jobs')
        .skipWhile(jobs => !jobs.length),
      this.filterCriteria$
    )
      .skipWhile(([jobs]) => !jobs.length)
      .map(([jobs, fc]) => fc)
      .switchMap(this.filterJobListByClient)
      .switchMap(this.filterJobListByCrew)
      .switchMap(this.filterJobListByDateRange)
      .skipWhile(c => c.jobs === undefined || !c.jobs.length)
      .subscribe(c => {
        this.jobs = c.jobs;
        this.isFiltered = c.jobs.length !== this.jobsInView.length;
        this.filterMessage = this.getFilterMessage(c);
        this.elseBlock = !this.jobs.length ? this.noJobsView : this.loadingView;

        this.ref.markForCheck();
      });

    const user = JSON.parse(localStorage.getItem('user'));

    if (user !== null && user.online) {
      this.stateManager.getModel('applicationMaintenanceJobs')
        .skipWhile(lastTimeRun => lastTimeRun === undefined)
        .filter(lastTimeRun => !moment(lastTimeRun).isSame(moment(), 'days'))
        .switchMapTo(crewName$)
        .take(1)
        .filter(crewName => crewName.currentUserCrew.name === 'Admin')
        .switchMapTo(this.removeImages.execute())
        .switchMapTo(this.dataService.updateApplicationMaintenanceJobs())
        .take(1)
        .subscribe();
    }
  }

  public doInfinite(infiniteScroll) {
    this.infiniteScroll = infiniteScroll;
    // this.loader.show();
    this.actions.loadMoreData()
      .switchMap(this.disableInfinitieScroll)
      .map(disable => {
        this.infiniteScroll.enable(disable);
        this.loadMoreBtnDisabled = !disable;
      })
      // .do(() => this.loader.hide())
      .take(1)
      .subscribe(() => infiniteScroll.complete());
  }

  public loadMoreData() {
    this.loader.show();
    this.actions.loadMoreData()
      .take(1)
      .map(disable => this.loadMoreBtnDisabled = disable)
      .do(() => this.loader.hide())
      .subscribe();
  }

  public addToDownloadReportQueue(jobKey) {
    this.checkBoxesState[jobKey] = !this.checkBoxesState[jobKey];
    const jobKeyIndex = this.downloadReportQueue.findIndex(q => q === jobKey);
    jobKeyIndex === -1 ? this.downloadReportQueue.push(jobKey) : this.downloadReportQueue.splice(jobKeyIndex, 1);

  }

  public generatePDFReport() {
    const clients$ = this.stateManager.getModel('clients');
    this.showGenerateReport = !this.showGenerateReport;

    this.subscription = Observable.from(this.downloadReportQueue)
      .mergeMapTo(clients$, (jobKey, clients) => ({ jobKey, clients }))
      .mergeMap(({ jobKey, clients }) => {
        const job = this.jobsInView.find(j => j.key === jobKey);
        this.actions.openedInspectionReport(job.client, job.key, job.date);

        return Observable.from<IClient>(clients)
          .filter(c => c.key === job.client)
          .mergeMapTo(this.getOpenedInspectionReport.get(),
          (client, inspectItemsInReport) => ({ client, inspectItemsInReport }))
          .mergeMap(({ client, inspectItemsInReport }) =>
            this.pdfGenerator.pdfData(client, job.certification, inspectItemsInReport, job.completionDate),
          ({ client }, data) => ({ client, data, job }));

      })
      .take(1)
      .subscribe(({ data, client, job }) => {
        this.updateJobStatus(job.key);
        this.resetDownloadQueue();
        saveAs(data, `${client.firstName.trim()} ${client.lastName.trim()}.pdf`);
      });
  }

  public isFinalized(jobKey) {
    const job = this.jobs.find(j => j.key === jobKey);
    return job.status === 1 || job.status === 4;
  }

  /**
   * show a popup of inspection items for a particular client
   *
   * @param {Client} client
   *
   * @memberOf JobListComponent
   */
  public presentPopup(job: IJob) {

    this.hasInspectionReport(job)
      .withLatestFrom(this.stateManager.getModel('currentUser'))
      .take(1)
      .subscribe(([hasInspection, currentUser]) => {
        const alert = this.alertCtrl.create({
          title: 'Job',
          message: '',
          // cssClass: 'inspection',
          buttons: [
            {
              text: 'Open Inspection',
              cssClass: this.popUpOpenCssClass(hasInspection, job.status, currentUser),
              handler: () => {
                this.openInspection(job);
              }
            },
            {
              text: hasInspection ? 'Edit Inspection' : 'Create Inspection',
              cssClass: this.popUpCssClass(job.status, currentUser),
              handler: () => {
                this.createInspection(job);
              }
            },
            {
              text: 'Edit Job',
              cssClass: this.popUpCssClass(job.status, currentUser),
              handler: () => {
                this.editJob(job);
              }
            },
            {
              text: 'Delete Job',
              cssClass: this.popUpCssClass(job.status, currentUser),
              handler: () => {
                this.confirmDelete(job.key);
              }
            },
            {
              text: 'View Finalize Report',
              cssClass: [Status.closed, Status.downloaded].includes(job.status) ? 'none' : 'hide',
              handler: () => {
                // store the client's info in state manager
                this.actions.openedInspectionReport(job.client, job.key, job.date);
                this.generateReport();
              }
            },
            {
              text: 'Edit Client',
              handler: () => {
                this.editClient(job);
              }
            },
            {
              text: 'Cancel',
              role: 'cancel'
            },
          ]
        });
        alert.present();
      });
  }

  /**
   * Display Filter Popover
   *
   * @param {HTMLEvent} ,type
   *
   * @memberOf JobListComponent
   */
  public displayFilterPopover($event, type) {

    switch (type) {
      case 'client':
        this.objectNamePipe('client', this.getClientNames)
          .map(clients => Array.of('All Clients', ...clients))
          .switchMap<any[], { item: string }>(items => Observable.fromPromise(this.presentPopover($event, items)))
          .take(1)
          .subscribe(data => {
            this.filterJobList({ [type]: data.item });
          });
        break;

      case 'crew':
        this.objectNamePipe('assigned', this.getCrews)
          .map(crews => Array.of('All Crews', ...this.removeDuplicates(crews)))
          .switchMap<any[], { item: string }>(items => Observable.fromPromise(this.presentPopover($event, items)))
          .take(1)
          .subscribe(data => {
            this.filterJobList({ [type]: data.item });
          });
        break;

      case 'date':
        Observable.fromPromise(this.presentPopover($event, null, DateHeaderComponent))
          .skipWhile(data => data === undefined)
          .take(1)
          .subscribe(data => {
            this.filterJobList({ [type]: data });
          });

        break;

      default:
        break;
    }
  }

  public onDateFilterSelect(view: string) {
    this.jobs = this.filterJobListByDate(view, this.nonFilterJobs);
    if (this.infiniteScroll) {
      this.infiniteScroll.enable(true);
    }

    this.ref.markForCheck();
  }

  /**
   * ngOnDestroy
   * 
   * @memberof JobListComponent
   */
  public ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  /**
   * reorderItems
   * 
   * @param {{ from: number, to: number }} indexes 
   * @memberof JobListComponent
   */
  public reorderItems(indexes: { from: number, to: number }) {
    this.jobs = reorderArray(this.jobs, indexes);

    this.reorderJobModel(indexes, this.jobs);
  }

  public onSync() {
    this.actions.syncOfflineQueue();
  }

  /****************************************************************
   *
   * Private Methods
   *
   ****************************************************************/

  private removeDuplicates(crews: ICrew[]) {
    return crews.reduce((acc, i) => acc.includes(i) ? acc : [...acc, i], []);
  }

  private getClientNames = (v) => {
    return new ClientRecordPipe(this.stateManager).transform(v)
      .map(c => `${c.firstName} ${c.lastName}`);
  }

  private getCrews = (v) => {
    return this.stateManager.getModel('crews')
      .publishReplay(1)
      .refCount()
      .switchMap<ICrew[], string>(c => {
        return c.filter(crew => crew.key === v)
          .map(crew => crew.name);
      });
  }

  private updateJobStatus(key) {
    this.stateManager.getModel('jobs')
      .switchMap<IJob[], IJob>(jobs => jobs)
      .find(job => job.key === key)
      .map(job => ({ ...job, status: Status.downloaded }))
      .map(job => this.actions.updateJob(job))
      .take(1)
      .subscribe();
  }

  private filterJobList(filterBy: { [name: string]: any }) {
    this.filterCriteria$
      .take(1)
      .map(c => {
        return Object.assign({}, c, filterBy);
      })
      .subscribe(c => this.filterCriteria$.next(c));
  }

  private popUpCssClass(status, currentUser) {
    return ![Status.closed, Status.downloaded].includes(status) || currentUser.adminstration ? 'none' : 'hide';
  }

  private popUpOpenCssClass(hasInspection, status, currentUser) {
    return (hasInspection && ![Status.closed, Status.downloaded].includes(status)) ||
      (currentUser.adminstration && hasInspection)
      ? 'none' : 'hide';
  }

  private resetDownloadQueue() {
    this.checkBoxesState = {};
    this.downloadReportQueue = [];
    this.showGenerateReport = false;
  }
  private getFilterMessage(c) {
    c = c || {};
    const o = Object.assign({}, c);
    delete o.jobs;

    return Object.keys(o)
      .map(k => `${k.toUpperCase()} by ${this.getDateString(c[k])}`)
      .join(' & ');
  }

  private getDateString(d) {
    return typeof d === 'string' ? d : this.dateRange(d);
  }

  private dateRange(d) {
    return `${moment(d.startDate).format('MM/DD/YYYY')} - ${moment(d.endDate).format('MM/DD/YYYY')}`;
  }

  private objectNamePipe(prop, filterFunc) {
    return Observable.from(this.jobsInView)
      .map(job => job[prop])
      .mergeMap<any, any>(item => filterFunc(item).take(1))
      .toArray()
      .map(items => items.sort(this.sort.get));
  }

  private presentPopover($event, items, component: any = JobHeaderMenuComponent) {
    return new Promise((resolve, reject) => {
      const popover = this.popoverCtrl.create(component, { items });

      popover.onDidDismiss((data) => {
        if (data !== null) {
          resolve(data);
        }

      });

      popover.present({
        ev: $event
      });

    });
  }

  private reorderJobModel(indexes, jobs: IJob[]) {
    jobs.forEach((job, index) => {
      job.order = index;
      this.dataService.addJob(job, true);
    });
  }

  /**
   * Take you to the create inspection page
   *
   * @param {Job} job
   *
   * @memberOf JobListComponent
   */
  private createInspection(job: IJob) {
    this.actions.changeSideMenu('inspectionItemMenu');
    this.goToPage.execute(CreateInspectionComponent, {
      clientId: job.client,
      date: job.date,
      jobKey: job.key
    });

    // store the client's info in state manager
    this.actions.openedInspectionReport(job.client, job.key, job.date);
  }

  /**
   * Confirm a delete for client
   *
   * @param {any} key
   *
   * @memberOf ClientListComponent
   */
  private confirmDelete(key: string) {
    const alert = this.alertCtrl.create({
      title: 'Delete Job',
      message: 'Are you sure you want to delete this job?',
      buttons: [
        {
          text: 'No',
          handler: () => { }
        },
        {
          text: 'Yes',
          handler: () => {
            this.getJobInspectionReportDeletePath(key)
              .take(1)
              .subscribe(({ path, inspectionKey }) => {
                if (path !== 'noInspectionReport') {
                  this.dataService.deleteInspectionReport(path);
                }
                this.actions.deleteItemFromModel(key, 'jobs');
                this.dataService.deleteJob(key);
                if (inspectionKey) {
                  this.actions.deleteItemFromModel(inspectionKey, 'inspections');
                  this.dataService.deleteInspection(inspectionKey);
                }
              });
          }
        }
      ]
    });
    alert.present();

  }


  private getJobInspectionReportDeletePath(jobKey) {
    return this.stateManager.getModel('jobs')
      .map(jobs => jobs.find(job => job.key === jobKey))
      .withLatestFrom(this.stateManager.getModel('inspectionReports'), (job, reports: { [name: string]: IInspectionReport }[]) => {
        if (job === undefined) {
          return { report: undefined, date: undefined, client: undefined };
        }

        const { client, date } = job;
        const report = reports.find(_report => {
          const itemkey = Object.keys(_report)[0];
          return _report[itemkey].clientId === client && _report[itemkey].jobKey === jobKey;
        });

        return { report, date, client };
      })
      .switchMap(({ client, date }) => this.getInspectionKey(client, date),
      ({ report, date }, inspectionKey) => ({ report, date, inspectionKey }))

      .map(({ report, date, inspectionKey }) => {
        if (report === undefined) {
          return { path: 'noInspectionReport', inspectionKey };
        }

        const _key = Object.keys(report)[0];
        const inspectionReportKey = report.key;
        const path = `${inspectionReportKey}`;
        return { path, inspectionKey };
      });
  }

  getInspectionKey(clientId, date) {
    return this.stateManager.getModel('inspections')
      .map<IInspection[], IInspection>(inspections =>
        inspections
          .filter(inspection =>
            inspection.clientId === clientId && inspection.date === date)[0]
      )

      .map(inspection => inspection ? inspection.key : undefined);
  }

  /**
   * Edit Job
   *
   * @private
   * @param {Job} job
   *
   * @memberof JobListComponent
   */
  private editJob(job: IJob) {
    const { key, client } = job;
    const popover = this.popoverCtrl.create(CreateJobComponent, { clientKey: client, jobKey: key });
    popover.present();
  }

  /**
   * Edit Client
   *
   * @private
   * @param {Job} job
   *
   * @memberof JobListComponent
   */
  private editClient(job: IJob) {
    this.navCtrl.setRoot(EditClientComponent, {
      key: job.client,
      page: JobListComponent
    }, { animate: true, direction: 'forward' });
  }

  /**
   * Has an Inspection Report
   *
   * @private
   * @param {any} job
   * @returns
   *
   * @memberof JobListComponent
   */
  private hasInspectionReport(job) {
    return this.stateManager.getModel('inspections')
      .map<IInspection[], boolean>(inspections => {
        return inspections.some(inspection => {
          return inspection.clientId === job.client &&
            inspection.date === job.date &&
            (inspection.jobKey ? inspection.jobKey === job.key : true);
        });
      });
  }

  /**
   * Generate Inspection
   *
   * @private
   * @param {any} sideMenuType
   *
   * @memberof CreateInspectionComponent
   */
  private openInspection(job: IJob) {

    // load inspection items 
    this.stateManager.getModel('inspections')
      .switchMap<IInspection[], IInspection>(inspections => inspections)
      .filter(inspection =>
        inspection.clientId === job.client &&
        inspection.date === job.date &&
        (inspection.jobKey ? inspection.jobKey === job.key : true)
      )
      .take(1)
      .subscribe(inspection => {

        // load inspection items for the client based on date
        this.actions.loadInspection(inspection);

        // load sideMenu view with inspectionMenu
        this.actions.changeSideMenu('inspectionMenu');

        // store the client's info in state manager
        this.actions.openedInspectionReport(job.client, job.key, job.date);

        // this.goToPage(InspectionSheetComponent, { name: inspection['Chimney-0'].name, uuid: 0 });
        const prop = this.getChimneyName.get(inspection)[0];
        const inspectionItem = {
          name: inspection[prop].name,
          uuid: inspection[prop].uuid,
          index: 0,
          parentIndex: undefined,
          locationIndex: undefined,
          type: undefined,
          notValid: false
        };
        this.goToPage.execute(InspectionSheetComponent, inspectionItem);
        this.actions.openedInspection(inspectionItem);
        this.actions.setActiveMenuItem(inspectionItem);

      }, (error) => console.log(error));
  }

  private filterJobListByDateRange = (c: { jobs: IJob[], [name: string]: any }) => {
    if (c.date === undefined) {
      return Observable.of(c);
    }

    const jobs = c.jobs
      .filter(job => moment(job.date).isBetween(c.date.startDate, c.date.endDate, null, '[]'));

    return Observable.of(Object.assign({}, c, { jobs }));
  }

  /**
   * filterJobListByCrew
   *
   * @private
   * @param {{ crew: string }} param
   *
   * @memberof JobListComponent
   */
  private filterJobListByCrew = (c: { jobs: IJob[], [name: string]: any }) => {

    if (c.crew === 'All Crews' || c.crew === undefined) {
      return Observable.of(c);
    }

    return this.stateManager.getModel('crews')
      .switchMap<ICrew[], ICrew>(crews => crews)
      .filter(crew => crew.name === c.crew)
      .map(crew => {
        const jobs = c.jobs
          .filter(job => job.assigned === crew.key);

        return Object.assign({}, c, { jobs });
      });
  }

  private filterJobListByClient = (c: { [name: string]: any }) => {
    if (c.client === 'All Clients' || c.client === undefined) {
      return Observable.of({ jobs: c.jobs || this.jobsInView, ...c });
    }

    return this.stateManager.getModel('clients')
      .switchMap<IClient[], IClient>(clients => clients)
      .filter(client => `${client.firstName.trim()} ${client.lastName.trim()}` === c.client)
      .map<IClient, { jobs: IJob[], [name: string]: any }>(client => {


        const jobs = c.jobs || this.jobsInView
          .filter(job => job.client === client.key);

        return { jobs, ...c };
      });
  }

  /**
   * subscription
   * 
   * @private
   * 
   * @memberof JobListComponent
   */
  private set subscription(subscription) {
    this.subscriptions.push(subscription);

  }

  /**
   * Sort jobs based on job order
   * 
   * @private
   * @param {IJob[]} jobs 
   * @param {string[]} jobsOrder 
   * @returns 
   * @memberof JobListComponent
   */
  private sortJobsByProp(jobs: IJob[], prop, reverse = false) {
    jobs = Array.from(jobs)
      .sort((x, y) => {
        // debugger;
        return this.sort.get(x[prop], y[prop]);
      });

    return reverse ? jobs.reverse() : jobs;
  }

  /**
   * Update indexes from reorder a job based on the full job list vs filter list
   * 
   * @private
   * @param {any} indexes 
   * @param {IJob[]} otherJobsList 
   * @param {IJob[]} jobsList 
   * @returns 
   * @memberof JobListComponent
   */
  private updateReorderIndexes(indexes, otherJobsList: IJob[], jobsList: IJob[]) {
    // pull the key for the two jobs reordered from the filter list
    const fromKey = jobsList[indexes.from].key;
    const toKey = jobsList[indexes.to].key;

    // pull the index for the two jobs reordered from the full list
    const from = otherJobsList.findIndex(job => job.key === fromKey);
    const to = otherJobsList.findIndex(job => job.key === toKey);

    // update the indexes to include the two jobs reorder indexes from the full list
    return Object.assign({}, indexes, { from, to });
  }

  private generateReport() {

    Observable.combineLatest<IUser, IClient, IReport[]>(
      this.stateManager.getModel('currentUser'),
      this.getOpenedClientInfo.get(),
      this.getOpenedInspectionReport.get()
    )
      .take(1)
      .subscribe(([user, client, inspectItemsInReport]) => {
        const { firstName, lastName, CSIA } = user;
        const name = `${firstName} ${lastName}`;

        this.goToPage.execute(ReportComponent, { user, client, inspectItemsInReport });

      });
  }

  /**
   * Adds additional days to day if it is a weekend
   * 
   * @readonly
   * @private
   * @memberof JobListComponent
   */
  private daysToAdd(day) {

    let daysToAdd = 0;

    if (day === 6) {
      daysToAdd = 2;
    } else if (day === 0) {
      daysToAdd = 1;
    }

    return daysToAdd;
  }

  /**
   * filterJobListByDate
   * 
   * @param {string} view 
   * @memberof JobListComponent
   */
  private filterJobListByDate(view: string, jobs: IJob[] = []) {
    const date = moment();
    let filterJob: IJob[] = [];

    date.add(this.daysToAdd(moment().day()), 'day');

    switch (DateFilterView[view]) {
      case DateFilterView.tomorrow:
        const day = date.add(1, 'day').day();
        date.add(this.daysToAdd(day), 'day');
        filterJob = jobs.filter(job => date.isSame(job.date, 'day'));
        break;
      case DateFilterView.all:
        filterJob = jobs;
        break;
      case DateFilterView.incomplete:
        filterJob = jobs
          .filter(job => job.status === Status.open)
          .filter(job => moment().isAfter(job.date, 'day'));
        break;
      case DateFilterView.completed:
        filterJob = jobs
          .filter(job => [Status.closed, Status.downloaded].includes(job.status))
          .filter(job => date.isSame(job.date, 'day'));
        break;
      default:
        filterJob = jobs
          .reduce((acc, job) => {
            if (date.isSame(job.date, 'day')) {
              if (job.status === Status.open) {
                return acc.concat(job);
              }
            }
            return acc;
          }, []);
        break;
    }

    this.actions.setJobListViewState(view);

    this.elseBlock = !filterJob.length ? this.noJobsView : this.loadingView;

    this.ref.markForCheck();

    this.jobsInView = filterJob;

    return filterJob;

  }

  private fixDate = (jobs: IJob[]) => {
    return jobs.map(job => Object.assign({}, job, { date: this.getDate(job.date) }));
  }

  private filterDateView = (jobs: IJob[]) => {
    return this.filterJobListByDate(DateFilterView[this.dateView], jobs);
  }

  private filterByClient(jobs: IJob[], clients: IClient[]) {
    return jobs.filter(job => clients.some(client => client.key === job.client));
  }

  private filterByAssignCrew({ jobs, currentUserCrew }) {
    return jobs.filter(job => currentUserCrew.name === 'Admin' || job.assigned === currentUserCrew.key);
  }

  private sortByCrews(_jobs: IJob[]) {
    return Observable.from(_jobs)
      .groupBy((job) => job.date)
      .mergeMap(innerObs => innerObs

        .groupBy(job => job.assigned)
        .mergeMap(obs => obs.toArray().map(jobs => {
          return this.sortJobsByProp(jobs, 'order');
        }))
        .reduce((acc, jobs) => Array.of(...acc, ...jobs), [])
      )
      .reduce((acc, jobs) => Array.of(...acc, ...jobs), []);
  }



  private getDate(date) {
    const justDate = moment(date).format('YYYY-MM-DD');
    return moment(justDate).valueOf();
  }

  private getCrewNameForUser(crew$: Observable<ICrew[]>, currentUser$: Observable<IUser>) {
    return crew$
      .skipWhile(crews => !crews.length)
      .withLatestFrom(currentUser$)
      .take(1)
      .map(([crews, currentUser]) => crews.find(crew => crew.key === currentUser.crew))
      .map(crew => ({ currentUserCrew: crew }));

  }

  private setCurrentUserView(currentUser$: Observable<IUser>) {
    currentUser$
      .skipWhile(currentUser => !Object.keys(currentUser).length)
      .take(1)
      .subscribe(currentUser => {
        this.dateView = DateFilterView[currentUser.jobListView || 'today'];
        this.hasAllTab = currentUser.adminstration;
        this.hasTomorrowTab = currentUser.adminstration;
        this.hasCheckboxes = currentUser.adminstration;
        this.showReorder = currentUser.adminstration;
        this.showSyncBtn = !currentUser.online;
        this.ref.markForCheck();

      });
  }

  private disableInfinitieScroll = (disable) => {
    if (disable) {
      return of(!disable);
    }

    return this.currentUser$
      .pipe(
      map(this.hasCurrentViewJobs)
      );
  }

  private hasCurrentViewJobs = (user: IUser) => {
    const jobListView = DateFilterView[user.jobListView];
    if (jobListView === DateFilterView.all) {
      return true;
    }
    const hasCurrentViewJobs = this.nonFilterJobs.some(job => moment(job.date).isBefore(moment(), 'day'));
    return !hasCurrentViewJobs;
  }
}
