import { Component, OnInit } from '@angular/core';
import { ViewController, NavParams, PopoverController, Platform } from 'ionic-angular';
import { DesktopCalendarComponent } from '../../../../shared/components/desktop-calendar/desktop-calendar.component';
import { StateManagerService } from 'app/shared/services/sassy-state-manager-ng2/src/state-manager.service';
import { SortService } from '../../../../shared/services/sort.service';

import * as moment from 'moment';
import { IJob } from '../../../../shared/interfaces/job';


@Component({
  selector: 'wells-date-header',
  templateUrl: './date-header.component.html',
  styleUrls: ['./date-header.component.css']
})
export class DateHeaderComponent implements OnInit {

  public startDate: number;
  public endDate: number;
  public job: any = { startDate: undefined, endDate: undefined };

  constructor(
    private viewController: ViewController,
    private navParams: NavParams,
    private popoverCtrl: PopoverController,
    private stateManager: StateManagerService,
    private sort: SortService,
    public platform: Platform,
  ) { }




  ngOnInit() {
    this.stateManager.getModel('jobs')
      .map<IJob[], IJob[]>(jobs => Array.from(jobs).sort((x, y) => this.sort.get(x.date, y.date)))
      .map(jobs => ({ startDate: new Date(jobs[0].date).toISOString(), endDate: new Date(jobs[jobs.length - 1].date).toISOString() }))
      .subscribe(job => {
        this.job = job;
      })

  }

  public openCalendar($event, type) {
    const popover = this.popoverCtrl.create(DesktopCalendarComponent, undefined, { cssClass: 'hidden' });

    popover.onDidDismiss((date) => {
      date = this.getDate(date);
      const isAllow = type === 'startDate' ? moment(date).isSameOrBefore(this.job.endDate) : moment(date).isSameOrAfter(this.job.startDate);
      if (isAllow) {
        this.job[type] = date;
      }
    });

    popover.present({
      ev: $event
    });
  }

  /**
   * Close Popover
   * 
   * 
   * @memberof CreateJobComponent
   */
  public closePopover() {
    this.viewController.dismiss();
  }

  /**
   * Close Popover
   * 
   * 
   * @memberof CreateJobComponent
   */
  public filter(item) {
    this.viewController.dismiss({ startDate: this.job.startDate, endDate: this.job.endDate });
  }

  private getDate(date) {
    const justDate = moment(date).format('YYYY-MM-DD');
    return moment(justDate).valueOf();
  }

}
