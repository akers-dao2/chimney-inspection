import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { ViewController } from 'ionic-angular';

import { JobHeaderMenuComponent } from './job-header-menu.component';

import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ClientNamePipe } from '../../../../shared/pipes/client-name.pipe';
import { CrewNamePipe } from '../../../../shared/pipes/crew-name.pipe';

describe('JobHeaderMenuComponent', () => {
  let component: JobHeaderMenuComponent;
  let fixture: ComponentFixture<JobHeaderMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        JobHeaderMenuComponent,
        ClientNamePipe,
        CrewNamePipe
      ],
      providers: [
        {
          provide: ViewController, factory: () => {
            return {};
          }
        }
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ]
    })
      .compileComponents();
  }));



  beforeEach(() => {
    fixture = TestBed.createComponent(JobHeaderMenuComponent);
    component = fixture.componentInstance;
    component.viewCtrl = {
      data: {
        crews: []
      }
    } as any;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
