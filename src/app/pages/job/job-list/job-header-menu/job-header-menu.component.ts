import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';

import { ViewController } from 'ionic-angular';

import { ICrew } from '../../../../shared/interfaces/crew';

@Component({
  selector: 'wells-job-header-menu',
  templateUrl: './job-header-menu.component.html',
  styleUrls: ['./job-header-menu.component.scss']
})
export class JobHeaderMenuComponent implements OnInit {
  public items: any;
  public form: FormGroup;

  constructor(
    public viewCtrl: ViewController,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.items = [];
    this.createForm();
    this.form.valueChanges
      .map(({ search }) =>
        !search.length ? [] : this.filterItems(search)
      )
      .subscribe(value =>
        this.items = value
      );
  }

  public filterItems(search) {
    return this.viewCtrl.data.items.filter(item => item.toLowerCase().includes(search.toLowerCase()));
  }

  public onClick(item: any) {
    this.viewCtrl.dismiss({ item });
  }

  public createForm() {
    this.form = this.fb.group({
      search: ['']
    });
  }
}
