import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ViewController, NavParams, PopoverController, Platform } from 'ionic-angular';

import * as moment from 'moment';

/** Interfaces */
import { ICrew } from '../../../shared/interfaces/crew';
import { IJob } from '../../../shared/interfaces/job';

/** Services */
import { StateManagerService } from 'app/shared/services/sassy-state-manager-ng2/src/state-manager.service';
import { ActionService } from 'app/shared/services/action.service';
import { DataService } from 'app/shared/services/data.service';
import { AuditRecord } from '../../../shared/classes/audit-record';


/** Classes */
import { Job } from '../../../shared/classes/job';

import { DesktopCalendarComponent } from '../../../shared/components/desktop-calendar/desktop-calendar.component';

@Component({
  selector: 'wells-create-job',
  templateUrl: './create-job.component.html',
  styleUrls: ['./create-job.component.scss']
})
export class CreateJobComponent implements OnInit {

  public job = {
    date: new Date().toISOString(),
    assigned: '',
    key: undefined
  };

  public crews: Observable<ICrew[]>;
  public clientKey: string;
  public jobKey: string;

  constructor(
    private stateManager: StateManagerService,
    private viewController: ViewController,
    private action: ActionService,
    private navParams: NavParams,
    private dataService: DataService,
    private popoverCtrl: PopoverController,
    public platform: Platform,
  ) { }

  /****************************************************************
   *
   * Public Methods
   *
   ****************************************************************/


  /**
   * OnInit life cycle hook
   * 
   * 
   * @memberof CreateJobComponent
   */
  public ngOnInit() {
    this.crews = this.stateManager.getModel('crews') as any;
    this.clientKey = this.navParams.get('clientKey');
    this.jobKey = this.navParams.get('jobKey');

    if (this.jobKey) {
      this.stateManager.getModel('jobs')
        .take(1)
        .subscribe((jobs: Job[]) => {
          const job = jobs.find(_job_ => (_job_.key === this.jobKey));

          this.job = {
            date: new Date(job.date).toISOString(),
            assigned: job.assigned,
            key: job.key
          };

        });
    }

    this.action.addAuditRecord(new AuditRecord('Access create job page', 'create-job'));

  }

  /**
   * Create or Update Job
   * 
   * 
   * @memberof CreateJobComponent
   */
  public createJob() {
    const { assigned, client, date, key } =
      Object.assign({}, this.job, { date: this.getDate(this.job.date), client: this.clientKey });

    this.stateManager.getModel('jobs')
      .withLatestFrom(this.stateManager.getModel('jobCount'))
      .map<[IJob[], any], IJob[]>(([jobs, jobCount]) => {
        const jobOrder = jobCount.value + 1;
        const job = new Job(
          client,
          assigned,
          0,
          date,
          false,
          false,
          jobOrder,
          key
        );

        if (!this.jobKey) {
          delete job.key;
        }

        this.dataService.addJob(job, this.jobKey !== undefined)
          .skipWhile(newClient => newClient === null)
          .pluck<Job, string>('newKey')
          .subscribe(newKey => {
            job.key = newKey;
            this.action.createJob(job);
            this.action.addJobToCount(jobOrder);

          }, error => console.log(error));

        this.closePopover();

        return jobs;
      })
      .take(1)
      .subscribe();
  }

  public startDate($event) {
    const popover = this.popoverCtrl.create(DesktopCalendarComponent, undefined, { cssClass: 'hidden' });

    popover.onDidDismiss((date) => {
      this.job.date = date;
    });

    popover.present({
      ev: $event
    });
  }

  /**
   * Close Popover
   * 
   * 
   * @memberof CreateJobComponent
   */
  public closePopover() {
    this.viewController.dismiss();
  }

  private getDate(date) {
    const justDate = moment(date).format('YYYY-MM-DD');
    return moment(justDate).valueOf();
  }
}
