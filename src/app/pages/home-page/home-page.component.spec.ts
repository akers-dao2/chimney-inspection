import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { SharedModule } from '../../shared/shared.module';
import { HomePageComponent } from './home-page.component';

import {
  NavParams,
  ItemSliding,
  App, Config,
  Form,
  IonicModule,
  Keyboard,
  DomController,
  MenuController,
  NavController,
  Platform,
  GestureController,
  ModalController
} from 'ionic-angular';

import { ConfigMock, PlatformMock } from '../../shared/mocks/ionic-mocks';

import { Client } from '../../shared/classes/client';

import { DataService } from '../../shared/services/data.service';
import { ActionService } from '../../shared/services/action.service';
import { StateManagerService } from 'app/shared/services/sassy-state-manager-ng2/src/state-manager.service';
import { UtilityService } from '../../shared/services/utility.service';
import { FirebaseService } from '../../shared/services/firebase.service';
import { ChimneyNamesService } from '../../shared/services/chimney-names.service';
import { PdfMakerService } from '../../shared/services/pdf-maker.service';
import { LoginService } from '../../shared/services/login.service';

xdescribe('HomePageComponent', () => {
  let component: HomePageComponent;
  let fixture: ComponentFixture<HomePageComponent>;
  let actionService: ActionService;
  let stateManager: StateManagerService;
  let dataService: DataService;
  let modalController: ModalController;
  let loginService: LoginService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        SharedModule,
        IonicModule.forRoot(HomePageComponent)
      ],
      declarations: [
        HomePageComponent
      ],
      providers: [
        DataService,
        ActionService,
        StateManagerService,
        UtilityService,
        FirebaseService,
        ChimneyNamesService,
        LoginService,
        App,
        DomController,
        Form,
        Keyboard,
        MenuController,
        NavController,
        GestureController,
        ModalController,
        { provide: NavParams, useClass: () => { } },
        { provide: Platform, useClass: PlatformMock },
        { provide: Config, useClass: ConfigMock },
        PdfMakerService
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ]
    });
  });

  beforeEach(inject(
    [
      ActionService,
      StateManagerService,
      DataService,
      ModalController,
      LoginService
    ],
    (
      _actionService_: ActionService,
      _stateManager_: StateManagerService,
      _dataService_: DataService,
      _ModalController_: ModalController,
      _loginService_: LoginService
    ) => {
      actionService = _actionService_;
      stateManager = _stateManager_;
      dataService = _dataService_;
      modalController = _ModalController_;
      loginService = _loginService_;

      spyOn(actionService, 'loadJobs');
      spyOn(actionService, 'loadCrews');
      spyOn(actionService, 'loadClients');
      
      spyOn(stateManager, 'getModel').and.callFake(() => new BehaviorSubject([]));

      spyOn(dataService, 'getUserInfo').and.callFake(() => new BehaviorSubject({}));
      spyOn(dataService, 'signIn').and.callFake(() => new BehaviorSubject({}));
      spyOn(dataService, 'getJobs').and.callFake(() => new BehaviorSubject({}));
      spyOn(dataService, 'getCrews').and.callFake(() => new BehaviorSubject({}));
      spyOn(dataService, 'getClients').and.callFake(() => new BehaviorSubject({}));
      spyOn(dataService, 'getjob').and.callFake(() => new BehaviorSubject({}));

      spyOn(modalController, 'create').and.returnValue({ present: () => { } });

      spyOn(loginService, 'displayLogin');
      spyOn(loginService, 'setCurrentUser');

    }));

  beforeEach(inject([], () => {
    fixture = TestBed.createComponent(HomePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call openSearchModal()', () => {
    component.openSearchModal();

    expect(modalController.create).toHaveBeenCalled();
  });
});
