import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { ModalController } from 'ionic-angular';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/skipWhile';

/** Components */
import { SearchModalComponent } from '../search-modal/search-modal.component';


@Component({
  selector: 'wells-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  public signature: string;

  private subscriptions: Subscription[] = [];

  constructor(
    private modalCtrl: ModalController,
  ) { }

  /****************************************************************
   * 
   * Public Methods
   *
   ****************************************************************/

  /**
   * OnInit life cycle hook
   *
   *
   * @memberOf HomePageComponent
   */
  public ngOnInit() { }

  /**
   * Open the search modal for job and client search
   *
   *
   * @memberOf HomePageComponent
   */
  public openSearchModal() {
    const modal = this.modalCtrl.create(SearchModalComponent);
    modal.present();
  }

}
