import {
  Component, OnInit,
  TemplateRef, OnDestroy, ViewChild,
  ChangeDetectionStrategy, ChangeDetectorRef
} from '@angular/core';

import { StateManagerService } from 'app/shared/services/sassy-state-manager-ng2/src/state-manager.service';

import { SetState, AutoUnsubscribe, StateDecoratorChange } from '../../shared/decorators/state-manager-core'

import { IAuditRecord } from '../../shared/interfaces/audit-record';

import { ActionService } from '../../shared/services/action.service'
import { DataService } from '../../shared/services/data.service'

interface IState {
  auditRecords: IAuditRecord[]
}

@SetState({
  models: ['auditRecords']
})
@Component({
  selector: 'wells-audit-report',
  templateUrl: './audit-report.component.html',
  styleUrls: ['./audit-report.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class AuditReportComponent implements OnInit, OnDestroy, StateDecoratorChange {
  public elseBlock: TemplateRef<any> = null;
  public state: IState;
  public auditRecords: IAuditRecord[] = [];
  public subscriptions: any[] = [];

  @ViewChild('noRecords') noRecordsView: TemplateRef<any> = null;
  @ViewChild('loading') loadingView: TemplateRef<any> = null;

  constructor(
    private stateManager: StateManagerService,
    private ref: ChangeDetectorRef,
    private action: ActionService,
    private dataService: DataService,
  ) { }

  ngOnInit() {
    // start with the loadingView
    this.loadAuditRecords();
    this.elseBlock = this.loadingView;
  }

  ngStateDecoratorChange(state: IState) {
    this.auditRecords = state.auditRecords;
  }

  @AutoUnsubscribe()
  ngOnDestroy() {

  }

  private loadAuditRecords() {
    this.subscription = this.dataService.getAuditRecords()
      .skipWhile(auditRecords => !auditRecords.length)
      .take(1)
      .subscribe(
      (auditRecords: IAuditRecord[]) => this.action.loadAuditRecords(auditRecords),
      error => console.log(error)
      );
  }

  private set subscription(subscription) {
    this.subscriptions.push(subscription)
  }

}
