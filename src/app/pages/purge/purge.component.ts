import { Component, OnInit } from '@angular/core';
import { DataService } from 'app/shared/services/data.service';
import { tap, filter, switchAll, take, map, toArray, concatMap } from 'rxjs/operators';
import { IInspection } from 'app/shared/interfaces/inspection';
import { IInspectionReport } from 'app/shared/interfaces/inspection-report';
import * as moment from 'moment';
import { IJob } from 'app/shared/interfaces';

@Component({
  selector: 'wells-purge',
  templateUrl: './purge.component.html',
  styleUrls: ['./purge.component.scss']
})
export class PurgeComponent implements OnInit {
  private filterByDate = filter<IInspectionReport>(r => {
    const endDate = moment().subtract(30, 'days');
    return r.reports ? moment(parseInt(Object.keys(r.reports)[0], 10)).isSameOrBefore(endDate) : true;
  });

  constructor(private dataService: DataService) { }

  async ngOnInit() {

    const inspectionReports: IInspectionReport[] = await this.dataService.getInspectionReports(5000)
      .pipe(
        filter(r => !!r.length),
        take<any>(1),
        switchAll<any>(),
        map<any, IInspectionReport>(r => ({ ...r[Object.keys(r)[0]], inspectionReportKey: r.key })),
        this.filterByDate,
        concatMap((j: any) => this.dataService.deleteInspectionReport(j.inspectionReportKey)),
        // toArray(),
        tap(console.log)
      )
      .toPromise();

    // this.dataService.getJobs(5000)
    //   .pipe(
    //     filter(r => !!r.length),
    //     take<any>(1),
    //     switchAll<IJob>(),
    //     filter(j => inspectionReports.some(r => r.jobKey === j.key)),
    //     concatMap(j => this.dataService.deleteJob(j.key)),
    //     // toArray(),
    //     tap(console.log)
    //   )
    //   .subscribe();

    // this.dataService.getInspections(5000)
    //   .pipe(
    //     filter(r => !!r.length),
    //     take<any>(1),
    //     switchAll<IInspection>(),
    //     filter(j => inspectionReports.some(r => r.jobKey === j.jobKey)),
    //     concatMap(j => this.dataService.deleteInspection(j.key)),
    //     // toArray(),
    //     tap(console.log)
    //   )
    //   .subscribe();

    // this.dataService.getClients(5000)
    //   .pipe(
    //     filter(r => !!r.length),
    //     take<any>(1),
    //     switchAll<IInspection>(),
    //     filter(j => inspectionReports.some(r => r.clientId === j.key)),
    //     concatMap(j => this.dataService.deleteClient(j.key)),
    //     // toArray(),
    //     tap(console.log)
    //   )
    //   .subscribe();
  }



}
