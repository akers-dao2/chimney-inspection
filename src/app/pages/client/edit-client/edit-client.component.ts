import { Component, OnInit, OnDestroy } from '@angular/core';
import { NavParams, AlertController, NavController, PopoverController, ViewController } from 'ionic-angular';
import { StateManagerService } from 'app/shared/services/sassy-state-manager-ng2/src/state-manager.service';
import { Observable } from 'rxjs/Observable';

import { DataService } from 'app/shared/services/data.service';
import { ActionService } from 'app/shared/services/action.service';
import { PresentToastService } from '../../../shared/services/present-toast.service'
import { GoToPageService } from '../../../shared/services/go-to-page.service';
import { AuditRecord } from '../../../shared/classes/audit-record';

import { Client } from 'app/shared/classes/client';

import { ClientListComponent } from '../client-list/client-list.component';
import { InspectionSheetComponent } from '../../inspection/inspection-sheet/inspection-sheet.component';
import { BoroTownshipSelectComponent } from '../../client/boro-township-select/boro-township-select.component';

@Component({
  selector: 'wells-edit-client',
  templateUrl: './edit-client.component.html',
  styleUrls: ['./edit-client.component.scss']
})
export class EditClientComponent implements OnInit, OnDestroy {
  public firstName: string;
  public middleName: string;
  public lastName: string;
  public address: string;
  public city: string;
  public state: string;
  public zip: string;
  public boro: string;
  public townships$: Observable<string[]>;
  public subscriptions: any[] = [];

  private id: any;
  private page: any;
  private location: string;
  private modal: string;

  constructor(
    private params: NavParams,
    private stateManager: StateManagerService,
    private action: ActionService,
    private dataService: DataService,
    private alertCtrl: AlertController,
    private nav: NavController,
    private presentToast: PresentToastService,
    private popoverCtrl: PopoverController,
    private viewCtrl: ViewController,
    private goToPage: GoToPageService,
  ) { }


  /**
   * OnInit LifeCycle Hook
   *
   * @memberOf EditClientComponent
   */
  ngOnInit() {
    this.page = this.params.get('page') || ClientListComponent;
    this.location = this.params.get('location');
    this.modal = this.params.get('modal');

    this.townships$ = this.stateManager.getModel('townships') as any

    this.stateManager.getModel('clients')
      .take(1)
      .subscribe((clients: Client[]) => {
        const client = clients.find(_client => (_client.key === this.params.get('key')));

        this.id = this.params.get('key');
        this.firstName = client.firstName;
        this.lastName = client.lastName;
        this.address = client.address;
        this.city = client.city;
        this.state = client.state;
        this.zip = client.zip;
        this.boro = client.township;
      });

    this.action.addAuditRecord(new AuditRecord('Access edit client page', 'edit-client'));

  }


  /**
   * Create new client action
   *
   * @memberOf AddClientComponent
   */
  public updateClient() {
    const client = new Client(
      this.firstName,
      this.lastName,
      this.address,
      this.city,
      this.state,
      this.zip,
      this.boro
    );

    client.key = this.id;

    if (client.isValid) {
      this.action.updateClient(client);
      this.dataService.updateClient(client);
      this.presentToast.show('This client has been updated!');
      this.navigateToPage()

      this.action.addAuditRecord(new AuditRecord(`Edit client #key: ${this.id}`, 'edit-client'));
    } else {
      this.showAlert('Please make sure all field are filled out!');
    }
  }


  /**
   * Cancels create client
   *
   * @memberOf AddClientComponent
   */
  public cancel() {
    this.navigateToPage()
  }


  /**
   * Pops alert
   *
   *
   * @memberOf AddClientComponent
   */
  public showAlert(msg: string) {
    const alert = this.alertCtrl.create({
      title: 'Alert!',
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present();
  }

  /**
   * Show Boro/Township select popover
   * 
   * @param {any} $event 
   * 
   * @memberof EditClientComponent
   */
  showBoroTownshipSelect($event) {
    const popover = this.popoverCtrl.create(BoroTownshipSelectComponent);
    popover.onDidDismiss(({ boro }) => {
      if (boro !== null) {
        this.boro = boro;
      }

    });

    popover.present({
      ev: $event
    });
  }

  public ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe)
  }

  private navigateToPage() {
    switch (this.location) {
      case 'inspectionSheet':
        this.action.changeSideMenu('inspectionMenu');
        this.subscription = this.stateManager.getModel('openedInspection')
          .take(1)
          .subscribe(inspectionItem => {
            this.action.setActiveMenuItem(inspectionItem);

            if (this.modal) {
              this.viewCtrl.dismiss();
            } else {
              this.goToPage.execute(InspectionSheetComponent, inspectionItem)
            }
          })
        break;

      default:
        if (this.modal) {
          this.viewCtrl.dismiss();
        } else {
          this.goToPage.execute(this.page);
        }
        break;
    }
  }

  private set subscription(subscription) {
    this.subscriptions.push(subscription)
  }
}
