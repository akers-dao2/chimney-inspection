import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { StateManagerService } from 'app/shared/services/sassy-state-manager-ng2/src/state-manager.service';
import { ViewController, Content } from 'ionic-angular';

import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'wells-boro-township-select',
  templateUrl: './boro-township-select.component.html',
  styleUrls: ['./boro-township-select.component.scss']
})
export class BoroTownshipSelectComponent implements OnInit, OnDestroy {

  public townships$: Observable<string[]>;
  public subscriptions: any[] = [];

  @ViewChild(Content) content: Content;

  constructor(
    private stateManager: StateManagerService,
    private viewController: ViewController
  ) { }

  ngOnInit() {
    this.townships$ = this.stateManager.getModel('townships') as any
    this.content.resize();
  }

  /**
   * Filter Townships
   * 
   * @param {any} $event 
   * 
   * @memberof BoroTownshipSelectComponent
   */
  public filterTownships($event) {
    this.subscription = this.stateManager.getModel('townships')
      .map(townships =>
        townships
          .filter(township =>
            $event === 'clear' ? true : township.toLowerCase().includes($event.target.value.toLowerCase())
          )
      )
      .subscribe(townships =>
        this.townships$ = Observable.of(townships)
      )
  }

  /**
   * onItemClick
   * 
   * @param {any} item 
   * 
   * @memberof BoroTownshipSelectComponent
   */
  public onItemClick(boro) {
    this.viewController.dismiss({ boro });
  }

  public ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe)
  }

  private set subscription(subscription) {
    this.subscriptions.push(subscription)
  }
}
