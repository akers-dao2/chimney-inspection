import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoroTownshipSelectComponent } from './boro-township-select.component';

describe('BoroTownshipSelectComponent', () => {
  let component: BoroTownshipSelectComponent;
  let fixture: ComponentFixture<BoroTownshipSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoroTownshipSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoroTownshipSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
