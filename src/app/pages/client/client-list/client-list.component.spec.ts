/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { StateManagerService } from 'app/shared/services/sassy-state-manager-ng2/src/state-manager.service';
import { IonicModule } from 'ionic-angular';

import { Observable } from 'rxjs/Observable';
import { ClientListComponent } from './client-list.component';


xdescribe('ClientListComponent', () => {
  let component: ClientListComponent;
  let fixture: ComponentFixture<ClientListComponent>;
  let functionTest: Function;
  const fakeData = {name: 'fake_client'};
  const fakeGetModel = jasmine.createSpy('getModel');

  class MockStateManagerService {
    getModel(str) {
      fakeGetModel(str);

      functionTest = () => {
        return fakeData;
      };
      return Observable.of([]);
    }
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        IonicModule.forRoot(ClientListComponent)
      ],
      declarations: [ClientListComponent],
      providers: [
        { provide: StateManagerService, useClass: MockStateManagerService }
      ]
    })
    .compileComponents();
  }));

  beforeEach(inject([StateManagerService], (_StateManagerService_) => {
    fixture = TestBed.createComponent(ClientListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('Life Cycle methods', () => {
    it('should do setup - ngOnInit()', () => {
      expect(fakeGetModel).toHaveBeenCalledWith('clients');
      expect(functionTest()).toEqual(fakeData);
    });
  });
});
