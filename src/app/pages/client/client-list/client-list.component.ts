import { Component, OnInit, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef, ViewEncapsulation } from '@angular/core';
import { StateManagerService } from 'app/shared/services/sassy-state-manager-ng2/src/state-manager.service';
import { Observable } from 'rxjs/Observable';

import { AlertController, NavController, PopoverController, ToastController, Toast } from 'ionic-angular';

import { DataService } from 'app/shared/services/data.service';
import { ActionService } from 'app/shared/services/action.service';
import { AuditRecord } from '../../../shared/classes/audit-record';
import { SortService } from '../../../shared/services/sort.service';

import { EditClientComponent } from '../edit-client/edit-client.component';
import { CreateJobComponent } from '../../job/create-job/create-job.component';
import { JobHeaderMenuComponent } from '../../job/job-list/job-header-menu/job-header-menu.component';

import { ClientNamePipe } from '../../../shared/pipes/client-name.pipe';

import { Client } from 'app/shared/classes/client';

import { IClient } from 'app/shared/interfaces';
// tslint:disable-next-line:max-line-length
import { take, skipWhile, bufferCount, switchAll, delay, switchMap, concatMap, tap, publishReplay, filter, refCount, map, toArray } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { from } from 'rxjs/observable/from';
import { ClientRecordPipe } from '../../../shared/pipes/client-record.pipe';




@Component({
  selector: 'wells-client-list',
  templateUrl: './client-list.component.html',
  styleUrls: ['./client-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ClientListComponent implements OnInit, OnDestroy {
  public clients: IClient[] = [];
  public clientsList: IClient[] = [];
  public subscriptions: any[] = [];
  private clients$: Observable<IClient[]>;
  private toast: Toast;

  constructor(
    private stateManager: StateManagerService,
    private alertCtrl: AlertController,
    private actions: ActionService,
    private dataService: DataService,
    private nav: NavController,
    private popoverCtrl: PopoverController,
    private ref: ChangeDetectorRef,
    private sort: SortService,
    private toastCtrl: ToastController
  ) { }

  /****************************************************************
   * 
   * Public Methods
   *
   ****************************************************************/

  /**
   * OnInit LifeCycle Hook
   *
   *
   * @memberOf ClientListComponent
   */
  ngOnInit() {
    const skipUndefined = skipWhile(t => t === undefined);
    // tslint:disable-next-line:max-line-length
    const sortClientsAndStreamByLastName = switchMap<IClient[], IClient>(c => from(c.slice(0).sort((x, y) => this.sort.get(x.lastName, y.lastName))));
    const sortClientsByLastName = switchMap<IClient[], IClient[]>(c => of(c.slice(0).sort((x, y) => this.sort.get(x.lastName, y.lastName))));
    this.clients$ = this.stateManager.getModel('clients').pipe(skipUndefined, sortClientsByLastName, publishReplay(1), refCount());

    this.clients$
      .pipe(
        tap(v => this.clientsList = v)
      )
      .subscribe();

    this.clients$
      .pipe(
        tap(this.presentToast),
        take<any>(1),
        switchAll<IClient>(),
        bufferCount(50),
        concatMap((v, i) => i === 0 ? of(v) : of(v).pipe(delay(3000))),
        tap(v => {
          this.clients = this.clients.concat(v);
          this.ref.markForCheck();
        })
      )
      .subscribe(null, null, () => this.toast.dismiss());


    // this.action.addAuditRecord(new AuditRecord('Access client list page', 'client-list'));

  }

  public trackByFn(i, v) {
    return v.id;
  }

  public filterClientsList(searchItem: any) {

    if (searchItem.type === 'mousedown') {
      this.clients = this.clientsList;
      return;
    }

    from(this.clients$)
      .pipe(
        tap<any>(null),
        take(1),
        switchAll<IClient>(),
        filter(client =>
          `${client.firstName.toLowerCase().trim()} ${client.lastName.toLowerCase().trim()}`.includes(searchItem.target.value.toLowerCase())
        ),
        toArray()
      )
      .subscribe(clients => {
        this.clients = clients.slice(0).sort((x, y) => this.sort.get(x.firstName, y.firstName));
        this.ref.markForCheck();
      });
  }

  private presentToast = () => {
    this.toast = this.toastCtrl.create({
      message: 'Loading Clients',
      cssClass: 'cust-toast'
    });

    this.toast.present();
  }

  /**
   * show a popup of inspection items for a particular client
   *
   * @param {Client} client
   *
   * @memberOf ClientListComponent
   */
  public presentPopup(client: Client) {

    const name = `${(client.firstName || '')} ${(client.lastName || '')}`;

    const alert = this.alertCtrl.create({
      title: name,
      message: '',
      cssClass: 'inspection',
      buttons: [
        {
          text: 'Create Job',
          handler: () => {
            this.createJob(client.key);
          }
        },
        {
          text: 'Edit Client',
          handler: () => {
            this.gotoPage(client.key, EditClientComponent);
          }
        },
        {
          text: 'Delete Client',
          handler: () => {
            this.confirmDelete(client.key);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
      ]
    });
    alert.present();
  }

  public displayFilterPopover($event, type) {

    switch (type) {
      case 'name':
        this.objectNamePipe('key', this.getClientNames)
          .map(clients => Array.of('All Clients', ...clients))
          .switchMap<any[], { item: string }>(items => Observable.fromPromise(this.presentPopover($event, items)))
          .take(1)
          .subscribe(data => this.filterJobListByClient(data));
        break;

      default:
        break;
    }
  }

  public ngOnDestroy() {
    this.toast.dismiss();
    this.subscriptions.forEach(subscription => subscription.unsubscribe);
  }


  /****************************************************************
   * 
   * Private Methods
   *
   ****************************************************************/

  private getClientNames = (key) => {
    return this.clients$
    .pipe(
      tap<any>(null),
      switchAll<IClient>(),
      filter(client => client.key === key),
      map(c => `${c.firstName} ${c.lastName}`)
    );
  }

  private filterJobListByClient(param: { item: string }) {
    if (param.item === 'All Clients') {
      this.clients = this.clientsList;
      this.ref.markForCheck();
      return;
    }

    from(this.clients$)
      .pipe(
        tap<any>(null),
        switchAll<IClient>(),
        filter(client =>
          `${client.firstName.trim()} ${client.lastName.trim()}` === param.item
        ),
        take(1)
      )
      .subscribe(client => {
        this.clients = this.clientsList
          // .sort((x, y) => this.sort.get(x.lastName, y.lastName))
          .filter(c => c.key === client.key);

        // this.elseBlock = !this.jobs.length ? this.noJobsView : this.loadingView;

        this.ref.markForCheck();
      });
  }

  private objectNamePipe(prop, filterFunc) {
    return Observable.from(this.clientsList)
    .map(job => job[prop])
    .mergeMap<any, any>(item => filterFunc(item).take(1))
    .toArray();
  }

  /**
   * Confirm a delete for client
   *
   * @param {any} key
   *
   * @memberOf ClientListComponent
   */
  private confirmDelete(key) {
    const alert = this.alertCtrl.create({
      title: 'Delete Client',
      message: 'Are you sure you want to delete this client?',
      buttons: [
        {
          text: 'No',
          handler: () => { }
        },
        {
          text: 'Yes',
          handler: () => {
            // this.action.deleteClient(key);
            this.dataService.deleteClient(key);
          }
        }
      ]
    });
    alert.present();

  }


  /**
   * addComponentToNav
   *
   * @private
   *
   * @memberOf SideMenuComponent
   */
  private gotoPage(key: any, component: any) {
    this.nav.setRoot(component, { key }, { animate: true, direction: 'forward' });
  }

  /**
   * Create job for a client
   * 
   * @private
   * @param {any} clientKey 
   * 
   * @memberof ClientListComponent
   */
  private createJob(clientKey) {
    const popover = this.popoverCtrl.create(CreateJobComponent, { clientKey });
    popover.present();
  }

  private set subscription(subscription) {
    this.subscriptions.push(subscription);
  }

  private presentPopover($event, items) {
    return new Promise((resolve, reject) => {
      const popover = this.popoverCtrl.create(JobHeaderMenuComponent, { items });

      popover.onDidDismiss((data) => {
        if (data !== null) {
          resolve(data);
        }

      });

      popover.present({
        ev: $event
      });

    });
  }
}