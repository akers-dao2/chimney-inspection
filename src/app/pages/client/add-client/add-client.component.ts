import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { AlertController, NavController, PopoverController } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';

/** Classes */
import { Client } from '../../../shared/classes/client';
import { AuditRecord } from '../../../shared/classes/audit-record';

/** Services */
import { StateManagerService } from 'app/shared/services/sassy-state-manager-ng2/src/state-manager.service';
import { ActionService } from '../../../shared/services/action.service';
import { DataService } from '../../../shared/services/data.service';
import { PresentToastService } from '../../../shared/services/present-toast.service';

/** Components */
import { ClientListComponent } from '../client-list/client-list.component';
import { BoroTownshipSelectComponent } from '../../client/boro-township-select/boro-township-select.component';


@Component({
  selector: 'wells-add-client',
  templateUrl: './add-client.component.html',
  styleUrls: ['./add-client.component.scss']
})
export class AddClientComponent implements OnInit, OnDestroy {
  public firstName: string;
  public lastName: string;
  public address: string;
  public city: string;
  public state = 'PA'
  public zip: string;
  public boro: string;
  public townships$: Observable<string[]>;
  public subscriptions: any[] = [];

  constructor(
    private action: ActionService,
    private dataService: DataService,
    public alertCtrl: AlertController,
    public nav: NavController,
    public stateManager: StateManagerService,
    private popoverCtrl: PopoverController,
    private presentToast: PresentToastService,
  ) { }

  ngOnInit() {
    this.townships$ = this.stateManager.getModel('townships') as any
    this.action.addAuditRecord(new AuditRecord('Access add client page', 'add-client'));
  }
  /**
   * Create new client action
   *
   * @memberOf AddClientComponent
   */
  public addClient() {
    const client = new Client(
      this.firstName,
      this.lastName,
      this.address,
      this.city,
      this.state,
      this.zip,
      this.boro
    );

    if (client.isValid) {
      this.subscription = this.dataService.addClient(client)
        .skipWhile(newClient => newClient === null)
        .pluck<Client, string>('newKey')
        .subscribe(key => {
          client.key = key;
          // this.action.addClient(client);
          this.action.addAuditRecord(new AuditRecord(`Add new client #key: ${key}`, 'add-client'));

        }, error => console.log(error))

      this.presentToast.show('Your new client has been added!');
      this.nav.setRoot(ClientListComponent, null, { animate: true, direction: 'forward' });
    } else {
      this.showAlert('Please make sure all field are filled out!');
    }
  }


  /**
   * Pops alert
   *
   *
   * @memberOf AddClientComponent
   */
  public showAlert(msg: string) {
    const alert = this.alertCtrl.create({
      title: 'Alert!',
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present();
  }

  public ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe)
  }

  /**
   * Show Boro/Township select popover
   * 
   * @param {any} $event 
   * 
   * @memberof EditClientComponent
   */
  showBoroTownshipSelect($event) {
    const popover = this.popoverCtrl.create(BoroTownshipSelectComponent);
    popover.onDidDismiss(({ boro }) => {
      if (boro !== null) {
        this.boro = boro;
      }

    });

    popover.present({
      ev: $event
    });
  }

  private set subscription(subscription) {
    this.subscriptions.push(subscription)
  }

}
