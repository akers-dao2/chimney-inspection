import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { AlertController, NavController, IonicModule } from 'ionic-angular';
import { ActionService } from '../../../shared/services/action.service';
import { DataService } from '../../../shared/services/data.service';
import { StateManagerService } from 'app/shared/services/sassy-state-manager-ng2/src/state-manager.service';
import { UtilityService } from '../../../shared/services/utility.service';
import { FirebaseService } from '../../../shared/services/firebase.service';
import { ChimneyNamesService } from '../../../shared/services/chimney-names.service';

import { Client } from '../../../shared/classes/client';

import { AddClientComponent } from './add-client.component';
import { IClient } from 'app/shared/interfaces';

xdescribe('AddClientComponent', () => {
  let component: AddClientComponent;
  let fixture: ComponentFixture<AddClientComponent>;

  let nav: NavController;
  let alert: AlertController;
  let actions: ActionService;
  let data: DataService;

  const fakeClientObj = new Client(
    'fake first',
    'fake middle',
    'fake last',
    'fake address',
    'fake city',
    'fake state',
    'fake zip'
  );

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        IonicModule.forRoot(AddClientComponent)
      ],
      declarations: [AddClientComponent],
      providers: [
        FirebaseService,
        UtilityService,
        StateManagerService,
        NavController,
        AlertController,
        ActionService,
        DataService,
        ChimneyNamesService
      ],
    })
      .compileComponents();
  }));

  beforeEach(inject([NavController, AlertController, ActionService, DataService],
    (_NavController_, _AlertController_, _ActionService_, _DataService_) => {
      nav = _NavController_;
      alert = _AlertController_;
      actions = _ActionService_;
      data = _DataService_;

      fixture = TestBed.createComponent(AddClientComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();

      spyOn(actions, 'addClient');
      spyOn(data, 'addClient');
      spyOn(alert, 'create').and.callThrough();
      spyOn(nav, 'pop');
      spyOn(component, 'showAlert').and.callFake(() => {
        alert.create({
          title: 'Alert!',
          subTitle: 'fake message',
          buttons: ['OK']
        });
      });

      component.firstName = fakeClientObj.firstName;
      component.lastName = fakeClientObj.lastName;
      component.address = fakeClientObj.address;
      component.state = fakeClientObj.state;
      component.zip = fakeClientObj.zip;
      component.boro = fakeClientObj.township;
      component.city = fakeClientObj.city;
    }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('Custom Methods', () => {
    it('should be able to add clients - addClient()', () => {
      component.addClient();

      expect(actions.addClient).toHaveBeenCalledWith(fakeClientObj);
      expect(data.addClient).toHaveBeenCalledWith(fakeClientObj);
      expect(component.showAlert).toHaveBeenCalled();
    });

    it('should throw an alert if all field arent enter with values - addClient()', () => {
      component.boro = undefined;
      component.addClient();

      expect(component.showAlert).toHaveBeenCalledWith('Please make sure all field are filled out!');
    });

    it('should show alert with message - showAlert()', () => {
      component.showAlert('fake message');

      expect(alert.create).toHaveBeenCalled();
    });
  });
});
