import { Component, OnInit } from '@angular/core';
import { ViewController, ToastController, AlertController } from 'ionic-angular';

import { ActionService } from '../../../shared/services/action.service'
import { DataService } from '../../../shared/services/data.service'
import { PresentToastService } from '../../../shared/services/present-toast.service'

@Component({
  selector: 'wells-township-loader',
  templateUrl: './township-loader.component.html',
  styleUrls: ['./township-loader.component.scss']
})
export class TownshipLoaderComponent {

  public file: File;

  private townshipsData: string[];

  constructor(
    private actionService: ActionService,
    private dataService: DataService,
    private viewController: ViewController,
    private presentToast: PresentToastService,
    private alertCtrl: AlertController,
  ) { }

  /****************************************************************
   *
   * Public Methods
   *
   ****************************************************************/


  /**
   * Handle File Import
   * 
   * @param {FileList} file 
   * 
   * @memberof TownshipLoaderComponent
   */
  public handleFile(file: FileList) {
    const reader = new FileReader();
    this.file = file[0];

    if (!this.isValidFileType(file[0])) {
      this.displayErrorAlert();
    }

    reader.onload = ((e) => {
      this.townshipsData = (<any>e.target).result.split('\n');
    });

    reader.readAsText(file[0]);
  }

  /**
   * loadFile
   * 
   * 
   * @memberof TownshipLoaderComponent
   */
  public loadFile() {
    if (!this.isValidFileType(this.file)) {
      this.displayErrorAlert();
      return;
    }
    this.actionService.loadTownships(this.townshipsData);
    this.dataService.updateTownships(this.townshipsData);
    this.presentToast.show('File loaded successfully');
    this.closePopover();
  }

  /**
   * closePopover
   * 
   * 
   * @memberof TownshipLoaderComponent
   */
  public closePopover() {
    this.viewController.dismiss();
  }

  /**
   * displayFileInputInfo
   * 
   * 
   * @memberof TownshipLoaderComponent
   */
  public displayFileInputInfo() {
    const alert = this.alertCtrl.create({
      title: 'File Format',
      subTitle: `The file should be a text file.
      The delimiter for the file is a return/enter key after each Boro/Township.`,
      buttons: [
        {
          text: 'OK',
          role: 'cancel'
        }
      ]
    });
    alert.present();
  }

  /****************************************************************
   *
   * Private Methods
   *
   ****************************************************************/

  /**
   * isValidFileType
   * 
   * @private
   * @param {File} file 
   * @returns 
   * 
   * @memberof TownshipLoaderComponent
   */
  private isValidFileType(file: File) {
    const type = file ? file.type : null;
    return type === 'text/plain';
  }

  /**
   * displayErrorAlert
   * 
   * @private
   * 
   * @memberof TownshipLoaderComponent
   */
  private displayErrorAlert() {
    const alert = this.alertCtrl.create({
      title: 'Invalid File Type!',
      subTitle: 'Load a valid file. Only text files are allowed.',
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
          handler: () => {
            const file = new File([''], 'Choose a file');
            this.file = file;
          }
        }
      ]
    });
    alert.present();
  }
}
