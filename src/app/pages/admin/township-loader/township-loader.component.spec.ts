import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TownshipLoaderComponent } from './township-loader.component';

describe('TownshipLoaderComponent', () => {
  let component: TownshipLoaderComponent;
  let fixture: ComponentFixture<TownshipLoaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TownshipLoaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TownshipLoaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
