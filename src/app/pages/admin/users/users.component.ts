import { Component, OnInit, OnDestroy } from '@angular/core';

/** Services */
import { AlertController, NavController } from 'ionic-angular';
import { StateManagerService } from 'app/shared/services/sassy-state-manager-ng2/src/state-manager.service';
import { DataService } from 'app/shared/services/data.service';
import { ActionService } from 'app/shared/services/action.service';
import { AuditRecord } from '../../../shared/classes/audit-record';

import { AddUserComponent } from '../add-or-edit-user/add-or-edit-user.component';

import { User } from '../../../shared/classes/user';
import { take } from 'rxjs/operators';

@Component({
  selector: 'wells-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit, OnDestroy {
  public users: Array<User>;
  public subscriptions: any[] = [];

  constructor(
    private stateManager: StateManagerService,
    private alertCtrl: AlertController,
    private action: ActionService,
    private dataService: DataService,
    private nav: NavController
  ) { }

  ngOnInit() {
    this.subscription = this.stateManager.getModel('users')
      .pipe(take(1))
      .subscribe(users => {
        this.users = users;
      });

    this.action.addAuditRecord(new AuditRecord('Access Users list page', 'users'));

  }
  /**
   * Shows menu options
   *
   * @param {User} user
   *
   * @memberOf ClientListComponent
   */
  showMenuOptions(user: User) {

    const name = user.firstName + ' ' + user.lastName;

    const alert = this.alertCtrl.create({
      title: name,
      message: '',
      cssClass: 'inspection',
      buttons: [
        {
          text: 'Edit User',
          handler: () => {
            this.editUser(user.key);
          }
        },
        {
          text: 'Delete User',
          handler: () => {
            this.confirmDelete(user.key);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
      ]
    });
    alert.present();
  }


  /**
   * Confirm a delete for user
   *
   * @param {any} id
   *
   * @memberOf ClientListComponent
   */
  confirmDelete(id) {
    const alert = this.alertCtrl.create({
      title: 'Delete Client',
      message: 'Are you sure you want to delete this client?',
      buttons: [
        {
          text: 'No',
          handler: () => { }
        },
        {
          text: 'Yes',
          handler: () => {
            this.action.deleteUser(id);
            this.dataService.deleteUser(id);
            this.action.addAuditRecord(new AuditRecord(`Delete user: ${id}`, 'users'));
          }
        }
      ]
    });
    alert.present();

  }

  public ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe)
  }


  /**
   * addComponentToNav
   *
   * @private
   *
   * @memberOf SideMenuComponent
   */
  private editUser(key: any) {
    this.nav.setRoot(AddUserComponent, { key }, { animate: true, direction: 'forward' });
  }

  private set subscription(subscription) {
    this.subscriptions.push(subscription)
  }
}
