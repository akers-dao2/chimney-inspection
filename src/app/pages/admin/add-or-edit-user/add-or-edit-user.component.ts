import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ActionService } from 'app/shared/services/action.service';
import { DataService } from 'app/shared/services/data.service';
import { StateManagerService } from 'app/shared/services/sassy-state-manager-ng2/src/state-manager.service';
import { CrewNamePipe } from '../../../shared/pipes/crew-name.pipe';
import { PresentToastService } from 'app/shared/services/present-toast.service';
import { SortService } from '../../../shared/services/sort.service';
import { AuditRecord } from '../../../shared/classes/audit-record';

import { AlertController, NavController, NavParams } from 'ionic-angular';

import { User } from 'app/shared/classes/user';

import { ICrew } from 'app/shared/interfaces/crew';
import { IUser } from 'app/shared/interfaces/user';

import { UsersComponent } from '../users/users.component';


@Component({
  selector: 'wells-add-user',
  templateUrl: './add-or-edit-user.component.html',
  styleUrls: ['./add-or-edit-user.component.scss']
})
export class AddUserComponent implements OnInit, OnDestroy {
  public user = new User();
  public crews: Observable<ICrew[]>;
  public isEditMode = false;

  public state: any;
  public subscriptions: any[] = [];

  constructor(
    private action: ActionService,
    private dataService: DataService,
    private alertCtrl: AlertController,
    private stateManager: StateManagerService,
    private nav: NavController,
    private navParams: NavParams,
    private crewName: CrewNamePipe,
    private presentToast: PresentToastService,
    private sortService: SortService
  ) { }

  ngOnInit() {
    const paramKey = this.navParams.get('key');

    if (paramKey) {
      this.isEditMode = true;
      this.subscription = this.stateManager.getModel('users')
        .switchMap<IUser[], IUser>(users => users)
        .filter(user => (user.key === paramKey))
        .map(user => this.user = Object.assign(new User(), user))
        .take(1)
        .subscribe();
    }

    this.crews = this.stateManager.getModel('crews').map((crews: ICrew[]) =>
      crews.slice(0).sort((x, y) => this.sortService.get(x.name, y.name))
    ) as any;

    const description = paramKey ? 'Access edit user page' : 'Access add user page';

    this.action.addAuditRecord(new AuditRecord(description, 'add-or-edit-user.component'));
  }


  /**
   * Create new client action
   *
   * @memberOf AddClientComponent
   */
  public addUser() {

    if (this.user.isValid) {
      if (this.user.key) {
        // this.user.key = this.key;
        this.action.updateUser(this.user);
        this.dataService.updateUser(this.user);

        // update currentUser model if the user being updated is the current user
        this.subscription = this.stateManager.getModel('currentUser')
          .filter(currentUser => currentUser.key === this.user.key)
          .subscribe(currentUser => this.action.setCurrentUser(this.user));
      } else {
        this.subscription = Observable.fromPromise(this.dataService.addUser(this.user) as Promise<Observable<any>>)
          .skipWhile(obs =>
            obs === undefined
          )
          .switch()
          .skipWhile(user =>
            user === null
          )
          .pluck<User, string>('newKey')
          .subscribe(key => {
            this.user.key = key;
            this.action.addUser(this.user);
          }, error => console.log(error));
      }
      const text = this.navParams.get('key') ? 'updated' : 'added';
      this.presentToast.show(`User has been ${text}!`);
      this.gotoUserList();
    } else {
      this.showAlert('Please make sure all field are filled out!');
    }
  }


  /**
   * Pops alert
   *
   *
   * @memberOf AddClientComponent
   */
  public showAlert(msg: string) {
    const alert = this.alertCtrl.create({
      title: 'Alert!',
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present();
  }


  public ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe);
  }

  private set subscription(subscription) {
    this.subscriptions.push(subscription);
  }
  /**
   * addComponentToNav
   *
   * @private
   *
   * @memberOf SideMenuComponent
   */
  private gotoUserList() {
    this.nav.setRoot(UsersComponent, { animate: true, direction: 'forward' });
  }
}
