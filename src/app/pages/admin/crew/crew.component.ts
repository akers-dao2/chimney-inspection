import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ICrew } from '../../../shared/interfaces/crew';

import { StateManagerService } from 'app/shared/services/sassy-state-manager-ng2/src/state-manager.service';
import { ActionService } from '../../../shared/services/action.service';
import { DataService } from '../../../shared/services/data.service';
import { AuditRecord } from '../../../shared/classes/audit-record';

@Component({
  selector: 'wells-crew',
  templateUrl: './crew.component.html',
  styleUrls: ['./crew.component.scss']
})
export class CrewComponent implements OnInit {

  public crews: Observable<ICrew[]>;

  constructor(
    private stateManager: StateManagerService,
    private action: ActionService,
    private dataService: DataService,
  ) { }

  /****************************************************************
   *
   * Public Methods
   *
   ****************************************************************/


  /**
   * OnInit LifeCycle Hook
   *
   *
   * @memberof CreateJobComponent
   */
  public ngOnInit() {
    this.crews = this.stateManager.getModel('crews') as any;
    this.action.addAuditRecord(new AuditRecord('Access crew page', 'crew'));
  }

  /**
   * Add new crew name to list
   * 
   * @param {string} name 
   * 
   * @memberof CrewComponent
   */
  public addCrew(name: string) {
    this.action.addCrew({ name, key: name });
    this.dataService.addCrew({ name, key: name });
    this.action.addAuditRecord(new AuditRecord('Access crew page', `Add crew: ${name}`));
  }

  /**
   * Update crew name
   * 
   * @param {string} name 
   * @param {string} key 
   * 
   * @memberof CrewComponent
   */
  public updateCrew(name: string, key: string) {
    this.action.addCrew({ name, key }, true);
    this.dataService.addCrew({ name, key }, true)
    this.action.addAuditRecord(new AuditRecord('Access crew page', 'Update crew'));
  }

  /**
   * Remove crew
   * 
   * @param {ICrew} crew 
   * 
   * @memberof CrewComponent
   */
  public removeCrew(crew: ICrew) {
    this.action.deleteCrew(crew.key);
    this.dataService.deleteCrew(crew.key);
    this.action.addAuditRecord(new AuditRecord('Access crew page', `Remove crew: ${crew.name}`));
  }

}
