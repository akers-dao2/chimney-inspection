import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportConflictManagerComponent } from './report-conflict-manager.component';

describe('ReportConflictManagerComponent', () => {
  let component: ReportConflictManagerComponent;
  let fixture: ComponentFixture<ReportConflictManagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportConflictManagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportConflictManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
