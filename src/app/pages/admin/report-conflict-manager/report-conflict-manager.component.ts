import { Component, OnInit } from '@angular/core';
import { StateManagerService } from 'app/shared/services/sassy-state-manager-ng2/src/state-manager.service';
import { Observable } from 'rxjs/observable';
import { tap, switchAll, take, toArray, map, filter, switchMap, mergeMap, find } from 'rxjs/operators';
import * as moment from 'moment';
import { IClient } from 'app/shared/interfaces';
import { IInspectionReport } from 'app/shared/interfaces/inspection-report';
import { IAppliance } from 'app/shared/interfaces/appliance';
import { DataService } from 'app/shared/services/data.service';
import { AlertController } from 'ionic-angular';
import { PresentToastService } from 'app/shared/services/present-toast.service';

@Component({
  selector: 'wells-report-conflict-manager',
  templateUrl: './report-conflict-manager.component.html',
  styleUrls: ['./report-conflict-manager.component.scss']
})
export class ReportConflictManagerComponent implements OnInit {
  public inspectionReports: IInspectionReport[] = []
  private inspectionReports$: Observable<IInspectionReport[] | any>;
  private clients$: Observable<IClient[] | any>;

  constructor(
    private stateManager: StateManagerService,
    private dataService: DataService,
    private alertCtrl: AlertController,
    private presentToast: PresentToastService
  ) { }

  ngOnInit() {
    this.inspectionReports$ = this.stateManager.getModel('inspectionReports').share();
    this.clients$ = this.stateManager.getModel('clients').share();

    this.loadInvalidReports()
  }

  /**
   * Get inspection report by UUID
   *
   * @param {*} report
   * @param {*} appliance
   * @returns
   * @memberof ReportConflictManagerComponent
   */
  public getInspectionReportByUUID(report, appliance) {
    const key = Object.keys(report)[0];
    const date = Object.keys(report[key].reports)[0];
    return report[key].reports[date][appliance];
  }

  /**
   * Remove appliance from report
   *
   * @param {Event} $event
   * @param {*} report
   * @param {*} applianceId
   * @memberof ReportConflictManagerComponent
   */
  public async delete($event: Event, report, applianceId) {
    $event.stopImmediatePropagation();
    if (report.invalidAppliances.length !== 1) {
      this.confirmDelete(report, applianceId);
    }
  }

  private confirmDelete(report, applianceId) {
    const alert = this.alertCtrl.create({
      title: 'Delete Appliance',
      message: 'Are you sure you want to delete this appliance?',
      buttons: [
        {
          text: 'No',
          handler: () => { }
        },
        {
          text: 'Yes',
          handler: async () => {
            const clientId = Object.keys(report)[0];
            const key = report.key
            const date = parseInt(Object.keys(report[clientId].reports)[0], 10);
            await this.dataService.deleteInspectionReportItem(key, clientId, date, parseInt(applianceId, 10)).toPromise();
            report.invalidAppliances = report.invalidAppliances.filter(a => a !== applianceId);
            this.presentToast.show(`${applianceId} has been removed`);
          }
        }
      ]
    });
    alert.present();

  }

  /**
   * Load invalid reports
   *
   * @private
   * @memberof ReportConflictManagerComponent
   */
  private loadInvalidReports() {
    this.inspectionReports$
      .pipe(
        take(1),
        switchAll(),
        filter((v: IInspectionReport) => {
          const key = Object.keys(v)[0];
          if (v[key].reports === undefined) {
            return false
          }
          const date = parseInt(Object.keys(v[key].reports)[0], 10);
          const weekAgo = moment().subtract(7, 'days');
          const tommorrow = moment().add(1, 'days');
          return moment(new Date(date)).isBetween(weekAgo, tommorrow, 'day');
        }),
        filter(v => {
          const uuids = this.getUUIDs(v);
          return uuids.some((element, index) => uuids.indexOf(element) !== index);
        }),
        this.addClientName(),
        this.addPropWithInvalidUUIDs(),
        toArray(),
        tap(v => this.inspectionReports = v.slice(0)),
        take(1)
      )
      .subscribe()
  }

  /**
   * Add client name to object
   *
   * @private
   * @returns
   * @memberof ReportConflictManagerComponent
   */
  private addClientName() {
    return mergeMap<IInspectionReport, IClient, any>(v =>
      this.clients$
        .pipe(
          take(1),
          switchAll(),
          find((c: IClient) => {
            const key = Object.keys(v)[0];
            return c.key === v[key].clientId;
          })
        ), (v, c) => ({ ...v, clientName: c })
    );
  }

  /**
   * Add invalid UUIDs to object
   *
   * @private
   * @returns
   * @memberof ReportConflictManagerComponent
   */
  private addPropWithInvalidUUIDs() {
    return map((v: IInspectionReport) => {
      const uuids = this.getUUIDs(v);
      const invalidUUIDs = uuids.reduce((acc, uuid, index) =>
        uuids.indexOf(uuid) !== index && !acc.includes(uuid) ? [...acc, uuid] : acc, []
      );
      const invalidAppliances = invalidUUIDs.reduce((acc, u) => {
        return [...acc, ...this.getInspectionReportIDsByUUID(v, u)]
      }, [])
      return { ...v, invalidAppliances };
    });
  }

  /**
   * get UUIDs
   *
   * @private
   * @param {*} v
   * @returns
   * @memberof ReportConflictManagerComponent
   */
  private getUUIDs(v) {
    const key = Object.keys(v)[0];
    const date = Object.keys(v[key].reports)[0];
    const appliances: IAppliance = v[key].reports[date];
    return Object.keys(appliances).map(k => appliances[k].uuid);
  }

  /**
   * Get inspection report ids by UUID
   *
   * @private
   * @param {IInspectionReport} v
   * @param {*} uuid
   * @returns
   * @memberof ReportConflictManagerComponent
   */
  private getInspectionReportIDsByUUID(v: IInspectionReport, uuid) {
    const key = Object.keys(v)[0];
    const date = Object.keys(v[key].reports)[0];
    const appliances: IAppliance = v[key].reports[date];
    return Object.keys(appliances).filter(k => appliances[k].uuid === uuid);
  }
}
