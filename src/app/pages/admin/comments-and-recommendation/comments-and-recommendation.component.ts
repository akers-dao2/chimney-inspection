import { Component, OnInit } from '@angular/core';
import { StateManagerService } from 'app/shared/services/sassy-state-manager-ng2/src/state-manager.service';
import { IComment } from '../../../shared/interfaces/comments';
import { Observable } from 'rxjs/Observable';
import { ActionService } from '../../../shared/services/action.service';
import { SortService } from '../../../shared/services/sort.service';
import { IRecommendation } from '../../../shared/interfaces/recommendations';
import { reorderArray } from 'ionic-angular';

@Component({
  selector: 'wells-comments-and-recommendation',
  templateUrl: './comments-and-recommendation.component.html',
  styleUrls: ['./comments-and-recommendation.component.scss']
})
export class CommentsAndRecommendationComponent implements OnInit {

  public commentsOrRecommendations: { name: string, index: number }[];
  public type = 'comments';
  public openedGroup: string;
  public enableEditMode = false;
  private comments$: Observable<IComment>
  private recommendations$: Observable<IRecommendation>
  private obs$: Observable<IComment | IRecommendation>

  constructor(
    private stateManager: StateManagerService,
    private actions: ActionService,
    private sort: SortService
  ) { }

  ngOnInit() {
    this.comments$ = this.stateManager.getModel('comments').publishReplay(1).refCount();
    this.recommendations$ = this.stateManager.getModel('recommendations').publishReplay(1).refCount();
    this.obs$ = this.type === 'comments' ? this.comments$ : this.recommendations$;

    Observable.combineLatest(this.comments$, this.recommendations$)
      .do(() => {
        if (this.openedGroup) {
          this.refreshItemView(this.openedGroup);
        } else {
          this.refreshGroupingView();
        }
      })
      .subscribe()
  }

  public segmentChanged() {
    this.obs$ = this.type === 'comments' ? this.comments$ : this.recommendations$;
    this.refreshGroupingView();
  }

  public loadCommentsOrRecommendations(grouping: string, type: string) {
    this.stateManager.getModel(type)
      .take(1)
      .map(g => g[grouping])
      .subscribe(commentsOrRecommendations => this.commentsOrRecommendations = commentsOrRecommendations)
  }

  public add(value: string) {
    if (!this.openedGroup) {
      this.actions.addGrouping(value, this.type);
    } else {
      this.actions.addItem(value, this.openedGroup, this.type);
    }
  }

  public update(value: string, originalGrouping: string, index) {
    if (!this.openedGroup) {
      this.actions.updateGrouping(value, originalGrouping, this.type);
    } else {
      this.actions.updateItem(value, this.openedGroup, this.type, index);
    }
  }

  public remove(value: string, index: number) {
    if (!this.openedGroup) {
      this.actions.removeGrouping(value, this.type);
    } else {
      this.actions.removeItem(this.openedGroup, this.type, index);
    }
  }

  public onClick(grouping: string) {
    if (!this.openedGroup) {
      this.openedGroup = grouping;
      this.refreshItemView(grouping)
    }
  }

  public refreshGroupingView() {
    this.openedGroup = undefined;
    this.enableEditMode = false;
    this.obs$
      .map(c => Object.keys(c).sort((x, y) => this.sort.get(x, y)))
      .take(1)
      .switchMap(comment => comment)
      .map((key, index) => ({ name: key, index, displayName: this.convertKeyReadableString(key) }))
      .toArray()
      .do(c => this.commentsOrRecommendations = c)
      .subscribe()
  }

  /**
   * reorderItems
   * 
   * @param {{ from: number, to: number }} indexes 
   * @memberof JobListComponent
   */
  public reorderItems(indexes: { from: number, to: number }) {
    const commentsOrRecommendations = this.commentsOrRecommendations.slice(0);
    this.commentsOrRecommendations = reorderArray(commentsOrRecommendations, indexes);
    this.actions.updateItemsOrder(this.commentsOrRecommendations, this.openedGroup, this.type)
  }

  private convertKeyReadableString(s: string) {
    return s.replace(/[A-Z]/, ` ${s[s.search(/[A-Z]/)]}`)
      .replace(/[a-z]/, `${s[s.search(/[a-z]/)].toUpperCase()}`);
  }

  private refreshItemView(grouping: string) {
    this.obs$
      .take(1)
      .switchMap(c => c[grouping])
      .map((key, index) => ({ name: key, index, displayName: key }))
      .toArray()
      .do(c => this.commentsOrRecommendations = c)
      .subscribe()
  }
}
