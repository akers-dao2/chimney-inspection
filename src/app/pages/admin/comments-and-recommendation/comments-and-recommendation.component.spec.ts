import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommentsAndRecommendationComponent } from './comments-and-recommendation.component';

describe('CommentsAndRecommendationComponent', () => {
  let component: CommentsAndRecommendationComponent;
  let fixture: ComponentFixture<CommentsAndRecommendationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommentsAndRecommendationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentsAndRecommendationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
