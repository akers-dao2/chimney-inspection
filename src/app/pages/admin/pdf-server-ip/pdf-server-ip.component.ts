import { Component, OnInit } from '@angular/core';
import { ViewController, ToastController, AlertController } from 'ionic-angular';

import { ActionService } from '../../../shared/services/action.service';
import { DataService } from '../../../shared/services/data.service';
import { PresentToastService } from '../../../shared/services/present-toast.service';

@Component({
    selector: 'wells-township-loader',
    template: `
    <ion-header>
        <ion-navbar color="header" hideBackButton>
            <button ion-button menuToggle style="display:block">
            <ion-icon name="menu"></ion-icon>
            </button>
            <ion-title color="light">PDF IP Address</ion-title>
        </ion-navbar>
    </ion-header>
    <ion-content>
        <ion-input type="text" [(ngModel)]="ipAddress.value" placeholder="Add IP Address"></ion-input>
        <button ion-button color="primary" (click)="addIpAddress()">Accept</button>
    </ion-content>
  `,
})
export class PDFServerIPComponent implements OnInit {

    public ipAddress: { value: string } = { value: undefined };

    constructor(
        private dataService: DataService,
        private presentToast: PresentToastService
    ) { }

    /****************************************************************
     *
     * Public Methods
     *
     ****************************************************************/

    ngOnInit() {
        this.dataService.getIPAddress()
            .subscribe(ip => this.ipAddress = ip);
    }
    public addIpAddress() {
        this.dataService.addIpAddress(this.ipAddress)
            .subscribe(() => {
                this.presentToast.show('IP Address Updated Successfully');
            });
    }

}
