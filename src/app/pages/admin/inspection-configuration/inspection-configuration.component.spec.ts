import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InspectionConfigurationComponent } from './inspection-configuration.component';

describe('InspectionConfigurationComponent', () => {
  let component: InspectionConfigurationComponent;
  let fixture: ComponentFixture<InspectionConfigurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InspectionConfigurationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InspectionConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
