import { Component, OnInit } from '@angular/core';
import { ActionService } from '../../../shared/services/action.service';
import { ApplianceTypes } from '../../../shared/enums/appliances';
import { FlueVentComponent } from '../../inspection/inspection-sheet/flue-vent/flue-vent.component';
import { FlueVent, Chimney, HeatingAppliances, Stoves, Fireplace, DryerVent } from '../../../shared/classes';
import { StateManagerService } from 'app/shared/services/sassy-state-manager-ng2/src/state-manager.service';

@Component({
  selector: 'wells-inspection-configuration',
  templateUrl: './inspection-configuration.component.html',
  styleUrls: ['./inspection-configuration.component.scss']
})
export class InspectionConfigurationComponent implements OnInit {
  public type = 0;
  public inspectionSheet;
  public ApplianceTypes = ApplianceTypes;

  constructor(
    private actions: ActionService,
    private stateManager: StateManagerService
  ) { }

  ngOnInit() {
    this.actions.changeSideMenu('inspectionConfigItemsMenu');
    this.stateManager.getModel('inspectionConfigurationItem')
      .do(type => {
        this.setInspectionSheet(type)
      })
      .subscribe();
  }

  private setInspectionSheet(type) {
    this.type = parseInt(ApplianceTypes[type], 10);
    switch (type) {
      case 'Flue/Vent':
        this.inspectionSheet = new FlueVent();
        break;
      case 'Dryer Vent':
        this.inspectionSheet = new DryerVent();
        break;
      case 'Masonry Fireplace':
      case 'Prefab Fireplace':
        this.inspectionSheet = new Fireplace();
        break;
      case 'Stoves':
        this.inspectionSheet = new Stoves();
        break;
      case 'Heating Appliances':
        this.inspectionSheet = new HeatingAppliances();
        break;
      default:
        this.inspectionSheet = new Chimney();
        break;
    }
  }

}
