import { async, ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ViewController, IonicModule } from 'ionic-angular';

import { ActionService } from '../../shared/services/action.service';
import { StateManagerService } from 'app/shared/services/sassy-state-manager-ng2/src/state-manager.service';
import { UtilityService } from '../../shared/services/utility.service';
import { DataService } from '../../shared/services/data.service';
import { FirebaseService } from '../../shared/services/firebase.service';

import { IJob, IClient } from '../../shared/interfaces';
import { Status } from '../../shared/enums/status';

import { SearchModalComponent } from './search-modal.component';
import { HomePageComponent } from '../home-page/home-page.component';

xdescribe('SearchModalComponent', () => {
  let component: SearchModalComponent;
  let fixture: ComponentFixture<SearchModalComponent>;
  let stateManager: StateManagerService;
  let viewController: ViewController;
  const models: string[] = [];
  const fakeClients: IClient[] = [{
    firstName: 'fake',
    lastName: 'fake',
    address: '',
    city: '',
    state: '',
    zip: '',
    township: '',
    key: '',
    isValid: false
  }];

  const fakeJobs: IJob[] = [{
    assigned: 'fake',
    status: Status.open,
    date: 0,
    inspection: false,
    internal: false,
    client: '',
    certification: false,
    signature: '',
    order: 1,
    user: null
  }];

  class MockStateManagerService {
    getModel(model: string) {
      models.push(model);
      if (model === 'clients') {
        return Observable.of(fakeClients);
      } else if (model === 'jobs') {
        return Observable.of(fakeJobs);
      }
    }
  }

  class MockActionService { }

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        IonicModule.forRoot(SearchModalComponent)
      ],
      declarations: [SearchModalComponent],
      providers: [
        { provide: StateManagerService, useClass: MockStateManagerService },
        {
          provide: ViewController, useFactory: () => {
            return new ViewController();
          }
        },
        { provide: ActionService, useClass: MockActionService },
        UtilityService,
        FirebaseService
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  });

  beforeEach(inject([StateManagerService, ViewController], (_StateManagerService_, _ViewController_) => {
    stateManager = _StateManagerService_;
    viewController = _ViewController_;

    fixture = TestBed.createComponent(SearchModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    spyOn(stateManager, 'getModel');
    spyOn(viewController, 'dismiss');
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('Life Cycle Methods', () => {
    it('should setup for OnInit()', () => {
      expect(models[0]).toEqual('clients');
      expect(models[1]).toEqual('jobs');
    });
  });

  xdescribe('Custom Methods', () => {
    it('should get search items - getItems', () => {
      const ev = {
        target: {
          value: 'fak'
        }
      };
      component.getItems(ev);

      expect((<IJob[]>component.jobs)).toEqual(fakeJobs);
      expect(component.clients).toEqual(fakeClients);
    });

    it('should dismiss search', () => {
      component.dismiss();

      expect(viewController.dismiss).toHaveBeenCalled();
    });
  });
});
