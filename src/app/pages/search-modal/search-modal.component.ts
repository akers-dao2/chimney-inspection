import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ViewController } from 'ionic-angular';

import { ActionService } from '../../shared/services/action.service';
import { StateManagerService } from 'app/shared/services/sassy-state-manager-ng2/src/state-manager.service';
import { UtilityService } from '../../shared/services/utility.service';

import { IClient } from '../../shared/interfaces';

import { Job } from '../../shared/classes/job';

@Component({
  templateUrl: 'search-modal.component.html',
  styleUrls: ['./search-modal.component.scss']
})
export class SearchModalComponent implements OnInit, OnDestroy {
  public clients: IClient[];
  public jobs: Job[];
  public subscriptions: any[] = [];

  private _clients: IClient[];
  private _jobs: Job[];


  /**
   * Creates an instance of SearchModalComponent.
   *
   * @param {ViewController} viewCtrl
   * @param {StateManagerService} stateManager
   * @param {ActionService} actions
   * @param {UtilityService} utility
   *
   * @memberOf SearchModalComponent
   */
  constructor(
    private viewCtrl: ViewController,
    private stateManager: StateManagerService,
    private actions: ActionService,
    private utility: UtilityService
  ) {
    this.clients = [];
    this.jobs = [];
  }


  ngOnInit() {
    this.stateManager.getModel('clients')
      .subscribe(data => {
        this._clients = data;
      });

    this.stateManager.getModel('jobs')
      .subscribe(data => {
        this._jobs = data;
      });
  }

  public ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe)
  }

  private set subscription(subscription) {
    this.subscriptions.push(subscription)
  }


  /**
   *
   *
   * @param {*} ev
   *
   * @memberOf SearchModalComponent
   */
  public getItems(ev: any) {
    // set val to the value of the searchbar
    const searchVal = ev.target.value;

    this.jobs = <Job[]>this.searchFilter(searchVal, this._jobs);
    this.clients = <IClient[]>this.searchFilter(searchVal, this._clients);
  }


  /**
   *
   *
   * @memberOf SearchModalComponent
   */
  public dismiss() {
    this.viewCtrl.dismiss();
  }


  /**
   * Filter a supplied val from an array
   *
   * @private
   * @param {string} val
   * @param {Array<any>} arr
   * @returns {(IJob[] | IClient[])}
   *
   * @memberOf SearchModalComponent
   */
  private searchFilter(val: string, arr: Array<any>): Job[] | IClient[] {
    return arr.filter((obj: Job | IClient): boolean => {
      return val.length >= 3 &&
        this.utility
          .toArray(obj)
          .join(' ')
          .toLowerCase()
          .includes(val);
    });
  }
}
