import { Component, ViewChild, Input, OnInit } from '@angular/core';
import { NavController, MenuController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { PopoverController } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import { AddClientComponent } from '../client/add-client/add-client.component';
import { JobListComponent } from '../job/job-list/job-list.component';
import { ClientListComponent } from '../client/client-list/client-list.component';
import { AddUserComponent } from '../admin/add-or-edit-user/add-or-edit-user.component';
import { CrewComponent } from '../admin/crew/crew.component';
import { InspectionSheetComponent } from '../inspection/inspection-sheet/inspection-sheet.component';
import { UsersComponent } from '../admin/users/users.component';
import { PDFServerIPComponent } from '../admin/pdf-server-ip/pdf-server-ip.component';
import { TownshipLoaderComponent } from '../admin/township-loader/township-loader.component';
import { ReportComponent } from '../report/report.component';
import { AuditReportComponent } from '../audit-report/audit-report.component';

/** Services */
import { ActionService } from '../../shared/services/action.service';
import { StateManagerService } from 'app/shared/services/sassy-state-manager-ng2/src/state-manager.service';
import { InspectionItemService } from '../../shared/services/inspection-item.service';
import { ChimneyNamesService } from '../../shared/services/chimney-names.service';
import { PdfMakerService } from '../../shared/services/pdf-maker.service';
import { ValidateInspectionReportService } from '../../shared/services/validate-inspection-report.service';
import { PresentToastService } from '../../shared/services/present-toast.service';
import { GetOpenedClientInfoService } from '../../shared/services/get-opened-client-info.service';
import { GetOpenedInspectionReportService } from '../../shared/services/get-opened-inspection-report.service';
import { GetOrderListOfInspectionsService } from '../../shared/services/get-order-list-of-inspections.service';
import { GoToPageService } from '../../shared/services/go-to-page.service';
import { GetInspectItemListService } from '../../shared/services/get-inspect-item-list.service';
import { LoaderService } from '../../shared/services/loader.service';

/** Interfaces */
import { INavigation } from '../../shared/interfaces/navigation';
import { SideMenuType } from '../../shared/interfaces/side-menu-type';
import { IInspectionItem } from '../../shared/interfaces/inspection-item';
import { IInspection } from '../../shared/interfaces/inspection';
import { IAlertOptionInput } from '../../shared/interfaces/alert-option-input';
import { IChimneyItem } from '../../shared/interfaces/chimney-item';
import { IAppliance } from 'app/shared/interfaces/appliance';
import { IInspectionReport } from 'app/shared/interfaces/inspection-report';
import { IOpenedInspectionReport } from 'app/shared/interfaces/opened-inspection-report';
import { IUser } from 'app/shared/interfaces/user';
import { IClient } from 'app/shared/interfaces/client';
import { IReport } from 'app/shared/interfaces/report';

/** Enums */
import { ApplianceTypes } from '../../shared/enums/appliances';

import { Appliance } from '../../shared/classes/appliance';
import { Client } from '../../shared/classes/client';
import { CommentsAndRecommendationComponent } from '../admin/comments-and-recommendation/comments-and-recommendation.component';
import { InspectionConfigurationComponent } from '../admin/inspection-configuration/inspection-configuration.component';
import { InspectionConfigurationItemService } from '../../shared/services/inspection-configuration-item.service';
import { ReportConflictManagerComponent } from '../admin/report-conflict-manager/report-conflict-manager.component';
import { PurgeComponent } from '../purge/purge.component';

declare const pdfMake;

@Component({
  selector: 'wells-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss']
})
export class SideMenuComponent implements OnInit {
  public clientsMenu: boolean;
  public clientsMenuIcon = 'ios-arrow-forward';
  public optionsMenu: boolean;
  public optionsMenuIcon = 'ios-arrow-forward';
  public isInspectionList = false;
  public sideMenuType$: any;
  public inspection$: any;
  public ApplianceTypes = ApplianceTypes;
  public itemClickedIndex;
  public chimneyClickedIndex = 0;
  public parentIndex;
  public hasAdminMenu = false;
  public adminMenuItems = [];
  public lastSyncUpdate: Observable<number>;
  private currentUser: IUser;

  @Input() nav: NavController;

  constructor(
    private actionService: ActionService,
    private stateManager: StateManagerService,
    private popoverCtrl: PopoverController,
    private inspectionItemService: InspectionItemService,
    private chimneyNames: ChimneyNamesService,
    private pdfMaker: PdfMakerService,
    private menuCtrl: MenuController,
    private validateInspectionReport: ValidateInspectionReportService,
    private presentToast: PresentToastService,
    private getOpenedClientInfo: GetOpenedClientInfoService,
    private getOpenedInspectionReport: GetOpenedInspectionReportService,
    private getOrderListOfInspections: GetOrderListOfInspectionsService,
    private goToPage: GoToPageService,
    private getInspectItemList: GetInspectItemListService,
    private alertCtrl: AlertController,
    private loader: LoaderService,
    private inspectionConfigurationItem: InspectionConfigurationItemService
  ) { }

  /****************************************************************
   *
   * Public Methods
   *
   ****************************************************************/

  public async ngOnInit() {
    this.sideMenuType$ = this.stateManager.getModel('navigation')
      .pluck<INavigation, SideMenuType>('sideMenuType');

    this.stateManager.getModel('navigation')
      .subscribe(() => {
        this.itemClickedIndex = undefined;
        this.chimneyClickedIndex = 0;
      });

    // list of items to inspection
    this.inspection$ = this.stateManager.getModel('inspection')
      .map<IInspection, IChimneyItem[]>(chimneys => {
        return this.chimneyNames.get(chimneys)
          .filter(key => !(key === 'clientId' || key === 'date' || key === 'jobKey'))
          .map((key, index) => Object.assign({},
            {
              name: chimneys[key].name || key,
              uuid: index.toString(),
              index,
              appliances: chimneys[key].inspectionItems,
              notValid: chimneys[key].notValid || false,
            })
          );
      });

    this.stateManager.getModel('activeMenuItem')
      .subscribe((inspectionItem) => {
        // controls the active css class
        if (inspectionItem.parentIndex !== undefined) {
          this.chimneyClickedIndex = undefined;
          this.itemClickedIndex = inspectionItem.index;
          this.parentIndex = inspectionItem.parentIndex;
        } else {
          this.itemClickedIndex = undefined;
          this.chimneyClickedIndex = inspectionItem.index;
        }
      });

    this.currentUser = await this.stateManager.getModel('currentUser').skipWhile(v => !Object.keys(v).length).take(1).toPromise();

    this.hasAdminMenu = this.currentUser.adminstration;

    this.adminMenuItems = this.getAdminMenuItems();

    this.lastSyncUpdate = this.stateManager.getModel('lastSyncUpdate');

  }

  /**
   * onMenuClick
   *
   * @param {string} menu
   *
   * @memberOf SideMenuComponent
   */
  public onMenuClick(menu: string, value?: string | IAppliance) {
    let closeMenu = true;

    switch (menu) {
      case 'clients-list':
        this.goToPage.execute(ClientListComponent);
        // this.loader.show('loading...')
        //   .then(() => {
        //   })

        break;
      case 'jobs-list':
        this.goToPage.execute(JobListComponent);
        break;
      case 'inspectionItem':
        this.addInspectionItemToList(value as string);
        break;
      case 'inspection':
        this.openInspection(value as IAppliance);
        break;
      case 'add-client':
        this.goToPage.execute(AddClientComponent);
        break;
      case 'add-users':
        this.goToPage.execute(AddUserComponent);
        break;
      case 'users':
        this.goToPage.execute(UsersComponent);
        break;
      case 'crews':
        this.goToPage.execute(CrewComponent);
        break;
      case 'commentsAndRecommendations':
        this.goToPage.execute(CommentsAndRecommendationComponent);
        break;
      case 'pdf-server-ip':
        this.goToPage.execute(PDFServerIPComponent);
        break;
      case 'inspect-config':
        this.goToPage.execute(InspectionConfigurationComponent);
        break;
      case 'inspectionConfigItems':
        this.inspectionConfigurationItem.open(value as string);
        break;
      case 'load-townships':
        this.openTownshipsLoader();
        break;
      case 'purge':
        this.goToPage.execute(PurgeComponent);
        break;
      case 'audit-report':
        this.goToPage.execute(AuditReportComponent);
        break;
      case 'report-conflict':
        this.goToPage.execute(ReportConflictManagerComponent);
        break;
      case 'log-out':
        this.actionService.logOutUser();
        setTimeout(() => {
          this.goToPage.execute(JobListComponent);
        }, 1000);
        break;
      default:
        this[menu + `Menu`] = !this[menu + `Menu`];
        this[menu + `MenuIcon`] = this[menu + `Menu`] ? 'ios-arrow-down' : 'ios-arrow-forward';
        closeMenu = false;
        break;
    }

    if (closeMenu) {
      this.menuCtrl.close();
    }
  }

  /**
   * setInspectionItemsList
   *
   * @private
   *
   * @memberOf SideMenuComponent
   */
  public get inspectionItems() {
    return [
      'Chimney',
      'Flue/Vent',
      'Masonry Fireplace',
      'Prefab Fireplace',
      'Stoves',
      'Heating Appliances'
    ];
  }

  private getAdminMenuItems() {
    return [
      { label: 'Users List', menu: 'users', icon: 'md-people' },
      { label: 'Add Users', menu: 'add-users', icon: 'md-person-add' },
      { label: 'Crews', menu: 'crews', icon: 'md-hammer' },
      { label: 'Comments And Recommendations', menu: 'commentsAndRecommendations', icon: 'md-list-box' },
      { label: 'Load Townships', menu: 'load-townships', icon: 'md-cloud-upload' },
      { label: 'PDF Server IP', menu: 'pdf-server-ip', icon: 'md-construct' },
      // { label: 'Inspection Configuration', menu: 'inspect-config', icon: 'md-construct' },
      { label: 'Report Conflict Manager', menu: 'report-conflict', icon: 'md-cog' },
      { label: 'Purge', menu: 'purge', icon: 'md-cog' },
    ];
  }

  public previewReport() {
    this.stateManager.getModel('inspectionReports')
      .combineLatest(
      this.stateManager.getModel('openedInspectionReport'),
      this.stateManager.getModel('inspection'),
    )
      .map(([inspectionReports, report, inspection]) => {
        const inspectionReport = inspectionReports.find(item => {
          const key = Object.keys(item)[0];
          return item[key].clientId === report.clientId && item[key].jobKey === report.jobKey;
        });
        return { inspectionReports, report, inspectionReport, inspection };
      })
      .map(({ inspectionReports, report, inspectionReport, inspection }) => {
        if (inspectionReport === undefined || inspectionReport[report.clientId] === undefined) {
          return {};
        }
        return inspectionReport ? Object.assign({}, inspectionReport[report.clientId].reports[report.jobDate], { inspection }) : {};
      })
      .take(1)
      .subscribe(inspectionReportSheets => {
        // remove props from validation check
        delete inspectionReportSheets.signature;
        delete inspectionReportSheets.status;

        const invalidReports = this.validateInspectionReport.verify(inspectionReportSheets);
        this.actionService.setInspectionItemsCompletionStatus(invalidReports);
        if (!!invalidReports.length) {
          this.presentToast.show('There are missing items on the report.  Please update and click preview again.');
        } else {
          this.generateReport();
        }
        this.menuCtrl.close();
      });
  }
  /****************************************************************
   *
   * Private Methods
   *
   ****************************************************************/

  /**
   * Add Inspection Item To List
   *
   * @private
   * @param {string} inspectionItem
   *
   * @memberOf SideMenuComponent
   */
  private addInspectionItemToList(inspectionItem: string) {
    this.inspectionItemService.addToList(inspectionItem);
  }

  /**
   * Open Inspection
   *
   * @private
   * @param {string} inspectionItem
   *
   * @memberOf SideMenuComponent
   */
  private openInspection(inspectionItem: IAppliance) {
    this.actionService.openedInspection(inspectionItem);
    this.goToPage.execute(InspectionSheetComponent, inspectionItem);

    // controls the active css class
    this.actionService.setActiveMenuItem(inspectionItem);
  }

  /**
   * open Townships Loader Component
   * 
   * @private
   * 
   * @memberof SideMenuComponent
   */
  private openTownshipsLoader() {
    const popover = this.popoverCtrl.create(TownshipLoaderComponent);
    popover.present();
  }

  /**
   * generate PDF Report
   * 
   * @private
   * 
   * @memberof SideMenuComponent
   */
  private generateReport() {
    Observable.combineLatest<IClient, IReport[]>(
      this.getOpenedClientInfo.get(),
      this.getOpenedInspectionReport.get()
    )
      .take(1)
      .subscribe(([client, inspectItemsInReport]) => {
        const { firstName, lastName, CSIA } = this.currentUser;
        const name = `${firstName} ${lastName}`;

        this.displayCertificationAlert(this.currentUser, client, inspectItemsInReport);
      });
  }

  private displayCertificationAlert(user, client, inspectItemsInReport) {
    const confirm = this.alertCtrl.create({
      title: 'Type Of Report',
      message: 'Is this a certification report?',
      buttons: [
        {
          text: 'Yes',
          handler: () => {
            this.goToPage.execute(ReportComponent, { user, client, inspectItemsInReport, cert: true });
          }
        },
        {
          text: 'No',
          handler: () => {
            this.goToPage.execute(ReportComponent, { user, client, inspectItemsInReport, cert: false });
          }
        }
      ]
    });
    confirm.present();
  }

}
