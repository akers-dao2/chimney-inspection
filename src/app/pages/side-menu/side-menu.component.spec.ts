import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA, Component } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import {
  NavParams,
  ItemSliding,
  App, Config,
  Form,
  IonicModule,
  Keyboard,
  DomController,
  MenuController,
  NavController,
  Platform,
  GestureController,
  ModalController,
  AlertController
} from 'ionic-angular';

import { ConfigMock, PlatformMock } from '../../shared/mocks/ionic-mocks';

import { SharedModule } from '../../shared/shared.module';

import { SideMenuComponent } from './side-menu.component';
import { JobListComponent } from '../job/job-list/job-list.component';

/** Services */
import { ActionService } from '../../shared/services/action.service';
import { InspectionItemService } from '../../shared/services/inspection-item.service';
import { StateManagerService } from 'app/shared/services/sassy-state-manager-ng2/src/state-manager.service';
import { UtilityService } from '../../shared/services/utility.service';
import { DataService } from '../../shared/services/data.service';
import { FirebaseService } from '../../shared/services/firebase.service';
import { ChimneyNamesService } from '../../shared/services/chimney-names.service';

describe('SideMenuComponent', () => {
  let component: SideMenuComponent;
  let fixture: ComponentFixture<MockComponent>;
  let actionService: ActionService;
  let stateManager: StateManagerService;
  let dataService: DataService;
  let utilityService: UtilityService;
  let firebaseService: FirebaseService;
  let modalController: ModalController;
  let inspectionItemService: InspectionItemService;
  let navController: NavController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        SharedModule,
        IonicModule.forRoot(MockComponent)
      ],
      declarations: [
        SideMenuComponent,
        MockComponent
      ],
      providers: [
        ActionService,
        StateManagerService,
        UtilityService,
        DataService,
        FirebaseService,
        InspectionItemService,
        ChimneyNamesService,
        AlertController,
        App,
        DomController,
        Form,
        Keyboard,
        MenuController,
        NavController,
        GestureController,
        ModalController,
        { provide: NavParams, useClass: () => { } },
        { provide: Platform, useClass: PlatformMock },
        { provide: Config, useClass: ConfigMock }
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ]
    });

  });

  beforeEach(inject(
    [
      ActionService,
      StateManagerService,
      DataService,
      UtilityService,
      FirebaseService,
      ModalController,
      InspectionItemService,
      NavController
    ],
    (
      _actionService_: ActionService,
      _stateManager_: StateManagerService,
      _dataService_: DataService,
      _utilityService_: UtilityService,
      _firebaseService_: FirebaseService,
      _modalController_: ModalController,
      _inspectionItemService_: InspectionItemService,
      _navController_: NavController
    ) => {
      actionService = _actionService_;
      stateManager = _stateManager_;
      dataService = _dataService_;
      utilityService = _utilityService_;
      firebaseService = _firebaseService_;
      modalController = _modalController_;
      inspectionItemService = _inspectionItemService_;
      navController = _navController_;

      spyOn(inspectionItemService, 'addToList');
      spyOn(navController, 'getViews');
      spyOn(stateManager, 'getModel').and.callFake(() => new BehaviorSubject([]));

    }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MockComponent);
    component = fixture.debugElement.children[0].componentInstance;
    fixture.detectChanges();
  });

  it('should add to nav', () => {
    component.onMenuClick('clients-list');
    expect(component.nav.length).toEqual(1);
  });

  // it('should not add to nav', () => {
  //   const page: any = { instance: new JobListComponent() };
  //   component.nav.push(page);
  //   component.onMenuClick('clients list');
  //   expect(component.nav.length).toEqual(1);
  // });

  it('should activate clientsMenu dropdown', () => {
    component.onMenuClick('clients');
    expect(component.clientsMenu).toBeTruthy();
  });

  it('should deactivate clientsMenu dropdown', () => {
    component.onMenuClick('clients');
    component.onMenuClick('clients');
    expect(component.clientsMenu).toBeFalsy();
  });

  it('should change ios-arrow-forward -> ios-arrow-down', () => {
    component.onMenuClick('clients');
    expect(component.clientsMenuIcon).toEqual('ios-arrow-down');
  });

  it('should change ios-arrow-down -> ios-arrow-forward ', () => {
    component.onMenuClick('clients');
    component.onMenuClick('clients');
    expect(component.clientsMenuIcon).toEqual('ios-arrow-forward');
  });
});

@Component({
  selector: 'mockComponent',
  template: `<wells-side-menu [nav]="nav"></wells-side-menu>`
})
/**
 * name
 */
class MockComponent {

  public nav = new Nav();
}

class Nav {
  private comps = [];
  public get length() {
    return this.comps.length;
  }

  public getViews() {
    return this.comps;
  }

  public setRoot(comp) {
    this.comps.push(comp);
  }

  public push(comp) {
    this.comps.push(comp);
  }
}