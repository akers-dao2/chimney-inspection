import { Component, OnInit } from '@angular/core';
import { NavParams } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';

import { IClient } from '../../shared/interfaces/client';
import { IUser } from '../../shared/interfaces/user';
import { IReport } from '../../shared/interfaces/report';
import { IPhoto } from '../../shared/interfaces/photo';
import { IJob } from '../../shared/interfaces/job';
import { IOpenedInspectionReport } from '../../shared/interfaces/opened-inspection-report';

import { Status } from '../../shared/enums/status';
import { InspectionStatus } from '../../shared/enums/inspection-status';
import { GetOpenedInspectionReportService } from '../../shared/services/get-opened-inspection-report.service';
import { PdfGeneratorService } from '../../shared/services/pdf-generator.service';
import { PresentToastService } from 'app/shared/services/present-toast.service';

import { ActionService } from '../../shared/services/action.service';
import { GoToPageService } from '../../shared/services/go-to-page.service';
import { GetJobDateInspectItemsService } from '../../shared/services/get-job-date-inspect-items.service';
import { StateManagerService } from 'app/shared/services/sassy-state-manager-ng2/src/state-manager.service';
import { DataService } from '../../shared/services/data.service';

import { JobListComponent } from '../job/job-list/job-list.component';

import { saveAs } from 'file-saver';
import { LoginService } from 'app/shared/services/login.service';

@Component({
  selector: 'wells-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {

  public user: IUser;
  public client: IClient;
  public isCertification: boolean;
  public signature: Observable<string>;
  public commentsAndRecommendations: any[];
  public requiredPics: IPhoto[];
  public dangerReportComments$: any;
  public canClose: Observable<boolean>;
  public Status = Status;
  public isSignatureLocked: Observable<boolean>;
  public isOnline = true;
  public inspectItemsInReport: IReport[];
  public today: Date;
  public partialOrUnlinedStoves: any[] = [];

  constructor(
    private params: NavParams,
    private actionService: ActionService,
    private goToPage: GoToPageService,
    private getJobDateInspectItems: GetJobDateInspectItemsService,
    private stateManager: StateManagerService,
    private dataService: DataService,
    private getOpenedInspectionReport: GetOpenedInspectionReportService,
    private pdfGenerator: PdfGeneratorService,
    private presentToast: PresentToastService,
    private loginService: LoginService
  ) { }

  ngOnInit() {

    this.getJobDetails.pluck('user')
      .take(1)
      .map(user => this.user = user ? user : this.params.get('user'))
      .subscribe();

    this.client = this.params.get('client');
    this.isCertification = this.params.get('cert');
    this.inspectItemsInReport = this.params.get('inspectItemsInReport');
    this.commentsAndRecommendations = this.getCommentsAndRecommendations(this.inspectItemsInReport);
    this.requiredPics = this.getRequiredPics(this.inspectItemsInReport);
    this.signature = this.getJobDetails.pluck('signature');
    this.dangerReportComments$ = this.getDangerReportComments(this.commentsAndRecommendations);
    this.canClose = this.getCanClose();
    this.isSignatureLocked = this.isSigLocked();
    this.partialOrUnlinedStoves = this.getPartialOrUnlinedStoves(this.inspectItemsInReport);
    this.isOnline = true;

    this.today = new Date();
  }

  /**
   * Checks to see if there are any properties on the object to display
   * 
   * @param {any} key 
   * @returns 
   * @memberof ReportComponent
   */
  public doesObjectHaveItems(key: any) {
    return this.keysWithPropsToDisplay.includes(key);
  }

  /**
   * Checks to see if there are any properties with a fraction to display
   * 
   * @param {any} key 
   * @returns 
   * @memberof ReportComponent
   */
  public hasFraction(key) {
    return this.keysWithFractions.includes(key);
  }

  /**
   * Go to job list and reset back to the standard side menu
   *
   *
   * @memberof InspectionSheetComponent
   */
  public goToJobList(status) {
    if (status) {
      this.getJobDetails
        .take(1)
        .map(job => Object.assign({}, job, { user: this.user, status, certification: this.isCertification, completionDate: Date.now() }))
        .subscribe(job =>
          this.actionService.updateJob(job)
        );
    }

    this.goToPage.execute(JobListComponent);
    this.actionService.changeSideMenu('standardMenu');
    this.actionService.resetInspection();
    this.actionService.resetOpenedInspection();
  }

  public toDataURL(signature) {
    this.getJobDetails
      .take(1)
      .map(job => Object.assign({}, job, { signature: signature.data }))
      .subscribe(job => this.actionService.updateJob(job));
  }

  public print() {
    this.presentToast.show(`Generating PDF Report`);

    Observable.combineLatest(
      this.stateManager.getModel('clients'),
      this.getJobDetails
    )
      .take(1)
      .switchMap(([clients, job]) => {
        return Observable.from<IClient>(clients)
          .filter<IClient>(c => c.key === job.client)
          .switchMapTo(this.getOpenedInspectionReport.get().take(1),
          (client, inspectItemsInReport) => ({ client, inspectItemsInReport }))
          .map(({ client, inspectItemsInReport }) => {
            return { client, inspectItemsInReport, job };
          });
      })
      .switchMap(({ client, inspectItemsInReport, job }) =>
        this.pdfGenerator.pdfData(client, job.certification, inspectItemsInReport, job.completionDate),
      ({ client }, data) => ({ client, data }))
      .take(1)
      .subscribe(({ data, client }) => {
        this.presentToast.show(`PDF Report Created`);
        saveAs(data, `${client.firstName} ${client.lastName}.pdf`);
      });

  }

  public resetStatus() {
    this.getJobDetails
      .take(1)
      .map(job => Object.assign({}, job, { status: Status.open }))
      .subscribe(job => {
        delete job.user;
        this.actionService.updateJob(job);
      });
  }




  private get getJobDetails() {
    return Observable.combineLatest<IJob[], IOpenedInspectionReport>(
      this.stateManager.getModel('jobs'),
      this.stateManager.getModel('openedInspectionReport')
    )
      .map(([jobs, openedInspection]) =>
        jobs.find(j => j.key === openedInspection.jobKey)
      );
  }

  /**
   * Returns the list of properties with required pictures on them
   * 
   * @private
   * @param {any} inspectItemsInReport 
   * @returns 
   * @memberof ReportComponent
   */
  private getRequiredPics(inspectItemsInReport) {
    return inspectItemsInReport
      .filter(item => Object.keys(item).some(key => key === 'requiredPics'))
      .reduce((pics, item) => {
        const newPics = item.requiredPics.pictures.map((pic: IPhoto) => {
          let name = item.name;
          const downloadLink = this.getDownloadLink(pic.downloadLink);
          let fileName = pic.fileName ? pic.fileName : new RegExp(/(?:o\/)(.+)(?=\?)/).exec(downloadLink as string);

          // temp code.  will be removed in production since all pictures will have a filename
          if (typeof fileName !== 'string') {
            fileName = fileName !== null ? fileName[1] : undefined;
          }

          if (pic.name === 'front_of_house') {
            name = 'Front Of House';
          }

          if (this.loginService.retrieveCredentialsLocally.online) {
            pic.downloadLink = this.dataService.getImage(fileName) as any;
          } else {
            pic.downloadLink = this.dataService.getStorageItem(fileName);
          }

          return Object.assign({}, pic, { name });
        });

        return Array.of(...pics, ...newPics);
      }, []);
  }

  private getDownloadLink(link) {
    if (typeof link !== 'string') {
      link = Object.keys(link).filter(key => typeof link[key] === 'string').map(key => link[key]);
      link = link !== null ? link[0] : undefined;
    }

    return link;
  }
  /**
   * Returns the properties with comments and recommendations on them
   * 
   * @private
   * @param {any} inspectItemsInReport 
   * @returns 
   * @memberof ReportComponent
   */
  private getCommentsAndRecommendations(inspectItemsInReport) {
    return inspectItemsInReport
      .filter(item => {
        if (!!Object.keys(item).length) {
          return Object.keys(item).some(key =>
            item[key] !== null &&
            item[key].hasOwnProperty('comments') &&
            item[key].status === InspectionStatus['failed']
          );
        }

        return false;
      })
      .map(inspectItems => {
        return Object.assign({}, inspectItems, { name: this.newInspectName(inspectItems) });
      });
  }

  private getDangerReportComments(inspectItemsInReport) {
    return this.stateManager.getModel('comments')
      .map(comments => {
        return this.inspectItemsInReport.filter(item => {


          if (!!Object.keys(item).length) {
            const keysWithComments = Object.keys(item).filter(key => item[key] !== null && item[key].hasOwnProperty('comments'));
            if (!!keysWithComments.length) {

              if (item.applianceType === 0) {
                return keysWithComments.some(key => {
                  return item[key].status === InspectionStatus['failed'] && key === 'condition';
                });
              }

              return keysWithComments.some(key => {
                if (item[key].comments === null) {
                  return false;
                }

                return item[key].comments.some((commentIndex) => {
                  return comments[key][commentIndex].toLowerCase().includes('trigger');

                }) && item[key].status === InspectionStatus['failed'];
              });


            }
            return false;
          }

          return false;
        });

      })
      .map(inspectItems => {
        return inspectItems.map(i => {
          return Object.assign({}, i, { name: this.newInspectName(i) });
        });
      });
  }

  private newInspectName(item) {
    const chimneysList = this.chimneysList;
    const parentIndex = item.uuid[0];
    const chimneyName = this.getChimneyName(chimneysList, parentIndex);
    const applianceName = item.name;
    let name: string;

    if (chimneyName === applianceName) {
      name = chimneyName;
    } else {
      name = `${chimneyName}: ${applianceName}`;
    }

    return name;
  }

  private get chimneysList() {
    return this.inspectItemsInReport.filter(item => item.uuid.length === 1);
  }

  private getChimneyName(chimneysList: IReport[], index: string) {
    return chimneysList.find(chimney => chimney.uuid === index).name;
  }

  /**
   * Can you finalize the report
   * 
   * @private
   * @returns 
   * @memberof ReportComponent
   */
  private getCanClose() {
    return this.getJobDetails
      .map<IJob, boolean>(job =>
        job.signature !== undefined && job.signature !== 'clear' && job.status !== Status.closed
      );
  }

  /**
   * Is the signature pad locked
   * 
   * @private
   * @returns 
   * @memberof ReportComponent
   */
  private isSigLocked() {
    return this.getJobDetails
      .pluck<IJob, Status>('status')
      .map(status => Status.closed === status);

  }

  /**
   * The keys for properties that have an object as value
   * 
   * @readonly
   * @private
   * @memberof ReportComponent
   */
  private get keysWithPropsToDisplay() {
    return [
      'crown',
      'stovePipe',
      'stoveClearances',
      'hearthPadProtection',
      'firebox',
      'hearth',
      'surroundClearance',
      'stackPipe'
    ];
  }

  /**
   * Keys that have fraction associated with them
   * 
   * @readonly
   * @private
   * @memberof ReportComponent
   */
  private get keysWithFractions() {
    return [
      'outside1',
      'outside3',
      'inside1',
      'inside3',
    ];
  }

  private getPartialOrUnlinedStoves(inspectItemsInReport: IReport[]) {
    return inspectItemsInReport
      .filter(i => i.applianceType === 3 && i.type === 'Insert' && (['Partial Liner', 'No Liner'].includes(i.ifInsert)))
      .reduce((applianceNames, item) => {
        const applianceName = this.getApplianceName(applianceNames, item, inspectItemsInReport);
        return [...applianceNames, applianceName];
      }, []);
  }

  private getApplianceName(applianceNames: string[], item, inspectItemsInReport: IReport[]) {
    const chimneyNumber = parseInt(item.uuid[0], 10) + 1;
    const applianceName = `Chimney ${chimneyNumber}: ${this.getFlueName(inspectItemsInReport, item)}: ${item.name}`;
    return applianceName;
  }

  private getFlueName(inspectItemsInReport: IReport[], inspectItem) {
    const itemIndex = inspectItemsInReport.findIndex(item => item.uuid === inspectItem.uuid);
    let flueName;
    for (let index = itemIndex; index > 0; index--) {
      if (inspectItemsInReport[index].applianceType === 0) {
        flueName = inspectItemsInReport[index].name;
        break;
      }
    }
    return flueName;
  }
}
