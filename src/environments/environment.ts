// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyDVtqkBjzRZIJRoqHrg0ZdARPACWjIhEGQ',
    authDomain: 'ping-online.firebaseapp.com',
    databaseURL: 'https://ping-online.firebaseio.com',
    projectId: 'ping-online',
    storageBucket: 'ping-online.appspot.com',
    messagingSenderId: '117544396504'
  }
};
