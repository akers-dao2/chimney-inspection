## Chimney Inspection Report

---------
	
Application for generating Chimney Inspection Reports

The chimney inspection report is an application that an inspector uses during their visual examination of a house's chimney.  Reports are generated in PDF format, allowing for a professionally looking report with attractive headers and borders.

**Installation:**

You need to node and npm. At the root of the project type:

```node
npm install
```

**Update Firebase Configuration:**
```
src/app/shared/services/firebase.service.ts
```

**Run Project:**

```node
npm run start
```

**Software Used:**

1. Angular (https://angular.io)
1. Angular Material (https://material.angular.io/)
1. Ionic (http://ionicframework.com)
1. FireBase (https://www.firebase.com)
1. PouchDB (https://pouchdb.com)


----------
**Features:**

- Offers CRUD for inspection reports
- Signature Capture
- Printing Reports
- Real-Time Data storage
- Offline Mode
- Responsive Design Layout
- Local storage via pouchDB database
- Customized state management pattern, built on top of RxJS

